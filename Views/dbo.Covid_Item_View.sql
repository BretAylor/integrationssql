SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE VIEW [dbo].[Covid_Item_View]
AS
WITH cte_item_data AS (
    SELECT
        Trim(A.ItemNumber)                                                         AS ItemNumber
      , A.ItemDescription1
      , B.VendorNumber
      , B.VendorName
      , B.BuyerInitial
      , Trim(Left(d.CCDATA, CharIndex(' ', d.CCDATA, 1) - 1))                      AS Buyer
      , A.ItemClass
      , Trim(Replace(C.CCDATA, '00N', ''))                                         AS ItemClassDesc
      , A.DateAdded2                                                               AS DateAddedRAW
      , Cast(Concat(A.Century1, Right(Concat('000000', A.DateAdded2), 6)) AS date) AS DateAdded
    FROM    BI_STAGING.dbo.ItemMaster                   A
        LEFT JOIN BI_STAGING.dbo.PurchasingVendorMaster B
            ON A.VendorNumber = B.VendorNumber

        LEFT JOIN Demand_Planning.dbo.Item_Class_Desc   C
            ON A.ItemClass = C.Item_Class

        LEFT JOIN BI_STAGING.dbo.Buyer                  d
            ON Trim(B.BuyerInitial) = Right(Trim(d.CCCTLK), Len(B.BuyerInitial))
               AND  d.CCAPID = 'PO'
               AND  d.CCRCCD = 'BY'
)
   , cte_covid_items AS (
    SELECT
        B.*
        --a.Item
      , A.ETA
      , A.ETA_Date
      , A.Notes
      , A.Notes_Date
      , A.Owner
      , A.Status
      , A.Order_Limit
      , A.Category
      , A.[Wzr_URL]
      , A.[FS_URL]
      , A.Alias
      , A.FS_Alias
      , A.Alias_Desc
    FROM    dbo.COVID_Items      A
        INNER JOIN cte_item_data B
            ON A.Item = B.ItemNumber
--WHERE a.STATUS <> 'Disabled'
)
   , cte_FS_Item AS (
    SELECT
        a.inv_mast_uid
      , a.item_id
      , a.item_desc
      , a.date_created AS ITM_Created
      , b.SupplierPartNo
      , CASE
            WHEN b.SupplierPartNo IS NULL
                THEN a.item_id
            ELSE b.SupplierPartNo
        END            AS [ItemNum]
    FROM    BI_STAGING.dbo.ViewInvMastP21              a
        INNER JOIN BI_STAGING.dbo.InventorySupplierP21 b
            ON a.inv_mast_uid = b.InvMastUid
               AND  b.SupplierId = 100054
)
   , cte_inventory AS (
    SELECT
        b.ItemNumber
      , Sum(b.QuantityOnHandUM1)                                                                    AS Qty_OH
        --a.Qty_OH - (a.Aloc_Qty + (a.IP_Qty*-1)) AS Net_OH,
      , Sum(b.QuantityOnHandUM1 - (b.QuantityAllocatedUM1 + (b.InventoryQuantityUnpostedUM1 * -1))) AS Net_OH
      , Sum(b.QuantityonPurchaseOrderUM1)                                                           AS Qty_On_Order
      , Max(b.DateAdded)                                                                            AS Data_Refreshed
    FROM    cte_covid_items                  a
        LEFT JOIN BI_STAGING.dbo.ItemBalance b
            ON a.ItemNumber = Trim(b.ItemNumber)
    GROUP BY
        b.ItemNumber
)
   , cte_POs AS (
    SELECT
        b.Item
      , Cast(Round(Sum(a.OrderQuantity - a.QuantityReceivedtodate), 0) AS dec(38, 2))                                         AS Open_qty
      , Cast(Round(Sum(((a.OrderQuantity - a.QuantityReceivedtodate) * (a.OrderAmount / a.OrderQuantity))), 2) AS dec(38, 2)) AS Open_Ord_Amt
    FROM    BI_STAGING.dbo.OpenPurchaseOrderLines a
        RIGHT JOIN dbo.COVID_Items                b
            ON Trim(a.ItemNumber) = b.Item
    WHERE   a.VendorNumber NOT LIKE 'WTV%'
    GROUP BY
        b.Item
)
    --   , cte_shipments AS (
    --    SELECT
    --        b.ItemNumber
    --      , Sum(a.QuantityShipped) AS Qty_Sold
    --    FROM    dbo.R18_Sales_Exploded a
    --        RIGHT JOIN cte_covid_items b
    --            ON a.ItemNum = b.ItemNumber
    --               AND  a.InvoiceDate >= 200316
    --    GROUP BY
    --        b.ItemNumber
    --)
    --   , cte_sales AS (
    --    SELECT
    --        b.ItemNumber
    --      , Sum(a.QuantityOrdered) AS SO_Qty
    --    FROM    BI_STAGING.dbo.OpenOrderDetail a
    --        RIGHT JOIN cte_covid_items         b
    --            ON a.ItemNumber = b.ItemNumber
    --    GROUP BY
    --        b.ItemNumber
    --)
   , cte_consol AS (
    SELECT
        a.*
      , f.item_id AS [Item_Number_P21]
      , b.Qty_OH
      , b.Net_OH
      , b.Qty_On_Order
      , e.Open_Ord_Amt
        --, c.Qty_Sold
        --, d.SO_Qty
      , b.Data_Refreshed
    FROM    cte_covid_items     a
        LEFT JOIN cte_inventory b
            ON a.ItemNumber = b.ItemNumber
        --LEFT JOIN cte_shipments    c
        --    ON a.ItemNumber = c.ItemNumber
        --LEFT JOIN cte_sales        d
        --    ON a.ItemNumber = d.ItemNumber

        LEFT JOIN cte_POs       e
            ON a.ItemNumber = e.Item

        LEFT JOIN cte_FS_Item   f
            ON a.ItemNumber = f.SupplierPartNo
)
SELECT
    a.Category
  , CASE
        WHEN a.Net_OH > 0
            THEN 'In Stock'
        ELSE 'Out of Stock'
    END                       AS Stock_Status
  , a.ItemClassDesc           AS [Item Class]
    --, a.ItemNumber              AS [Item Number]
  , CASE
        WHEN a.Alias IS NOT NULL
            THEN a.Alias
        ELSE a.ItemNumber
    END                       AS [Item Number]
  , CASE
        WHEN a.FS_Alias IS NOT NULL
            THEN a.FS_Alias
        ELSE a.Item_Number_P21
    END                       AS [Item Number P21]
    --, CASE
    --      WHEN a.URL IS NOT NULL
    --          THEN Concat('=hyperlink(', '"', a.URL, '" , "', a.ItemNumber, '")')
    --      ELSE a.ItemNumber
    --  END                       AS [Item Link]
    --, a.Item_Number_P21         AS [Item Number P21]
  , CASE
        WHEN a.Alias_Desc IS NOT NULL
            THEN a.Alias_Desc
        ELSE a.ItemDescription1
    END                       AS [Description]
  , a.Qty_OH                  AS [Qty OH]
  , a.Net_OH                  AS [Net OH]
  , a.Qty_On_Order            AS [PO Qty]
  , IsNull(a.Open_Ord_Amt, 0) AS [PO Value]
    --, IsNull(a.Qty_Sold, 0)                           AS [Shp Qty]
    --, IsNull(a.Qty_OH - a.Net_OH, 0)                  AS [SO Qty]
    --, IsNull((a.Qty_Sold + (a.Qty_OH - a.Net_OH)), 0) AS Demand
  , Upper(a.ETA)              AS ETA
  , a.ETA_Date                AS Updated
  , a.Order_Limit
  , a.Notes
  , a.Notes_Date
  , Upper(a.Owner)            AS [Prod Mgr]
  , a.VendorNumber            AS [Vnd Num]
  , a.VendorName              AS [Vnd Name]
  , a.Buyer
  , a.DateAdded               AS [Add Date]
  , a.[Wzr_URL]
  , a.[FS_URL]
  , a.Data_Refreshed
FROM    cte_consol a
WHERE   a.[Status] <> 'Disabled';   --AND a.Category IN('Standard Item', 'New Item');
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwho_buys_what] AS 
SELECT a.ItemNum, a.ActualSellPrice, a.ShippingLocation, a.QuantityShipped, a.TotalLineAmount, a.InvoiceDate, a.CustomerNum, a.PrimarySalesmanNum, cm.CustomerClass, cm.City, cm.StateProvince, cm.ZipCode4,d.ItemDescription1, d.ItemClass, d.SuspendCode, d.ItemSubclass, d.BillofMaterialType, d.VendorNumber, d.DiscontinuedCode, d.UserField2, d.userfield3 FROM bi_staging.dbo.InvoicelineFranchise a
LEFT JOIN bi_staging.dbo.CustomerMaster cm	ON
a.CustomerNum = cm.CustomerNum	
LEFT JOIN bi_staging.dbo.itemmaster d ON
a.ItemNum	= d.ItemNumber	

GO

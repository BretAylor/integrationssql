SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[Item_Class_Desc]
AS(
SELECT
    CCCONO
  , CCAPID
  , CCCTLK
  , CCRCCD
  , Trim(Replace(CCDATA,'00N','')) AS CCDATA
  , DateAdded
  , Right(RTrim(CCCTLK), 2) AS 'Item_Class'
FROM    BI_STAGING.dbo.Buyer
WHERE
    CCRCCD = 'PC'
    AND CCAPID = 'IA'
    AND Len(CCCTLK) = 5);
GO

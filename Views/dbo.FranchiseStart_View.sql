SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[FranchiseStart_View]
AS
SELECT EmployeeKey,  DD.FiscalYear
FROM dbo.DimEmployee EM
    left JOIN dbo.DimDate DD
        ON DD.Year = EM.SalesmanStartDateYear AND DD.Month = EM.SalesmanStartDateMon AND DD.DayOfMonth = EM.SalesmanStartDateDay
WHERE TerminationDateYear = 0 

GO

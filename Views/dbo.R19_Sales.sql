SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--TRUNCATE TABLE dbo.R18_Sales;

--DECLARE @END   AS nvarchar(8)
--      , @BEGIN AS nvarchar(8);
----@M1 AS Date;

--SET @END = Convert(nvarchar(8), EOMonth(DateAdd(M, -1, GetDate())), 12); -- UPDATE to -1
--PRINT @END;
--SET @BEGIN = Convert(nvarchar(8), DateAdd(D, 1, EOMonth(DateAdd(M, -18, @END))), 12);
--PRINT @BEGIN;
--SET	@M1  = DATEADD(M,-1, @END); PRINT @m1

--SELECT * FROM bi_staging.dbo.InvoiceLineFranchise WHERE Format(InvoiceDate,'YYMMDD') >= @BEGIN AND Format(InvoiceDate,'YYMMDD') <= @end

CREATE VIEW [dbo].[R19_Sales]
AS
WITH cte_get_prd_sales AS (
    SELECT  *
    FROM    BI_STAGING.dbo.InvoiceLineFranchise a
    WHERE
        a.InvoiceDate <= Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)
        AND a.InvoiceDate >= Convert(nvarchar(8), DateAdd(D, 1, EOMonth(DateAdd(M, -19, Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)))), 12)
)
   , CTE_get_all_sales AS (
    SELECT
        B.CustomerClass         AS cust_class
      , d.CustomerGLCode
      , A.ID
      , A.CompanyNum
      , A.CustomerNum
      , A.OrderNum
      , A.HistorySequenceNum
      , A.OrderSequenceNum
      , A.PrimarySalesmanNum
      , A.ItemNum
      , A.ItemDescription1
      , A.QuantityShipped
      , A.ListPrice
      , A.ActualSellPrice
      , A.CurrentAverageCost
      , A.TotalLineAmount
      , A.InvoiceDate
      , A.Type
      , A.ItemClass
      , A.ShippingLocation
      , A.QuantityOrdered
      , A.QuantityBackordered
      , A.DateAdded
      , CASE
            WHEN Left(A.ItemNum, 3) = '975'
                THEN 'Custom Kit'
            WHEN C.BillofMaterialType <> ''
                THEN 'Valid - Kit/Assortment'
            ELSE 'Valid - Component'
        END                     AS [Exl_Category]
      , CASE
            WHEN F.SplitProfitProgramFlag = 'Y'
                THEN 'GPS'
            WHEN E.Division = 'A1S'
                THEN 'A1'
            WHEN E.Division = 'SCL'
                THEN 'Inside Sales'
            WHEN a.PrimarySalesmanNum = '992'
                THEN 'Rep 992 - Internal'
            WHEN C.ItemClass = '90'
                THEN 'IC 90 - Bill Only'
            WHEN C.ItemClass = '91'
                THEN 'IC 91 - FDP'
            WHEN C.ItemClass = '93'
                THEN 'IC 93 - VSN'
            WHEN C.ItemClass = '82'
                THEN 'IC 82 - Promo'
            WHEN C.ItemClass = '89'
                THEN 'IC 89 - Specials'
            WHEN C.ItemClass = '80'
                THEN 'IC 80 - Handouts'
            ELSE 'Franchise'
        END                     AS Sls_Ctg
      , G.OrderCreateSystemDate AS [Order_Date]
    FROM    cte_get_prd_sales                         A
        INNER JOIN BI_STAGING.dbo.CustomerMaster      B
            ON A.CustomerNum = B.CustomerNum

        INNER JOIN BI_STAGING.dbo.ItemMaster          C
            ON A.ItemNum = C.ItemNumber

        INNER JOIN BI_STAGING.dbo.InvoiceFranchise    d
            ON A.HistorySequenceNum = d.HistorySequenceNum

        INNER JOIN BI_STAGING.dbo.EmployeeSup         E
            ON A.PrimarySalesmanNum = E.SalesRepresentativeNum

        INNER JOIN BI_STAGING.dbo.CustomerSup         F
            ON A.CustomerNum = F.CustomerNum

        INNER JOIN BI_STAGING.dbo.InvoiceFranchiseSup G
            ON A.HistorySequenceNum = G.HistorySequenceNum
    WHERE
        (
            A.CompanyNum = '1'
            --AND A.invoicedate >= Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)
            --AND A.invoicedate <= Convert(nvarchar(8), DateAdd(D, 1, EOMonth(DateAdd(M, -19, Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)))), 12)
            AND d.OrderType <> 'I'
            AND C.ItemClass <> '90'
            AND d.CustomerGLCode IN
                    (
                        'FR'
                      , 'NF'
                      , 'SA'
                      , 'TF'
                    )
        )
        OR
        (
            A.CompanyNum = '1'
            --AND A.invoicedate >= Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)
            --AND A.invoicedate <= Convert(nvarchar(8), DateAdd(D, 1, EOMonth(DateAdd(M, -19, Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)))), 12)              AND d.OrderType = 'I'
            AND C.ItemClass = '91'
            AND d.CustomerGLCode <> 'TG'
        )
        OR
        (
            A.CompanyNum = '1'
            --AND A.invoicedate >= Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)
            --AND A.invoicedate <= Convert(nvarchar(8), DateAdd(D, 1, EOMonth(DateAdd(M, -19, Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)))), 12)              AND d.OrderType = 'I'
            AND d.CustomerGLCode = 'TF'
        )
        OR
        (
            A.CompanyNum = '1'
            --AND A.invoicedate >= Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)
            --AND A.invoicedate <= Convert(nvarchar(8), DateAdd(D, 1, EOMonth(DateAdd(M, -19, Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)))), 12)              AND d.OrderType = 'I'
            AND E.RepTypeCode = 'WR'
        )
)
--INSERT INTO dbo.R18_Sales
SELECT  *
--INTO SALES_FINAL_FY2019 
FROM    CTE_get_all_sales A;
--SELECT a.Exl_Category, SUM(a.quantityshipped * a.ListPrice) as FC_Sales, Sum(a.quantityshipped * a.ActualSellPrice) AS CUS_Sales FROM cte_get_all_sales a GROUP BY a.Exl_Category ORDER BY a.Exl_Category asc


GO

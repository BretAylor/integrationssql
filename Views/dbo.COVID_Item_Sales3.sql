SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[COVID_Item_Sales3]
AS(
SELECT
    a.ItemNum
  , Sum(a.QuantityShipped)                        AS Qty_Shipped
  , a.InvoiceDate
  , Sum(a.CurrentAverageCost * a.QuantityShipped) AS W_COGS
  , Sum(a.ListPrice * a.QuantityShipped)          AS F_COGS
  , Sum(a.ActualSellPrice * a.QuantityShipped)    AS C_Sales
FROM    BI_STAGING.dbo.InvoiceLineFranchise a
WHERE
    a.ItemNum IN
        (
            '865.1015'
          , '994.9555'
          , '891.687'
          , '994.8607055QT'
          , '994.9550'
          , '994.9551'
          , '994.9552'
          , '994.9553'
        )
GROUP BY a.ItemNum, a.InvoiceDate)
GO

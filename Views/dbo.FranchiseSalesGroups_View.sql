SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[FranchiseSalesGroups_View]
as
SELECT 
       DD.FiscalYear,
	   FR.EmployeeKey,
       EM.SalesRepresentativeNum,
	   SalesRepresentativeName,
       SUM(FR.NetSales) AS NetSales
       
FROM dbo.DimEmployee EM INNER JOIN dbo.FactInvoiceFranchise FR ON EM.EmployeeKey = FR.EmployeeKey 
INNER JOIN dbo.DimDate DD ON DD.DateKey = FR.DateKey 
WHERE TerminationDateYear = 0
GROUP BY SalesRepresentativeName,
       FR.EmployeeKey,
       EM.SalesRepresentativeNum,
	   DD.FiscalYear
GO

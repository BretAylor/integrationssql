SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[Inv_Total_Itm_Cls_All_Locs]
AS
WITH cte_p21_inv AS (
    SELECT
        CASE
            WHEN NullIf(b.supplier_part_no, '') IS NULL
                THEN c.item_id
            ELSE b.supplier_part_no
        END                                AS Item_Num
      , a.location_id                      AS Whse
      , a.qty_on_hand                      AS Qty_OH
      , a.moving_average_cost              AS Avg_Cost
      , (a.qty_on_hand - a.qty_in_process) AS Net_Inv
      , c.class_id1                        AS Item_Class
    FROM    OpenQuery
            ([WINEPISQLVSQL01\EPIMSSQL]
           , 'Select inv_mast_uid, location_id, qty_on_hand, qty_in_process, moving_average_cost from p21.dbo.inv_Loc --where qty_on_hand <> 0'
            )                                                                                                                                      a
        LEFT JOIN OpenQuery
                  ([WINEPISQLVSQL01\EPIMSSQL], 'Select inv_mast_uid, supplier_part_no from p21.dbo.inventory_supplier where supplier_id = 100054') b
            ON a.inv_mast_uid = b.inv_mast_uid

        LEFT JOIN OpenQuery
                  ([WINEPISQLVSQL01\EPIMSSQL], 'Select inv_mast_uid, item_id, class_id1 from p21.dbo.inv_mast')                                    c
            ON a.inv_mast_uid = c.inv_mast_uid
    WHERE
        a.location_id = 1006
        OR  a.location_id >= 1400
)
   , cte_as400_inv AS (
    SELECT
        z.ItemNumber                                                                             AS Item_Num
      , z.WarehouseID                                                                            AS Whse
      , z.QuantityOnHandUM1                                                                      AS Qty_OH
        --, z.QuantityAllocatedUM1
        --, z.QuantityonBackorderUM1 
      , z.AverageCost                                                                            AS Avg_Cost
        --, ((z.QuantityOnHandUM1 - z.QuantityAllocatedUM1) - z.QuantityonBackorderUM1) AS Net_Inv
      , (z.QuantityOnHandUM1 - (z.QuantityAllocatedUM1 + (z.InventoryQuantityUnpostedUM1 * -1))) AS Net_Inv
      , z.ItemClass                                                                              AS Item_Class
    FROM    BI_STAGING.dbo.ItemBalance z
    WHERE --z.QuantityOnHandUM1 <> 0 AND 
        z.WarehouseID IN
            (
                '1'
              , '3'
              , '4'
              , '5'
            )
)
   , cte_merge_inv AS (
    SELECT  *
    FROM    cte_p21_inv
    UNION ALL
    SELECT  *
    FROM    cte_as400_inv
)
   , cte_calc_agg AS (
    SELECT
        a.Item_Num
      , CASE
            WHEN Sum(a.Qty_OH) = 0
                THEN Avg(a.Avg_Cost)
            ELSE Sum(a.Qty_OH * a.Avg_Cost) / Sum(a.Qty_OH)
        END                         AS Avg_Cost
      , Sum(a.Qty_OH)               AS Total_OH_Qty
      , Sum(a.Qty_OH * a.Avg_Cost)  AS Total_OH_Val
      , Sum(a.Net_Inv)              AS Net_OH_Qty
      , Sum(a.Net_Inv * a.Avg_Cost) AS Net_OH_Val
      , a.Item_Class
      , b.CCDATA                    AS Class_Name
    FROM    cte_merge_inv             a
        LEFT JOIN dbo.Item_Class_Desc b
            ON b.Item_Class = a.Item_Class
    GROUP BY
        a.Item_Num
      , a.Item_Class
      , b.CCDATA
)
   , cte_item_add_date AS (
    SELECT
        a.*
      , CASE
            WHEN b.DateAdded2 IS NULL
                THEN c.date_created
            ELSE Cast(Right(Concat('000000', b.DateAdded2), 6) AS date)
        END AS Create_date
      , b.BillofMaterialType
    FROM    cte_calc_agg                        a
        LEFT JOIN BI_STAGING.dbo.ItemMaster     b
            ON a.Item_Num = b.ItemNumber

        LEFT JOIN BI_STAGING.dbo.ViewInvMastP21 c
            ON a.Item_Num = c.item_id
)
   , cte_final AS (
    SELECT
        a.Item_Num
      , a.Avg_Cost
      , a.Total_OH_Qty
      , a.Total_OH_Val
      , a.Net_OH_Qty
      , a.Net_OH_Val
      , a.Item_Class
      , a.Class_Name
      , a.Create_date
      , DateDiff(M, a.Create_date, DateAdd(M, -1, GetDate())) AS Age
      , a.BillofMaterialType
    FROM    cte_item_add_date a
)
SELECT  *
FROM    cte_final;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[ANSP_2019] AS 

    With cte_ANSP
     AS (SELECT DISTINCT       
                a.itemnumber, 
                SUM(b.QuantityShipped) AS Tot_Units_Sold,
				Count(distinct b.historysequencenum) AS Sample_Size,
                CASE
                    WHEN SUM(b.QuantityShipped) = 0 THEN 0
                    ELSE SUM((b.QuantityShipped * b.ActualSellPrice)) / SUM(b.QuantityShipped)
                END AS ANSP
                FROM  bi_staging.dbo.itemmaster a
                      LEFT JOIN dbo.FCOST_Rev_Sls_Hst_2019 b
                      ON a.itemnumber = b.ItemNum
                WHERE NULLIF(a.billofmaterialtype, '') IS NULL
                      AND a.SuspendCode <> 'S'
                      AND b.actualsellprice > 0
					  AND b.QuantityShipped > 0
					  AND b.PrimarySalesmanNum <> 585
					  AND b.New_Class <> '12'

                GROUP BY 
                         a.ItemNumber
         --HAVING SUM(b.QuantityShipped) > 0
         --ORDER BY a.ItemNumber ASC
         )
	SELECT * FROM cte_ansp
GO

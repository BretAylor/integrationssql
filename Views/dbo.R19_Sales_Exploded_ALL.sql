SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


Create VIEW [dbo].[R19_Sales_Exploded_ALL]
AS


/*******************************************************
***** Script for SelectTopNRows command from SSMS  *****
*******************************************************/
--TRUNCATE TABLE dbo.R18_Sales_Exploded;

/********************
Get Period Sales
********************/
--WITH cte_get_prd_sales AS (
--    SELECT  *
--    FROM    BI_STAGING.dbo.InvoiceLineFranchise a
--    WHERE
--        a.InvoiceDate <= Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)
--        AND a.InvoiceDate >= Convert(nvarchar(8), DateAdd(D, 1, EOMonth(DateAdd(M, -19, Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)))), 12)
--)
--   , CTE_get_sales AS (
--    SELECT
--        B.CustomerClass         AS cust_class
--      , d.CustomerGLCode
--      , A.ID
--      , A.CompanyNum
--      , A.CustomerNum
--      , A.OrderNum
--      , A.HistorySequenceNum
--      , A.OrderSequenceNum
--      , A.PrimarySalesmanNum
--      , A.ItemNum
--      , A.ItemDescription1
--      , A.QuantityShipped
--      , A.ListPrice
--      , A.ActualSellPrice
--      , A.CurrentAverageCost
--      , A.TotalLineAmount
--      , A.InvoiceDate
--      , A.[Type]
--      , A.ItemClass
--      , A.ShippingLocation
--      , A.QuantityOrdered
--      , A.QuantityBackordered
--      , A.DateAdded
--      , CASE
--            WHEN Left(A.ItemNum, 3) = '975'
--                THEN 'Custom Kit'
--            WHEN C.BillofMaterialType <> ''
--                THEN 'Valid - Kit/Assortment'
--            ELSE 'Valid - Component'
--        END                     AS [Exl_Category]
--      , CASE
--            WHEN F.SplitProfitProgramFlag = 'Y'
--                THEN 'GPS'
--            WHEN E.Division = 'A1S'
--                THEN 'A1'
--            WHEN E.Division = 'SCL'
--                THEN 'Inside Sales'
--            WHEN PrimarySalesmanNum = '992'
--                THEN 'Rep 992 - Internal'
--            WHEN C.ItemClass = '90'
--                THEN 'IC 90 - Bill Only'
--            WHEN C.ItemClass = '91'
--                THEN 'IC 91 - FDP'
--            WHEN C.ItemClass = '93'
--                THEN 'IC 93 - VSN'
--            WHEN C.ItemClass = '82'
--                THEN 'IC 82 - Promo'
--            WHEN C.ItemClass = '89'
--                THEN 'IC 89 - Specials'
--            WHEN C.ItemClass = '80'
--                THEN 'IC 80 - Handouts'
--            ELSE 'Franchise'
--        END                     AS Sls_Ctg
--      , G.OrderCreateSystemDate AS [Order_Date]
--    FROM    cte_get_prd_sales                         A
--        INNER JOIN BI_STAGING.dbo.CustomerMaster      B
--            ON A.CustomerNum = B.CustomerNum

--        INNER JOIN BI_STAGING.dbo.ItemMaster          C
--            ON A.ItemNum = C.ItemNumber

--        INNER JOIN BI_STAGING.dbo.InvoiceFranchise    d
--            ON A.HistorySequenceNum = d.HistorySequenceNum

--        INNER JOIN BI_STAGING.dbo.EmployeeSup         E
--            ON A.PrimarySalesmanNum = E.SalesRepresentativeNum

--        INNER JOIN BI_STAGING.dbo.CustomerSup         F
--            ON A.CustomerNum = F.CustomerNum

--        INNER JOIN BI_STAGING.dbo.InvoiceFranchiseSup G
--            ON A.HistorySequenceNum = G.HistorySequenceNum
--    WHERE
--        (
--            A.CompanyNum = '1'
--            --AND A.invoicedate >= Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)
--            --AND A.invoicedate <= Convert(nvarchar(8), DateAdd(D, 1, EOMonth(DateAdd(M, -19, Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)))), 12)
--            AND d.OrderType <> 'I'
--            AND C.ItemClass <> '90'
--            AND d.CustomerGLCode IN
--                    (
--                        'FR'
--                      , 'NF'
--                      , 'SA'
--                      , 'TF'
--                    )
--        )
--        OR
--        (
--            A.CompanyNum = '1'
--            --AND A.invoicedate >= Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)
--            --AND A.invoicedate <= Convert(nvarchar(8), DateAdd(D, 1, EOMonth(DateAdd(M, -19, Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)))), 12)              AND d.OrderType = 'I'
--            AND C.ItemClass = '91'
--            AND d.CustomerGLCode <> 'TG'
--        )
--        OR
--        (
--            A.CompanyNum = '1'
--            --AND A.invoicedate >= Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)
--            --AND A.invoicedate <= Convert(nvarchar(8), DateAdd(D, 1, EOMonth(DateAdd(M, -19, Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)))), 12)              AND d.OrderType = 'I'
--            AND d.CustomerGLCode = 'TF'
--        )
--        OR
--        (
--            A.CompanyNum = '1'
--            --AND A.invoicedate >= Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)
--            --AND A.invoicedate <= Convert(nvarchar(8), DateAdd(D, 1, EOMonth(DateAdd(M, -19, Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)))), 12)              AND d.OrderType = 'I'
--            AND E.RepTypeCode = 'WR'
--        )
--)

    WITH cte_get_sales AS (
        SELECT  a.cust_class
              , a.CustomerGLCode
              , a.ID
              , a.CompanyNum
              , a.customernum
              , a.OrderNum
              , a.HistorySequenceNum
              , a.OrderSequenceNum
              , a.primarysalesmannum
              , a.ItemNum
              , a.ItemDescription1
              , a.QuantityShipped
              , a.listprice
              , a.actualsellprice
              , a.currentaveragecost
              , a.totallineamount
              , a.Invoice_Date AS InvoiceDate
              , a.TYPE
              , a.itemclass
              , a.ShippingLocation
              , a.QuantityOrdered
              , a.QuantityBackordered
              , a.DateAdded
              , a.Exl_Category
              , a.Sls_Ctg
              , a.Order_Date
        FROM    dbo.R19_Sales_P21 a
    --WHERE a.Exl_Category IN ('Valid - Kit/Assortment', 'Valid - Component')
    )

    /********************
Get Current Franchise Cost
********************/
   , cte_get_FCOST AS (
    SELECT
        A.ItemNumber
      , Avg(A.UserCost) AS [ListPrice]
    FROM    BI_STAGING.dbo.ItemBalance A
    WHERE   SuspendCode <> 'S'
    GROUP BY
        A.ItemNumber
)
   , cte_mfg_items AS (
    SELECT  DISTINCT
            ParentItemNumber
    FROM    BI_STAGING.dbo.MaterialComponent
)
    /*******************************
Get Sales for manufactured items
*******************************/
   , cte_mfg_sales AS (
    SELECT
        A.cust_class
      , A.CustomerGLCode
      , A.ID
      , A.CompanyNum
      , A.CustomerNum
      , A.OrderNum
      , A.HistorySequenceNum
      , A.OrderSequenceNum
      , A.PrimarySalesmanNum
      , A.ItemNum
      , A.ItemDescription1
      , A.QuantityShipped
      , A.ListPrice
      , A.ActualSellPrice
      , A.CurrentAverageCost
      , A.TotalLineAmount
      , A.InvoiceDate
      , A.[Type]
      , A.ItemClass
      , A.ShippingLocation
      , A.QuantityOrdered
      , A.QuantityBackordered
      , A.DateAdded
      , A.Exl_Category
      , A.Sls_Ctg
      , A.Order_Date
      , B.ParentItemNumber
    FROM    CTE_get_sales       A
        LEFT JOIN cte_mfg_items B
            ON A.ItemNum = B.ParentItemNumber
    WHERE
        B.ParentItemNumber IS NOT NULL
        AND A.QuantityShipped <> 0
        AND A.Exl_Category NOT IN
                ('Custom Kit')
)
    /*****************************************************
Get Custom Kit Components - Specific to Customer/Order
*****************************************************/
   , cte_Cust_Kit_sales AS (
    SELECT
        A.cust_class
      , A.CustomerGLCode
      , A.ID
      , A.CompanyNum
      , A.CustomerNum
      , A.OrderNum
      , A.HistorySequenceNum
      , A.OrderSequenceNum
      , A.PrimarySalesmanNum
      , A.ItemNum
      , A.ItemDescription1
      , A.QuantityShipped
      , A.ListPrice
      , A.ActualSellPrice
      , A.CurrentAverageCost
      , A.TotalLineAmount
      , A.InvoiceDate
      , A.[Type]
      , A.ItemClass
      , A.ShippingLocation
      , A.QuantityOrdered
      , A.QuantityBackordered
      , A.DateAdded
      , A.Exl_Category
      , A.Sls_Ctg
      , A.Order_Date
      , B.parentitemnumber    AS BOM
      , B.componentitemnumber AS Comp_Itm
      , B.quantityperparent   AS Tot_Qty
    FROM    CTE_get_sales                    A
        INNER JOIN BI_STAGING.dbo.BOMHistory B
            ON A.ItemNum = B.parentitemnumber
               AND  A.HistorySequenceNum = B.historysequencenumber
               AND  A.OrderSequenceNum = B.ordersequencenumber
    WHERE
        --B.parentitemnumber IS NOT NULL
        A.QuantityShipped <> 0
        AND A.Exl_Category IN
                ('Custom Kit')
)
   , cte_cvt_cust_kit_sales AS (
    SELECT
        A.ID
      , A.Exl_Category
      , A.Sls_Ctg
      , A.CompanyNum
      , A.CustomerNum
      , A.OrderNum
      , A.HistorySequenceNum
      , A.OrderSequenceNum
      , A.PrimarySalesmanNum
      , A.Comp_Itm                      AS ItemNum
      , (A.Tot_Qty * A.QuantityShipped) AS QuantityShipped
      , CASE
            WHEN A.InvoiceDate < 190201
                THEN Cast(Replace(Replace(C.[2018], '"', ''), ',', '') AS decimal(28, 5))
            WHEN A.InvoiceDate < 200106
                THEN Cast(Replace(Replace(C.[2019], '"', ''), ',', '') AS decimal(28, 5))
            ELSE Cast(Replace(Replace(d.ListPrice, '"', ''), ',', '') AS decimal(28, 5))
        END                             AS ListPrice
      , 0                               AS ActualSellPrice
      , A.InvoiceDate
      , A.ItemClass
      , A.ShippingLocation
      , A.ItemNum                       AS MfgItem
      , A.QuantityShipped               AS KitQuantityShipped
      , A.Order_Date
    FROM    cte_Cust_Kit_sales  A
        LEFT JOIN dbo.FCOST_HST C
            ON A.Comp_Itm = C.Item_Number

        LEFT JOIN cte_get_FCOST d
            ON A.comp_itm = d.ItemNumber
)
    /***************************
Get Sales for standard items
***************************/
   , cte_not_mfg_sales AS (
    SELECT  *
    FROM    CTE_get_sales       A
        LEFT JOIN cte_mfg_items B
            ON A.ItemNum = B.ParentItemNumber
    WHERE   NullIf(B.ParentItemNumber, '') IS NULL  --AND a.Exl_Category IN ('Valid - Kit/Assortment', 'Valid - Component')
)
   , Cte_sing_comp_kits AS (
    SELECT
        BOM
      , Count(Comp_Itm) AS Num_Comp
    FROM    dbo.BOM_ExplodeV3
    GROUP BY
        dbo.BOM_ExplodeV3.BOM
    HAVING  Count(Comp_Itm) = 1
)
    /***********************************
Convert Sales for manufactured items
***********************************/
   , cte_convert_sales AS (
    SELECT
        A.ID
      , A.Exl_Category
      , A.Sls_Ctg
      , A.CompanyNum
      , A.CustomerNum
      , A.OrderNum
      , A.HistorySequenceNum
      , A.OrderSequenceNum
      , A.PrimarySalesmanNum
      , B.Comp_Itm                      AS ItemNum
      , (B.Tot_Qty * A.QuantityShipped) AS QuantityShipped
      , CASE
            WHEN A.InvoiceDate < 190201
                THEN Cast(Replace(Replace(C.[2018], '"', ''), ',', '') AS decimal(28, 5))
            WHEN A.InvoiceDate < 200106
                THEN Cast(Replace(Replace(C.[2019], '"', ''), ',', '') AS decimal(28, 5))
            WHEN d.Num_Comp = 1
                THEN Cast((A.ListPrice / B.Tot_Qty) AS decimal(18, 3))
            ELSE Cast(Replace(Replace(E.ListPrice, '"', ''), ',', '') AS decimal(28, 5))
        END                             AS ListPrice
      , CASE
            WHEN (B.Tot_Qty * A.QuantityShipped) = 0
                THEN 0
            WHEN d.Num_Comp = 1
                THEN Cast(A.TotalLineAmount / (B.Tot_Qty * A.QuantityShipped) AS decimal(18, 3))
            ELSE 0
        END                             AS ActualSellPrice
      , A.InvoiceDate
      , A.ItemClass
      , A.ShippingLocation
      , A.ItemNum                       AS MfgItem
      , A.QuantityShipped               AS KitQuantityShipped
      , A.Order_Date
    FROM    cte_mfg_sales           A
        LEFT JOIN dbo.BOM_ExplodeV3 B
            ON A.ItemNum = B.BOM

        LEFT JOIN dbo.FCOST_HST     C
            ON B.Comp_Itm = C.Item_Number

		LEFT JOIN Cte_sing_comp_kits d
            ON A.ItemNum = d.BOM

        LEFT JOIN cte_get_FCOST     E
            ON B.Comp_Itm = E.ItemNumber
)
   , cte_union_sales AS (
    SELECT
        A.ID
      , A.Exl_Category
      , A.Sls_Ctg
      , A.CompanyNum
      , A.CustomerNum
      , A.OrderNum
      , A.HistorySequenceNum
      , A.OrderSequenceNum
      , A.PrimarySalesmanNum
      , A.ItemNum
      , A.QuantityShipped
      , A.ListPrice
      , A.ActualSellPrice
      , A.InvoiceDate
      , A.ItemClass
      , A.ShippingLocation
      , '' AS MfgItem
      , 0  AS KitQuantityShipped
      , A.Order_Date
    FROM    cte_not_mfg_sales A
    UNION ALL
    SELECT
        B.ID
      , B.Exl_Category
      , B.Sls_Ctg
      , B.CompanyNum
      , B.CustomerNum
      , B.OrderNum
      , B.HistorySequenceNum
      , B.OrderSequenceNum
      , B.PrimarySalesmanNum
      , B.ItemNum
      , B.QuantityShipped
      , B.ListPrice
      , B.ActualSellPrice
      , B.InvoiceDate
      , B.ItemClass
      , B.ShippingLocation
      , B.MfgItem
      , B.KitQuantityShipped
      , B.Order_Date
    FROM    cte_convert_sales B
    UNION ALL
    SELECT
        C.ID
      , C.Exl_Category
      , C.Sls_Ctg
      , C.CompanyNum
      , C.CustomerNum
      , C.OrderNum
      , C.HistorySequenceNum
      , C.OrderSequenceNum
      , C.PrimarySalesmanNum
      , C.ItemNum
      , C.QuantityShipped
      , C.ListPrice
      , C.ActualSellPrice
      , C.InvoiceDate
      , C.ItemClass
      , C.ShippingLocation
      , C.MfgItem
      , C.KitQuantityShipped
      , C.Order_Date
    FROM    cte_cvt_cust_kit_sales C
--UNION ALL
--SELECT
--		a.ID, 
--		a.Exl_Category,
--              a.CompanyNum, 
--              a.CustomerNum, 
--              a.OrderNum, 
--              a.HistorySequenceNum, 
--              a.OrderSequenceNum, 
--              a.PrimarySalesmanNum, 
--              a.ItemNum, 
--              a.QuantityShipped, 
--              a.ListPrice, 
--              a.ActualSellPrice, 
--              --a.CurrentAverageCost, 
--              --a.TotalLineAmount, 
--              a.InvoiceDate, 
--              a.ItemClass, 
--              a.ShippingLocation, 
--              '' AS MfgItem, 
--              0 AS KitQuantityShipped
--		FROM CTE_get_sales a
--		WHERE a.Exl_Category NOT IN ('Valid - Kit/Assortment', 'Valid - Component')
)
SELECT
    a.*
  , Concat(a.ItemNum, '_', b.ItemDescription1) AS [Item_Desc]
FROM    cte_union_sales                 a
    LEFT JOIN BI_STAGING.dbo.ItemMaster b
        ON a.ItemNum = b.ItemNumber;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[R19_Exploded_Plus_Open]
AS
SELECT
    'Order' AS Order_Type
  , a.ID
  , a.Exl_Category
  , a.Sls_Ctg
  , a.CompanyNum
  , a.CustomerNum
  , a.OrderNum
  , a.OrdGenNum
  , a.OrderStatusCode
  , a.OrderType
  , a.HoldCode
  , a.Allocated
  , a.DropShip
  , a.Specials
  , a.HistorySequenceNum
  , a.OrderSequenceNum
  , a.PrimarySalesmanNum
  , a.ItemNum
  , a.QuantityShipped
  , a.ListPrice
  , a.ActualSellPrice
  , a.InvoiceDate
  , a.ItemClass
  , a.ShippingLocation
  , a.MfgItem
  , a.KitQuantityShipped
  , a.Order_Date
  , a.Item_Desc
FROM    dbo.Open_Orders_Exploded a
UNION ALL
SELECT
    'Invoice' AS Order_Type
  , b.ID
  , b.Exl_Category
  , b.Sls_Ctg
  , b.CompanyNum
  , b.CustomerNum
  , b.OrderNum
  , 0 AS OrdGenNum
  , '' AS OrderStatusCode
  , '' AS OrderType
  , '' AS HoldCode
  , '' AS Allocated
  , '' AS DropShip
  , '' AS Specials
  , b.HistorySequenceNum
  , b.OrderSequenceNum
  , b.PrimarySalesmanNum
  , b.ItemNum
  , b.QuantityShipped
  , b.ListPrice
  , b.ActualSellPrice
  , b.InvoiceDate
  , b.ItemClass
  , b.ShippingLocation
  , b.MfgItem
  , b.KitQuantityShipped
  , b.Order_Date
  , b.Item_Desc
FROM    dbo.R19_Sales_Exploded b;
GO

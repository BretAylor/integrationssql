SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[Inventory_Refresh_TMSTMP] as
SELECT Max(a.dateadded) AS Data_Refreshed FROM bi_staging.dbo.ItemBalance a
GO

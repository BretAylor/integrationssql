SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE View [dbo].[CustomerValidationView]
as

SELECT 
       'Customer submitted while package load is running. The Customer will be loaded shortly' AS [On Hold],
       CustomerNumber,
	   PepperiCustomerNumber,
       CustomerName,
       CreatedDate

FROM 
       [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].Customer 
WHERE DATEPART(MINUTE, GETDATE()) BETWEEN 0 AND 29 AND 
CreatedDate BETWEEN  CAST(CAST(CAST(GETDATE() AS date) AS varchar(10)) +' '+CAST(DATEPART(HOUR,GETDATE())  AS CHAR(2)) +':'+'00:00' AS DATETIME)  AND CAST(CAST(CAST(GETDATE() AS date) AS varchar(10)) +' '+CAST(DATEPART(HOUR,GETDATE())  AS CHAR(2)) +':'+'20:00' AS DATETIME)



UNION ALL 

SELECT 
      'Customer submitted while package load is running. The Customer will be loaded shortly' AS [On Hold],
       CustomerNumber ,
	   PepperiCustomerNumber,
       CustomerName,
       CreatedDate
FROM 
[wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].Customer  
      
WHERE DATEPART(MINUTE, GETDATE()) BETWEEN 30 AND 59 AND 
CreatedDate BETWEEN CAST(CAST(CAST(GETDATE() AS date) AS varchar(10)) +' '+ CAST(DATEPART(HOUR,GETDATE())  AS CHAR(2)) +':'+'30:00'AS DATETIME)  AND 
CAST(CAST(CAST(GETDATE() AS date) AS varchar(10)) +' '+ CAST(DATEPART(HOUR,GETDATE())  AS CHAR(2)) +':'+'50:00'AS DATETIME) 

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[POs_To_Cancel]
AS(
SELECT b.VendorNumber
     , b.PurchaseOrderNumber
     , b.LineSequenceNo
     , a.Whse
     , a.ItemNumber
     , (b.OrderQuantity - b.QuantityReceivedtodate) AS Open_Qty
	 , b.UnshippedAmount
     , b.OrderDate AS [Order_Date(YYMMDD)]
     , b.DueDate AS [Due_Date(YYMMDD)]
FROM dbo.Open_Ord_Amt_Demand                        a
    LEFT JOIN BI_STAGING.dbo.OpenPurchaseOrderLines b
        ON a.ItemNumber = b.ItemNumber
           AND a.Whse = b.WarehouseID
WHERE b.LineItemStatus <> 'C' AND b.VendorNumber NOT LIKE 'WTV%'
--ORDER BY VendorNumber asc
		   );
GO

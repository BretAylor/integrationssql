SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[Customer_Class_New_Desc] as
(
SELECT a.Customer, a.Customer_Name, a.New_Class, a.New_Sub_Class,b.Class_Description, b.Subclass_Description
FROM dbo.cust_class_Mst_New a
LEFT JOIN dbo.cust_classes_new b ON
a.New_Class = b.New_Class AND
a.New_Sub_Class = b.New_Subclass
)
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[Open_Ord_Amt_Demand]
AS
WITH CTE_Demand
AS (
   SELECT a.ItemNumber
        , B.ShippingLocation
        --, d.BuyerInitial
        , Sum(B.QuantityShipped) AS Demand_Tot
   --, Sum(b.OrderQuantity - b.QuantityReceivedtodate) AS Open_Qty
   --, Sum(b.UnshippedAmount)                          AS Open_Value
   FROM BI_STAGING.dbo.ItemMaster          a
       LEFT JOIN dbo.Sales_Demand_Exploded B
           ON a.ItemNumber = B.ItemNum
   --LEFT JOIN BI_STAGING.dbo.OpenPurchaseOrderLines b
   --    ON a.ItemNum = b.ItemNumber
   --       AND a.ShippingLocation = b.WarehouseID
   --LEFT JOIN bi_staging.dbo.ItemMaster c
   --ON a.ItemNum = c.ItemNumber
   --LEFT JOIN bi_staging.dbo.PurchasingVendorMaster d
   --ON c.VendorNumber = d.VendorNumber
   WHERE B.Exl_Category <> 'Rep 992 - Internal'
   GROUP BY a.ItemNumber
          , B.ShippingLocation
--,d.BuyerInitial
--HAVING Sum(b.UnshippedAmount) <> 0;

)
   , cte_open_PO
AS (SELECT a.ItemNumber
         , a.WarehouseID                                   AS Whse
         , d.BuyerInitial                                  AS Buyer
         , Count(DISTINCT a.PurchaseOrderNumber)           AS Open_POs
         , Sum(a.OrderQuantity - a.QuantityReceivedtodate) AS Open_Qty
         , Sum(a.UnshippedAmount)                          AS Order_Dollars
         , b.Demand_Tot
    FROM BI_STAGING.dbo.OpenPurchaseOrderLines           a
        LEFT JOIN CTE_Demand                             b
            ON a.ItemNumber = b.ItemNumber
               AND a.WarehouseID = b.ShippingLocation
        LEFT JOIN BI_STAGING.dbo.OpenPurchaseorderHeader d
            ON a.PurchaseOrderNumber = d.PurchaseOrderNumber
        LEFT JOIN BI_STAGING.dbo.ItemMaster              c
            ON a.ItemNumber = c.ItemNumber
    WHERE a.VendorNumber NOT LIKE 'WTV%'
          AND a.LineItemStatus <> 'C'
    GROUP BY a.ItemNumber
           , a.WarehouseID
           , b.Demand_Tot
           , d.BuyerInitial)
   , CTE_Inventory
AS (SELECT a.*
         , b.MinimumOnHandQuantity1 AS Min_OH
         , b.MaximumOnHandQuantity1 AS Max_OH
         , b.QuantityOnHandUM1      AS Qty_OH
         , b.VendorNumber           AS Vendor
         , b.QuantityAllocatedUM1   AS Aloc_Qty
         , b.QuantityonBackorderUM1 AS BO_Qty
    FROM cte_open_PO                         a
        LEFT JOIN BI_STAGING.dbo.ItemBalance b
            ON a.ItemNumber = b.ItemNumber
               AND a.Whse = b.WarehouseID),
cte_final AS (

SELECT a.*
     , CASE
           WHEN a.Demand_Tot = 0 THEN
               '999999'
           ELSE
               Round(a.Open_Qty / a.Demand_Tot, 2)
       END AS Yrs_On_Ord
     , CASE
           WHEN a.Demand_Tot = 0 THEN
               '999999'
           ELSE
               Round((a.Qty_OH - a.Aloc_Qty) / a.Demand_Tot, 2)
       END AS Yrs_OH
     , CASE
           WHEN a.Demand_Tot = 0 THEN
               '999999'
           ELSE
               Round(((a.Qty_OH + a.Open_Qty) - a.Aloc_Qty) / a.Demand_Tot, 2)
       END AS Yrs_Covered
FROM CTE_Inventory a
WHERE a.Open_Qty > 0

)

SELECT * 
FROM cte_final A 
--WHERE A.ORDER_dollars >= 1000 AND
--a.Buyer = 'LR' AND a.Yrs_Covered >= 1


--AND a.QuantityonBackorderUM1 > a.QuantityAllocatedUM1;l


--SELECT A.ItemNum
--     , A.ShippingLocation
--	 --, d.BuyerInitial
--     , a.QuantityShipped                          AS Demand_Tot
--     , B.PurchaseOrderNumber
--	 , B.OrderQuantity - B.QuantityReceivedtodate  AS Open_Qty
--     , B.UnshippedAmount                           AS Open_Value
--FROM dbo.Sales_Demand_Exploded                      A
--    LEFT JOIN BI_STAGING.dbo.OpenPurchaseOrderLines B
--        ON A.ItemNum = B.ItemNumber
--           AND A.ShippingLocation = B.WarehouseID
--		   WHERE B.OrderQuantity - B.QuantityReceivedtodate <> 0
--		   AND a.ItemNum = '715.8520'
--		   --GROUP BY a.ItemNum



--SELECT A.exl_category, SUM(A.quantityshipped) AS demand FROM dbo.sales_demand_exploded A 
--WHERE 
----a.Exl_Category = 'Rep 992 - Internal' AND 
--a.ItemNum = '715.8520'
--GROUP BY a.exl_category
GO

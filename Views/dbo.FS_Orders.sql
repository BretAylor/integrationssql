SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[FS_Orders] AS (

SELECT * FROM OpenQuery([WINEPISQLVSQL01\EPIMSSQL],


'WITH CTE_Item_Type
     AS (SELECT       
                --c.location_id, 
                --c.location_name,
                a.inv_mast_uid,
				a.item_id,
				--a.moving_average_cost,
                --Sum(a.qty_on_hand) AS Qty, 
                --Sum(a.qty_on_hand * a.moving_average_cost) AS Inv_Val, 
                --COUNT(DISTINCT b.item_id) AS SKU_Count,
                CASE
                                             WHEN Left(b.item_id, 2) IN(''FZ'', ''FX'', ''FS'') THEN ''Special''
                    ELSE ''Standard''
                END AS Item_Type
                FROM  p21.dbo.inv_mast a
                      LEFT JOIN p21.dbo.inv_mast b
                      ON a.inv_mast_uid = b.inv_mast_uid
                --WHERE a.qty_on_hand > 0 
				--AND a.inv_mast_uid = 164464
				),

				cte_get_POs AS (
				/****** Script for SelectTopNRows command from SSMS  ******/
SELECT 
	  a.po_no
	, a.created_by
	, e.name AS Created_Name
	, a.location_id
	, c.supplier_id
	, c.supplier_name
	, b.inv_mast_uid AS bInvMastUID
	, d.inv_mast_uid AS dInvMastUID
	, d.item_id
	, d.Item_Type
	, b.qty_ordered
	, b.unit_price
	, b.qty_ordered*b.unit_price AS Cost
	, Month(a.date_created) AS MM
	, Year(a.date_created) AS YY
	, a.date_created
  FROM [P21].[dbo].[po_hdr] a
  INNER JOIN p21.dbo.po_line b ON
  a.po_no = b.po_no
  INNER JOIN p21.dbo.supplier c 
  ON a.supplier_id = c.supplier_id
  LEFT JOIN CTE_Item_Type d
  ON b.inv_mast_uid = d.inv_mast_uid
  LEFT JOIN p21.dbo.users e
  ON Replace(a.created_by,''WINZERNT\'','''') = e.id
  WHERE DateDiff(MONTH,a.date_created,GetDate()) <=12
 -- GROUP BY a.created_by
	--,a.location_id
	--,c.supplier_name
	--,a.date_created
				)


SELECT * FROM cte_get_POs'))
GO

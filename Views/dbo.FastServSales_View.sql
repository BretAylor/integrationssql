SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- Get YTDSales and MTDSales totals
Create VIEW [dbo].[FastServSales_View]
AS 

SELECT 
	   DD.FiscalYear,
	   DT.[MonthName] AS CurrentMonthName,
	   DT.[FiscalMonth] AS CurrentFiscalMonth,
	   EM.SalesRepID,
	   FS.EmployeeKey,
	   ISNULL(EM.FirstName,'') + ' ' +  ISNULL(EM.Middle,'') + ' ' + ISNULL(EM.LastName,'') as SalesRepresentativeName,
	   FS.BranchKey,
	   BR.BranchID,
	   BR.BranchDescription,
	   ROUND(SUM(CASE WHEN DD.FiscalMonth = DT.[FiscalMonth] THEN FS.NetSales ELSE 0 end),0) AS MtdSales,
	   ROUND(SUM(FS.NetSales),0) AS YtdSales ,
	   FS.DateAdded
	   
   FROM BI_DWH.[dbo].[FactFastServNetSales] FS
    INNER JOIN BI_DWH.dbo.DimDate DD
       ON FS.DateKey = DD.DateKey
	LEFT outer JOIN BI_DWH.dbo.DimBranchP21 BR 
	    ON FS.BranchKey = BR.BranchKey
    LEFT outer JOIN dbo.DimEmployeeP21 EM
		ON EM.EmployeeKey = FS.EmployeeKey
	LEFT OUTER JOIN BI_DWH.dbo.DimDate DT
	    ON CAST(GETDATE() AS DATE) = DT.Date
	
WHERE DD.FiscalYear = DT.FiscalYear --@CurrentFYear 
GROUP  BY
       DD.FiscalYear,
	   EM.SalesRepID,
	   ISNULL(EM.FirstName,'') + ' ' +  ISNULL(EM.Middle,'') + ' ' + ISNULL(EM.LastName,'') ,
	   DT.[MonthName],
	   DT.[FiscalMonth],
	   DT.[MonthName],
	   BR.BranchID,
	   BR.BranchDescription ,
	   FS.DateAdded,
	   FS.EmployeeKey,
	   FS.BranchKey

GO

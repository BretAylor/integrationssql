SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[ITM_CLASS_SALES] AS (

SELECT
    a.InvoiceDate
  , b.ItemClass
  , Sum(a.CurrentAverageCost*a.QuantityShipped) AS COGS
  , Sum(a. ListPrice * a.QuantityShipped) AS F_Sales
  , Sum(a.TotalLineAmount) AS C_Sales

FROM    dbo.R18_Sales_P21_Added         a
    LEFT JOIN BI_STAGING.dbo.ItemMaster b
        ON a.ItemNum = b.ItemNumber
WHERE a.QuantityShipped > 0
GROUP BY     a.InvoiceDate
  , b.ItemClass


)
GO

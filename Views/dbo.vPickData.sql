SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vPickData] AS
(

SELECT a.*, b.ItemNumber,b.UserField1 FROM dbo.pick_data a
LEFT JOIN bi_staging.dbo.ItemMaster b ON
a.AAMPKITNO = b.ItemNumber
--WHERE Cast(Replace(Left(a.aampkdate,10),'-','') AS date) IN('20191014', '20191015')
)
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[Covid_Sales_Open_History_Exploded] AS

SELECT a.*
	 FROM dbo.R19_Exploded_Plus_Open a
INNER JOIN dbo.covid_items b
ON a.ItemNum = b.Item

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--TRUNCATE TABLE dbo.R18_Sales;

--DECLARE @END   AS nvarchar(8)
--      , @BEGIN AS nvarchar(8);
----@M1 AS Date;

--SET @END = Convert(nvarchar(8), EOMonth(DateAdd(M, -1, GetDate())), 12); -- UPDATE to -1
--PRINT @END;
--SET @BEGIN = Convert(nvarchar(8), DateAdd(D, 1, EOMonth(DateAdd(M, -18, @END))), 12);
--PRINT @BEGIN;
--SET	@M1  = DATEADD(M,-1, @END); PRINT @m1

--SELECT * FROM bi_staging.dbo.InvoiceLineFranchise WHERE Format(InvoiceDate,'YYMMDD') >= @BEGIN AND Format(InvoiceDate,'YYMMDD') <= @end

CREATE VIEW [dbo].[R19_Sales_P21]
AS

/*Get AS400 Sales for Period*/
WITH cte_get_prd_sales AS (
    SELECT  *
    FROM    BI_STAGING.dbo.InvoiceLineFranchise a
    WHERE
        a.InvoiceDate <= Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)
        AND a.InvoiceDate >= Convert(nvarchar(8), DateAdd(D, 1, EOMonth(DateAdd(M, -19, Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)))), 12)
)
    /*Build Item Conversion for P21>AS400*/
   , cte_build_conversion AS (
    SELECT
        a.inv_mast_uid
      , a.item_id
      , a.item_desc
      , a.date_created AS ITM_Created
      , b.SupplierPartNo
      , CASE
            WHEN NullIf(Trim(b.SupplierPartNo),'') IS NULL
                THEN a.item_id
            ELSE b.SupplierPartNo
        END            AS [ItemNum]
      , c.BillofMaterialType
      , a.class_id1
    FROM    BI_STAGING.dbo.ViewInvMastP21              a
        INNER JOIN BI_STAGING.dbo.InventorySupplierP21 b
            ON a.inv_mast_uid = b.InvMastUid
               AND  b.SupplierId = 100054

        LEFT JOIN BI_STAGING.dbo.ItemMaster            c
            ON b.SupplierPartNo = c.ItemNumber
)
    /*Get P21 Sales*/
   , cte_get_sales_p21 AS (
    SELECT
        ''                                       AS cust_class
      , ''                                       AS CustomerGLCode
      , a.invoice_line_uid                       AS [ID]
      , a.companyid                              AS [CompanyNum]
      , b.customerid                             AS [customernum]
      , a.orderno                                AS [OrderNum]
      , a.invoiceno                              AS [HistorySequenceNum]
      , a.[lineno]                               AS [OrderSequenceNum]
      , a.gl_revenue_account_no                  AS [primarysalesmannum]
      , c.ItemNum                                AS [ItemNum]
      , a.item_desc                              AS [ItemDescription1]
      , a.qtyshipped                             AS [QuantityShipped]
      , 0                                        AS [listprice]
      , a.unitprice                              AS [actualsellprice]
      , a.cogsamount / a.qtyshipped              AS [currentaveragecost]
      , a.extendedprice                          AS [totallineamount]
      , Convert(nvarchar(8), b.invoice_date, 12) AS [Invoice_Date]
      , 'I'                                      AS [TYPE]
      , c.class_id1                              AS [itemclass]
      , Right(a.gl_revenue_account_no, 4)        AS [ShippingLocation]
      , a.qty_requested                          AS [QuantityOrdered]
      , 0                                        AS [QuantityBackordered]
      , GetDate()                                AS [DateAdded]
      , Convert(nvarchar(8), b.order_date, 12)   AS [Order_Date]
    FROM    BI_STAGING.dbo.InvoiceLineP21    a
        INNER JOIN BI_STAGING.dbo.InvoiceP21 b
            ON a.invoiceno = b.invoiceno
               AND  b.invoice_date >= Convert(nvarchar(8), DateAdd(MONTH, DateDiff(MONTH, 0, GetDate()) - 18, 0), 12)
               AND  b.invoice_date <= Convert(nvarchar(8), DateAdd(MONTH, 1 + DateDiff(MONTH, 0, GetDate()), -1), 12)
        --OpenQuery
        --           ([WINEPISQLVSQL01\EPIMSSQL]
        --          , 'Select a.*, b.order_date,b.customer_id, b.invoice_date from p21.dbo.invoice_line a
        --			INNER JOIN p21.dbo.invoice_hdr b
        --			on a.invoice_no = b.invoice_no
        --			where inv_mast_uid is not null 
        --				and b.invoice_date >= Convert(nvarchar(8),DateAdd(Month,DateDiff(month,0,GETDATE())-18,0),12) 
        --				and b.invoice_date  <= Convert(nvarchar(8),DateAdd(Month,1+DateDiff(month,0,GETDATE()),-1),12) '
        --           )                          a

        INNER JOIN cte_build_conversion      c
            ON a.invmastuid = c.inv_mast_uid
               AND  a.otherchargeitem = 'N'
--LEFT JOIN BI_STAGING.dbo.ItemMaster    c
--    ON b. = c.sup
)
    /*Convert Sales to AS400 Item*/
   , cte_p21_xref AS (
    SELECT
        a.cust_class
      , a.CustomerGLCode
      , a.[ID]
      , a.[CompanyNum]
      , a.[customernum]
      , a.[OrderNum]
      , a.[HistorySequenceNum]
      , a.[OrderSequenceNum]
      , a.[primarysalesmannum]
      , a.[ItemNum]
      , a.[ItemDescription1]
      , a.[QuantityShipped]
      , a.[listprice]
      , a.[actualsellprice]
      , a.[currentaveragecost]
      , a.[totallineamount]
      , a.[Invoice_Date]
      , a.[TYPE]
      , a.[itemclass]
      , a.[ShippingLocation]
      , a.[QuantityOrdered]
      , a.[QuantityBackordered]
      , a.[DateAdded]
      , CASE
            WHEN Left(a.ItemNum, 3) = '975'
                THEN 'Custom Kit'
            WHEN C.BillofMaterialType <> ''
                THEN 'Valid - Kit/Assortment'
            ELSE 'Valid - Component'
        END AS [Exl_Category]
      , CASE
            WHEN a.ShippingLocation = '1006'
                THEN 'A1'
            WHEN a.ShippingLocation = '1052'
                THEN 'SPD'
            ELSE 'FS'
        END AS Sls_Ctg
      , a.[Order_Date]
    FROM    cte_get_sales_p21               a
        LEFT JOIN BI_STAGING.dbo.ItemMaster C
            ON a.ItemNum = C.ItemNumber
)
   , CTE_get_all_sales AS (
    SELECT
        B.CustomerClass                                             AS cust_class
      , d.CustomerGLCode
      , A.ID
      , A.CompanyNum
      , A.CustomerNum
      , A.OrderNum
      , A.HistorySequenceNum
      , A.OrderSequenceNum
      , A.PrimarySalesmanNum
      , A.ItemNum
      , A.ItemDescription1
      , A.QuantityShipped
      , A.ListPrice
      , A.ActualSellPrice
      , A.CurrentAverageCost
      , A.TotalLineAmount
      , A.InvoiceDate
      , A.[Type]
      , c.ItemClass
      , A.ShippingLocation
      , A.QuantityOrdered
      , A.QuantityBackordered
      , A.DateAdded
      , CASE
            WHEN Left(A.ItemNum, 3) = '975'
                THEN 'Custom Kit'
            WHEN C.BillofMaterialType <> ''
                THEN 'Valid - Kit/Assortment'
            ELSE 'Valid - Component'
        END                                                         AS [Exl_Category]
      , CASE
            WHEN F.SplitProfitProgramFlag = 'Y'
                THEN 'GPS'
            WHEN E.Division = 'A1S'
                THEN 'A1'
            WHEN E.Division = 'SCL'
                THEN 'Inside Sales'
            WHEN A.PrimarySalesmanNum = '992'
                THEN 'Rep 992 - Internal'
            WHEN C.ItemClass = '90'
                THEN 'IC 90 - Bill Only'
            WHEN C.ItemClass = '91'
                THEN 'IC 91 - FDP'
            WHEN C.ItemClass = '93'
                THEN 'IC 93 - VSN'
            WHEN C.ItemClass = '82'
                THEN 'IC 82 - Promo'
            WHEN C.ItemClass = '89'
                THEN 'IC 89 - Specials'
            WHEN C.ItemClass = '80'
                THEN 'IC 80 - Handouts'
            ELSE 'Franchise'
        END                                                         AS Sls_Ctg
      , Convert(nvarchar(8), Right(G.OrderCreateSystemDate, 6), 12) AS [Order_Date]
    FROM    cte_get_prd_sales                         A
        INNER JOIN BI_STAGING.dbo.CustomerMaster      B
            ON A.CustomerNum = B.CustomerNum

        INNER JOIN BI_STAGING.dbo.ItemMaster          C
            ON A.ItemNum = C.ItemNumber

        INNER JOIN BI_STAGING.dbo.InvoiceFranchise    d
            ON A.HistorySequenceNum = d.HistorySequenceNum

        INNER JOIN BI_STAGING.dbo.EmployeeSup         E
            ON A.PrimarySalesmanNum = E.SalesRepresentativeNum

        INNER JOIN BI_STAGING.dbo.CustomerSup         F
            ON A.CustomerNum = F.CustomerNum

        INNER JOIN BI_STAGING.dbo.InvoiceFranchiseSup G
            ON A.HistorySequenceNum = G.HistorySequenceNum
    WHERE
        (
            A.CompanyNum = '1'
            --AND A.invoicedate >= Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)
            --AND A.invoicedate <= Convert(nvarchar(8), DateAdd(D, 1, EOMonth(DateAdd(M, -19, Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)))), 12)
            AND d.OrderType <> 'I'
            AND C.ItemClass <> '90'
            AND d.CustomerGLCode IN
                    (
                        'FR'
                      , 'NF'
                      , 'SA'
                      , 'TF'
                    )
        )
        OR
        (
            A.CompanyNum = '1'
            --AND A.invoicedate >= Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)
            --AND A.invoicedate <= Convert(nvarchar(8), DateAdd(D, 1, EOMonth(DateAdd(M, -19, Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)))), 12)              AND d.OrderType = 'I'
            AND C.ItemClass = '91'
            AND d.CustomerGLCode <> 'TG'
        )
        OR
        (
            A.CompanyNum = '1'
            --AND A.invoicedate >= Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)
            --AND A.invoicedate <= Convert(nvarchar(8), DateAdd(D, 1, EOMonth(DateAdd(M, -19, Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)))), 12)              AND d.OrderType = 'I'
            AND d.CustomerGLCode = 'TF'
        )
        OR
        (
            A.CompanyNum = '1'
            --AND A.invoicedate >= Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)
            --AND A.invoicedate <= Convert(nvarchar(8), DateAdd(D, 1, EOMonth(DateAdd(M, -19, Convert(nvarchar(8), EOMonth(DateAdd(M, -0, GetDate())), 12)))), 12)              AND d.OrderType = 'I'
            AND E.RepTypeCode = 'WR'
        )
)
   , cte_union_all AS (
    SELECT  *
    FROM    cte_p21_xref
    UNION ALL
    SELECT  *
    FROM    CTE_get_all_sales
)
--INSERT INTO dbo.R18_Sales
SELECT  *
--INTO SALES_FINAL_FY2019 
FROM    cte_union_all A;
--SELECT a.Exl_Category, SUM(a.quantityshipped * a.ListPrice) as FC_Sales, Sum(a.quantityshipped * a.ActualSellPrice) AS CUS_Sales FROM cte_get_all_sales a GROUP BY a.Exl_Category ORDER BY a.Exl_Category asc


GO

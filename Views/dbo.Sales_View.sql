SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[Sales_View]
AS
SELECT 'Franchise' AS [Type],
       1 AS TypeNum,
       INF.OrderKey AS OrderId,
       INF.DateKey,
       DD.[Date],
	   LEFT(DD.[DayName],3) AS [DayName] ,
	   DD.FullDate  +' - '+ LEFT(DD.[DayName],3) AS DateDay,
       DD.MonthName,
       DD.FiscalMonth,
       DD.FiscalYear,
       CU.CustomerKey,
       CU.CustomerNum,
       CU.CustomerName,
       ST.ShipmentKey,
       ST.ShipToNum,
       ST.ShipToName,
       PR.ProductKey,
       PR.ProductNum,
       PR.ProductDescription,
	   PR.UnitOfMeasure AS UOM,
	   PR.ProductNum +' - ' + PR.ProductDescription AS ProductNumDesc,
       PR.ItemClass,
       IC.[Description] AS ItemClassName,
       EM.EmployeeKey,
       EM.SalesRepresentativeNum,
       EM.SalesRepresentativeName,
       NULL AS BranchKey,
       INF.QuantityShipped,
       INF.ListPrice,
       INF.WinzerCost,
       INF.FranchiseCost,
       INF.NetSales,
       GETDATE() AS DateAdded
FROM BI_DWH.[dbo].[FactInvoiceFranchise] INF
    INNER JOIN BI_DWH.dbo.DimDate DD
        ON INF.DateKey = DD.DateKey
    LEFT OUTER JOIN BI_DWH.dbo.DimCustomer CU
        ON CU.CustomerKey = INF.CustomerKey
    LEFT OUTER JOIN BI_DWH.dbo.DimProduct PR
        ON PR.ProductKey = INF.ProductKey
    LEFT OUTER JOIN BI_DWH.dbo.DimEmployee EM
        ON EM.EmployeeKey = INF.EmployeeKey
    LEFT OUTER JOIN BI_DWH.dbo.DimShipTo ST
        ON ST.ShipmentKey = INF.ShipmentKey
    LEFT OUTER JOIN BI_DWH.dbo.DimItemClass IC
        ON IC.ItemClassKey = PR.ItemClassKey
WHERE DD.FiscalYear >= 2018
UNION ALL
/* Add P21 costs*/
SELECT 'FastServ' AS [Type],
       2 AS TypeNum,
       FS.OrderNo AS OrderId,
       FS.DateKey,
       DD.[Date],
	   LEFT(DD.[DayName],3) AS [DayName] ,
	   DD.FullDate  +' - '+ LEFT(DD.[DayName],3) AS DateDay,
       DD.MonthName,
       DD.FiscalMonth,
       DD.FiscalYear,
       CU.CustomerKey,
       CU.CustomerId,
       CU.CustomerName,
       ST.ShipmentKey,
       ST.ShipToNum,
       ST.ShipToName,
       PR.ProductKey,
       PR.ItemId,
       PR.ItemDesc,
	   NULL AS UOM,
	   PR.ItemID +' - ' + PR.ItemDesc AS ProductNumDesc,
       NULL ItemClass,
       NULL AS ItemClassName,
       EM.EmployeeKey,
       EM.SalesRepID AS SalesRepresentativeNum,
       ISNULL(EM.FirstName, '') + ' ' + ISNULL(EM.Middle, '') + ' ' + ISNULL(EM.LastName, '') AS SalesRepresentativeName,
       BranchKey,
       FS.QtyShipped AS QuantityShipped,
       FS.UnitPrice,
       FS.WinzerCost,
       FS.FranchiseCost,
       FS.NetSales,
       GETDATE() AS DateAdded
FROM BI_DWH.[dbo].[FactFastServNetSales] FS
    INNER JOIN BI_DWH.dbo.DimDate DD
        ON FS.DateKey = DD.DateKey
    LEFT OUTER JOIN BI_DWH.dbo.DimCustomerP21 CU
        ON CU.CustomerKey = FS.CustomerKey
    LEFT OUTER JOIN BI_DWH.dbo.DimProductP21 PR
        ON PR.ProductKey = FS.ProductKey
    LEFT OUTER JOIN BI_DWH.dbo.DimEmployeeP21 EM
        ON EM.EmployeeKey = FS.EmployeeKey
    LEFT OUTER JOIN BI_DWH.dbo.DimShipTo ST
        ON ST.ShipmentKey = FS.ShipmentKey
WHERE DD.FiscalYear >= 2018
UNION ALL
SELECT 'A1 Chemical-Reno' AS [Type],
       3 AS TypeNum,
       FC.OrderNo AS OrderId,
       FC.DateKey,
       DD.[Date],
	   LEFT(DD.[DayName],3) AS [DayName] ,
	   DD.FullDate  +' - '+ LEFT(DD.[DayName],3) AS DateDay,
       DD.MonthName,
       DD.FiscalMonth,
       DD.FiscalYear,
       CU.CustomerKey,
       CU.CustomerNum,
       CU.CustomerName,
       ST.ShipmentKey,
       ST.ShipToNum,
       ST.ShipToName,
       PR.ProductKey,
       PR.ProductNum,
       PR.ProductDescription,
	   PR.UnitOfMeasure  AS UOM,
	   PR.ProductNum +' - ' + PR.ProductDescription AS ProductNumDesc,
       PR.ItemClass,
       IC.[Description] AS ItemClassName,
       EM.EmployeeKey,
       EM.SalesRepresentativeNum,
       EM.SalesRepresentativeName,
       NULL AS BranchKey,
       FC.QtyShipped AS QuantityShipped,
       FC.UnitPrice,
       FC.WinzerCost,
       FC.FranchiseCost,
       FC.NetSales,
       GETDATE() AS DateAdded
FROM BI_DWH.dbo.FactA1ChemicalNetSales FC
    INNER JOIN BI_DWH.dbo.DimDate DD
        ON FC.DateKey = DD.DateKey
    LEFT OUTER JOIN BI_DWH.dbo.DimCustomer CU
        ON CU.CustomerKey = FC.CustomerKey
    LEFT OUTER JOIN BI_DWH.dbo.DimProduct PR
        ON PR.ProductKey = FC.ProductKey
    LEFT OUTER JOIN BI_DWH.dbo.DimEmployee EM
        ON EM.EmployeeKey = FC.EmployeeKey
    LEFT OUTER JOIN BI_DWH.dbo.DimShipTo ST
        ON ST.ShipmentKey = FC.ShipmentKey
    LEFT OUTER JOIN BI_DWH.dbo.DimItemClass IC
        ON IC.ItemClassKey = PR.ItemClassKey
WHERE DD.FiscalYear >= 2018
      AND [Type] = 'A1Reno'
UNION ALL
SELECT 'A1 Chemical-LasVegas' AS [Type],
       4 AS TypeNum,
       FC.OrderNo AS OrderId,
       FC.DateKey,
       DD.[Date],
	   LEFT(DD.[DayName],3) AS [DayName] ,
	   DD.FullDate  +' - '+ LEFT(DD.[DayName],3) AS DateDay,
       DD.MonthName,
       DD.FiscalMonth,
       DD.FiscalYear,
       CU.CustomerKey,
       CU.CustomerId,
       CU.CustomerName,
       ST.ShipmentKey,
       ST.ShipToNum,
       ST.ShipToName,
       PR.ProductKey,
       PR.ItemId,
       PR.ItemDesc,
	   NULL AS UOM,
	   PR.ItemID +' - ' + PR.ItemDesc AS ProductNumDesc,
       NULL ItemClass,
       NULL AS ItemClassName,
       EM.EmployeeKey,
       EM.SalesRepID,
       ISNULL(EM.FirstName, '') + ' ' + ISNULL(EM.Middle, '') + ' ' + ISNULL(EM.LastName, ''),
       NULL AS BranchKey,
       FC.QtyShipped AS QuantityShipped,
       FC.UnitPrice,
       FC.WinzerCost,
       FC.FranchiseCost,
       FC.NetSales,
       GETDATE() AS DateAdded
FROM BI_DWH.dbo.FactA1ChemicalNetSales FC
    INNER JOIN BI_DWH.dbo.DimDate DD
        ON FC.DateKey = DD.DateKey
    LEFT OUTER JOIN BI_DWH.dbo.DimCustomerP21 CU
        ON CU.CustomerKey = FC.CustomerKey
    LEFT OUTER JOIN BI_DWH.dbo.DimProductP21 PR
        ON PR.ProductKey = FC.ProductKey
    LEFT OUTER JOIN BI_DWH.dbo.DimEmployeeP21 EM
        ON EM.EmployeeKey = FC.EmployeeKey
    LEFT OUTER JOIN BI_DWH.dbo.DimShipTo ST
        ON ST.ShipmentKey = FC.ShipmentKey
WHERE DD.FiscalYear >= 2018
      AND [Type] = 'A1LasVegas'
UNION ALL
/* Add Costs for P21 Speedy Clean */
SELECT 'Speedy Clean' AS [Type],
       5 AS TypeNum,
       SC.InvoiceNo AS OrderId,
       SC.DateKey,
       DD.[Date],
	   LEFT(DD.[DayName],3) AS [DayName] ,
	   DD.FullDate  +' - '+ LEFT(DD.[DayName],3) AS DateDay,
       DD.MonthName,
       DD.FiscalMonth,
       DD.FiscalYear,
       CU.CustomerKey,
       CU.CustomerId,
       CU.CustomerName,
       ST.ShipmentKey,
       ST.ShipToNum,
       ST.ShipToName,
       PR.ProductKey,
       PR.ItemId,
       PR.ItemDesc,
	   NULL AS UOM, 
	   PR.ItemID +' - ' + PR.ItemDesc AS ProductNumDesc,
       NULL ItemClass,
       NULL AS ItemClassName,
       EM.EmployeeKey,
       EM.SalesRepID,
       ISNULL(EM.FirstName, '') + ' ' + ISNULL(EM.Middle, '') + ' ' + ISNULL(EM.LastName, ''),
       NULL AS BranchKey,
       SC.QtyShipped AS QuantityShipped,
       SC.UnitPrice,
       SC.WinzerCost,
       SC.FranchiseCost,
       SC.NetSales AS NetSales,
       GETDATE() AS DateAdded
FROM BI_DWH.[dbo].[FactSpeedyCleanNetSales] SC
    INNER JOIN BI_DWH.dbo.DimDate DD
        ON SC.DateKey = DD.DateKey
    LEFT OUTER JOIN BI_DWH.dbo.DimCustomerP21 CU
        ON CU.CustomerKey = SC.CustomerKey
    LEFT OUTER JOIN BI_DWH.dbo.DimProductP21 PR
        ON PR.ProductKey = SC.ProductKey
    LEFT OUTER JOIN BI_DWH.dbo.DimEmployeeP21 EM
        ON EM.EmployeeKey = SC.EmployeeKey
    LEFT OUTER JOIN BI_DWH.dbo.DimShipTo ST
        ON ST.ShipmentKey = SC.ShipmentKey
WHERE DD.FiscalYear >= 2018
UNION ALL
SELECT 'Other' AS [Type],
       6 AS TypeNum,
       NULL AS OrderId,
       DD.DateKey,
       DD.[Date],
	   LEFT(DD.[DayName],3) AS [DayName] ,
	   DD.FullDate  +' - '+ LEFT(DD.[DayName],3) AS DateDay,
       DD.MonthName,
       DD.FiscalMonth,
       DD.FiscalYear,
       NULL AS CustomerKey,
       NULL AS CustomerNum,
       NULL AS CustomerName,
       NULL AS ShipmentKey,
       NULL AS ShipToNum,
       NULL AS ShipToName,
       NULL AS ProductKey,
	   NULL AS ItemId,
	   NULL AS ItemDesc,
	   NULL AS UOM, 
	   NULL AS ProductNumDesc,
	   NULL ItemClass,
       NULL AS ItemClassName, 
       NULL AS EmployeeKey,
	   NULL AS EmployeeId,
	   NULL AS EmployeeName,
       NULL AS BranchKey,
       0 AS QuantityShipped,
       0 AS UnitPrice,
       0 AS WinzerCost,
       0 AS FranchiseCost,
       SUM([TransactionAmount]) AS NetSales,
       GETDATE() AS DateAdded
FROM BI_DWH.dbo.FactGLDailyNetSales GL
    LEFT OUTER JOIN DimDate DD
        ON GL.TransactionDateKey = DD.DateKey
WHERE GL.PostingYear >= 2018
GROUP BY DD.DateKey,
         DD.[Date],
         DD.MonthName,
         DD.FiscalMonth,
         DD.FiscalYear,
		 LEFT(DD.[DayName],3) ,
		  DD.FullDate  +' - '+ LEFT(DD.[DayName],3);
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[8] 4[14] 2[59] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 14
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'Sales_View', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'Sales_View', NULL, NULL
GO

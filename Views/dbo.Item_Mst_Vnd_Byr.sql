SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[Item_Mst_Vnd_Byr] as
SELECT
    Trim(a.ItemNumber) AS ItemNumber
  , a.ItemDescription1
  , b.VendorNumber
  , b.VendorName
  , b.BuyerInitial
  , a.ItemClass
  , Trim(Replace(c.CCDATA, '00N', '')) AS ItemClassDesc
  , a.DateAdded2 AS DateAddedRAW
  , Cast(Concat(a.Century1,Right(Concat('000000', a.DateAdded2), 6)) AS date) AS DateAdded
FROM    BI_STAGING.dbo.ItemMaster                    a
    INNER JOIN BI_STAGING.dbo.PurchasingVendorMaster b
        ON a.VendorNumber = b.VendorNumber
	LEFT JOIN Demand_Planning.dbo.Item_Class_Desc c ON
	a.ItemClass = c.Item_Class



GO

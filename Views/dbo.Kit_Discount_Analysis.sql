SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[Kit_Discount_Analysis] AS 
WITH Cte_sing_comp_kits
     AS (SELECT      
                BOM, 
                COUNT(comp_itm) AS Num_Comp
                FROM dbo.BOM_ExplodeV3
                GROUP BY 
                         dbo.BOM_ExplodeV3.BOM
                HAVING COUNT(comp_itm) = 1),
cte_get_sales
     AS (SELECT       
                a.ItemNum,sum(a.QuantityShipped) AS [QuantityShipped], sum(Quantityshipped*actualsellprice)/sum(quantityshipped) AS [ANSP]
                FROM  bi_staging.dbo.invoicelinefranchise a
                      LEFT JOIN BI_STAGING.dbo.CustomerSup b
                      ON a.CustomerNum = b.CustomerNum
					  LEFT JOIN cte_sing_comp_kits c
					  ON a.ItemNum	= c.BOM
                WHERE invoicedate >= 180801
                      AND invoicedate <= 190731
                      AND a.companynum = '1'
                      AND itemclass NOT IN
					  ('90' --Bill Only
                      , '91' --FDP
                      , '93' --VSN
                      , '82' --Promo Items
                      , '80' --HANDOUTS, BROCHURES, MANUALS  00N
                      , '89' --Specials
					  )
                      AND primarysalesmannum NOT IN('992')
                      AND primarysalesmannum NOT IN('121', '103', '152', '106', '123', '125', '150', '992')
					  AND b.SplitProfitProgramFlag <> 'Y'
					  AND c.BOM	IS NOT NULL
					  AND a.ActualSellPrice	>0
					  AND a.QuantityShipped	>0
					  GROUP BY a.ItemNum),

     cte_convert_price_hst
     AS (SELECT      
                a.Item_Number, 
                a.["Date_Added"] AS [Date_Added], 
                CONVERT(DATE, a.["Date_Added"]) AS [Date_Added_DT], 
                CAST(Replace(replace(a.[2015], '"', ''), ',', '') AS DECIMAL(28, 5)) AS [2015], 
                CAST(Replace(replace(a.[2016], '"', ''), ',', '') AS DECIMAL(28, 5)) AS [2016], 
                CAST(Replace(replace(a.[2017], '"', ''), ',', '') AS DECIMAL(28, 5)) AS [2017], 
                CAST(Replace(replace(a.[2018], '"', ''), ',', '') AS DECIMAL(28, 5)) AS [2018], 
                CAST(Replace(replace(a.[2019], '"', ''), ',', '') AS DECIMAL(28, 5)) AS [2019], 
                CAST(b.FC_2020 AS DECIMAL(28, 5)) AS [2020]
                FROM dbo.FCOST_HST a
                     LEFT JOIN dbo.Fcost_2020 b
                     ON a.Item_Number = b.Item_Number)
     SELECT       
            a.bom,
			e.QuantityShipped,
            b.[2019] AS [Kit_FC], 
            c.Comp_Itm, 
            c.Tot_Qty, 
            d.[2019] AS [COMP_EA_FC], 
            (d.[2019] * c.Tot_Qty) AS [Kit_Qty_FC], 
            (b.[2019] / (CAST(c.Tot_Qty AS DECIMAL(18, 3)) * d.[2019])) AS [Kit_DSC_FC],
			(((d.[2019] * c.Tot_Qty)*e.QuantityShipped) - (b.[2019] * e.QuantityShipped)) AS [FC_Conv_Impact],
			e.ANSP
            FROM  Cte_sing_comp_kits a
                  LEFT JOIN cte_convert_price_hst b
                  ON a.BOM = b.Item_Number
                  LEFT JOIN dbo.BOM_ExplodeV3 c
                  ON a.BOM = c.BOM
                  LEFT JOIN cte_convert_price_hst d
                  ON c.Comp_Itm = d.Item_Number
				  LEFT JOIN cte_get_sales e ON
				  a.BOM = e.ItemNum
            WHERE NULLIF(d.[2019], 0) IS NOT NULL
                  AND NULLIF(b.[2019], 0) IS NOT NULL
				  AND e.QuantityShipped	IS NOT null;
GO

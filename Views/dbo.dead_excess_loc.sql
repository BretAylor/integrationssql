SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[dead_excess_loc] AS 

SELECT a.* FROM dbo.all_inv a
LEFT JOIN dbo.dead_excess b
ON a.Item_Num = b.Item_Num
AND b.Category IN ('Dead', 'Excess')
AND b.Exempt_Flag = 'N'
GO

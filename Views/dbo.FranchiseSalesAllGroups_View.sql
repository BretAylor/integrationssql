SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[FranchiseSalesAllGroups_View]
as
SELECT *   FROM(
-- walk-on 
SELECT 'Walk-on' AS [Type],
       DD.FiscalYear,
	   FR.EmployeeKey,
       EM.SalesRepresentativeNum,
	   SalesRepresentativeName,
       SUM(FR.NetSales) AS NetSales
       
FROM dbo.DimEmployee EM INNER JOIN dbo.FactInvoiceFranchise FR ON EM.EmployeeKey = FR.EmployeeKey 
INNER JOIN FranchiseStart_View ST ON ST.EmployeeKey = EM.EmployeeKey
INNER JOIN dbo.DimDate DD ON DD.DateKey = FR.DateKey AND DD.FiscalYear =  ST.FiscalYear
WHERE TerminationDateYear = 0 
GROUP BY 
       FR.EmployeeKey,
       SalesRepresentativeName,
       EM.SalesRepresentativeNum,
	   DD.FiscalYear

UNION ALL 
-- top 20   
SELECT 'Top 20' AS [Type],
       rs.FiscalYear,
	   rs.EmployeeKey,
       rs.SalesRepresentativeNum,
	   rs.SalesRepresentativeName,
       rs.NetSales AS NetSales
FROM (
    SELECT 'Top 20' AS [Type],
       FiscalYear,
	   EmployeeKey,
       SalesRepresentativeNum,
	   SalesRepresentativeName,
       NetSales , ROW_NUMBER() 
      OVER (Partition BY FiscalYear
            ORDER BY NetSales DESC ) AS Rank
    FROM FranchiseSalesGroups_View
    ) rs WHERE Rank <= 20

UNION all
-- bottom 20
SELECT 'Bottom 20' AS [Type],
       rs.FiscalYear,
	   rs.EmployeeKey,
       rs.SalesRepresentativeNum,
	   rs.SalesRepresentativeName,
       rs.NetSales AS NetSales
FROM (
    SELECT 'Bottom 20' AS [Type],
       FiscalYear,
	   EmployeeKey,
       SalesRepresentativeNum,
	   SalesRepresentativeName,
       NetSales , ROW_NUMBER() 
      OVER (Partition BY FiscalYear
            ORDER BY NetSales asc ) AS Rank
    FROM FranchiseSalesGroups_View
    ) rs WHERE Rank <= 20) AS a
GO

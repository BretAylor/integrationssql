SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [dbo].[Open_Orders_Exploded]
AS


/*******************************************************
***** Script for SelectTopNRows command from SSMS  *****
*******************************************************/
--TRUNCATE TABLE dbo.R18_Sales_Exploded;

/********************
Get Period Sales
********************/
WITH cte_get_open_orders AS (
    SELECT
        a.CustomerClass
      , a.CustomerGLCode
      , ''                                      AS ID
      , a.CompanyNumber                         AS CompanyNum
      , a.CustomerNumber                        AS CustomerNum
      , a.OrderNumber                           AS OrderNum
      , a.OrderGenerationNumber                 AS OrdGenNum
      , a.OrderStatusCode
      , a.OrderType                             AS OrderType
      , a.HoldCode                              AS HoldCode
      , a.AllocatedCode                         AS Allocated
      , b.DropShipCode                          AS DropShip
      , b.SpecOrdCd                             AS Specials
      , a.ParentOrderNumber                     AS HistorySequenceNum
      , b.OrderSequenceNumber                   AS OrderSequenceNum
      , a.PrimarySalesman                       AS PrimarySalesmanNum
      , b.ItemNumber                            AS ItemNum
      , b.ItemDescription1
      , b.QuantityOrdered                       AS QuantityShipped
      , b.ListPrice
      , b.ActualSellPrice
      , b.CurrentAverageCost
      , (b.QuantityOrdered * b.ActualSellPrice) AS TotalLineAmount
      , b.DueDate                               AS InvoiceDate
      , b.LineItemType                          AS [Type]
      , b.ItemClass
      , a.WarehouseID                           AS ShippingLocation
      , b.QuantityOrdered
      , b.QuantityBackordered
      , a.DateAdded
      , a.EntryDate                             AS [Order_Date]
    FROM    BI_STAGING.dbo.OpenOrderHeader        a
        INNER JOIN BI_STAGING.dbo.OpenOrderDetail b
            ON a.ParentOrderNumber = b.ParentOrderNumber
               AND  a.OrderGenerationNumber = b.OrderGenerationNumber
               AND  b.LineItemType = 'I'
    WHERE
        a.CompanyNumber <> 99
        AND a.CustomerName NOT LIKE '%FASTSERV%'
        AND a.CustomerName NOT LIKE '%A1 CHEMICAL%'
)
   , CTE_get_sales AS (
    SELECT
        A.CustomerClass AS cust_class
      , A.CustomerGLCode
      , A.ID
      , A.CompanyNum
      , A.CustomerNum
      , A.OrderNum
      , A.OrdGenNum
      , A.OrderStatusCode
      , A.OrderType
      , A.HoldCode
      , A.Allocated
      , A.DropShip
      , A.Specials
      , A.HistorySequenceNum
      , A.OrderSequenceNum
      , A.PrimarySalesmanNum
      , A.ItemNum
      , A.ItemDescription1
      , A.QuantityShipped
      , A.ListPrice
      , A.ActualSellPrice
      , A.CurrentAverageCost
      , A.TotalLineAmount
      , A.InvoiceDate
      , A.[Type]
      , A.ItemClass
      , A.ShippingLocation
      , A.QuantityOrdered
      , A.QuantityBackordered
      , A.DateAdded
      , CASE
            WHEN Left(A.ItemNum, 3) = '975'
                THEN 'Custom Kit'
            WHEN C.BillofMaterialType <> ''
                THEN 'Valid - Kit/Assortment'
            ELSE 'Valid - Component'
        END             AS [Exl_Category]
      , CASE
            WHEN F.SplitProfitProgramFlag = 'Y'
                THEN 'GPS'
            WHEN E.Division = 'A1S'
                THEN 'A1'
            WHEN E.Division = 'SCL'
                THEN 'Inside Sales'
            WHEN A.PrimarySalesmanNum = '992'
                THEN 'Rep 992 - Internal'
            WHEN A.ItemClass = '90'
                THEN 'IC 90 - Bill Only'
            WHEN A.ItemClass = '91'
                THEN 'IC 91 - FDP'
            WHEN A.ItemClass = '93'
                THEN 'IC 93 - VSN'
            WHEN A.ItemClass = '82'
                THEN 'IC 82 - Promo'
            WHEN A.ItemClass = '89'
                THEN 'IC 89 - Specials'
            WHEN A.ItemClass = '80'
                THEN 'IC 80 - Handouts'
            ELSE 'Franchise'
        END             AS Sls_Ctg
      , A.Order_Date    AS [Order_Date]
    FROM    cte_get_open_orders                  A
        INNER JOIN BI_STAGING.dbo.CustomerMaster B
            ON A.CustomerNum = B.CustomerNum

        INNER JOIN BI_STAGING.dbo.ItemMaster     C
            ON A.ItemNum = C.ItemNumber

        INNER JOIN BI_STAGING.dbo.EmployeeSup    E
            ON A.PrimarySalesmanNum = E.SalesRepresentativeNum

        INNER JOIN BI_STAGING.dbo.CustomerSup    F
            ON A.CustomerNum = F.CustomerNum
)
    --cte_get_sales AS (
    --    SELECT  *
    --    FROM    dbo.R19_Sales a
    ----WHERE a.Exl_Category IN ('Valid - Kit/Assortment', 'Valid - Component')
    --)

    /********************
Get Current Franchise Cost
********************/
   , cte_get_FCOST AS (
    SELECT
        A.ItemNumber
      , Avg(A.UserCost) AS [ListPrice]
    FROM    BI_STAGING.dbo.ItemBalance A
    WHERE   SuspendCode <> 'S'
    GROUP BY
        A.ItemNumber
)
   , cte_mfg_items AS (
    SELECT  DISTINCT
            ParentItemNumber
    FROM    BI_STAGING.dbo.MaterialComponent
)
    /*******************************
Get Sales for manufactured items
*******************************/
   , cte_mfg_sales AS (
    SELECT  *
    FROM    CTE_get_sales       A
        LEFT JOIN cte_mfg_items B
            ON A.ItemNum = B.ParentItemNumber
    WHERE
        B.ParentItemNumber IS NOT NULL
        AND A.QuantityShipped <> 0
        AND A.Exl_Category NOT IN
                ('Custom Kit')
)
    /*****************************************************
Get Custom Kit Components - Specific to Customer/Order
*****************************************************/
   , cte_Cust_Kit_sales AS (
    SELECT
        A.*
      , B.parentitemnumber    AS BOM
      , B.componentitemnumber AS Comp_Itm
      , B.quantityperparent   AS Tot_Qty
    FROM    CTE_get_sales                   A
        LEFT JOIN BI_STAGING.dbo.BOMHistory B
            ON A.ItemNum = B.parentitemnumber
               AND  A.HistorySequenceNum = B.historysequencenumber
               AND  A.OrderSequenceNum = B.ordersequencenumber
    WHERE
        B.parentitemnumber IS NOT NULL
        AND A.QuantityShipped <> 0
        AND A.Exl_Category IN
                ('Custom Kit')
)
   , cte_cvt_cust_kit_sales AS (
    SELECT
        A.ID
      , A.Exl_Category
      , A.Sls_Ctg
      , A.CompanyNum
      , A.CustomerNum
      , A.OrderNum
      , A.OrdGenNum
      , A.OrderStatusCode
      , A.OrderType
      , A.HoldCode
      , A.Allocated
      , A.DropShip
      , A.Specials
      , A.HistorySequenceNum
      , A.OrderSequenceNum
      , A.PrimarySalesmanNum
      , A.Comp_Itm                      AS ItemNum
      , (A.Tot_Qty * A.QuantityShipped) AS QuantityShipped
      , CASE
            WHEN A.InvoiceDate < 190201
                THEN Cast(Replace(Replace(C.[2018], '"', ''), ',', '') AS decimal(28, 5))
            WHEN A.InvoiceDate < 200106
                THEN Cast(Replace(Replace(C.[2019], '"', ''), ',', '') AS decimal(28, 5))
            ELSE Cast(Replace(Replace(d.ListPrice, '"', ''), ',', '') AS decimal(28, 5))
        END                             AS ListPrice
      , 0                               AS ActualSellPrice
      , A.InvoiceDate
      , A.ItemClass
      , A.ShippingLocation
      , A.ItemNum                       AS MfgItem
      , A.QuantityShipped               AS KitQuantityShipped
      , A.Order_Date
    FROM    cte_Cust_Kit_sales  A
        LEFT JOIN dbo.FCOST_HST C
            ON A.Comp_Itm = C.Item_Number

        LEFT JOIN cte_get_FCOST d
            ON A.comp_itm = d.ItemNumber
)
    /***************************
Get Sales for standard items
***************************/
   , cte_not_mfg_sales AS (
    SELECT  *
    FROM    CTE_get_sales       A
        LEFT JOIN cte_mfg_items B
            ON A.ItemNum = B.ParentItemNumber
    WHERE   NullIf(B.ParentItemNumber, '') IS NULL  --AND a.Exl_Category IN ('Valid - Kit/Assortment', 'Valid - Component')
)
   , Cte_sing_comp_kits AS (
    SELECT
        BOM
      , Count(Comp_Itm) AS Num_Comp
    FROM    dbo.BOM_ExplodeV3
    GROUP BY
        dbo.BOM_ExplodeV3.BOM
    HAVING  Count(Comp_Itm) = 1
)
    /***********************************
Convert Sales for manufactured items
***********************************/
   , cte_convert_sales AS (
    SELECT
        A.ID
      , A.Exl_Category
      , A.Sls_Ctg
      , A.CompanyNum
      , A.CustomerNum
      , A.OrderNum
      , A.OrdGenNum
      , A.OrderStatusCode
      , A.OrderType
      , A.HoldCode
      , A.Allocated
      , A.DropShip
      , A.Specials
      , A.HistorySequenceNum
      , A.OrderSequenceNum
      , A.PrimarySalesmanNum
      , B.Comp_Itm                      AS ItemNum
      , (B.Tot_Qty * A.QuantityShipped) AS QuantityShipped
      , CASE
            WHEN A.InvoiceDate < 190201
                THEN Cast(Replace(Replace(C.[2018], '"', ''), ',', '') AS decimal(28, 5))
            WHEN A.InvoiceDate < 200106
                THEN Cast(Replace(Replace(C.[2019], '"', ''), ',', '') AS decimal(28, 5))
            WHEN d.Num_Comp = 1
                THEN Cast((A.ListPrice / B.Tot_Qty) AS decimal(18, 3))
            ELSE Cast(Replace(Replace(E.ListPrice, '"', ''), ',', '') AS decimal(28, 5))
        END                             AS ListPrice
      , CASE
            WHEN (B.Tot_Qty * A.QuantityShipped) = 0
                THEN 0
            WHEN d.Num_Comp = 1
                THEN Cast(A.TotalLineAmount / (B.Tot_Qty * A.QuantityShipped) AS decimal(18, 3))
            ELSE 0
        END                             AS ActualSellPrice
      , A.InvoiceDate
      , A.ItemClass
      , A.ShippingLocation
      , A.ItemNum                       AS MfgItem
      , A.QuantityShipped               AS KitQuantityShipped
      , A.Order_Date
    FROM    cte_mfg_sales            A
        LEFT JOIN dbo.BOM_ExplodeV3  B
            ON A.ItemNum = B.BOM

        LEFT JOIN dbo.FCOST_HST      C
            ON B.Comp_Itm = C.Item_Number

        LEFT JOIN Cte_sing_comp_kits d
            ON A.ItemNum = d.BOM

        LEFT JOIN cte_get_FCOST      E
            ON B.Comp_Itm = E.ItemNumber
)
   , cte_union_sales AS (
    SELECT
        A.ID
      , A.Exl_Category
      , A.Sls_Ctg
      , A.CompanyNum
      , A.CustomerNum
      , A.OrderNum
      , A.OrdGenNum
      , A.OrderStatusCode
      , A.OrderType
      , A.HoldCode
      , A.Allocated
      , A.DropShip
      , A.Specials
      , A.HistorySequenceNum
      , A.OrderSequenceNum
      , A.PrimarySalesmanNum
      , A.ItemNum
      , A.QuantityShipped
      , A.ListPrice
      , A.ActualSellPrice
      , A.InvoiceDate
      , A.ItemClass
      , A.ShippingLocation
      , '' AS MfgItem
      , 0  AS KitQuantityShipped
      , A.Order_Date
    FROM    cte_not_mfg_sales A
    UNION ALL
    SELECT
        B.ID
      , B.Exl_Category
      , B.Sls_Ctg
      , B.CompanyNum
      , B.CustomerNum
      , B.OrderNum
      , B.OrdGenNum
      , B.OrderStatusCode
      , B.OrderType
      , B.HoldCode
      , B.Allocated
      , B.DropShip
      , B.Specials
      , B.HistorySequenceNum
      , B.OrderSequenceNum
      , B.PrimarySalesmanNum
      , B.ItemNum
      , B.QuantityShipped
      , B.ListPrice
      , B.ActualSellPrice
      , B.InvoiceDate
      , B.ItemClass
      , B.ShippingLocation
      , B.MfgItem
      , B.KitQuantityShipped
      , B.Order_Date
    FROM    cte_convert_sales B
    UNION ALL
    SELECT
        C.ID
      , C.Exl_Category
      , C.Sls_Ctg
      , C.CompanyNum
      , C.CustomerNum
      , C.OrderNum
      , C.OrdGenNum
      , C.OrderStatusCode
      , C.OrderType
      , C.HoldCode
      , C.Allocated
      , C.DropShip
      , C.Specials
      , C.HistorySequenceNum
      , C.OrderSequenceNum
      , C.PrimarySalesmanNum
      , C.ItemNum
      , C.QuantityShipped
      , C.ListPrice
      , C.ActualSellPrice
      , C.InvoiceDate
      , C.ItemClass
      , C.ShippingLocation
      , C.MfgItem
      , C.KitQuantityShipped
      , C.Order_Date
    FROM    cte_cvt_cust_kit_sales C
--UNION ALL
--SELECT
--		a.ID, 
--		a.Exl_Category,
--              a.CompanyNum, 
--              a.CustomerNum, 
--              a.OrderNum, 
--              a.HistorySequenceNum, 
--              a.OrderSequenceNum, 
--              a.PrimarySalesmanNum, 
--              a.ItemNum, 
--              a.QuantityShipped, 
--              a.ListPrice, 
--              a.ActualSellPrice, 
--              --a.CurrentAverageCost, 
--              --a.TotalLineAmount, 
--              a.InvoiceDate, 
--              a.ItemClass, 
--              a.ShippingLocation, 
--              '' AS MfgItem, 
--              0 AS KitQuantityShipped
--		FROM CTE_get_sales a
--		WHERE a.Exl_Category NOT IN ('Valid - Kit/Assortment', 'Valid - Component')
)
SELECT
    a.ID
  , a.Exl_Category
  , a.Sls_Ctg
  , a.CompanyNum
  , a.CustomerNum
  , a.OrderNum
  , a.OrdGenNum
  , a.OrderStatusCode
  , a.OrderType
  , a.HoldCode
  , a.Allocated
  , a.DropShip
  , a.Specials
  , a.HistorySequenceNum
  , a.OrderSequenceNum
  , a.PrimarySalesmanNum
  , Trim(a.ItemNum)                                        AS ItemNum
  , a.QuantityShipped
  , a.ListPrice
  , a.ActualSellPrice
  , a.InvoiceDate
  , a.ItemClass
  , a.ShippingLocation
  , a.MfgItem
  , a.KitQuantityShipped
  , Right(a.Order_Date, 6)                                 AS Order_Date
  , Concat(Trim(a.ItemNum), ' ', Trim(b.ItemDescription1)) AS [Item_Desc]
FROM    cte_union_sales                 a
    LEFT JOIN BI_STAGING.dbo.ItemMaster b
        ON a.ItemNum = b.ItemNumber;;

--SELECT
--    A.ID
--  , A.Exl_Category
--  , A.Sls_Ctg
--  , A.CompanyNum
--  , A.CustomerNum
--  , A.OrderNum
--  , A.HistorySequenceNum
--  , A.OrderSequenceNum
--  , A.PrimarySalesmanNum
--  , A.ItemNum
--  , A.QuantityShipped
--  , A.ListPrice
--  , A.ActualSellPrice
--  , A.InvoiceDate
--  , A.Order_Date
--  , A.ShippingLocation
--  , A.MfgItem
--  , A.KitQuantityShipped
--  , B.New_Class
--  , B.Class_Description
--  , B.New_Sub_Class
--  , B.Subclass_Description
--  , d.Item_Class
--  , Trim(Replace(d.CCDATA, '00N', '')) AS [Item_Cls_Desc]
----INTO --#slstemp
--FROM    cte_union_sales                   A
--    LEFT JOIN dbo.Customer_Class_New_Desc B
--        ON --Get new customer class, not housed in AS400
--        A.CustomerNum = B.Customer
--    LEFT JOIN BI_STAGING.dbo.ItemMaster   C
--        ON --Get current item class instead of historical class
--        A.ItemNum = C.ItemNumber
--        AND C.SuspendCode <> 'S'
--    LEFT JOIN dbo.Item_Class_Desc         d
--        ON --Get current item class description from BI_Staging (ORCTL)
--        C.ItemClass = d.Item_Class;


GO

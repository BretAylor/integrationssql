SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[CV_contigency] AS 
WITH cte_get_sales AS (
    SELECT  *
    FROM    dbo.R18_Sales_Exploded_P21_Added
    WHERE
        Exl_Category = 'Valid - Kit/Assortment'
        AND MfgItem <> ''
        AND InvoiceDate >= 190301
        AND InvoiceDate <= 200229
        OR  Exl_Category NOT IN
                (
                    'Valid - Kit/Assortment'
                  , 'Custom Kit'
                  , 'IC 93 - VSN'
                  , 'IC 89 - Specials'
                  , 'IC 91 - FDP'
                  , 'IC 90 - Bill Only'
                )
            AND InvoiceDate >= 190301
            AND InvoiceDate <= 200229
)
   , cte_agg_sales AS (
    SELECT
        a.ItemNum
      , Count(a.ID)                          AS SO_Lines
      , Sum(a.QuantityShipped)               AS Qty_Shipped
      , Count(DISTINCT a.CustomerNum)        AS Cust_Cnt
      , Count(DISTINCT a.PrimarySalesmanNum) AS Rep_Cnt
      , b.ANSP
    FROM    cte_get_sales       a
        LEFT JOIN dbo.ANSP_2019 b
            ON a.ItemNum = b.itemnumber
    GROUP BY
        a.ItemNum
      , b.ANSP
)
   , cte_add_vendor AS (
    SELECT
        Trim(a.ItemNum)                                  AS ItemNum
      , a.SO_Lines
      , a.Qty_Shipped
      , a.Cust_Cnt
      , a.Rep_Cnt
      , a.ANSP
      , c.ItemSubclass
      , b.[2020_Push]
      , a.Qty_Shipped * b.[2020_Push]                    AS Sales
      , c.VendorNumber
	  , f.VendorName
      , d.Cost                                           AS Price
      , Cast(d.Cost / e.UMConversionFactor AS dec(8, 4)) AS Conv_Cost
      , e.MinimumOrderQuantity                           AS MOQ
      , e.LeadTime
	  , d.BuyerInitial
    FROM    cte_agg_sales                          a
        LEFT JOIN dbo.FCOST_MST                    b
            ON a.ItemNum = b.Item_Number

        LEFT JOIN BI_STAGING.dbo.ItemMaster        c
            ON a.ItemNum = c.ItemNumber

        LEFT JOIN BI_STAGING.dbo.POVendorItemPrice d
            ON c.ItemNumber = d.ItemNumber
               AND  c.VendorNumber = d.VendorNumber

        LEFT JOIN BI_STAGING.dbo.VendorItem        e
            ON c.ItemNumber = e.ItemNumber
               AND  c.VendorNumber = e.VendorNumber
		LEFT JOIN bi_staging.dbo.PurchasingVendorMaster f
			ON c.VendorNumber = f.VendorNumber
    WHERE
        c.SuspendCode = ''
        ----AND c.ItemSubclass IN
        --        (
        --            'A'
        --          , 'B'
        --        )
        --AND b.[2020_Push] IS NOT NULL
        AND c.VendorNumber <> '666'
)
   , cte_usage AS (
    SELECT
        a.ItemNumber
      , Sum(a.Monthly_Demand)         AS M_Demand
      , Count(DISTINCT a.WarehouseID) AS Whse_Cnt
      , Sum(a.Qty_OH)                 AS Qty_OH
      , Sum(a.Open_Qty)               AS Qty_OO
    FROM    dbo.Open_Ord_Amt_Demand_Tot a
    GROUP BY
        a.ItemNumber
)
SELECT
    a.*
  , b.M_Demand
  , b.Whse_Cnt
  , b.Qty_OH
  , b.Qty_OO
  , b.Qty_OH/b.M_Demand AS MOH
  , b.Qty_OO/b.M_Demand AS MOO
  , (b.Qty_OH+b.Qty_OO)/b.M_Demand AS MOH_MOO_Tot
  , (a.ANSP * b.M_Demand) * 12 AS Annual_Rev

FROM    cte_add_vendor  a
    LEFT JOIN cte_usage b
        ON a.ItemNum = b.ItemNumber
WHERE b.M_Demand > 0;

GO

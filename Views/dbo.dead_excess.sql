SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[dead_excess]
AS
WITH cte_agg_Inv AS (
    SELECT
        a.Item_Num
      , a.Item_Class
      , b.CCDATA AS Class_Name
      , Sum(a.Qty_OH)               AS Qty_OH
      , Sum(a.Net_Inv)              AS Net_Inv
      , Sum(a.Qty_OH * a.Avg_Cost)  AS Qty_OH$
      , Sum(a.Net_Inv * a.Avg_Cost) AS Net_Inv$
    FROM    dbo.all_inv               a
        LEFT JOIN dbo.Item_Class_Desc b
            ON a.Item_Class = b.Item_Class
    GROUP BY
        Item_Num
      , a.Item_Class
      , b.CCDATA
)
   , cte_item_add_date AS (
    SELECT
        a.*
      , CASE
            WHEN b.DateAdded2 IS NULL
                THEN c.date_created
            ELSE Cast(Right(Concat('000000', b.DateAdded2), 6) AS date)
        END AS Create_date
      , b.BillofMaterialType
    FROM    cte_agg_Inv                         a
        LEFT JOIN BI_STAGING.dbo.ItemMaster     b
            ON a.Item_Num = b.ItemNumber

        LEFT JOIN BI_STAGING.dbo.ViewInvMastP21 c
            ON a.Item_Num = c.item_id
)
   , CTE_agg_demand_18 AS (
    SELECT
        a.ItemNum
      , Sum(a.QuantityShipped) AS Qty_Shipped_18
    FROM    dbo.R19_Sales_Exploded_ALL a
    GROUP BY
        a.ItemNum
)
   , cte_agg_demand_12 AS (
    SELECT
        a.ItemNum
      , Sum(a.QuantityShipped) AS Qty_Shipped_12
    FROM    dbo.R19_Sales_Exploded_ALL a
    WHERE   Cast(InvoiceDate AS nvarchar(MAX)) >= DateAdd(D, 1, EOMonth(DateAdd(M, -12, EOMonth(DateAdd(M, -1, GetDate())))))
    GROUP BY
        a.ItemNum
	--ORDER BY a.ItemNum desc
)
   , cte_merge_all_data AS (
    SELECT
        a.*
      , b.Qty_Shipped_18
      , d.Qty_Shipped_12
      , CASE
            WHEN DateDiff(M, c.Create_date, DateAdd(M, -1, GetDate())) = 0
                THEN d.Qty_Shipped_12
            WHEN DateDiff(M, c.Create_date, DateAdd(M, -1, GetDate())) < 12
				THEN (d.Qty_Shipped_12 / (DateDiff(M, c.Create_date, DateAdd(M, -1, GetDate()))))
			ELSE (d.Qty_Shipped_12 / 12)
        END                                                   AS Avg_Month
      , DateDiff(M, c.Create_date, DateAdd(M, -1, GetDate())) AS Age
      , c.Create_date
      , c.BillofMaterialType
    FROM    cte_agg_Inv             a
        LEFT JOIN CTE_agg_demand_18 b
            ON a.Item_Num = b.ItemNum

        LEFT JOIN cte_item_add_date c
            ON a.Item_Num = c.Item_Num

        LEFT JOIN cte_agg_demand_12 d
            ON a.Item_Num = d.ItemNum
)
   , cte_Reserve_Calc AS (
    SELECT
        a.*
      , CASE
            WHEN a.Age < 12
                THEN 'Healthy'
			WHEN NullIf(a.Qty_Shipped_18, 0) IS NULL
                 OR a.Qty_Shipped_18 < 0
                THEN 'Dead'
            --WHEN a.Qty_Shipped_12 <= 0
            --    THEN 'Excess'
            WHEN a.Qty_OH / (a.Qty_Shipped_18 / 18) > 36
                THEN 'Excess'
            ELSE 'Healthy'
        END AS Category
      , CASE
            WHEN NullIf(a.Qty_Shipped_18, 0) IS NULL
                 OR a.Qty_Shipped_18 < 0
                THEN a.[Qty_OH$]
            WHEN a.Age < 12
                THEN 0
            --WHEN a.Qty_Shipped_12 <= 0
            --    THEN (((a.Qty_OH / (a.Qty_Shipped_18 / 18)) - 36) / (a.Qty_OH / (a.Qty_Shipped_18 / 18))) * a.[Qty_OH$]
            WHEN a.Qty_OH / (a.Qty_Shipped_18 / 18) > 36
                THEN (((a.Qty_OH / (a.Qty_Shipped_18 / 18)) - 36) / (a.Qty_OH / (a.Qty_Shipped_18 / 18))) * a.[Qty_OH$]
            ELSE 0
        END AS Excess_Amt
      , CASE
            WHEN b.UserField1 IS NULL
                THEN 'N'
            ELSE Replace(Right(Left(b.UserField1, 2), 1), ' ', 'N')
        END AS Exempt_Flag
    FROM    cte_merge_all_data                   a
        LEFT JOIN BI_STAGING.dbo.ItemSuplemental b
            ON a.Item_Num = b.ItemNumber
)
   , cte_reserve_alloc AS (
    SELECT
        a.*
      , CASE
            WHEN Sum(b.Qty_OH) IS NULL
                THEN 0
            ELSE Sum(b.Qty_OH) / a.Qty_OH
        END AS FS_Inv_Share
      , CASE
            WHEN Sum(b.Qty_OH) IS NULL
                THEN 0
            ELSE (Sum(b.Qty_OH) / a.Qty_OH) * a.Excess_Amt
        END AS FS_Reserve
    FROM    cte_Reserve_Calc  a
        LEFT JOIN dbo.all_inv b
            ON a.Item_Num = b.Item_Num
               AND  Left(b.Whse, 2) = 14
    GROUP BY
        a.Item_Num
	  , a.Item_Class
	  ,a.Class_Name
      , a.Qty_OH
      , a.Net_Inv
      , a.[Qty_OH$]
      , a.[Net_Inv$]
      , a.Qty_Shipped_18
      , a.Qty_Shipped_12
      , a.Avg_Month
      , a.Age
      , a.Create_date
      , a.BillofMaterialType
      , a.Category
      , a.Excess_Amt
      , a.Exempt_Flag
)
SELECT  Trim(a.Item_Num) AS Item_Num
      , a.Item_Class
	  , a.Class_Name
      , a.Qty_OH
      , a.Net_Inv
      , a.[Qty_OH$]
      , a.[Net_Inv$]
      , a.Qty_Shipped_18
      , a.Qty_Shipped_12
      , a.Avg_Month
      , a.Age
      , a.Create_date
      , a.BillofMaterialType
      , a.Category
      , a.Excess_Amt
      , a.Exempt_Flag
      , a.FS_Inv_Share
      , a.FS_Reserve
FROM    cte_reserve_alloc a;
--Select * FROM dbo.all_inv a WHERE a.Item_Num = ''
GO

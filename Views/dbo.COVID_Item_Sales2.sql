SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[COVID_Item_Sales2] AS(

SELECT * FROM bi_staging.dbo.InvoiceLineFranchise a WHERE a.ItemNum IN
(
 '865.1015'
, '994.9555'
, '891.687'
, '994.8607055QT'
, '994.9550'
, '994.9551'
, '994.9552'
, '994.9553'
)
)
GO

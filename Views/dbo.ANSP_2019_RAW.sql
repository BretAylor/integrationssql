SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[ANSP_2019_RAW] AS 

    WITH cte_ANSP
     AS (SELECT DISTINCT       
                a.itemnumber, 
                Sum(b.QuantityShipped) AS Tot_Units_Sold,
				Count(DISTINCT b.historysequencenum) AS Sample_Size,
                CASE
                    WHEN Sum(b.QuantityShipped) = 0 THEN 0
                    ELSE Sum((b.QuantityShipped * b.ActualSellPrice)) / Sum(b.QuantityShipped)
                END AS ANSP,
				CASE
                    WHEN Sum(b.QuantityShipped) = 0 THEN 0
                    ELSE Sum((b.QuantityShipped * b.CurrentAverageCost)) / Sum(b.QuantityShipped)
                END AS AVG_Cost,
								CASE
                    WHEN Sum(b.QuantityShipped) = 0 THEN 0
                    ELSE Sum((b.QuantityShipped * b.ListPrice)) / Sum(b.QuantityShipped)
                END AS FC_AVG
                FROM  bi_staging.dbo.itemmaster a
                      LEFT JOIN dbo.SALES_FINAL_CY2019 b
                      ON a.itemnumber = b.ItemNum
                WHERE a.SuspendCode <> 'S'
                      AND b.actualsellprice > 0
					  AND b.QuantityShipped > 0
					  AND b.Exl_Category IN ('Valid - Kit/Assortment', 'Valid - Component')
					  --AND b.New_Class <> '12'

                GROUP BY 
                         a.ItemNumber
         --HAVING SUM(b.QuantityShipped) > 0
         --ORDER BY a.ItemNumber ASC
         )
	SELECT * FROM cte_ansp
GO

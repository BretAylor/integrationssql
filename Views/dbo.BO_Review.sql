SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[BO_Review]
AS 

----PRINT @END;
--SET @BEGIN = Convert(nvarchar(8), DateAdd(D, 1, EOMonth(DateAdd(M, -12, @END))), 12);
----PRINT @BEGIN;

WITH CTE_Demand AS (
    SELECT
        a.ItemNumber
      , a.WarehouseID
      , a.AverageCost
        --, Trim(B.ShippingLocation) AS ShippingLocation
        --, d.BuyerInitial
      , Sum(B.QuantityShipped) AS Demand_Tot
    --, Sum(b.OrderQuantity - b.QuantityReceivedtodate) AS Open_Qty
    --, Sum(b.UnshippedAmount)                          AS Open_Value
    FROM    BI_STAGING.dbo.ItemBalance   a
        LEFT JOIN bi_staging.dbo.InvoiceLineFranchise B
            ON a.ItemNumber = b.ItemNum
               AND  a.WarehouseID = b.ShippingLocation
			   AND B.invoicedate >= Convert(nvarchar(8), DateAdd(D, 1, EOMonth(DateAdd(M, -13, GetDate()))), 12)
			   AND B.InvoiceDate < Convert(nvarchar(8), EOMonth(DateAdd(M, -1, GetDate())), 12)
	WHERE a.WarehouseID IN('1', '4', '3', '5')		   
    --LEFT JOIN BI_STAGING.dbo.OpenPurchaseOrderLines b
    --    ON a.ItemNum = b.ItemNumber
    --       AND a.ShippingLocation = b.WarehouseID
    --LEFT JOIN bi_staging.dbo.ItemMaster c
    --ON a.ItemNum = c.ItemNumber
    --LEFT JOIN bi_staging.dbo.PurchasingVendorMaster d
    --ON c.VendorNumber = d.VendorNumber
 
    GROUP BY
        a.ItemNumber
      , a.WarehouseID
      , a.AverageCost
--,d.BuyerInitial
--HAVING Sum(b.UnshippedAmount) <> 0;

)
   , cte_get_PO AS (
    SELECT
        a.ItemNumber
      , a.WarehouseID                                   AS Whse
      , Min(a.DueDate)									AS Next_PO_Due
	  --, d.BuyerInitial                                  AS Buyer
      , Count(DISTINCT a.PurchaseOrderNumber)           AS Open_POs
      , Sum(a.OrderQuantity - a.QuantityReceivedtodate) AS Open_Qty
      , Sum(a.UnshippedAmount)                          AS Order_Dollars
    FROM    BI_STAGING.dbo.OpenPurchaseOrderLines a
    --LEFT JOIN BI_STAGING.dbo.OpenPurchaseOrderLines a
    --    ON b.ItemNumber = a.ItemNumber
    --AND a.WarehouseID = b.ShippingLocation
    --LEFT JOIN BI_STAGING.dbo.OpenPurchaseorderHeader d
    --    ON a.PurchaseOrderNumber = d.PurchaseOrderNumber
    --LEFT JOIN BI_STAGING.dbo.ItemMaster              c
    --    ON a.ItemNumber = c.ItemNumber
    WHERE   a.LineItemStatus <> 'C'
    --AND a.VendorNumber NOT LIKE 'WTV%'
    GROUP BY
        a.ItemNumber
      , a.WarehouseID
)
   , cte_open_PO AS (
    SELECT
        a.ItemNumber
      , a.WarehouseID
      , a.AverageCost
      , a.Demand_Tot
      , b.Open_POs
      , b.Open_Qty
      , b.Order_Dollars
	  , b.Next_PO_Due
    FROM    CTE_Demand       a
        LEFT JOIN cte_get_PO b
            ON a.ItemNumber = b.ItemNumber
               AND  a.WarehouseID = b.Whse
)
   , CTE_Inventory AS (
    SELECT
        a.ItemNumber
      , a.WarehouseID
      , a.AverageCost
      , a.Open_POs
      , a.Open_Qty
      , a.Order_Dollars
      , a.Demand_Tot
	  , b.QuotedLeadTime
      , b.MinimumOnHandQuantity1 AS Min_OH
      , b.MaximumOnHandQuantity1 AS Max_OH
      , b.QuantityOnHandUM1      AS Qty_OH
      , b.QuantityAllocatedUM1   AS Aloc_Qty
      , b.QuantityonBackorderUM1 AS BO_Qty
	  , b.QuantityCommittedUnitofMeasure1 AS Com_Qty
	  , b.InventoryQuantityUnpostedUM1 AS IP_Qty
	  , a.Next_PO_Due
	  , b.DateAdded
    FROM    cte_open_PO                      a
        LEFT JOIN BI_STAGING.dbo.ItemBalance b
            ON a.ItemNumber = b.ItemNumber
               AND  a.WarehouseID = b.WarehouseID
			    AND b.WarehouseID IN ('1', '4', '3', '5')
    --GROUP BY
    --    a.ItemNumber
    --  , a.WarehouseID
    --  , a.AverageCost
    --  , a.Open_POs
    --  , a.Open_Qty
    --  , a.Order_Dollars
    --  , a.Demand_Tot
)
   , cte_final AS (
    SELECT
        a.ItemNumber
      , a.WarehouseID
	  , b.VendorNumber
	  , c.VendorName
	  , c.LeadTime
	  , c.BuyerInitial
      , a.AverageCost
	  , a.Min_OH
	  , a.Max_OH
      , IsNull(a.Open_POs, 0)      AS Open_POs
      , IsNull(a.Open_Qty, 0)      AS Open_Qty
      , IsNull(a.Order_Dollars, 0) AS Order_Dollars
      , IsNull(a.Demand_Tot, 0)    AS Demand_Tot
      , IsNull(a.Qty_OH, 0)        AS Qty_OH
	  , a.Aloc_Qty
	  , a.BO_Qty
	  , a.Com_Qty
	  --, a.Qty_OH - a.Aloc_Qty - a.BO_Qty - a.Com_Qty AS Net_OH
	  , a.Qty_OH - (a.Aloc_Qty + (a.IP_Qty*-1)) AS Net_OH
	  , a.Next_PO_Due
      , CASE
            WHEN IsNull(a.Open_Qty, 0) = 0
                THEN 0
            WHEN a.Demand_Tot = 0
                THEN '999999'
            ELSE IsNull(Round((a.Open_Qty / a.Demand_Tot) * 1.5, 2), 0)
        END                        AS Yrs_On_Ord
      , CASE
            WHEN a.Demand_Tot = 0
                THEN '999999'
            ELSE IsNull(Round(((a.Qty_OH) / a.Demand_Tot) * 1.5, 2), 0)
        END                        AS Yrs_OH
      , CASE
            WHEN a.Demand_Tot = 0
                THEN '999999'
            ELSE Round((((IsNull(a.Qty_OH, 0) + IsNull(a.Open_Qty, 0))) / a.Demand_Tot) * 1.5, 2)
        END                        AS Yrs_Covered
		,CASE
			WHEN a.Demand_Tot = 0 THEN 0
			WHEN DateDiff(M,Cast(Right(Concat('000000',b.DateAdded2), 6) AS date), GetDate()) < 18 THEN
				a.Demand_Tot/DateDiff(M,Cast(Right(Concat('000000',b.DateAdded2), 6) AS date), GetDate())
				ELSE a.Demand_Tot/12
			END AS [Monthly_Demand]
		
		 --DateDiff(M,Cast(Right(Concat('000000',b.DateAdded2), 6) AS date), GetDate()) AS [Active_Months]
    FROM    CTE_Inventory a
	LEFT JOIN BI_STAGING.dbo.ItemMaster b
	ON a.ItemNumber = b.ItemNumber
	INNER JOIN BI_STAGING.dbo.PurchasingVendorMaster c
	ON b.VendorNumber = c.VendorNumber

--WHERE a.Open_Qty > 0
)
SELECT b.ItemClassDesc, a.*, b.ItemClass
--FROM CTE_Demand A;
FROM    cte_final A
LEFT JOIN dbo.Item_Mst_Vnd_Byr b
ON a.ItemNumber = b.ItemNumber
WHERE Cast(A.Net_OH AS decimal(18,2)) < 0;
--WHERE A.ORDER_dollars >= 1000 AND
--A.Buyer = 'LR'
--AND A.Yrs_Covered >= 1;


--AND a.QuantityonBackorderUM1 > a.QuantityAllocatedUM1;l


--SELECT A.ItemNum
--     , A.ShippingLocation
--	 --, d.BuyerInitial
--     , a.QuantityShipped                          AS Demand_Tot
--     , B.PurchaseOrderNumber
--	 , B.OrderQuantity - B.QuantityReceivedtodate  AS Open_Qty
--     , B.UnshippedAmount                           AS Open_Value
--FROM dbo.Sales_Demand_Exploded                      A
--    LEFT JOIN BI_STAGING.dbo.OpenPurchaseOrderLines B
--        ON A.ItemNum = B.ItemNumber
--           AND A.ShippingLocation = B.WarehouseID
--		   WHERE B.OrderQuantity - B.QuantityReceivedtodate <> 0
--		   AND a.ItemNum = '715.8520'
--		   --GROUP BY a.ItemNum



--SELECT A.exl_category, SUM(A.quantityshipped) AS demand FROM dbo.sales_demand_exploded A 
--WHERE 
----a.Exl_Category = 'Rep 992 - Internal' AND 
--a.ItemNum = '715.8520'
--GROUP BY a.exl_category

GO

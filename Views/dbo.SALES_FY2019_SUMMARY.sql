SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SALES_FY2019_SUMMARY] AS 
	SELECT a.CustomerNum, a.Cust_Class, a.PrimarySalesmanNum, a.ItemClass, a.Exl_Category, SUM(a.QuantityShipped * a.CurrentAverageCost) AS W_COGS ,SUM(a.QuantityShipped * a.ListPrice) AS F_COGS, SUM(a.QuantityShipped * a.ActualSellPrice) AS 'SALES' FROM dbo.SALES_FINAL_FY2019 a	
	GROUP BY a.CustomerNum, a.cust_class, a.PrimarySalesmanNum, a.ItemClass, a.Exl_Category


GO

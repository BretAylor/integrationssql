SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[P21_Inv_Line]
AS
SELECT
    a.*
	, a.UnitPrice * a.QtyShipped AS [Line_Value]
  , b.InvoiceLineUnitWinzerCost
  , b.InvoiceLineUnitOvrdFranchiseCost
  , c.CustomerID
  , c.SalesRepID
  , c.InvoiceType
  , c.LocationId
  , c.BranchId
  , c.ShipToNum
  , c.CompanyNo
FROM    BI_STAGING.dbo.InvoiceLineP21           a
    INNER JOIN BI_STAGING.dbo.WzrInvoiceLineP21 b
        ON a.InvoiceNo = b.InvoiceLineInvoiceId
           AND  a.[LineNo] = b.InvoiceLineId

    INNER JOIN BI_STAGING.dbo.InvoiceP21        c
        ON a.InvoiceNo = c.InvoiceNo;

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[PA1B_Analysis] AS 
 With cte_ord_hist AS (
	SELECT ilf.CustomerNum, ilf.PrimarySalesmanNum, ilf.QuantityShipped, ilf.QuantityOrdered, ilf.ItemClass, ilf.InvoiceDate, ilf.HistorySequenceNum, ilf.OrderSequenceNum, ilf.OrderNum, ilf.CompanyNum, ilf.QuantityBackordered, ilf.ShippingLocation, ilf.Type, ilf.TotalLineAmount, ilf.ActualSellPrice, ilf.ItemNum,
		CASE WHEN	osp.PA1BEnable = 'Y' THEN 'Y' ELSE	'N' END AS 'PA1B_Flag'
	FROM bi_staging.dbo.InvoiceLineFranchise ilf
	LEFT JOIN bi_staging.dbo.orderhistorydetailsupplemental osp ON
	ilf.HistorySequenceNum	= osp.HistorySequenceNumber AND
	ilf.OrderSequenceNum	= osp.EntrySequenceNumber
	),

	CTE_Item_Sup AS (
		SELECT ITS.ItemNumber, ITS.UserField1 AS STD_PKG FROM bi_staging.dbo.ItemMaster ITS
		),
	
	CTE_Ord_Itm_Calc AS (

	SELECT a.CustomerNum, a.PrimarySalesmanNum, a.QuantityShipped, a.QuantityOrdered, a.ItemClass, a.InvoiceDate, a.HistorySequenceNum, a.OrderSequenceNum, a.OrderNum, a.CompanyNum, a.QuantityBackordered, a.ShippingLocation, a.Type, a.TotalLineAmount, a.ActualSellPrice, a.ItemNum, a.PA1B_Flag, b.STD_PKG,
		CASE
			WHEN a.QuantityOrdered < b.STD_PKG THEN 'LT_STD_PK'
			WHEN a.QuantityOrdered = b.STD_PKG THEN 'STD_PK'
			WHEN(a.QuantityOrdered / b.STD_PKG) - FLOOR(a.QuantityOrdered / b.STD_PKG) = 0 THEN 'MULT_STD_PK'
			ELSE 'GT_STD_PK'
		END AS 'QTY_COMP'	
	FROM cte_ord_hist a
	LEFT JOIN cte_item_sup b ON
	a.itemnum = b.ItemNumber
		)

	SELECT *, Concat('20',left(InvoiceDate,2)) as [Year] FROM cte_ord_itm_calc
GO

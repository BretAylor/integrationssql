SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[Covid_Sales_History_Exploded] AS

SELECT a.*
	 FROM dbo.r19_Sales_exploded a
INNER JOIN dbo.covid_items b
ON a.ItemNum = b.Item

GO

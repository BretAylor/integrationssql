SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE View [dbo].[xxShipToValidationView]
as

SELECT 
       'ShipTo submitted while package load is running. The ShipTo will be loaded shortly' AS [On Hold],
       CustomerNumber,
	   ShipToNumber,
       CreatedDate

FROM 
       ShipTo
WHERE DATEPART(MINUTE, GETDATE()) BETWEEN 0 AND 29 AND 
CreatedDate BETWEEN  CAST(CAST(CAST(GETDATE() AS date) AS varchar(10)) +' '+CAST(DATEPART(HOUR,GETDATE())  AS CHAR(2)) +':'+'00:00' AS DATETIME)  AND CAST(CAST(CAST(GETDATE() AS date) AS varchar(10)) +' '+CAST(DATEPART(HOUR,GETDATE())  AS CHAR(2)) +':'+'20:00' AS DATETIME)



UNION ALL 

SELECT 
       'ShipTo submitted while package load is running. The ShipTo will be loaded shortly' AS [On Hold],
       CustomerNumber,
	   ShipToNumber,
       CreatedDate

FROM 
ShipTo
      
WHERE DATEPART(MINUTE, GETDATE()) BETWEEN 30 AND 59 AND 
CreatedDate BETWEEN CAST(CAST(CAST(GETDATE() AS date) AS varchar(10)) +' '+ CAST(DATEPART(HOUR,GETDATE())  AS CHAR(2)) +':'+'30:00'AS DATETIME)  AND 
CAST(CAST(CAST(GETDATE() AS date) AS varchar(10)) +' '+ CAST(DATEPART(HOUR,GETDATE())  AS CHAR(2)) +':'+'50:00'AS DATETIME) 

GO

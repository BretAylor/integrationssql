SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[FCOST_MST_%] as 

SELECT *, 
CASE
	WHEN a.[2019] = 0 THEN 0
	Else Cast(Round((a.[2020_Inf]/a.[2019])-1,4)*100 AS decimal(6,2))
End AS [2020_INF_Pct],
CASE
	WHEN a.[2020_INF] = 0 THEN 0
	Else Cast(Round((a.[2020_Push]/a.[2020_INF])-1,4)*100 AS decimal(6,2))
End AS [2020_Push_Pct],
CASE
	WHEN a.[2019] = 0 THEN 0
	Else Cast(Round((a.[2020_Push]/a.[2019])-1,4)*100 AS decimal(6,2))
End AS [2020_Tot_Pct]
FROM dbo.fcost_mst a
GO

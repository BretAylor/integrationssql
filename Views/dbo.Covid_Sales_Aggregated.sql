SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[Covid_Sales_Aggregated]
AS
SELECT
    a.Order_Type
  , a.Sls_Ctg
  , Trim(a.ItemNum) AS ItemNum
  , a.Item_Desc
  , a.Order_Date
  , a.InvoiceDate
  , Sum(a.QuantityShipped)                     AS QuantityShipped
  , Sum(a.QuantityShipped * a.ActualSellPrice) AS Tot_Sls_Cus
  , Sum(a.QuantityShipped * a.ListPrice)       AS Tot_Sls_Frch
FROM    dbo.R19_Exploded_Plus_Open a
    INNER JOIN dbo.COVID_Items     b
        ON a.ItemNum = b.Item
GROUP BY
    a.Order_Type
  , a.Sls_Ctg
  , a.ItemNum
  , a.Order_Date
  , a.InvoiceDate
  , a.Item_Desc;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[Split](@String VARCHAR(8000), @ChunkLen int)
returns @temptable
TABLE (nameIndex int identity(1,1),items VARCHAR(8000))
as
begin
	declare @idx INT
    DECLARE @Len int
	DECLARE @slice VARCHAR(8000)
    
	SET @Len = (SELECT LEN(@string))
	select @idx = 1

	if len(@String)<1 or @String
	is null return

	while @idx <= @Len
	BEGIN
    SET @Slice = SUBSTRING(@string,@idx,@chunklen)

	if(len(@slice)>0)
	insert
	into @temptable(Items) values(@slice)

	SET @IDX = @IDX + @Chunklen

	end

return
end
GO

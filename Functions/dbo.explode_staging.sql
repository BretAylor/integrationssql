SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





Create function [dbo].[explode_staging] (@par_id varchar(27), @qty decimal(38,5))
	Returns Table
	as
		Return (
			WITH EXPL as 
			(
			select par.parentitemnumber, par.componentitemnumber, cast(par.quantityperparent * @qty as decimal(38,2)) as Qty, par.UnitofMeasure as UOM
			from [bi_staging].[dbo].[materialcomponent] par
			where par.ParentItemNumber = @par_id


			union all


			select child.parentitemnumber, child.componentitemnumber, Cast(PARENT.Qty * Cast(child.quantityperparent as decimal(38,2)) as decimal(38,2)) as Qty, child.UnitofMeasure as UOM

			from EXPL PARENT, [bi_staging].[dbo].[materialcomponent] child

			where PARENT.componentitemnumber = child.parentitemnumber
			)
			Select @par_id as BOM, b.componentitemnumber as Comp_Itm, sum(b.Qty) as Tot_Qty, b.UOM ,(Select STUFF((Select ', ' + a.parentitemnumber from EXPL a where a.componentitemnumber = b.componentitemnumber FOR XML PATH('')), 1, 2, '')) as Par_List 
			From EXPL b where componentitemnumber not in(select parentitemnumber from EXPL) 
			Group By componentitemnumber, UOM
			)

GO

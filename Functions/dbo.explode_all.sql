SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE function [dbo].[explode_all] (@par_id varchar(27), @qty decimal(38,5))
	RETURNS TABLE
	as
		RETURN (
			WITH EXPL as 
			(
			SELECT par.BCPRIT, par.BCCMIT, cast(par.BCQTPR * @qty as decimal(38,2)) as BCQTPR, par.BCCMUM
			FROM dbo.BOMCO par
			WHERE par.BCPRIT = @par_id

			UNION ALL

			SELECT child.BCPRIT, child.BCCMIT, Cast(PARENT.BCQTPR * Cast(child.BCQTPR as decimal(38,2)) as decimal(38,2)) as BCQTPR, child.BCCMUM
			FROM EXPL PARENT, dbo.BOMCO child
			WHERE PARENT.BCCMIT = child.BCPRIT
			)

			SELECT @par_id as BOM, bccmit as Comp_Itm, sum(bcqtpr) as Tot_Qty, 
				(SELECT STUFF((SELECT ', ' + bcprit FROM EXPL a WHERE a.bccmit = b.bccmit FOR XML PATH('')), 1, 2, '')) as Par_List 
			FROM EXPL b WHERE BCCMIT not in(SELECT BCPRIT FROM EXPL) 
			GROUP BY bccmit, bccmum
			)

GO

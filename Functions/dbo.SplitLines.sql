SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create FUNCTION [dbo].[SplitLines]
(
    @pString    VARCHAR(max),
    @pLineLen   INT
)



RETURNS TABLE
   WITH SCHEMABINDING
AS  



RETURN
WITH
      E1(N) AS ( --=== Create Ten 1's
                 SELECT 1 UNION ALL SELECT 1 UNION ALL
                 SELECT 1 UNION ALL SELECT 1 UNION ALL
                 SELECT 1 UNION ALL SELECT 1 UNION ALL
                 SELECT 1 UNION ALL SELECT 1 UNION ALL
                 SELECT 1 UNION ALL SELECT 1 --10
               ),
      E2(N) AS (SELECT 1 FROM E1 a, E1 b),   --100
      E4(N) AS (SELECT 1 FROM E2 a, E2 b),   --10,000
cteTally(N) AS (SELECT ROW_NUMBER() OVER (ORDER BY (SELECT N)) FROM E4),
lines AS (
  SELECT TOP 1
1 as LineNumber,
         ltrim(rtrim(SUBSTRING(replace(@pString, ' ','^'), 1, N))) as Line,
         N + 1 as start
  FROM cteTally
  WHERE N <= DATALENGTH(replace(@pString, ' ','^')) + 1
    AND N <= @pLineLen --+ 1
   -- AND SUBSTRING(replace(@pString, ' ','^') + @pDelim, N, 1) = @pDelim
  ORDER BY N DESC
UNION ALL
  SELECT LineNumber, Line, start
  FROM (
    SELECT LineNumber + 1 as LineNumber,
           ltrim(rtrim(SUBSTRING(replace(@pString, ' ','^'), start, N))) as Line,
           start + N  as start, -- + 1
           ROW_NUMBER() OVER (ORDER BY N DESC) as r
    FROM cteTally, lines
    WHERE N <= DATALENGTH(replace(@pString, ' ','^')) + 1 - start
      AND N <= @pLineLen
   --   AND SUBSTRING(replace(@pString, ' ','^') + @pDelim, start + N, 1) = @pDelim
  ) A
  WHERE r = 1
)
SELECT LineNumber, replace(Line,'^', ' ' ) as Line
FROM lines


GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE function [dbo].[explode] (@par_id varchar(27), @qty decimal(38,5))
	Returns Table
	as
		Return (
			WITH EXPL as 
			(
			select par.BCPRIT, par.BCCMIT, cast(par.BCQTPR * @qty as decimal(38,2)) as BCQTPR, par.BCCMUM
			from dbo.BOMCO par
			where par.BCPRIT = @par_id


			union all


			select child.BCPRIT, child.BCCMIT, Cast(PARENT.BCQTPR * Cast(child.BCQTPR as decimal(38,2)) as decimal(38,2)) as BCQTPR, child.BCCMUM

			from EXPL PARENT, dbo.BOMCO child

			where PARENT.BCCMIT = child.BCPRIT
			)
			Select @par_id as BOM, bccmit as Comp_Itm, sum(bcqtpr) as Tot_Qty, (Select STUFF((Select ', ' + bcprit from EXPL a where a.bccmit = b.bccmit FOR XML PATH('')), 1, 2, '')) as Par_List 
			From EXPL b where BCCMIT not in(select BCPRIT from EXPL) 
			Group By bccmit, bccmum
			)

GO

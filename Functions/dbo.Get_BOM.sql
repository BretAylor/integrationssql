SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[Get_BOM] (@BOM varchar(27), @Qty decimal(5,0))
RETURNS table AS 
	RETURN(
		SELECT
             Trim(a.Comp_Itm) AS Comp_Itm
             , (a.Tot_Qty * @Qty) AS Order_Qty
			 FROM Demand_Planning.dbo.BOM_ExplodeV3 a WHERE a.BOM = @BOM
		)
GO

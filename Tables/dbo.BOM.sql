CREATE TABLE [dbo].[BOM]
(
[ParentItemNum] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentItemDescription] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuantityPerParent] [numeric] (8, 0) NULL,
[ComponentItemNum] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ComponentItemDescription] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReadId] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_BOM_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_BOM_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_BOM_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_BOM_ModifiedBy] DEFAULT (suser_name()),
[IsNew] [bit] NULL CONSTRAINT [DF_BOM_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO

CREATE TABLE [StagePA].[ExtractSalesOrderExclude]
(
[Order] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrderMessage] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO

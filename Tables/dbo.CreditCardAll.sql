CREATE TABLE [dbo].[CreditCardAll]
(
[Guid] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerNumber] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditCardNum] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExpirationDate] [int] NULL,
[CreditCardHolder] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

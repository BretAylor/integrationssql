CREATE TABLE [dbo].[DimWarehouse]
(
[WarehouseKey] [int] NOT NULL IDENTITY(1, 1),
[WarehouseID] [int] NULL,
[WarehouseName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

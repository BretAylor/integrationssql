CREATE TABLE [dbo].[SalesOrderLine]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[PepperiId] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LineNbr] [decimal] (5, 0) NOT NULL,
[WinzerProductNbr] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrandedProductNbr] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceWarehouse] [int] NULL,
[OrderQuantity] [decimal] (7, 0) NULL,
[OrderPrice] [decimal] (11, 4) NULL,
[Pa1bFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbrOfBags] [decimal] (3, 0) NULL,
[LabelCode] [varchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LabelQty] [decimal] (7, 0) NULL,
[Comment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisplayComment] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RowStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoChargeReasonCode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QtyBackordered] [decimal] (7, 0) NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_SalesOrderLine_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_SalesOrderLine_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL CONSTRAINT [DF_SalesOrderLine_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_SalesOrderLine_ModifiedBy] DEFAULT (suser_name()),
[ErpSyncDate] [datetime] NULL,
[ErpSyncBy] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FdpSupplier] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FdpPoNumber] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderCost] [decimal] (11, 4) NULL,
[FdpReference] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpecialFrCostId] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerPartNumber] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentItemNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalFranchisePrice] [decimal] (11, 4) NULL,
[LandedCost] [decimal] (11, 4) NULL,
[UnitOfMeasure] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerContract] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemDescription] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AS400OrderNbr] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgSalesOrderLineHist]
ON [dbo].[SalesOrderLine]
AFTER UPDATE, INSERT, DELETE
AS
BEGIN
    SET NOCOUNT ON;
INSERT INTO [Hist].[SalesOrderLine]
           ([PepperiId]
           ,[LineNbr]
           ,[WinzerProductNbr]
           ,[BrandedProductNbr]
           ,[SourceWarehouse]
           ,[OrderQuantity]
           ,[OrderPrice]
           ,[Pa1bFlag]
           ,[NbrOfBags]
           ,[LabelCode]
           ,[LabelQty]
           ,[Comment]
           ,[DisplayComment]
           ,[RowStatus]
           ,[NoChargeReasonCode]
           ,[QtyBackordered]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[ErpSyncDate]
           ,[ErpSyncBy]
           ,[FdpSupplier]
           ,[FdpPoNumber]
           ,[OrderCost]
           ,[FdpReference]
           ,[SpecialFrCostId]
           ,[CustomerPartNumber]
           ,[ParentItemNumber]
           ,[OriginalFranchisePrice]
           ,[LandedCost]
           ,[UnitOfMeasure]
           ,[CustomerContract]
           ,[ItemDescription])
    SELECT  d.[PepperiId]
           ,d.[LineNbr]
           ,d.[WinzerProductNbr]
           ,d.[BrandedProductNbr]
           ,d.[SourceWarehouse]
           ,d.[OrderQuantity]
           ,d.[OrderPrice]
           ,d.[Pa1bFlag]
           ,d.[NbrOfBags]
           ,d.[LabelCode]
           ,d.[LabelQty]
           ,d.[Comment]
           ,d.[DisplayComment]
           ,d.[RowStatus]
           ,d.[NoChargeReasonCode]
           ,d.[QtyBackordered]
           ,d.[CreatedDate]
           ,d.[CreatedBy]
           ,d.[ModifiedDate]
           ,d.[ModifiedBy]
           ,d.[ErpSyncDate]
           ,d.[ErpSyncBy]
           ,d.[FdpSupplier]
           ,d.[FdpPoNumber]
           ,d.[OrderCost]
           ,d.[FdpReference]
           ,d.[SpecialFrCostId]
           ,d.[CustomerPartNumber]
           ,d.[ParentItemNumber]
           ,d.[OriginalFranchisePrice]
           ,d.[LandedCost]
           ,d.[UnitOfMeasure]
           ,d.[CustomerContract]
           ,d.[ItemDescription]
    FROM
        deleted d;
END
GO
ALTER TABLE [dbo].[SalesOrderLine] ADD CONSTRAINT [PK_SalesOrderLine] PRIMARY KEY CLUSTERED  ([PepperiId], [LineNbr]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20200507-165011] ON [dbo].[SalesOrderLine] ([CreatedDate]) INCLUDE ([PepperiId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20200507-164820] ON [dbo].[SalesOrderLine] ([ErpSyncDate]) INCLUDE ([PepperiId]) ON [PRIMARY]
GO

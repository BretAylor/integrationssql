CREATE TABLE [dbo].[FCOST_ITM_CLS_YoY_IMPACT]
(
[Item_class] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Class_Desc] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sls_2015_Equiv] [decimal] (38, 6) NULL,
[Sls_2016] [decimal] (38, 6) NULL,
[Pct_Inc_2016] [decimal] (18, 5) NULL,
[Sls_2016_Equiv] [decimal] (38, 6) NULL,
[Sls_2017] [decimal] (38, 6) NULL,
[Pct_Inc_2017] [decimal] (18, 5) NULL,
[Sls_2017_Equiv] [decimal] (38, 6) NULL,
[Sls_2018] [decimal] (38, 6) NULL,
[Pct_Inc_2018] [decimal] (18, 5) NULL,
[Sls_2018_Equiv] [decimal] (38, 6) NULL,
[Sls_2019] [decimal] (38, 6) NULL,
[Pct_Inc_2019] [decimal] (18, 5) NULL,
[Sls_2015_5YR_Equiv] [decimal] (38, 6) NULL,
[Sls_2019_5YR] [decimal] (38, 6) NULL,
[Pct_Inc_5YR] [decimal] (18, 5) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[PaymentTerm]
(
[Code] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_PaymentTerm_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_PaymentTerm_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL CONSTRAINT [DF_PaymentTerm_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_PaymentTerm_ModifiedBy] DEFAULT (suser_name())
) ON [PRIMARY]
GO

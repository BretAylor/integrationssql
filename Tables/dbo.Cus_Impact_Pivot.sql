CREATE TABLE [dbo].[Cus_Impact_Pivot]
(
[subClass_Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[%_Tot_Sls] [decimal] (28, 2) NULL,
[2019] [decimal] (28, 2) NULL,
[2020_INF] [decimal] (28, 2) NULL,
[2020_Push] [decimal] (28, 2) NULL,
[2020_Total] [decimal] (29, 2) NULL,
[1] [decimal] (38, 4) NULL,
[2] [decimal] (38, 4) NULL,
[3] [decimal] (38, 4) NULL,
[4] [decimal] (38, 4) NULL,
[5] [decimal] (38, 4) NULL,
[6] [decimal] (38, 4) NULL,
[7] [decimal] (38, 4) NULL,
[E] [decimal] (38, 4) NULL,
[2020_Tot_$] [decimal] (38, 6) NULL
) ON [PRIMARY]
GO

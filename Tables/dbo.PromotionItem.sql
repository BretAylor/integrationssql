CREATE TABLE [dbo].[PromotionItem]
(
[PromotionCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ItemNumber] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ItemPrice] [decimal] (18, 4) NOT NULL,
[PromotionStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreationTimestamp] [datetime2] NOT NULL,
[DateLastMaintained] [datetime2] NOT NULL,
[Deleted] [bit] NOT NULL CONSTRAINT [DF_PromotionItem_Deleted] DEFAULT ((0)),
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_PromotionItem_CreatedDate] DEFAULT (getdate()),
[ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_PromotionItem_ModifiedDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PromotionItem] ADD CONSTRAINT [PK_PromotionItem] PRIMARY KEY CLUSTERED  ([PromotionCode], [ItemNumber]) ON [PRIMARY]
GO

CREATE TABLE [StageAP].[VsnCatalogDetails]
(
[CatalogId] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CatalogPartNumber] [nvarchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PartDescription] [nvarchar] (62) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitOfMeasure] [nchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FranchiseCost] [decimal] (10, 4) NULL,
[StandardPackageQty] [decimal] (5, 0) NULL,
[EnforceBaggingQty] [bit] NULL,
[PackageAllInOneBag] [bit] NULL,
[KitType] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecommendedPrice] [decimal] (11, 4) NULL,
[AssignedWinzerProductNumber] [nvarchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FreightType] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FreightAmount] [decimal] (7, 2) NULL,
[WeightInPounds] [decimal] (11, 4) NULL,
[LandedCostSplitProfit] [decimal] (15, 4) NULL,
[FastservItemNumber] [nvarchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

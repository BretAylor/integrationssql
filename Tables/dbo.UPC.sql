CREATE TABLE [dbo].[UPC]
(
[UPITNO] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UPBRIT] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UPUNMS] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UPVNNO] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UPITD1] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UPITD2] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UPUS30] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO

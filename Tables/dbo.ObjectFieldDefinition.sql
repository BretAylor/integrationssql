CREATE TABLE [dbo].[ObjectFieldDefinition]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[OrderID] [int] NULL,
[ObjectType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObjectName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Section] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Field] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Definition] [nvarchar] (1500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ObjectFieldDefinition] ADD CONSTRAINT [PK_ObjectFieldDefinition] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO

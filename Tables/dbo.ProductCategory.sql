CREATE TABLE [dbo].[ProductCategory]
(
[Id] [int] NULL,
[Level1] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level1Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level2] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level2Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level3] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level3Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WebsiteId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [decimal] (18, 0) NULL,
[SmallImagePath] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [date] NULL CONSTRAINT [DF_ProductCategory_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ProductCategory_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [date] NULL CONSTRAINT [DF_ProductCategory_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ProductCategory_ModifiedBy] DEFAULT (suser_name())
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[FactFastServNetSales]
(
[DateKey] [int] NOT NULL,
[ProductKey] [int] NOT NULL,
[EmployeeKey] [int] NOT NULL,
[CustomerKey] [int] NOT NULL,
[ShipmentKey] [int] NULL,
[BranchKey] [int] NOT NULL,
[InvoiceNo] [int] NOT NULL,
[OrderNo] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LineNo] [int] NOT NULL,
[UnitPrice] [numeric] (19, 9) NULL,
[QtyShipped] [numeric] (19, 9) NULL,
[WinzerCost] [numeric] (19, 9) NULL,
[FranchiseCost] [numeric] (19, 9) NULL,
[NetSales] [numeric] (19, 9) NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactFastServNetSales] ADD CONSTRAINT [PK_FactFastServNetSales] PRIMARY KEY CLUSTERED  ([DateKey], [ProductKey], [EmployeeKey], [CustomerKey], [BranchKey], [InvoiceNo], [OrderNo], [LineNo]) ON [PRIMARY]
GO

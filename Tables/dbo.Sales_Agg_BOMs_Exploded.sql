CREATE TABLE [dbo].[Sales_Agg_BOMs_Exploded]
(
[Itm] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tot_Qty] [decimal] (38, 3) NULL,
[MMYY] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Pct_Sls_Z] [decimal] (38, 6) NULL
) ON [PRIMARY]
GO

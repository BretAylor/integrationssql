CREATE TABLE [dbo].[ProductCustomPropertyUpdates]
(
[Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErpNbr] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentTable] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_ProductCustomPropertyUpdates_CreatedOn] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ProductCustomPropertyUpdates_CreatedBy] DEFAULT (suser_name()),
[ModifiedOn] [datetime] NULL,
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsNew] [bit] NULL,
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO

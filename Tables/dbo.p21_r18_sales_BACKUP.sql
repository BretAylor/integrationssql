CREATE TABLE [dbo].[p21_r18_sales_BACKUP]
(
[ID] [int] NOT NULL,
[CompanyNum] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[customernum] [int] NOT NULL,
[OrderNum] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HistorySequenceNum] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrderSequenceNum] [numeric] (19, 0) NOT NULL,
[primarysalesmannum] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemNum] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemDescription1] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[QuantityShipped] [numeric] (19, 9) NOT NULL,
[listprice] [int] NOT NULL,
[actualsellprice] [numeric] (19, 9) NOT NULL,
[currentaveragecost] [numeric] (19, 4) NOT NULL,
[totallineamount] [numeric] (19, 4) NOT NULL,
[Invoice_Date] [datetime] NOT NULL,
[TYPE] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[itemclass] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ShippingLocation] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuantityOrdered] [numeric] (19, 9) NOT NULL,
[QuantityBackordered] [int] NOT NULL,
[DateAdded] [datetime] NOT NULL
) ON [PRIMARY]
GO

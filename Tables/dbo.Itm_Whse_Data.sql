CREATE TABLE [dbo].[Itm_Whse_Data]
(
[IBITNO] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IBWHID] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IBPAQT] [decimal] (11, 3) NOT NULL,
[IBMXPQ] [decimal] (11, 3) NOT NULL,
[IBCSFL] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IBCSDS] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IBCSQT] [numeric] (6, 0) NOT NULL,
[IBOVLC] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IBMPAQ] [decimal] (11, 3) NOT NULL,
[IBPAMS] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IBQTPP] [decimal] (11, 3) NOT NULL,
[IBPLID] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IBIMCD] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IBCOCD] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IBCOCC] [numeric] (2, 0) NOT NULL,
[IBCODT] [numeric] (6, 0) NOT NULL,
[IBLCCL] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IBITLB] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IBLPAC] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IBBXCD] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IBOPSL] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO

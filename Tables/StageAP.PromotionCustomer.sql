CREATE TABLE [StageAP].[PromotionCustomer]
(
[PromotionCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomerNumber] [decimal] (10, 0) NOT NULL,
[PromotionStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreationTimestamp] [datetime2] NOT NULL,
[DateLastMaintained] [datetime2] NOT NULL
) ON [PRIMARY]
GO

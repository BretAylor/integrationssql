CREATE TABLE [dbo].[VsnCatalogHeaderUpdates]
(
[CatalogId] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CatalogName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemPrefix] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnassignedItemSuffix] [varchar] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemClass] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PriceClass] [decimal] (3, 0) NULL,
[OrderMinFreightCostValue] [decimal] (9, 2) NULL,
[AllowBelowMinOrder] [bit] NULL,
[SurchargeBelowMin] [decimal] (9, 2) NULL,
[SurchargeBelowMinPercent] [decimal] (5, 2) NULL,
[IsActive] [bit] NULL,
[IsDownload] [bit] NULL,
[DateAvailable] [date] NULL,
[FreightType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FreightAmount] [decimal] (7, 2) NULL,
[Comment] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VsnVendorWebsite] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UsesReservedItemNumber] [bit] NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_VsnCatalogHeaderUpdates_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_VsnCatalogHeaderUpdates_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL,
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsNew] [bit] NULL,
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[R12_Sales_Temp]
(
[ItemNum] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Qty_Shipped] [decimal] (38, 9) NULL,
[itemclass] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Inv_YYMM] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

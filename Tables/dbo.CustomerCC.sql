CREATE TABLE [dbo].[CustomerCC]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Guid] [uniqueidentifier] NOT NULL,
[CCDescription] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CCType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CCNumber] [varbinary] (128) NOT NULL,
[CVVNumber] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastFourDigits] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExpirationDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomerNumber] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FirstName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MiddleName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ZipCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Note] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KeepOnFile] [bit] NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_CustomerCC_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_CustomerCC_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL,
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomerCC] ADD CONSTRAINT [PK_CustomerCC_1] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO

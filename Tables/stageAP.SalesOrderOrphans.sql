CREATE TABLE [stageAP].[SalesOrderOrphans]
(
[ORDNBR] [bigint] NOT NULL,
[OrdNbrMessage] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_SalesOrderOrphans_CreatedDate] DEFAULT (getdate())
) ON [PRIMARY]
GO

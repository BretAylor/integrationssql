CREATE TABLE [dbo].[fcost_mst_120919]
(
[Item_Number] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[2019] [decimal] (28, 5) NULL,
[2020_INF] [decimal] (28, 5) NULL,
[2020_Push] [decimal] (28, 5) NULL,
[Category] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INC_BKT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FLAGS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Chg_Notes] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Changed] [datetime] NOT NULL
) ON [PRIMARY]
GO

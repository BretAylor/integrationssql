CREATE TABLE [dbo].[FranchiseRemunerationReport]
(
[Repno] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Processdt] [numeric] (8, 0) NOT NULL,
[Lineno] [numeric] (5, 0) NOT NULL,
[Reportln] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_FranchiseRemunerationReport_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_FranchiseRemunerationReport_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL CONSTRAINT [DF_FranchiseRemunerationReport_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_FranchiseRemunerationReport_ModifiedBy] DEFAULT (suser_name())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FranchiseRemunerationReport] ADD CONSTRAINT [PK_FranchiseRemunerationReport] PRIMARY KEY CLUSTERED  ([Repno], [Processdt], [Lineno]) ON [PRIMARY]
GO

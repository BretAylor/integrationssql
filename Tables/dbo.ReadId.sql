CREATE TABLE [dbo].[Readid]
(
[ReadID] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SalesRepNumber] [decimal] (18, 0) NOT NULL,
[SalesRepName] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_ReadIdWorking_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ReadIdWorking_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_ReadIdWorking_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ReadIdWorking_ModifiedBy] DEFAULT (suser_name()),
[Deleted] [bit] NOT NULL CONSTRAINT [DF_ReadIdWorking_Deleted] DEFAULT ((0))
) ON [PRIMARY]
GO

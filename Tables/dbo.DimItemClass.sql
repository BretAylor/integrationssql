CREATE TABLE [dbo].[DimItemClass]
(
[ItemClassKey] [int] NOT NULL IDENTITY(1, 1),
[ItemClassCode] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimItemClass] ADD CONSTRAINT [PK_DimItemClass] PRIMARY KEY CLUSTERED  ([ItemClassKey]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ShipToUpdates]
(
[PepperiShipToId] [decimal] (18, 0) NULL,
[CustomerNumber] [decimal] (18, 0) NOT NULL,
[ShipToNumber] [decimal] (3, 0) NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Contact] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZipCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [decimal] (10, 0) NULL,
[PhoneExtension] [decimal] (14, 0) NULL,
[IsDefaultShipTo] [bit] NULL,
[DefaultWarehouse] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultShipVia] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultFreightChargeCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxCode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultPONumber] [varchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_ShipToUpdates2_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ShipToUpdates2_CreatedBy] DEFAULT (suser_name()),
[IsNew] [bit] NULL CONSTRAINT [DF_ShipToUpdates2_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO

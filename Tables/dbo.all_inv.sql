CREATE TABLE [dbo].[all_inv]
(
[Item_Num] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Whse] [numeric] (19, 0) NOT NULL,
[Qty_OH] [numeric] (34, 9) NULL,
[Avg_Cost] [numeric] (32, 9) NULL,
[Net_Inv] [numeric] (38, 9) NULL,
[Item_Class] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

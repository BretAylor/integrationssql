CREATE TABLE [dbo].[R18_Sales_Exploded_P21_Added_No_Kits]
(
[ID] [int] NOT NULL,
[Exl_Category] [varchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CompanyNum] [numeric] (2, 0) NULL,
[CustomerNum] [numeric] (10, 0) NULL,
[OrderNum] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HistorySequenceNum] [numeric] (7, 0) NOT NULL,
[OrderSequenceNum] [numeric] (19, 0) NOT NULL,
[PrimarySalesmanNum] [decimal] (28, 0) NULL,
[ItemNum] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuantityShipped] [decimal] (38, 6) NULL,
[InvoiceDate] [nvarchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemClass] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemSubclass] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShippingLocation] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MfgItem] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KitQuantityShipped] [decimal] (34, 9) NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_R18_Sales_Exploded_P21_Added_No_Kits] ON [dbo].[R18_Sales_Exploded_P21_Added_No_Kits] ([ID], [HistorySequenceNum], [OrderSequenceNum], [ShippingLocation], [ItemNum], [InvoiceDate], [Exl_Category], [OrderNum], [ItemClass]) ON [PRIMARY]
GO

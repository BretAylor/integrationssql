CREATE TABLE [dbo].[SalesOrderInclusionList]
(
[PepperiOrderNbr] [bigint] NOT NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_SalesOrderInclusionList_CreatedDate] DEFAULT (getdate())
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CustomerUpdatesOld]
(
[PadDescription] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerNumber] [decimal] (7, 0) NOT NULL,
[PepperiCustomerNumber] [decimal] (18, 0) NULL,
[CustomerName] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressLine1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressLine2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZipCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SalesRepNumber] [decimal] (18, 0) NULL,
[PurchasingContact] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PurchasingPhoneNumber] [decimal] (18, 0) NULL,
[PurchasingPhoneExtension] [decimal] (18, 0) NULL,
[PurchasingFax] [decimal] (18, 0) NULL,
[PurchasingContactEmail] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerClass] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ARAmountDue] [decimal] (11, 4) NULL,
[ARAmountCurrent] [decimal] (11, 4) NULL,
[AgeAmount030] [decimal] (11, 4) NULL,
[AgeAmount060] [decimal] (11, 4) NULL,
[AgeAmount090] [decimal] (11, 4) NULL,
[AgeAmount120] [decimal] (11, 4) NULL,
[TotalPayDays] [decimal] (18, 0) NULL,
[LastPaymentAmount] [decimal] (11, 4) NULL,
[LastPaymentDate] [date] NULL,
[LastPurchaseDate] [date] NULL,
[FirstSaleDate] [date] NULL,
[MtdSalesDollars] [decimal] (11, 4) NULL,
[YtdSalesDollars] [decimal] (11, 4) NULL,
[LastYearFiscalSales] [decimal] (11, 4) NULL,
[CreditLimit] [decimal] (11, 4) NULL,
[MtdNumberOrders] [decimal] (18, 0) NULL,
[YtdNumberOrders] [decimal] (18, 0) NULL,
[NumberOrdersLastFiscal] [decimal] (18, 0) NULL,
[TaxCode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcceptBackorders] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultShipVia] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TermsCode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultPriceLevel] [decimal] (13, 0) NULL,
[ChargeFreightCode] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultWarehouse] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultOrderComment] [decimal] (18, 0) NULL,
[PORequiredFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultShipTo] [decimal] (18, 0) NULL,
[PepperiDefaultShipTo] [decimal] (18, 0) NULL,
[ContractCode] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuggestedContract] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[APContact] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[APPhoneNumber] [decimal] (18, 0) NULL,
[APPhoneExtension] [decimal] (18, 0) NULL,
[APContactEmail] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShipTo000Contact] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShiptTo000Phone] [decimal] (18, 0) NULL,
[Shipto000Extension] [decimal] (18, 0) NULL,
[ShipTo000Email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultPONumber] [varchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrintDuplicatePickSlip] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SplitProfit] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObsolescenceEnabled] [bit] NULL,
[ObsolescenceStartDate] [date] NULL,
[ObsolescenceEndDate] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObsolescenceTargetDollars] [decimal] (11, 4) NULL,
[ObsolescenceDiscountPercent] [decimal] (5, 4) NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_CustomerUpdates_CreatedDate] DEFAULT (getdate()),
[ModifiedDate] [datetime] NULL,
[IsNew] [bit] NULL CONSTRAINT [DF_CustomerUpdates_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL CONSTRAINT [DF_CustomerUpdates_IsModified] DEFAULT ((0)),
[IsDeleted] [bit] NULL CONSTRAINT [DF_CustomerUpdates_IsDeleted] DEFAULT ((0)),
[PriceOnPicklist] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Promotion]
(
[PromotionCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PromotionDescription] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PromotionStartDateNumeric] [numeric] (8, 0) NOT NULL,
[PromotionStartDate] [date] NOT NULL,
[PromotionEndDateNumeric] [numeric] (8, 0) NOT NULL,
[PromotionEndDate] [date] NOT NULL,
[PromotionFlyerPath] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PromotionStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreationTimestamp] [datetime2] NOT NULL,
[DateLastMaintained] [datetime2] NOT NULL,
[Deleted] [bit] NOT NULL CONSTRAINT [DF_Promotion_Deleted] DEFAULT ((0)),
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_Promotion_CreatedDate] DEFAULT (getdate()),
[ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_Promotion_ModifiedDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Promotion] ADD CONSTRAINT [PK_PromotionCode] PRIMARY KEY CLUSTERED  ([PromotionCode]) ON [PRIMARY]
GO

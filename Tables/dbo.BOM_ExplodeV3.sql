CREATE TABLE [dbo].[BOM_ExplodeV3]
(
[BOM] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comp_Itm] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tot_Qty] [decimal] (10, 2) NULL,
[Par_List] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateAdded] [datetime] NULL CONSTRAINT [DF_BOM_ExplodeV3_DateAdded] DEFAULT (getdate())
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_BOM_ExplodeV3] ON [dbo].[BOM_ExplodeV3] ([BOM]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_BOM_ExplodeV3_1] ON [dbo].[BOM_ExplodeV3] ([Comp_Itm]) ON [PRIMARY]
GO

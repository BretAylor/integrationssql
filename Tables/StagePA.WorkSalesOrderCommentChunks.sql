CREATE TABLE [StagePA].[WorkSalesOrderCommentChunks]
(
[ID] [int] NOT NULL,
[PepperiOrderNbr] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommentLevel] [int] NOT NULL,
[comment] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayComment] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommentIndex] [int] NOT NULL,
[CommentChunk] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [StagePA].[WorkSalesOrderCommentChunks] ADD CONSTRAINT [PK_WorkSalesOrderCommentChunks] PRIMARY KEY CLUSTERED  ([PepperiOrderNbr], [CommentLevel], [CommentIndex]) ON [PRIMARY]
GO

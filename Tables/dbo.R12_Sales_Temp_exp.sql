CREATE TABLE [dbo].[R12_Sales_Temp_exp]
(
[ItemNum] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Qty_Shipped] [decimal] (38, 6) NULL,
[itemclass] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Inv_YYMM] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

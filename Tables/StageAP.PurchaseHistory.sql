CREATE TABLE [StageAP].[PurchaseHistory]
(
[CustomerNumber] [decimal] (7, 0) NOT NULL,
[SalesRepNumber] [decimal] (5, 0) NOT NULL,
[ProductNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductDescription] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PSDate] [decimal] (11, 0) NULL,
[InvoiceNumber] [decimal] (8, 0) NULL,
[OrderNumber] [decimal] (8, 0) NULL,
[OrderQuantity] [decimal] (7, 0) NULL,
[InvoiceQuantity] [decimal] (7, 0) NULL,
[BackorderQuantity] [decimal] (7, 0) NULL,
[NoChargeReasonCode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [date] NULL,
[OrderDate] [date] NULL,
[LastGrossMargin] [decimal] (5, 4) NULL,
[LastUnitSalesPrice] [decimal] (11, 4) NULL,
[YtdOrderedQuantity] [decimal] (7, 0) NULL,
[YtdSaleDollars] [decimal] (11, 4) NULL,
[CustomerPartNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment] [nvarchar] (62) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20200616-163932] ON [StageAP].[PurchaseHistory] ([OrderDate]) INCLUDE ([PSDate]) ON [PRIMARY]
GO

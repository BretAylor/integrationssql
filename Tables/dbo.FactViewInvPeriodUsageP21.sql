CREATE TABLE [dbo].[FactViewInvPeriodUsageP21]
(
[ID] [int] NOT NULL,
[ProductKey] [int] NOT NULL,
[item_id] [numeric] (19, 9) NULL,
[location_id] [numeric] (19, 0) NULL,
[inv_period_usage] [numeric] (19, 9) NULL,
[date_created] [datetime] NULL,
[date_last_modified] [datetime] NULL,
[last_maintained_by] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[scheduled_usage] [numeric] (19, 9) NULL,
[forecast_usage] [numeric] (19, 9) NULL,
[forecast_deviation_percentage] [numeric] (19, 8) NULL,
[mad_percentage] [numeric] (19, 8) NULL,
[filtered_usage] [numeric] (19, 9) NULL,
[number_of_orders] [numeric] (19, 0) NULL,
[edited] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[number_of_hits] [numeric] (19, 0) NULL,
[demand_period_uid] [int] NULL,
[inv_mast_uid] [int] NULL,
[usage_copied] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[inv_period_usage_uid] [int] NULL,
[mean_absolute_percent_error] [numeric] (19, 4) NULL,
[exceptional_sales_flag] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[exceptional_deviation_flag] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[reviewed_flag] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[last_reviewed_date] [datetime] NULL,
[usage_notes] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[forecast_adjustment_percent] [numeric] (19, 9) NULL,
[saved_filtered_usage] [numeric] (19, 9) NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactViewInvPeriodUsageP21] ADD CONSTRAINT [PK_FactViewInvPeriodUsageP21] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO

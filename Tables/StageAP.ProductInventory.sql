CREATE TABLE [StageAP].[ProductInventory]
(
[ItemNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ItemDescription] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillOfMaterialType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReaderId] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WarehouseID] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[City] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuantityAvailable] [numeric] (8, 0) NULL,
[QuantitySold] [numeric] (8, 0) NULL,
[NumberOfOrders] [numeric] (8, 0) NULL,
[RefreshTimeStamp] [datetime2] NULL
) ON [PRIMARY]
GO
ALTER TABLE [StageAP].[ProductInventory] ADD CONSTRAINT [PK_ProductInventory_1] PRIMARY KEY CLUSTERED  ([WarehouseID], [ItemNumber]) ON [PRIMARY]
GO

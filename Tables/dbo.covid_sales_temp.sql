CREATE TABLE [dbo].[covid_sales_temp]
(
[Order_Type] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Sls_Ctg] [varchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ItemNum] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Order_Date] [numeric] (10, 0) NULL,
[QuantityShipped] [decimal] (38, 5) NULL,
[Tot_Sls_Cus] [decimal] (38, 6) NULL,
[Tot_Sls_Frch] [decimal] (38, 6) NULL,
[AverageCost] [decimal] (28, 5) NULL
) ON [PRIMARY]
GO

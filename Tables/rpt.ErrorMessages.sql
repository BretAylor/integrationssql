CREATE TABLE [rpt].[ErrorMessages]
(
[ReportID] [int] NOT NULL,
[CustomerNbr] [bigint] NULL,
[PepperiOrderNbr] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorMessage] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateFirstObserved] [datetime] NOT NULL CONSTRAINT [DF_ErrorMessages_DateFirstObserved] DEFAULT (getdate()),
[DateLastObserved] [datetime] NOT NULL CONSTRAINT [DF_ErrorMessages_DateLastObserved] DEFAULT (getdate()),
[DateResolved] [datetime] NULL,
[DateUpdate] [datetime] NOT NULL CONSTRAINT [DF_ErrorMessages_DateUpdate] DEFAULT (getdate())
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20200414-131141] ON [rpt].[ErrorMessages] ([ReportID], [CustomerNbr], [PepperiOrderNbr], [DateResolved]) INCLUDE ([ErrorMessage]) ON [PRIMARY]
GO

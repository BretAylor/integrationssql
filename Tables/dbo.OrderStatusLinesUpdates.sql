CREATE TABLE [dbo].[OrderStatusLinesUpdates]
(
[PadDescription] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportingFranchise] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomerNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HostOrderNumber] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrderDate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EntrySequenceNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrderSequenceLineNumber] [numeric] (5, 0) NULL,
[PADOrderNumber] [numeric] (8, 0) NULL,
[ProcessingActivityDate] [decimal] (28, 0) NULL,
[LineItemType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemNumber] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemDescription1] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemDescription2] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerItemNumber] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VSNItemNumber] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemStatusCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EstimatedShipDate] [numeric] (8, 0) NULL,
[WerehouseID] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuantityOrdered] [decimal] (28, 0) NULL,
[QuantityShippable] [decimal] (28, 0) NULL,
[QuantityOnBakOrder] [decimal] (28, 0) NULL,
[ItemSellPrice] [decimal] (28, 4) NULL,
[ItemFranchiseCost] [decimal] (28, 4) NULL,
[ReturnReasonCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoChargeReasonCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DropShip] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpecialOrder] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_OrderStatusLineUpdates_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_OrderStatusLineUpdates_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL,
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsNew] [bit] NULL CONSTRAINT [DF_OrderStatusLineUpdates_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO

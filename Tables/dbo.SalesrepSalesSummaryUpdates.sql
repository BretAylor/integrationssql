CREATE TABLE [dbo].[SalesrepSalesSummaryUpdates]
(
[PadDescription] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcessDate] [numeric] (8, 0) NULL,
[SequenceNum] [numeric] (5, 0) NULL,
[TextAr] [varchar] (198) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_SalesrepSalesSummaryUpdates_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_SalesrepSalesSummaryUpdates_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL,
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsNew] [bit] NULL CONSTRAINT [DF_SalesrepSalesSummaryUpdates_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO

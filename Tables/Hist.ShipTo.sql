CREATE TABLE [Hist].[ShipTo]
(
[CustomerNumber] [decimal] (18, 0) NOT NULL,
[ShipToNumber] [decimal] (3, 0) NOT NULL,
[PepperiShipToId] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Contact] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZipCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [decimal] (14, 0) NULL,
[PhoneExtension] [decimal] (14, 0) NULL,
[IsDefaultShipTo] [bit] NULL,
[DefaultWarehouse] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultShipVia] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultFreightChargeCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultPONumber] [varchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxCode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL,
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModifiedDate] [datetime] NOT NULL,
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PepperiModifiedDate] [datetime] NULL,
[PepperiModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErpModifiedDate] [datetime] NULL,
[ErpModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErpSyncDate] [datetime] NULL,
[ErpSyncBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PepperiCustomerUUID] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Deleted] [bit] NOT NULL,
[DateAdded] [datetime] NOT NULL CONSTRAINT [DF_ShipTo_DateAdded] DEFAULT (getdate())
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20200521-202145] ON [Hist].[ShipTo] ([CustomerNumber], [ShipToNumber]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[PurchaseHistoryComponentUpdates]
(
[PadDescription] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerNumber] [numeric] (7, 0) NULL,
[CustomerName] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SalesRepNumber] [numeric] (5, 0) NULL,
[SequenceNumber] [numeric] (5, 0) NULL,
[ItemNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ComponentItemNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ComponentItemDescription] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuantityRequired] [numeric] (7, 0) NULL,
[LastPrice] [numeric] (11, 4) NULL,
[LastGrossMarginPercent] [numeric] (5, 4) NULL,
[CharachterRepresentation] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContractId] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_PurchaseHistoryComponentUpdates_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_PurchaseHistoryComponentUpdates_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL,
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsNew] [bit] NULL CONSTRAINT [DF_PurchaseHistoryComponentUpdates_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO

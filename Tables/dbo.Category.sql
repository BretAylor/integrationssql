CREATE TABLE [dbo].[Category]
(
[Id] [int] NULL,
[CategoryLevel1Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CategoryLevel1] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CategoryLevel2Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CategoryLevel2] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CategoryLevel3Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CategoryLevel3] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SmallImagePath] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WebsiteId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_Category_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Category_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL CONSTRAINT [DF_Category_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Category_ModifiedBy] DEFAULT (suser_name()),
[IsNew] [bit] NULL CONSTRAINT [DF_Category_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO

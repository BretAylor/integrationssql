CREATE TABLE [StageAP].[Promotion]
(
[PromotionCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PromotionDescription] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PromotionStartDateNumeric] [numeric] (8, 0) NOT NULL,
[PromotionEndDateNumeric] [numeric] (8, 0) NOT NULL,
[PromotionFlyerPath] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PromotionStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreationTimestamp] [datetime2] NOT NULL,
[DateLastMaintained] [datetime2] NOT NULL
) ON [PRIMARY]
GO

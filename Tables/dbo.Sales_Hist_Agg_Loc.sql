CREATE TABLE [dbo].[Sales_Hist_Agg_Loc]
(
[Itm] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WH] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Tot_Qty] [decimal] (38, 3) NULL,
[YYYYMM] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[FCOST_Rev_Sls_Hst]
(
[ID] [int] NOT NULL,
[Exl_Category] [varchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CompanyNum] [numeric] (2, 0) NULL,
[CustomerNum] [numeric] (10, 0) NULL,
[OrderNum] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HistorySequenceNum] [numeric] (7, 0) NOT NULL,
[OrderSequenceNum] [numeric] (5, 0) NOT NULL,
[PrimarySalesmanNum] [decimal] (28, 0) NULL,
[ItemNum] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuantityShipped] [decimal] (38, 5) NULL,
[ListPrice] [decimal] (28, 5) NULL,
[ActualSellPrice] [decimal] (28, 5) NULL,
[InvoiceDate] [numeric] (6, 0) NULL,
[ShippingLocation] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MfgItem] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KitQuantityShipped] [decimal] (28, 3) NULL,
[New_Class] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Class_Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[New_Sub_Class] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Subclass_Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Item_Class] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCDATA] [varchar] (243) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FCOST_Rev_Sls_Hst] ON [dbo].[FCOST_Rev_Sls_Hst] ([ItemNum], [PrimarySalesmanNum], [Exl_Category], [HistorySequenceNum], [OrderSequenceNum], [ID]) ON [PRIMARY]
GO

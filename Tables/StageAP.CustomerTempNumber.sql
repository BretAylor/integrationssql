CREATE TABLE [StageAP].[CustomerTempNumber]
(
[SalesRepNumber] [decimal] (5, 0) NULL,
[TempCustomerNumber] [decimal] (7, 0) NULL,
[CustomerNumber] [decimal] (6, 0) NULL
) ON [PRIMARY]
GO

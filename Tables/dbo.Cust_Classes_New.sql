CREATE TABLE [dbo].[Cust_Classes_New]
(
[Class_Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[New_Class] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[New_Subclass] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Subclass_Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO

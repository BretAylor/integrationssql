CREATE TABLE [dbo].[COVID_Items]
(
[Item] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[P21_Item] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ETA] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ETA_Date] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Net_OH_PD] [numeric] (18, 0) NULL,
[Qty_On_PO_PD] [numeric] (18, 0) NULL,
[Qty_Ship_PD] [numeric] (18, 0) NULL,
[Qty_SO_PD] [numeric] (18, 0) NULL,
[Status] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Owner] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes_Date] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Buyer] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Order_Limit] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Category] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Wzr_URL] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FS_URL] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Itm_Prio] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Publish] [int] NULL,
[Alias] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FS_Alias] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Alias_Desc] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_COVID_Items] ON [dbo].[COVID_Items] ([Item]) ON [PRIMARY]
GO

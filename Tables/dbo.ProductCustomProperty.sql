CREATE TABLE [dbo].[ProductCustomProperty]
(
[Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErpNbr] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentTable] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_ProductCustomProperties_CreatedOn] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ProductCustomProperties_CreatedBy] DEFAULT (suser_name()),
[ModifiedOn] [datetime] NULL CONSTRAINT [DF_ProductCustomProperty_ModifiedOn] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ProductCustomProperty_ModifiedBy] DEFAULT (suser_name()),
[IsNew] [bit] NULL CONSTRAINT [DF_ProductCustomProperty_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO

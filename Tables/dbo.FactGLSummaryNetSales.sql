CREATE TABLE [dbo].[FactGLSummaryNetSales]
(
[ChartOfAccountKey] [int] NULL,
[FiscalYear] [int] NULL,
[CompanyNum] [int] NULL,
[GlAccountNum] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Aug] [decimal] (28, 2) NULL,
[Sep] [decimal] (28, 2) NULL,
[Oct] [decimal] (28, 2) NULL,
[Nov] [decimal] (28, 2) NULL,
[Dec] [decimal] (28, 2) NULL,
[Jan] [decimal] (28, 2) NULL,
[Feb] [decimal] (28, 2) NULL,
[Mar] [decimal] (28, 2) NULL,
[Apr] [decimal] (28, 2) NULL,
[May] [decimal] (28, 2) NULL,
[Jun] [decimal] (28, 2) NULL,
[Jul] [decimal] (28, 2) NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO

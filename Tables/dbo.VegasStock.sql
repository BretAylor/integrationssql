CREATE TABLE [dbo].[VegasStock]
(
[LocationId] [numeric] (19, 0) NULL,
[FS_Item] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Wzr_Item] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemDesc] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bin] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Quantity] [numeric] (19, 9) NULL
) ON [PRIMARY]
GO

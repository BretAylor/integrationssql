CREATE TABLE [dbo].[P21_Periods]
(
[period] [numeric] (3, 0) NOT NULL,
[year_for_period] [numeric] (4, 0) NOT NULL,
[beginning_date] [datetime] NULL,
[ending_date] [datetime] NULL
) ON [PRIMARY]
GO

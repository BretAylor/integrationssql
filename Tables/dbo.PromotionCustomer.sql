CREATE TABLE [dbo].[PromotionCustomer]
(
[PromotionCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomerNumber] [decimal] (10, 0) NOT NULL,
[PromotionStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreationTimestamp] [datetime2] NOT NULL,
[DateLastMaintained] [datetime2] NOT NULL,
[Deleted] [bit] NOT NULL CONSTRAINT [DF_PromotionCustomer_Deleted] DEFAULT ((0)),
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_PromotionCustomer_CreatedDate] DEFAULT (getdate()),
[ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_PromotionCustomer_ModifiedDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PromotionCustomer] ADD CONSTRAINT [PK_PromotionCustomer] PRIMARY KEY CLUSTERED  ([PromotionCode], [CustomerNumber]) ON [PRIMARY]
GO

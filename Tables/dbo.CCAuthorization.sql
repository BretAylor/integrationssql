CREATE TABLE [dbo].[CCAuthorization]
(
[CompanyNumber] [numeric] (2, 0) NOT NULL,
[HistorySequenceNumber] [numeric] (7, 0) NOT NULL,
[CustomerNumber] [numeric] (10, 0) NULL,
[InvoiceNumber] [numeric] (8, 0) NOT NULL,
[TransactionCode] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransactionAmount] [decimal] (28, 2) NULL,
[AuthorizationNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ResponseCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionDate] [numeric] (8, 0) NOT NULL
) ON [PRIMARY]
GO

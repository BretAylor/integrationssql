CREATE TABLE [dbo].[SalesOrderTracking]
(
[ErpOrderNum] [numeric] (8, 0) NOT NULL,
[InvoiceNum] [numeric] (8, 0) NOT NULL,
[CustomerNbr] [decimal] (18, 0) NOT NULL,
[InvoiceDate] [numeric] (8, 0) NULL,
[CarrierId] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrackingNbr] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20190627-094957] ON [dbo].[SalesOrderTracking] ([ErpOrderNum]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20190627-095142] ON [dbo].[SalesOrderTracking] ([InvoiceNum]) ON [PRIMARY]
GO

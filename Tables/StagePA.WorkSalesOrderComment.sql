CREATE TABLE [StagePA].[WorkSalesOrderComment]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[PepperiOrderNbr] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommentLevel] [int] NOT NULL,
[comment] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayComment] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [StagePA].[WorkSalesOrderComment] ADD CONSTRAINT [PK_WorkSalesOrderComment] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO

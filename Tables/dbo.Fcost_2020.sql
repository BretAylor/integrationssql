CREATE TABLE [dbo].[Fcost_2020]
(
[Item_Number] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AVC_2019] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AVC_2020] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FC_2019] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FC_2020] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FC_2020_WRK] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Flag] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Fcost_2020] ON [dbo].[Fcost_2020] ([Item_Number]) ON [PRIMARY]
GO

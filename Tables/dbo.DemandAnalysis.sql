CREATE TABLE [dbo].[DemandAnalysis]
(
[Item] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Desc] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Buyer] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Max_Demand_Prd] [int] NULL,
[Status] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Whse] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Vendor] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor_Name] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Weight] [decimal] (9, 4) NULL,
[MOQ] [decimal] (9, 3) NULL,
[Avg_Cost] [decimal] (15, 5) NULL,
[Min_OH] [decimal] (11, 3) NULL,
[Max_OH] [decimal] (11, 3) NULL,
[Net_Inv_OH] [decimal] (13, 3) NULL,
[Qty_On_PO] [decimal] (11, 3) NULL,
[12_Avg] [decimal] (38, 6) NULL,
[S/X_12] [float] NULL,
[6_Avg] [decimal] (38, 6) NULL,
[S/X_6] [float] NULL,
[3_Avg] [decimal] (38, 6) NULL,
[S/X_3] [float] NULL
) ON [PRIMARY]
GO

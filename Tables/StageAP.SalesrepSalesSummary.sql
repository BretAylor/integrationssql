CREATE TABLE [StageAP].[SalesrepSalesSummary]
(
[PadDescription] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProcessDate] [numeric] (8, 0) NOT NULL,
[SequenceNum] [numeric] (5, 0) NOT NULL,
[TextAr] [varchar] (198) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[DailyInventoryDetails]
(
[Year] [int] NULL,
[Month] [int] NULL,
[MonthName] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FiscalYear] [int] NULL,
[FiscalMonth] [int] NULL,
[WarehouseID] [int] NULL,
[WarehouseName] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemClass] [int] NULL,
[ItemNum] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemClassName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BeginningDate] [date] NULL,
[ProcessDate] [date] NULL,
[BeginningInventory] [numeric] (31, 2) NULL,
[EndingInventory] [numeric] (31, 2) NULL,
[CostOfGoodsSold] [numeric] (31, 2) NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_MonthName_FiscalYear_BeginningDate_includes] ON [dbo].[DailyInventoryDetails] ([MonthName], [FiscalYear], [BeginningDate]) INCLUDE ([BeginningInventory], [FiscalMonth], [ItemClassName], [ItemNum], [WarehouseID], [WarehouseName], [Year]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_MonthName_FiscalYear_ProcessDate_includes] ON [dbo].[DailyInventoryDetails] ([MonthName], [FiscalYear], [ProcessDate]) INCLUDE ([CostOfGoodsSold], [EndingInventory], [FiscalMonth], [ItemClassName], [ItemNum], [WarehouseID], [WarehouseName], [Year]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ProcessDate] ON [dbo].[DailyInventoryDetails] ([ProcessDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Year_Month_BeginningDate_includes] ON [dbo].[DailyInventoryDetails] ([Year], [Month], [BeginningDate]) INCLUDE ([BeginningInventory], [ItemClassName], [ItemNum], [WarehouseID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Year_Month_ProcessDate_includes] ON [dbo].[DailyInventoryDetails] ([Year], [Month], [ProcessDate]) INCLUDE ([BeginningDate], [CostOfGoodsSold], [EndingInventory], [ItemClassName], [ItemNum], [WarehouseID]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[FCOST_HST]
(
[Item_Number] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Item_Description_1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Item_Class] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
["Date_Added"] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
["BOM_Type"] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
["BOM_Calc Cost"] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
["BOM_Cost_Factor"] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[2015] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[2016] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[2017] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[2018] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[2019] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Current_Cost] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

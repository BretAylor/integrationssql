CREATE TABLE [StageAP].[PromotionItem]
(
[PromotionCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ItemNumber] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ItemPrice] [decimal] (18, 4) NOT NULL,
[PromotionStatus] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreationTimestamp] [datetime2] NOT NULL,
[DateLastMaintained] [datetime2] NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SalesrepSalesSummary]
(
[PadDescription] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProcessDate] [numeric] (8, 0) NOT NULL,
[SequenceNum] [numeric] (5, 0) NOT NULL,
[TextAr] [varchar] (198) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_SalesrepSalesSummary_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_SalesrepSalesSummary_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_SalesrepSalesSummary_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_SalesrepSalesSummary_ModifiedBy] DEFAULT (suser_name())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SalesrepSalesSummary] ADD CONSTRAINT [PK_SalesrepSalesSummary] PRIMARY KEY CLUSTERED  ([PadDescription], [ProcessDate], [SequenceNum]) ON [PRIMARY]
GO

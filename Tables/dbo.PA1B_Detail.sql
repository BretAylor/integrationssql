CREATE TABLE [dbo].[PA1B_Detail]
(
[HistorySequenceNum] [numeric] (7, 0) NOT NULL,
[CustomerNum] [numeric] (10, 0) NULL,
[PrimarySalesmanNum] [decimal] (28, 0) NULL,
[QuantityOrdered] [decimal] (10, 3) NULL,
[ItemNum] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShippingLocation] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InvoiceDate] [numeric] (6, 0) NULL,
[STD_PK] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PA1BEnable] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemClass] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemNumber] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sales] [decimal] (38, 6) NULL,
[QTY_CHK2] [varchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Year] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO

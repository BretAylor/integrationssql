CREATE TABLE [dbo].[DimBranchP21]
(
[BranchKey] [int] NOT NULL IDENTITY(1, 1),
[CompanyId] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BranchDescription] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BranchId] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

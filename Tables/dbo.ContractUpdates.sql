CREATE TABLE [dbo].[ContractUpdates]
(
[CompanyNumber] [decimal] (2, 0) NOT NULL,
[ContractCode] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActualPrice] [decimal] (18, 5) NOT NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_ContractUpdates_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ContractUpdates_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL,
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsNew] [bit] NULL CONSTRAINT [DF_ContractUpdates_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL,
[PriceOnPicklist] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrintStatement] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrintShortDescriptionFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

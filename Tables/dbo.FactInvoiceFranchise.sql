CREATE TABLE [dbo].[FactInvoiceFranchise]
(
[DateKey] [int] NOT NULL,
[CustomerKey] [int] NOT NULL,
[EmployeeKey] [int] NOT NULL,
[ProductKey] [int] NOT NULL,
[ItemClassKey] [int] NOT NULL,
[OrderKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrderSequenceNum] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ShipmentKey] [int] NULL,
[InvoiceDate] [int] NULL,
[CustomerNum] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrimarySalesmanNum] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemNum] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuantityShipped] [int] NULL,
[ActualSellPrice] [decimal] (28, 5) NULL,
[ListPrice] [decimal] (28, 5) NULL,
[CurrentAverageCost] [decimal] (28, 5) NULL,
[FranchiseCost] [decimal] (28, 5) NULL,
[WinzerCost] [decimal] (28, 5) NULL,
[NetSales] [decimal] (28, 5) NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactInvoiceFranchise] ADD CONSTRAINT [PK_FactInvoiceFranchise] PRIMARY KEY CLUSTERED  ([DateKey], [CustomerKey], [EmployeeKey], [ProductKey], [ItemClassKey], [OrderKey], [OrderSequenceNum]) ON [PRIMARY]
GO

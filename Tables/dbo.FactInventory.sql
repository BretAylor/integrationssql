CREATE TABLE [dbo].[FactInventory]
(
[MetricId] [int] NULL,
[Metric] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateKey] [int] NULL,
[WarehouseKey] [int] NULL,
[WarehouseType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemClass] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Buyer] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Inventory] [numeric] (31, 2) NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DateKey] ON [dbo].[FactInventory] ([DateKey]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ShipTo]
(
[CustomerNumber] [decimal] (18, 0) NOT NULL,
[ShipToNumber] [decimal] (3, 0) NOT NULL,
[PepperiShipToId] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Contact] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZipCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [decimal] (14, 0) NULL,
[PhoneExtension] [decimal] (14, 0) NULL,
[IsDefaultShipTo] [bit] NULL,
[DefaultWarehouse] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultShipVia] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultFreightChargeCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultPONumber] [varchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxCode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_ShipTo_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ShipTo_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_ShipTo_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ShipTo_ModifiedBy] DEFAULT (suser_name()),
[PepperiModifiedDate] [datetime] NULL,
[PepperiModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErpModifiedDate] [datetime] NULL,
[ErpModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErpSyncDate] [datetime] NULL,
[ErpSyncBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PepperiCustomerUUID] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Deleted] [bit] NOT NULL CONSTRAINT [DF_ShipTo_Deleted] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgShipToHist]
ON [dbo].[ShipTo]
AFTER Update
AS
BEGIN
    SET NOCOUNT ON;
INSERT INTO [Hist].[ShipTo]
           ([CustomerNumber]
           ,[ShipToNumber]
           ,[PepperiShipToId]
           ,[Name]
           ,[Contact]
           ,[Address1]
           ,[Address2]
           ,[City]
           ,[State]
           ,[ZipCode]
           ,[Country]
           ,[PhoneNumber]
           ,[PhoneExtension]
           ,[IsDefaultShipTo]
           ,[DefaultWarehouse]
           ,[DefaultShipVia]
           ,[DefaultFreightChargeCode]
           ,[DefaultPONumber]
           ,[TaxCode]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[PepperiModifiedDate]
           ,[PepperiModifiedBy]
           ,[ErpModifiedDate]
           ,[ErpModifiedBy]
           ,[ErpSyncDate]
           ,[ErpSyncBy]
           ,[PepperiCustomerUUID]
           ,[Deleted])
    SELECT  d.[CustomerNumber]
           ,d.[ShipToNumber]
           ,d.[PepperiShipToId]
           ,d.[Name]
           ,d.[Contact]
           ,d.[Address1]
           ,d.[Address2]
           ,d.[City]
           ,d.[State]
           ,d.[ZipCode]
           ,d.[Country]
           ,d.[PhoneNumber]
           ,d.[PhoneExtension]
           ,d.[IsDefaultShipTo]
           ,d.[DefaultWarehouse]
           ,d.[DefaultShipVia]
           ,d.[DefaultFreightChargeCode]
           ,d.[DefaultPONumber]
           ,d.[TaxCode]
           ,d.[CreatedDate]
           ,d.[CreatedBy]
           ,d.[ModifiedDate]
           ,d.[ModifiedBy]
           ,d.[PepperiModifiedDate]
           ,d.[PepperiModifiedBy]
           ,d.[ErpModifiedDate]
           ,d.[ErpModifiedBy]
           ,d.[ErpSyncDate]
           ,d.[ErpSyncBy]
           ,d.[PepperiCustomerUUID]
           ,d.[Deleted]
    FROM
        deleted d;
END
GO
ALTER TABLE [dbo].[ShipTo] ADD CONSTRAINT [PK_ShipTo] PRIMARY KEY CLUSTERED  ([CustomerNumber], [ShipToNumber]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20190618-092015] ON [dbo].[ShipTo] ([PepperiShipToId]) ON [PRIMARY]
GO

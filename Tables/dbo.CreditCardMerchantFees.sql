CREATE TABLE [dbo].[CreditCardMerchantFees]
(
[FranchiseNum] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProcessDate] [numeric] (8, 0) NOT NULL,
[SequenceNum] [numeric] (5, 0) NOT NULL,
[ReportLine] [varchar] (198) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_CreditCardMerchantFees_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_CreditCardMerchantFees_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL CONSTRAINT [DF_CreditCardMerchantFees_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_CreditCardMerchantFees_ModifiedBy] DEFAULT (suser_name()),
[ErpSyncDate] [datetime] NULL CONSTRAINT [DF_CreditCardMerchantFees_ErpSyncDate] DEFAULT (getdate()),
[ErpSyncBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_CreditCardMerchantFees_ErpSyncBy] DEFAULT (suser_name()),
[IsNew] [bit] NULL CONSTRAINT [DF_CreditCardMerchantFees_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CreditCardMerchantFees] ADD CONSTRAINT [PK_CreditCardMerchantFees] PRIMARY KEY CLUSTERED  ([FranchiseNum], [ProcessDate], [SequenceNum]) ON [PRIMARY]
GO

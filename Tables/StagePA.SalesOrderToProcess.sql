CREATE TABLE [StagePA].[SalesOrderToProcess]
(
[PepperiOrderNbr] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedDate] [datetime] NULL
) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [ClusteredIndex-20200531-080215] ON [StagePA].[SalesOrderToProcess] ([CreatedDate], [PepperiOrderNbr]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ProductInventoryOld]
(
[ProductNumber] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WarehouseId] [int] NOT NULL,
[QuantityAvailable] [decimal] (19, 9) NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_ProductInventory_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ProductInventory_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL,
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsNew] [bit] NULL CONSTRAINT [DF_ProductInventory_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProductInventoryOld] ADD CONSTRAINT [PK_ProductInventory] PRIMARY KEY CLUSTERED  ([ProductNumber], [WarehouseId]) ON [PRIMARY]
GO

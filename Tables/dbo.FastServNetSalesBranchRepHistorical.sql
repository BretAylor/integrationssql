CREATE TABLE [dbo].[FastServNetSalesBranchRepHistorical]
(
[FiscalYear] [int] NULL,
[CurrentMonthName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SalesRepID] [int] NULL,
[SalesRepresentativeName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BranchID] [int] NULL,
[BranchDesciption] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MtdSales] [int] NULL,
[MtdGoal] [int] NULL,
[MthGoal] [int] NULL,
[YtdSales] [int] NULL,
[YtdGoal] [int] NULL,
[YthGoal] [int] NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO

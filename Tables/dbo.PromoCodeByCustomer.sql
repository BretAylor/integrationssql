CREATE TABLE [dbo].[PromoCodeByCustomer]
(
[PromoID] [int] NOT NULL,
[CustomerNumber] [int] NOT NULL,
[ItemNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PromoFranchiseCost] [decimal] (13, 4) NOT NULL,
[StartDate] [int] NOT NULL,
[EndDate] [int] NOT NULL,
[LinkToFlyer] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Deleted] [int] NOT NULL CONSTRAINT [DF_PromoCodeByCustomer_Deleted] DEFAULT ((0)),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_PromoCodeByCustomer_CreatedBy] DEFAULT (suser_name()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_PromoCodeByCustomer_ModifiedBy] DEFAULT (suser_name()),
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_PromoCodeByCustomer_CreatedDate] DEFAULT (getdate()),
[ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_PromoCodeByCustomer_ModifiedDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PromoCodeByCustomer] ADD CONSTRAINT [PK_PromoCodeByCustomer_1] PRIMARY KEY CLUSTERED  ([PromoID]) ON [PRIMARY]
GO

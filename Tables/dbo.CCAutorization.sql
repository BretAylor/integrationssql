CREATE TABLE [dbo].[CCAutorization]
(
[CompanyNumber] [numeric] (2, 0) NOT NULL,
[HistorySequenceNumber] [numeric] (7, 0) NOT NULL,
[CustomerNumber] [numeric] (10, 0) NULL,
[InvoiceNumber] [numeric] (8, 0) NOT NULL,
[TransactionCode] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransactionAmount] [decimal] (28, 2) NULL,
[AuthorizationNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ResponseCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionDate] [numeric] (8, 0) NOT NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_CCAutorization_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_CCAutorization_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL CONSTRAINT [DF_CCAutorization_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_CCAutorization_ModifiedBy] DEFAULT (suser_name())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCAutorization] ADD CONSTRAINT [PK_CCAutorization] PRIMARY KEY CLUSTERED  ([CompanyNumber], [HistorySequenceNumber], [InvoiceNumber], [TransactionCode], [AuthorizationNumber], [TransactionDate]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CustomerDeletes]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[DESPAD] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CUSTNO] [numeric] (7, 0) NULL,
[CSTNAM] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateAdded] [datetime] NULL CONSTRAINT [DF_CustomerDeletes_DateAdded] DEFAULT (getdate())
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[BillOnlyImages]
(
[Guid] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScannedImageUrl] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_BillOnlyImages_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_BillOnlyImages_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL CONSTRAINT [DF_BillOnlyImages_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_BillOnlyImages_ModifiedBy] DEFAULT (suser_name()),
[PepperiOrderId] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LineNbr] [decimal] (5, 0) NOT NULL,
[CustomerNumber] [decimal] (18, 0) NOT NULL,
[SalesRepNumber] [decimal] (18, 0) NOT NULL
) ON [PRIMARY]
GO

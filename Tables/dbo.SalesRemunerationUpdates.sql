CREATE TABLE [dbo].[SalesRemunerationUpdates]
(
[RepNum] [numeric] (5, 0) NULL,
[CalendarYear] [numeric] (4, 0) NULL,
[CalendarMonth] [numeric] (2, 0) NULL,
[CalendarDay] [numeric] (2, 0) NULL,
[FiscalYear] [numeric] (4, 0) NULL,
[FiscalMonth] [numeric] (2, 0) NULL,
[RemunerationPeriod] [numeric] (2, 0) NULL,
[RTDWinzerSales] [numeric] (9, 2) NULL,
[RTDWinzerSalesCost] [numeric] (9, 2) NULL,
[RTDGMPercent] [numeric] (5, 2) NULL,
[RTDGMAmount] [numeric] (9, 2) NULL,
[RTDInvoiceOnlySales] [numeric] (9, 2) NULL,
[RTDFranDirectSales] [numeric] (9, 2) NULL,
[RTDBillOnlySales] [numeric] (9, 2) NULL,
[RTDPersonalSales] [numeric] (9, 2) NULL,
[RTDTotalSales] [numeric] (9, 2) NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_SalesRemunerationUpdates_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_SalesRemunerationUpdates_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL CONSTRAINT [DF_SalesRemunerationUpdates_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_SalesRemunerationUpdates_ModifiedBy] DEFAULT (suser_name()),
[ErpSyncDate] [datetime] NULL CONSTRAINT [DF_SalesRemunerationUpdates_ErpSyncDate] DEFAULT (getdate()),
[ErpSyncBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_SalesRemunerationUpdates_ErpSyncBy] DEFAULT (suser_name()),
[IsNew] [bit] NULL CONSTRAINT [DF_SalesRemunerationUpdates_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO

CREATE TABLE [StagePA].[SalesOrderLineKitFormatted]
(
[PadDescription] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomerNbr] [decimal] (18, 0) NULL,
[PepperiOrderNbr] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LineNbr] [bigint] NOT NULL,
[LineItemType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ParentItemNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ComponentItem] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LineItemComment] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DetailCommentCode] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrderQuantity] [decimal] (7, 0) NOT NULL,
[LabelQty] [decimal] (7, 0) NOT NULL,
[OrderPrice] [decimal] (11, 4) NOT NULL,
[FranchiseCost] [decimal] (11, 4) NOT NULL,
[UnitOfMeasure] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomerContract] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BasePrice] [int] NOT NULL,
[CommissionCode] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommissionPercent] [int] NOT NULL,
[MarkupPercent] [int] NOT NULL,
[MarkupAmount] [int] NOT NULL,
[FreightUpChargeCode] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FreightUpChargePercent] [int] NOT NULL,
[FreightUpChargeAmount] [int] NOT NULL,
[BaseFranchiseCost] [numeric] (11, 4) NULL,
[QtyDiscountAmount] [int] NOT NULL,
[QtyBreakRate] [int] NOT NULL,
[PrefixBreakAmount] [int] NOT NULL,
[PrefixBreakRate] [int] NOT NULL,
[SplitsLoadedFranchise] [decimal] (11, 4) NOT NULL,
[AlphaUserField1] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlphaUserField2] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlphaUserField3] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlphaUserField4] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlphaUserField5] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlphaUserField6] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlphaUserField7] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlphaUserField8] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlphaUserField9] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlphaUserField10] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlphaUserField11] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlphaUserField12] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlphaUserField13] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlphaUserField14] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlphaUserField15] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlphaUserField16] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlphaUserField17] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlphaUserField18] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlphaUserField19] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlphaUserField20] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NumericUserField1] [int] NOT NULL,
[NumericUserField2] [int] NOT NULL,
[NumericUserField3] [int] NOT NULL,
[NumericUserField4] [int] NOT NULL,
[NumericUserField5] [int] NOT NULL,
[NumericUserField6] [int] NOT NULL,
[NumericUserField7] [int] NOT NULL,
[NumericUserField8] [int] NOT NULL,
[NumericUserField9] [int] NOT NULL,
[NumericUserField10] [int] NOT NULL,
[NumericUserField11] [int] NOT NULL,
[NumericUserField12] [int] NOT NULL,
[NumericUserField13] [int] NOT NULL,
[NumericUserField14] [int] NOT NULL,
[NumericUserField15] [int] NOT NULL,
[NumericUserField16] [int] NOT NULL,
[NumericUserField17] [int] NOT NULL,
[NumericUserField18] [int] NOT NULL,
[NumericUserField19] [int] NOT NULL,
[NumericUserField20] [int] NOT NULL,
[CreatedDate] [datetime] NULL
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [ClusteredIndex-20200531-080410] ON [StagePA].[SalesOrderLineKitFormatted] ([CreatedDate], [PepperiOrderNbr], [LineNbr]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[VsnCatalogDetailUpdates]
(
[CatalogId] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CatalogPartNumber] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PartDescription] [varchar] (62) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitOfMeasure] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FranchiseCost] [decimal] (10, 4) NULL,
[StandardPackageQty] [decimal] (5, 0) NULL,
[EnforceBaggingQty] [bit] NULL,
[PackageAllInOneBag] [bit] NULL,
[KitType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecommendedPrice] [decimal] (11, 4) NULL,
[AssignedWinzerProductNumber] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FreightType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FreightAmount] [decimal] (7, 2) NULL,
[WeightInPounds] [decimal] (11, 4) NULL,
[LandedCostSplitProfit] [decimal] (15, 4) NULL,
[FastservItemNumber] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_VsnCatalogDetailUpdates_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_VsnCatalogDetailUpdates_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL,
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsNew] [bit] NULL,
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[FactGLDailyNetSales]
(
[ChartOfAccountKey] [int] NULL,
[TransactionDateKey] [varchar] (82) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLPostingDate] [varchar] (82) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostingYear] [numeric] (4, 0) NULL,
[PostingPeriod] [numeric] (2, 0) NULL,
[PostingMonth] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompanyNum] [numeric] (2, 0) NULL,
[GlAccountNum] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionAmount] [numeric] (28, 2) NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO

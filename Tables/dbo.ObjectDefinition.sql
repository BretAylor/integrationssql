CREATE TABLE [dbo].[ObjectDefinition]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ObjectType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObjectName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NULL,
[Author] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RefreshPeriod] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RefreshFrequency] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RefreshTime] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Location] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceObject] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DestinationObject] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DestinationName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SSIS Package] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ObjectDefinition] ADD CONSTRAINT [PK_ObjectDefinition] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO

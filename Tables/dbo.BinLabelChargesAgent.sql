CREATE TABLE [dbo].[BinLabelChargesAgent]
(
[PadDescription] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcessDate] [numeric] (8, 0) NULL,
[SequenceNum] [numeric] (5, 0) NULL,
[ReportLine] [varchar] (198) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_BinLabelChargesAgent_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_BinLabelChargesAgent_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL CONSTRAINT [DF_BinLabelChargesAgent_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_BinLabelChargesAgent_ModifiedBy] DEFAULT (suser_name()),
[ErpSyncDate] [datetime] NULL CONSTRAINT [DF_BinLabelChargesAgent_ErpSyncDate] DEFAULT (getdate()),
[ErpSyncBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_BinLabelChargesAgent_ErpSyncBy] DEFAULT (suser_name()),
[IsNew] [bit] NULL CONSTRAINT [DF_BinLabelChargesAgent_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[IN_Catalog]
(
[PART] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PAGE] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IN_CAT] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO

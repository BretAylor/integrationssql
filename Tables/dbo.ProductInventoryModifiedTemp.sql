CREATE TABLE [dbo].[ProductInventoryModifiedTemp]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ItemNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemDescription] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillOfMaterialType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReaderId] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WarehouseID] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuantityAvailable] [numeric] (8, 0) NULL,
[QuantitySold] [numeric] (8, 0) NULL,
[NumberOfOrders] [numeric] (8, 0) NULL,
[RefreshTimeStamp] [datetime2] NULL,
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ProductInventoryModifiedTemp_CreatedBy_1] DEFAULT (suser_name()),
[CreatedDate] [datetime] NULL CONSTRAINT [DF_ProductInventoryModifiedTemp_CreatedDate_1] DEFAULT (getdate()),
[IsModified] [bit] NULL
) ON [PRIMARY]
GO

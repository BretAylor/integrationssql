CREATE TABLE [dbo].[FactScoreCardTargetsP21]
(
[ScorecardGroup] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BranchKey] [int] NOT NULL,
[EmployeeKey] [int] NOT NULL,
[LocationId] [numeric] (19, 0) NULL,
[SalesrepId] [numeric] (19, 0) NOT NULL,
[MtdTotal] [numeric] (19, 0) NULL,
[MtdGoal] [numeric] (19, 0) NULL,
[MthGoal] [numeric] (19, 0) NULL,
[YtdTotal] [numeric] (19, 0) NULL,
[YtdGoal] [numeric] (19, 0) NULL,
[YthGoal] [numeric] (19, 0) NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactScoreCardTargetsP21] ADD CONSTRAINT [PK_FactScoreCardTargetsP21_1] PRIMARY KEY CLUSTERED  ([ScorecardGroup], [BranchKey], [EmployeeKey]) ON [PRIMARY]
GO

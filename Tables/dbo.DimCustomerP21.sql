CREATE TABLE [dbo].[DimCustomerP21]
(
[CustomerKey] [int] NOT NULL IDENTITY(3000000, 1),
[CustomerId] [numeric] (19, 0) NULL,
[CompanyId] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimCustomerP21] ADD CONSTRAINT [PK_DimCustomerP21_1] PRIMARY KEY CLUSTERED  ([CustomerKey]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[DimProductP21]
(
[ProductKey] [int] NOT NULL IDENTITY(3000000, 1),
[InvMastUid] [int] NULL,
[ItemId] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemDesc] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimProductP21] ADD CONSTRAINT [PK_DimProductP21] PRIMARY KEY CLUSTERED  ([ProductKey]) ON [PRIMARY]
GO

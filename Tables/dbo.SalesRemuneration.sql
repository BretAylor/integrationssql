CREATE TABLE [dbo].[SalesRemuneration]
(
[RepNum] [numeric] (5, 0) NOT NULL,
[CalendarYear] [numeric] (4, 0) NOT NULL,
[CalendarMonth] [numeric] (2, 0) NOT NULL,
[CalendarDay] [numeric] (2, 0) NULL,
[FiscalYear] [numeric] (4, 0) NULL,
[FiscalMonth] [numeric] (2, 0) NULL,
[RemunerationPeriod] [numeric] (2, 0) NOT NULL,
[RTDWinzerSales] [numeric] (11, 2) NULL,
[RTDWinzerSalesCost] [numeric] (11, 2) NULL,
[RTDGMPercent] [numeric] (11, 2) NULL,
[RTDGMAmount] [numeric] (11, 2) NULL,
[RTDInvoiceOnlySales] [numeric] (11, 2) NULL,
[RTDFranDirectSales] [numeric] (11, 2) NULL,
[RTDBillOnlySales] [numeric] (11, 2) NULL,
[RTDPersonalSales] [numeric] (11, 2) NULL,
[RTDTotalSales] [numeric] (11, 2) NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_SalesRemuneration_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_SalesRemuneration_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL CONSTRAINT [DF_SalesRemuneration_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_SalesRemuneration_ModifiedBy] DEFAULT (suser_name()),
[ErpSyncDate] [datetime] NULL CONSTRAINT [DF_SalesRemuneration_ErpSyncDate] DEFAULT (getdate()),
[ErpSyncBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_SalesRemuneration_ErpSyncBy] DEFAULT (suser_name())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SalesRemuneration] ADD CONSTRAINT [PK_SalesRemuneration_1] PRIMARY KEY CLUSTERED  ([RepNum], [CalendarYear], [CalendarMonth], [RemunerationPeriod]) ON [PRIMARY]
GO

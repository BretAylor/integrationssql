CREATE TABLE [dbo].[DimChartOfAccount]
(
[ChartOfAccountKey] [int] NOT NULL IDENTITY(1, 1),
[InternalGlAcctNo] [numeric] (7, 0) NULL,
[CompanyNum] [numeric] (2, 0) NULL,
[GlAccountNum] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateCreated] [numeric] (6, 0) NULL,
[AccountType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description1] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimChartOfAccount] ADD CONSTRAINT [PK_DimChartOfAccount] PRIMARY KEY CLUSTERED  ([ChartOfAccountKey]) ON [PRIMARY]
GO

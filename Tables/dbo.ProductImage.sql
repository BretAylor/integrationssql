CREATE TABLE [dbo].[ProductImage]
(
[Id] [uniqueidentifier] NOT NULL,
[ErpNbr] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductId] [uniqueidentifier] NOT NULL,
[SortOrder] [int] NULL,
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SmallImagePath] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MediumImagePath] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LargeImagePath] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AltText] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_ProductImages_CreatedOn] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ProductImages_CreatedBy] DEFAULT (suser_name()),
[ModifiedOn] [datetime] NULL CONSTRAINT [DF_ProductImage_ModifiedOn] DEFAULT (getdate()),
[ModifiedBy] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ProductImage_ModifiedBy] DEFAULT (suser_name()),
[IsNew] [bit] NULL CONSTRAINT [DF_ProductImage_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO

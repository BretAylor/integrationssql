CREATE TABLE [dbo].[ExistingCustomerCC]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Guid] [uniqueidentifier] NULL,
[PepperiId] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCNumber] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastFourDigits] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CVVNumber] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExpirationDate] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerNumber] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZipCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Note] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KeepOnFile] [bit] NULL,
[IsNew] [bit] NULL CONSTRAINT [DF_ExistingCustomerCC_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_ExistingbCustomerCC_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ExistingCustomerCC_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL CONSTRAINT [DF_ExistingCustomerCC_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ExistingCustomerCC_ModifiedBy] DEFAULT (suser_name())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ExistingCustomerCC] ADD CONSTRAINT [PK_ExistingCustomerCC] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO

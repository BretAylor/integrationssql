CREATE TABLE [dbo].[ProductImageUpdates]
(
[Id] [uniqueidentifier] NOT NULL,
[ErpNbr] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductId] [uniqueidentifier] NOT NULL,
[SortOrder] [int] NULL,
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SmallImagePath] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MediumImagePath] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LargeImagePath] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AltText] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_ProductImagesUpdates_CreatedOn] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ProductImagesUpdates_CreatedBy] DEFAULT (suser_name()),
[ModifiedOn] [datetime] NULL,
[ModifiedBy] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsNew] [bit] NULL,
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[FactPurchaseHistory]
(
[CustomerKey] [int] NOT NULL,
[EmployeeKey] [int] NOT NULL,
[InventoryKey] [int] NOT NULL,
[ProductKey] [int] NOT NULL,
[DateKey] [int] NOT NULL,
[OrderKey] [int] NOT NULL,
[OrderDate] [numeric] (8, 0) NOT NULL,
[DESPAD] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerNum] [numeric] (7, 0) NULL,
[CustomerName] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TERRNO] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployeeNum] [numeric] (5, 0) NULL,
[ProductNum] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductDescription] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InventoryNum] [numeric] (8, 0) NOT NULL,
[InventoryDate] [numeric] (8, 0) NULL,
[OrderNum] [numeric] (8, 0) NOT NULL,
[HSTNBR] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderQuantity] [numeric] (7, 0) NOT NULL,
[InventoryQuantity] [numeric] (7, 0) NULL,
[BCKQTY] [numeric] (7, 0) NULL,
[LSTMAR] [numeric] (5, 4) NULL,
[COMMCD] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxFlag] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellingPrice] [numeric] (11, 4) NULL,
[TERMCD] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YTDORD] [numeric] (7, 0) NULL,
[YTDSLS] [numeric] (11, 4) NULL,
[UNTMEA] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NCCODE] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMPER] [numeric] (5, 4) NULL,
[WLITCL] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WLITSC] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WHOCAN] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NOTNAM] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InventoryDate2] [date] NULL,
[OrderDate2] [date] NOT NULL,
[ACUSTN] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROCOM] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PA1BIN] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NUMBAG] [numeric] (3, 0) NULL,
[DCOMCD] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PSDATE] [numeric] (8, 0) NULL,
[PSTIME] [numeric] (6, 0) NULL,
[PSORDN] [numeric] (8, 0) NULL,
[FRCOST] [numeric] (11, 4) NULL,
[PFCOST] [numeric] (11, 4) NULL,
[PROCO2] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPCSCT] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPBSPR] [numeric] (11, 4) NULL,
[CPBSCC] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPBSPC] [numeric] (5, 4) NULL,
[CPOVPR] [numeric] (11, 4) NULL,
[CPCMCD] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPPERC] [numeric] (5, 4) NULL,
[CPMKPC] [numeric] (5, 4) NULL,
[CPMKAM] [numeric] (11, 4) NULL,
[CPFRCD] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPFRPC] [numeric] (5, 4) NULL,
[CPFRAM] [numeric] (11, 4) NULL,
[CVPCDE] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CVPQTY] [numeric] (28, 5) NULL,
[CVPPRC] [numeric] (28, 5) NULL,
[FCRSCD] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FCPRWN] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PHUSR1] [numeric] (28, 5) NULL,
[PHUSR2] [numeric] (28, 5) NULL,
[PHUSRA] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PHUSRB] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PHUSRC] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactPurchaseHistory] ADD CONSTRAINT [PK_FactPurchaseHistory] PRIMARY KEY CLUSTERED  ([CustomerKey], [EmployeeKey], [InventoryKey], [ProductKey], [DateKey], [OrderKey], [OrderDate]) ON [PRIMARY]
GO

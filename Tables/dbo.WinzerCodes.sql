CREATE TABLE [dbo].[WinzerCodes]
(
[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_WinzerCodes_Guid] DEFAULT (newid()),
[Category] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FullText] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_WinzerCodes_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_WinzerCodes_CreatedBy] DEFAULT (suser_name()),
[ErpModifiedDate] [datetime] NULL,
[ErpModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErpSyncDate] [datetime] NULL,
[ModifiedDate] [datetime] NULL CONSTRAINT [DF_WinzerCodes_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_WinzerCodes_ModifiedBy] DEFAULT (suser_name())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WinzerCodes] ADD CONSTRAINT [PK_WinzerCodes] PRIMARY KEY CLUSTERED  ([Guid]) ON [PRIMARY]
GO

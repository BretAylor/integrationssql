CREATE TABLE [dbo].[CustomerCrossReference]
(
[PepperiCustomerId] [decimal] (18, 0) NOT NULL,
[AS400TempCustomerId] [int] NOT NULL IDENTITY(3000000, 1),
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_ShipToCrossReference_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ShipToCrossReference_CreatedBy] DEFAULT (suser_name())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomerCrossReference] ADD CONSTRAINT [PK_CustomerCrossReference_1] PRIMARY KEY CLUSTERED  ([AS400TempCustomerId]) ON [PRIMARY]
GO

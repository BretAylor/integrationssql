CREATE TABLE [dbo].[Contract]
(
[CompanyNumber] [int] NOT NULL,
[ContractCode] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActualPrice] [decimal] (18, 5) NOT NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_Contract_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_Contract_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL CONSTRAINT [DF_Contract_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Contract_ModifiedBy] DEFAULT (suser_name())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Contract] ADD CONSTRAINT [PK_Contract] PRIMARY KEY CLUSTERED  ([CompanyNumber], [ContractCode], [ProductNumber]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Contract] ON [dbo].[Contract] ([ContractCode], [ProductNumber]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Contract_ProductNumber] ON [dbo].[Contract] ([ProductNumber]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[OrdersOnHold]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[OrderId] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HoldReason] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Active] [tinyint] NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_OrdersOnHold_CreatedDate] DEFAULT (getdate())
) ON [PRIMARY]
GO

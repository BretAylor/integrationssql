CREATE TABLE [dbo].[PurchaseHistoryUpdates]
(
[CustomerNumber] [decimal] (7, 0) NULL,
[SalesRepNumber] [decimal] (5, 0) NULL,
[ProductNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductDescription] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceNumber] [decimal] (8, 0) NULL,
[OrderNumber] [decimal] (8, 0) NULL,
[OrderQuantity] [decimal] (7, 0) NULL,
[InvoiceQuantity] [decimal] (7, 0) NULL,
[BackorderQuantity] [decimal] (7, 0) NULL,
[NoChargeReasonCode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [date] NULL,
[OrderDate] [date] NULL,
[LastGrossMargin] [decimal] (5, 4) NULL,
[LastUnitSalesPrice] [decimal] (11, 4) NULL,
[YtdOrderedQuantity] [decimal] (7, 0) NULL,
[YtdSaleDollars] [decimal] (11, 4) NULL,
[CustomerPartNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment] [nvarchar] (62) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_PurchaseHistoryUpdates_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_PurchaseHistoryUpdates_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL,
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsNew] [bit] NULL CONSTRAINT [DF_PurchaseHistoryUpdates_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO

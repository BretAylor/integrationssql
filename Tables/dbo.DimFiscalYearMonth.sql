CREATE TABLE [dbo].[DimFiscalYearMonth]
(
[FiscalYear] [int] NULL,
[FiscalMonth] [tinyint] NULL,
[MonthName] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Website]
(
[Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [date] NULL CONSTRAINT [DF_Website_CreatedDate] DEFAULT (getdate()),
[ModifiedDate] [date] NULL CONSTRAINT [DF_Website_ModifiedDate] DEFAULT (getdate())
) ON [PRIMARY]
GO

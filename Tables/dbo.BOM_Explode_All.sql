CREATE TABLE [dbo].[BOM_Explode_All]
(
[BOM] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comp_Itm] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tot_Qty] [decimal] (10, 2) NULL,
[Par_List] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_BOM_Explode_All] ON [dbo].[BOM_Explode_All] ([BOM]) ON [PRIMARY]
GO

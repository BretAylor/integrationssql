CREATE TABLE [dbo].[SALES_FINAL_CY2019]
(
[cust_class] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[customerglcode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ID] [int] NOT NULL,
[CompanyNum] [numeric] (2, 0) NULL,
[CustomerNum] [numeric] (10, 0) NULL,
[OrderNum] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HistorySequenceNum] [numeric] (7, 0) NOT NULL,
[OrderSequenceNum] [numeric] (5, 0) NOT NULL,
[PrimarySalesmanNum] [decimal] (28, 0) NULL,
[ItemNum] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemDescription1] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuantityShipped] [decimal] (28, 3) NULL,
[ListPrice] [decimal] (28, 5) NULL,
[ActualSellPrice] [decimal] (28, 5) NULL,
[CurrentAverageCost] [decimal] (28, 5) NULL,
[TotalLineAmount] [decimal] (28, 5) NULL,
[InvoiceDate] [numeric] (6, 0) NULL,
[Type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemClass] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShippingLocation] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[QuantityOrdered] [decimal] (10, 3) NULL,
[QuantityBackordered] [decimal] (10, 3) NULL,
[DateAdded] [datetime] NULL,
[Exl_Category] [varchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO

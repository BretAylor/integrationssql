CREATE TABLE [dbo].[SFC_ANALYSIS]
(
[PrimarySalesmanNum] [decimal] (28, 0) NULL,
[ItemNum] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SFC_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SFC_ITM_CHK] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[2020_Imp_$] [decimal] (38, 6) NULL,
[2020_Imp_$_OLD] [decimal] (38, 6) NULL,
[Act_List] [decimal] (28, 5) NULL,
[2019] [decimal] (28, 5) NULL,
[2020_Push] [decimal] (28, 5) NULL
) ON [PRIMARY]
GO

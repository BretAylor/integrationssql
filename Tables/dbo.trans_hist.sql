CREATE TABLE [dbo].[trans_hist]
(
[IAITNO] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IAWHID] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IAUNMS] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IATRCC] [numeric] (2, 0) NOT NULL,
[IATRDT] [numeric] (6, 0) NOT NULL,
[MDY] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IATRCD] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IATRQT] [decimal] (11, 3) NOT NULL,
[IACONO] [numeric] (2, 0) NOT NULL,
[IAPOCC] [numeric] (2, 0) NOT NULL
) ON [PRIMARY]
GO

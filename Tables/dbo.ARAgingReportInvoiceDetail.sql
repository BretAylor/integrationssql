CREATE TABLE [dbo].[ARAgingReportInvoiceDetail]
(
[CustomerNumber] [decimal] (18, 0) NULL,
[PepperiCustomerNumber] [decimal] (18, 0) NULL,
[CustomerName] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SalesRepNumber] [decimal] (18, 0) NULL,
[PadDescription] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SequenceNum] [decimal] (5, 0) NULL,
[ARAmountDue] [decimal] (11, 4) NULL,
[ARAmountCurrent] [decimal] (11, 4) NULL,
[AgeAmount030] [decimal] (11, 4) NULL CONSTRAINT [DF__ARAgingRe__AgeAm__5E1FF51F] DEFAULT ((0.00)),
[AgeAmount060] [decimal] (11, 4) NULL CONSTRAINT [DF__ARAgingRe__AgeAm__5F141958] DEFAULT ((0.00)),
[AgeAmount090] [decimal] (11, 4) NULL CONSTRAINT [DF__ARAgingRe__AgeAm__60083D91] DEFAULT ((0.00)),
[AgeAmount120] [decimal] (11, 4) NULL CONSTRAINT [DF__ARAgingRe__AgeAm__60FC61CA] DEFAULT ((0.00)),
[InvoiceNumber] [decimal] (18, 0) NULL,
[InvoiceDate] [date] NULL,
[InvoiceAmountDue] [decimal] (11, 4) NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_ARAgingReportInvoiceDetail_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ARAgingReportInvoiceDetail_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL CONSTRAINT [DF_ARAgingReportInvoiceDetail_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ARAgingReportInvoiceDetail_ModifiedBy] DEFAULT (suser_name()),
[IsNew] [bit] NULL CONSTRAINT [DF_ARAgingReportInvoiceDetail_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SFC_ITM]
(
[SFC_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Item_num] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SFC] [decimal] (18, 5) NOT NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_SFC_ITM] ON [dbo].[SFC_ITM] ([SFC_ID], [Item_num]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[R18_Sales_P21_Added]
(
[cust_class] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerGLCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ID] [int] NOT NULL,
[CompanyNum] [numeric] (2, 0) NULL,
[CustomerNum] [numeric] (10, 0) NULL,
[OrderNum] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HistorySequenceNum] [numeric] (7, 0) NOT NULL,
[OrderSequenceNum] [numeric] (19, 0) NOT NULL,
[PrimarySalesmanNum] [decimal] (28, 0) NULL,
[ItemNum] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemDescription1] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuantityShipped] [decimal] (34, 9) NULL,
[ListPrice] [decimal] (28, 5) NULL,
[ActualSellPrice] [decimal] (32, 9) NULL,
[CurrentAverageCost] [decimal] (28, 5) NULL,
[TotalLineAmount] [decimal] (28, 5) NULL,
[InvoiceDate] [nvarchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemClass] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShippingLocation] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuantityOrdered] [decimal] (19, 9) NULL,
[QuantityBackordered] [decimal] (13, 3) NULL,
[DateAdded] [datetime] NULL,
[Exl_Category] [varchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_R18_Sales_P21_Added] ON [dbo].[R18_Sales_P21_Added] ([cust_class], [Exl_Category], [ItemClass], [HistorySequenceNum], [InvoiceDate], [ItemNum], [ShippingLocation], [OrderSequenceNum], [OrderNum]) ON [PRIMARY]
GO

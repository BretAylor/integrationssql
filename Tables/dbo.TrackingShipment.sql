CREATE TABLE [dbo].[TrackingShipment]
(
[InvoiceNumber] [numeric] (8, 0) NULL,
[SalesRep] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HostOrderNumber] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [numeric] (8, 0) NULL,
[ShipmentDescription] [varchar] (62) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CarrierID] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerNumber] [numeric] (7, 0) NULL,
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_TrackingShipment_CreatedBy] DEFAULT (suser_name()),
[CreatedDate] [datetime] NULL CONSTRAINT [DF_TrackingShipment_CreatedDate] DEFAULT (getdate())
) ON [PRIMARY]
GO

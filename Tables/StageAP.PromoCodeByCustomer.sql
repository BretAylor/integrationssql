CREATE TABLE [StageAP].[PromoCodeByCustomer]
(
[PromoID] [int] NOT NULL,
[CustomerNumber] [int] NOT NULL,
[ItemNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PromoFranchiseCost] [decimal] (13, 4) NOT NULL,
[StartDate] [int] NOT NULL,
[EndDate] [int] NOT NULL,
[LinkToFlyer] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

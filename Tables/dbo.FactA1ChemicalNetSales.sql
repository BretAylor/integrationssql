CREATE TABLE [dbo].[FactA1ChemicalNetSales]
(
[DateKey] [int] NOT NULL,
[ProductKey] [int] NOT NULL,
[EmployeeKey] [int] NOT NULL,
[CustomerKey] [int] NOT NULL,
[ShipmentKey] [int] NULL,
[InvoiceNo] [int] NOT NULL,
[OrderNo] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LineNo] [int] NOT NULL,
[UnitPrice] [numeric] (19, 9) NULL,
[QtyShipped] [int] NULL,
[FranchiseCost] [numeric] (19, 9) NULL,
[WinzerCost] [numeric] (19, 9) NULL,
[Cost] [numeric] (19, 9) NULL,
[NetSales] [numeric] (19, 9) NULL,
[Type] [nchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactA1ChemicalNetSales] ADD CONSTRAINT [PK_FactA1ChemicalNetSales] PRIMARY KEY CLUSTERED  ([DateKey], [ProductKey], [EmployeeKey], [CustomerKey], [InvoiceNo], [OrderNo], [LineNo]) ON [PRIMARY]
GO

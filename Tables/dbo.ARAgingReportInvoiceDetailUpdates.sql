CREATE TABLE [dbo].[ARAgingReportInvoiceDetailUpdates]
(
[CustomerNumber] [decimal] (18, 0) NULL,
[PepperiCustomerNumber] [decimal] (18, 0) NULL,
[CustomerName] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SalesRepNumber] [decimal] (18, 0) NULL,
[PadDescription] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SequenceNum] [decimal] (5, 0) NOT NULL,
[ARAmountDue] [decimal] (11, 4) NULL,
[ARAmountCurrent] [decimal] (11, 4) NULL,
[AgeAmount030] [decimal] (11, 4) NULL CONSTRAINT [DF__ARAgingRe__AgeAm__5E1FF51F2] DEFAULT ((0.00)),
[AgeAmount060] [decimal] (11, 4) NULL CONSTRAINT [DF__ARAgingRe__AgeAm__5F1419582] DEFAULT ((0.00)),
[AgeAmount090] [decimal] (11, 4) NULL CONSTRAINT [DF__ARAgingRe__AgeAm__60083D912] DEFAULT ((0.00)),
[AgeAmount120] [decimal] (11, 4) NULL CONSTRAINT [DF__ARAgingRe__AgeAm__60FC61CA2] DEFAULT ((0.00)),
[InvoiceNumber] [decimal] (18, 0) NOT NULL,
[InvoiceDate] [date] NULL,
[InvoiceAmountDue] [decimal] (11, 4) NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_ARAgingReportInvoiceDetailUpdates_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ARAgingReportInvoiceDetailUpdates_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL CONSTRAINT [DF_ARAgingReportInvoiceDetailUpdates_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ARAgingReportInvoiceDetailUpdates_ModifiedBy] DEFAULT (suser_name()),
[IsNew] [bit] NULL,
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ARAgingReportInvoiceDetailUpdates] ADD CONSTRAINT [PK_ARAgingReportInvoiceDetailUpdates] PRIMARY KEY CLUSTERED  ([PadDescription], [SequenceNum], [InvoiceNumber]) ON [PRIMARY]
GO

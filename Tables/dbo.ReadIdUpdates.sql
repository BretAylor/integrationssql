CREATE TABLE [dbo].[ReadIdUpdates]
(
[ReadId] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SalesRepNumber] [decimal] (18, 0) NOT NULL,
[SalesRepName] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_ReadIdUpdates_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ReadIdUpdates_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL,
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsNew] [bit] NULL CONSTRAINT [DF_ReadIdUpdates_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ProductInventoryDelete]
(
[PRODNO_ItemNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRDDES_ItemDescription] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMBMTP_BillOfMaterialType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[READID_ReaderId] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IBWHID_WarehouseID] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZTCITY_City] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QTYAVL_QuantityAvailable] [numeric] (8, 0) NULL,
[SOLD_QuantitySold] [numeric] (8, 0) NULL,
[ORDERS_NumberOfOrders] [numeric] (8, 0) NULL,
[LASTUPD_RefreshTimeStamp] [datetime2] NULL
) ON [PRIMARY]
GO

CREATE TABLE [StagePA].[SalesOrderLineNbr]
(
[LineNbr] [bigint] NOT NULL,
[PepperiID] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ParentItemNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrandedProductnbr] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LineItemType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Comment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrigLineNbr] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_SalesOrderLineNbr_CreatedDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [ClusteredIndex-20200531-095229] ON [StagePA].[SalesOrderLineNbr] ([CreatedDate], [PepperiID], [LineNbr]) ON [PRIMARY]
GO

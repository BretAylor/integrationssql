CREATE TABLE [dbo].[VSNCatalogDetailTemp]
(
[V2CTID] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[V2CTPN] [nvarchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[V2PDES] [nvarchar] (62) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[V2WUOM] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[V2FRCS] [numeric] (10, 4) NULL,
[V2STPK] [numeric] (5, 0) NULL,
[V2ENFR] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[V2PA1B] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[V2KITI] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[V2RPRC] [numeric] (11, 4) NULL,
[V2PROD] [nvarchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[V2FRTP] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[V2FRAM] [numeric] (7, 2) NULL,
[V2WGHT] [numeric] (11, 4) NULL,
[V2LNCS] [numeric] (15, 4) NULL,
[V2FSS#] [nvarchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

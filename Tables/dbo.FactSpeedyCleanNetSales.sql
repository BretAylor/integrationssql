CREATE TABLE [dbo].[FactSpeedyCleanNetSales]
(
[DateKey] [int] NOT NULL,
[ProductKey] [int] NOT NULL,
[EmployeeKey] [int] NOT NULL,
[CustomerKey] [int] NOT NULL,
[ShipmentKey] [int] NULL,
[InvoiceNo] [int] NOT NULL,
[LineNo] [int] NOT NULL,
[UnitPrice] [numeric] (19, 9) NULL,
[QtyShipped] [numeric] (19, 9) NULL,
[WinzerCost] [numeric] (19, 9) NULL,
[FranchiseCost] [numeric] (19, 9) NULL,
[NetSales] [numeric] (19, 9) NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactSpeedyCleanNetSales] ADD CONSTRAINT [PK_SpeedyCleanNetSales] PRIMARY KEY CLUSTERED  ([DateKey], [ProductKey], [EmployeeKey], [CustomerKey], [LineNo], [InvoiceNo]) ON [PRIMARY]
GO

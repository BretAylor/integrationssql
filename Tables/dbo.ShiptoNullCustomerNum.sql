CREATE TABLE [dbo].[ShiptoNullCustomerNum]
(
[CUSTNO] [numeric] (7, 0) NULL,
[PepperiCustomerid] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHIPTO] [numeric] (3, 0) NULL,
[SHPNAM] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHPCNT] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHPAD1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHPAD2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHPCTY] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHPSTA] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHPZIP] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PHONNO] [numeric] (10, 0) NULL,
[PHONEX] [numeric] (4, 0) NULL,
[DFSHTO] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NSDFWH] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHPVIA] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FRTCOD] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAXCOD] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NSDFPO] [nvarchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CMCTID] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TCSTNO] [numeric] (8, 0) NULL,
[blsDefaultShiptTo] [bit] NULL,
[NewPepperiCustomerid] [numeric] (18, 0) NULL,
[SHIPTOConverted] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DerivedCustomerNumber] [numeric] (18, 0) NULL,
[DerivedCustomerNumberConverted] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LKP_CustomerNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LKP_ShipToNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Lrg_Rep_Itm_Impact]
(
[PrimarySalesmanNum] [decimal] (28, 0) NULL,
[itemnum] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tot_Qty] [decimal] (38, 3) NULL,
[2020_FC_Tot] [decimal] (38, 6) NULL,
[2019_FC_Tot] [decimal] (38, 6) NULL,
[Net_Inc] [decimal] (38, 6) NULL,
[Pct_Inc] [decimal] (10, 2) NULL,
[Inf_Pct] [decimal] (10, 2) NULL,
[Sales_Total_Cust] [decimal] (38, 5) NULL,
[2019_F_GM] [decimal] (10, 2) NULL,
[2020_F_GM] [decimal] (10, 2) NULL,
[2020_Pct_Sls] [decimal] (10, 2) NULL,
[KC_Code] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FC_Tot_Dsc_2020] [decimal] (38, 6) NULL,
[Current_Cost] [decimal] (28, 5) NULL,
[Pct_Tot_Rep_Sls] [decimal] (38, 6) NULL
) ON [PRIMARY]
GO

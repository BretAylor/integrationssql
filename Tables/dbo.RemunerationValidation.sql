CREATE TABLE [dbo].[RemunerationValidation]
(
[REPNO] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROCESSDT] [numeric] (8, 0) NULL,
[LINENO] [numeric] (5, 0) NULL,
[REPORTLN] [varchar] (198) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodDate] [numeric] (8, 0) NULL
) ON [PRIMARY]
GO

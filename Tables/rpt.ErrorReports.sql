CREATE TABLE [rpt].[ErrorReports]
(
[reportID] [int] NOT NULL IDENTITY(1, 1),
[ReportName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_rpt.ErrorReports_DateCreated] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [rpt].[ErrorReports] ADD CONSTRAINT [PK_ErrorReports] PRIMARY KEY CLUSTERED  ([reportID]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[InvoiceLinesUpdates]
(
[PadDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InvoiceType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InvoiceDate] [numeric] (8, 0) NULL,
[HostOrderNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LineNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (62) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuantityOrdered] [numeric] (7, 0) NULL,
[QuantityInvoiced] [numeric] (7, 0) NULL,
[QuantityOnBackOrder] [numeric] (7, 0) NULL,
[UnitPrice] [numeric] (11, 4) NULL,
[Tax] [numeric] (11, 4) NULL,
[TaxFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExtendedPrice] [numeric] (11, 4) NULL,
[LotusNotesSecurity] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NotesNameSecurity] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoicedDateCCYYMMDD] [datetime] NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_InvoiceLinesUpdates_CreatedDate2] DEFAULT (getdate()),
[ModifiedDate] [datetime] NULL,
[IsNew] [bit] NULL CONSTRAINT [DF_InvoiceLinesUpdates_IsNew2] DEFAULT ((1)),
[IsModified] [bit] NULL CONSTRAINT [DF_InvoiceLinesUpdates_IsModified2] DEFAULT ((0)),
[IsDeleted] [bit] NULL CONSTRAINT [DF_InvoiceLinesUpdates_IsDeleted2] DEFAULT ((0))
) ON [PRIMARY]
GO

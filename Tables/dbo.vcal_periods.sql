CREATE TABLE [dbo].[vcal_periods]
(
[period] [numeric] (3, 0) NOT NULL,
[year_for_period] [numeric] (4, 0) NOT NULL,
[beginning_date] [datetime] NULL,
[ending_date] [datetime] NULL,
[Period_Type] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Rel_Period] [int] NULL,
[YYYYMM] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO

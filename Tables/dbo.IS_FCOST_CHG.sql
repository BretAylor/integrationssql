CREATE TABLE [dbo].[IS_FCOST_CHG]
(
[ItemNumber] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VendorNumber] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UMConversionFactor] [decimal] (28, 5) NULL,
[Cost] [decimal] (15, 5) NULL,
[F_Cost] [decimal] (18, 4) NULL
) ON [PRIMARY]
GO

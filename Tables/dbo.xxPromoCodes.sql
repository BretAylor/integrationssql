CREATE TABLE [dbo].[xxPromoCodes]
(
[Cust] [int] NOT NULL,
[Item] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Price] [float] NOT NULL,
[Start] [datetime2] NOT NULL,
[End] [datetime2] NOT NULL
) ON [PRIMARY]
GO

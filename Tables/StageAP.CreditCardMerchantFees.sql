CREATE TABLE [StageAP].[CreditCardMerchantFees]
(
[FranchiseNum] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProcessDate] [numeric] (8, 0) NOT NULL,
[SequenceNum] [numeric] (5, 0) NOT NULL,
[ReportLine] [varchar] (198) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [StageAP].[CreditCardMerchantFees] ADD CONSTRAINT [PK_CreditCardMerchantFees] PRIMARY KEY CLUSTERED  ([FranchiseNum], [ProcessDate], [SequenceNum]) ON [PRIMARY]
GO

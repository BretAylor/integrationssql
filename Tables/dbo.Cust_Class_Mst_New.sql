CREATE TABLE [dbo].[Cust_Class_Mst_New]
(
[Customer] [numeric] (18, 0) NOT NULL,
[Customer_Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[New_Class] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[New_Sub_Class] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Cust_Class_Mst_New] ADD CONSTRAINT [PK_Copy of Customer Classification -Completed  10-11-19] PRIMARY KEY CLUSTERED  ([Customer]) ON [PRIMARY]
GO

CREATE TABLE [rpt].[PepperiSalesOrder]
(
[PepperiOrderNbr] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrderDate] [datetime] NOT NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF__SalesOrde__Creat__46486B8E] DEFAULT (getdate())
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20200519-121642] ON [rpt].[PepperiSalesOrder] ([CreatedDate]) INCLUDE ([PepperiOrderNbr]) ON [PRIMARY]
GO

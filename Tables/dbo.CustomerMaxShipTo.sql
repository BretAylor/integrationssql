CREATE TABLE [dbo].[CustomerMaxShipTo]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[CustomerNumber] [numeric] (18, 0) NULL,
[DefaultShipTo] [numeric] (18, 0) NULL
) ON [PRIMARY]
GO

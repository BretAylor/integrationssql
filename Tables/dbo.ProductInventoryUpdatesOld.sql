CREATE TABLE [dbo].[ProductInventoryUpdatesOld]
(
[ProductNumber] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WarehouseId] [int] NOT NULL,
[QuantityAvailable] [decimal] (19, 9) NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_ProductInventoryUpdates_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ProductInventoryUpdates_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL,
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProductInventoryUpdatesOld] ADD CONSTRAINT [PK_ProductInventoryUpdates] PRIMARY KEY CLUSTERED  ([ProductNumber], [WarehouseId]) ON [PRIMARY]
GO

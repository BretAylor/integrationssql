CREATE TABLE [dbo].[DimShipToP21]
(
[ShipmentKey] [int] NOT NULL IDENTITY(3000000, 1),
[NbShipToNumber] [numeric] (10, 0) NULL,
[NbShipToCustomerId] [int] NULL,
[NbShipToSalesRepId] [int] NULL,
[NbShipToName] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbShipToContact] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbShipToAddress1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbShipToAddress2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbShipToCity] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbShipToState] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbShipToZipCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbShipToPhone] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NBShipToTaxCode] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbShipToDefaultShipVia] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbShipToDefaultFreightCode] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbShipToCustDefaultShipTo] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbShipToDefaultPONumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbShipToDefaultWarehouse] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbShipToDeleted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbShipToDefaultShipViaId] [numeric] (19, 0) NULL,
[NbShipToAplShpToNum2] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbShipToAplusCusNo] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbShipToAttention] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbShipToShippingWarehouse] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbShipToEmail] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbShipToFax] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NbDirectShpPlano] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimShipToP21] ADD CONSTRAINT [PK_DimShipToP21_1] PRIMARY KEY CLUSTERED  ([ShipmentKey]) ON [PRIMARY]
GO

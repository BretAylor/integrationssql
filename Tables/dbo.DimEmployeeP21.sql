CREATE TABLE [dbo].[DimEmployeeP21]
(
[EmployeeKey] [int] NOT NULL IDENTITY(3000000, 1),
[SalesRepID] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Middle] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DirectPhone] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SalesRep] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimEmployeeP21] ADD CONSTRAINT [PK_DimEmployeeP21_1] PRIMARY KEY CLUSTERED  ([EmployeeKey]) ON [PRIMARY]
GO

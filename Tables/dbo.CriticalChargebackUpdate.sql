CREATE TABLE [dbo].[CriticalChargebackUpdate]
(
[PadDescription] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProcessDate] [numeric] (8, 0) NOT NULL,
[SequenceNum] [numeric] (5, 0) NOT NULL,
[TextCB] [varchar] (198) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_CriticalChargebackUpdate_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_CriticalChargebackUpdate_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL CONSTRAINT [DF_CriticalChargebackUpdate_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_CriticalChargebackUpdate_ModifiedBy] DEFAULT (suser_name()),
[ErpSyncDate] [datetime] NULL CONSTRAINT [DF_CriticalChargebackUpdate_ErpSyncDate] DEFAULT (getdate()),
[ErpSyncBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_CriticalChargebackUpdate_ErpSyncBy] DEFAULT (suser_name()),
[IsNew] [bit] NULL CONSTRAINT [DF_CriticalChargebackUpdate_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CriticalChargebackUpdate] ADD CONSTRAINT [PK_CriticalChargeback_2] PRIMARY KEY CLUSTERED  ([PadDescription], [ProcessDate], [SequenceNum]) ON [PRIMARY]
GO

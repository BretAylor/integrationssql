CREATE TABLE [dbo].[DimMonthDate]
(
[MonthDateKey] [int] NOT NULL IDENTITY(1, 1),
[CalendarMonth] [tinyint] NULL,
[FiscalMonth] [tinyint] NULL,
[MonthName] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MonthNameShort] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimMonthDate] ADD CONSTRAINT [PK_DimMonthDate] PRIMARY KEY CLUSTERED  ([MonthDateKey]) ON [PRIMARY]
GO

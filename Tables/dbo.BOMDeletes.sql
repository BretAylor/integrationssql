CREATE TABLE [dbo].[BOMDeletes]
(
[ParentItemNum] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentItemDescription] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuantityPerParent] [numeric] (8, 0) NULL,
[ComponentItemNum] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ComponentItemDescription] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReadId] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_BOMDeletes_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_BOMDeletes_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_BOMDeletes_ModifiedDate] DEFAULT (getdate()),
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_BOMDeletes_ModifiedBy] DEFAULT (suser_name())
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[DefaultPriceChangeUpdates]
(
[DESPAD_PadDescription] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CUSTNO_CustomerNumber] [decimal] (7, 0) NULL,
[CSTNAM_CustomerName] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TERRNO_TerritoryNumber] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REPNUM_SalesRep#] [decimal] (5, 0) NULL,
[PRODNO_ProductNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRDESC_Description] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVNBR_InvoiceNumber] [decimal] (8, 0) NULL,
[INVDAT_InvoiceDate1] [decimal] (8, 0) NULL,
[ORDNBR_PadOrderNumber] [decimal] (8, 0) NULL,
[HSTNBR_Hostordernumber] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORDQTY_OrderQuantity] [decimal] (7, 0) NULL,
[INVQTY_InvoiceQuantity] [decimal] (7, 0) NULL,
[BCKQTY_BackorderQuantity] [decimal] (7, 0) NULL,
[LSTMAR_Lastgrossmargin] [decimal] (5, 4) NULL,
[COMMCD_CommissionCode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAXFLG_CustomerTaxableFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SELPRC_UnitSalesPrice] [decimal] (11, 4) NULL,
[ORDDAT_OrderDate1] [decimal] (8, 0) NULL,
[TERMCD_TermsCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YTDORD_YTDorderedquantity] [decimal] (7, 0) NULL,
[YTDSLS_YTDSalesDollars] [decimal] (11, 4) NULL,
[UNTMEA_UnitofMeasure] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NCCODE_NoChargeReasonCd] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMPER_CommissionPerc] [decimal] (5, 4) NULL,
[WLITCL_ItemClass] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WLITSC_ItemSubClass] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INDATE_InvoiceDate2] [date] NULL,
[ORDATE_OrderDate2] [date] NULL,
[ACUSTN_Alphacustomer#] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROCOM_ItemProductComment] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PA1BIN_PA1BINDICATOR] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NUMBAG_NumberOfBags] [decimal] (3, 0) NULL,
[DCOMCD_Detailcommentcode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PPHDAT_UserCreateTimeStamp] [datetime] NULL,
[RECDAT_PseudoCreateTimeStamp] [datetime] NULL,
[FRCOST_Currfrancostforcostc] [decimal] (11, 4) NULL,
[PFCOST_Proposedfranchisecost] [decimal] (11, 4) NULL,
[PROCO2_ItemProductComment2] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPCSCT_ContractCode] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPBSPR_BasePrice] [decimal] (11, 4) NULL,
[CPBSCC_BaseCommissionCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPBSPC_BaseCommissionPercent] [decimal] (5, 4) NULL,
[CPOVPR_OverridePrice] [decimal] (11, 4) NULL,
[CPCMCD_OverrideCommissionCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPPERC_OverrideCommissionPerce] [decimal] (5, 4) NULL,
[CPMKPC_MarkupPercent] [decimal] (5, 4) NULL,
[CPMKAM_MarkupAmount] [decimal] (11, 4) NULL,
[CPFRCD_FreightUpChargeCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPFRPC_FreightUpChargePercent] [decimal] (5, 4) NULL,
[CPFRAM_FreightUpChargeAmount] [decimal] (11, 4) NULL,
[CVPCDE_CVPCode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CVPQTY_CVPCodeMinQty] [decimal] (15, 5) NULL,
[CVPPRC_CVPCodeMaxSellPrice] [decimal] (15, 5) NULL,
[FCRSCD_FrCostReasonCode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FCPRWN_FrCostProcessingWindow] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PHUSR1_LoadedFrCostforSplits] [decimal] (15, 5) NULL,
[PHUSR2_UserField2] [decimal] (15, 5) NULL,
[PHUSRA_ProposedFrCostReasonCd] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PHUSRB_UserFieldB] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PHUSRC_UserFieldC] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_DefaultPriceChangeUpdates_CreatedDate] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_DefaultPriceChangeUpdates_CreatedBy] DEFAULT (suser_name()),
[ModifiedDate] [datetime] NULL,
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsNew] [bit] NULL CONSTRAINT [DF_DefaultPriceChangeUpdates_IsNew] DEFAULT ((1)),
[IsModified] [bit] NULL,
[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO

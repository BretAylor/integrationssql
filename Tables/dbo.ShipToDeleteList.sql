CREATE TABLE [dbo].[ShipToDeleteList]
(
[SHIPTO] [numeric] (3, 0) NULL,
[CustomerNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateCreated] [datetime] NULL CONSTRAINT [DF_ShipToDeleteList_DateCreated] DEFAULT (getdate()),
[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ShipToDeleteList_CreatedBy] DEFAULT (suser_name())
) ON [PRIMARY]
GO

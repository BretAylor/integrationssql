CREATE TABLE [dbo].[DimGenericCode]
(
[GenericCodeKey] [int] NOT NULL IDENTITY(1, 1),
[CCCONO] [numeric] (2, 0) NULL,
[CCAPID] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCCTLK] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCRCCD] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCDATA] [varchar] (243) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCCONOSup] [numeric] (2, 0) NULL,
[CCAPIDSup] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCCTLKSup] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCRCCDSup] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCDATASup] [varchar] (243) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimGenericCode] ADD CONSTRAINT [PK_DimGenericCode] PRIMARY KEY CLUSTERED  ([GenericCodeKey]) ON [PRIMARY]
GO

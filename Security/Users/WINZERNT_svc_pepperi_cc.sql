IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'WINZERNT\svc_pepperi_cc')
CREATE LOGIN [WINZERNT\svc_pepperi_cc] FROM WINDOWS
GO
CREATE USER [WINZERNT\svc_pepperi_cc] FOR LOGIN [WINZERNT\svc_pepperi_cc]
GO

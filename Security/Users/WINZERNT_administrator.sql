IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'WINZERNT\administrator')
CREATE LOGIN [WINZERNT\administrator] FROM WINDOWS
GO
CREATE USER [WINZERNT\administrator] FOR LOGIN [WINZERNT\administrator]
GO

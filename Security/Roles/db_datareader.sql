ALTER ROLE [db_datareader] ADD MEMBER [NT AUTHORITY\ANONYMOUS LOGON]
GO
ALTER ROLE [db_datareader] ADD MEMBER [WINZERNT\brush]
GO
ALTER ROLE [db_datareader] ADD MEMBER [WINZERNT\svc_pepperi_cc]
GO
ALTER ROLE [db_datareader] ADD MEMBER [WINZERNT\twenzel]
GO
ALTER ROLE [db_datareader] ADD MEMBER [WINZERNT\WINSQLBI01$]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[xxSalesOrderCustomerStillOnHoldNotification]
AS
BEGIN

DECLARE @xml NVARCHAR(MAX)
DECLARE @body NVARCHAR(MAX)
declare @Rowcount int
declare @RunDate datetime = getdate()

-- Error Processing
Begin Try Drop table #ErrorTable  end try Begin catch End Catch

Create table  #ErrorTable  
(ErrorMessage varchar(255) Not null
, PepperiOrderNbr varchar(255) not Null
, CustomerNbr varchar(255) not null)

-- Current Errors
Insert #ErrorTable 
(ErrorMessage, PepperiOrderNbr, CustomerNbr) 
Select [on hold],PepperiOrderNbr, CustomerNbr
FROM [172.25.6.17].[Pepperi_Staging].[dbo].[SalesOrderHeader_LinesValidationView] 
WHERE ISNULL(SalesOrderCreatedDate,SalesOrderLineCreatedDate) >'9/20/2019'
AND  [On Hold] IN ('Pepperi Customer Not in Cross Reference','Sales Order without Customer')

Select  @RowCount = @@ROWCOUNT

-- Update Resolved
update em
set DateResolved = @RunDate
,DateUpdate = @RunDate
FROM [rpt].[ErrorMessages] em
left join #ErrorTable e
on e.CustomerNbr = em.customernbr
and e.PepperiOrderNbr = em.PepperiOrderNbr
and em.ErrorMessage = e.ErrorMessage
where em.ReportID = 1
and em.DateResolved is null
and e.CustomerNbr is null 

Select  @RowCount = @Rowcount + @@ROWCOUNT

If @Rowcount > 0
Begin
-- Update Those Still in Error
update em
set DateLastObserved = @RunDate
,DateUpdate = @RunDate
FROM #ErrorTable e,
[rpt].[ErrorMessages] em
where em.ReportID = 1
and em.CustomerNbr = em.CustomerNbr
and em.PepperiOrderNbr = e.PepperiOrderNbr
and em.DateResolved is null
and em.ErrorMessage = e.ErrorMessage

-- Insert New
INSERT INTO [rpt].[ErrorMessages]
           ([ReportID]
           ,[CustomerNbr]
           ,[PepperiOrderNbr]
           ,[ErrorMessage]
		   ,DateFirstObserved
		   ,DateLastObserved)

select		1	[ReportID]
           ,e.[CustomerNbr]
           ,e.[PepperiOrderNbr]
           ,e.[ErrorMessage]
		   ,@RunDate
		   ,@Rundate
FROM #ErrorTable e
left join [rpt].[ErrorMessages] em
on e.CustomerNbr = em.CustomerNbr
and e.PepperiOrderNbr = em.PepperiOrderNbr
and em.ErrorMessage = e.ErrorMessage
where em.CustomerNbr is null 
and em.DateResolved is null

SET @xml = CAST(( 
SELECT [CustomerNbr] AS 'td',''
,[PepperiOrderNbr] AS 'td',''
,CONVERT(VARCHAR, DateFirstObserved, 120) AS 'td',''
,CONVERT(VARCHAR, DateLastObserved, 120) AS 'td',''
,case when DateResolved is null
then '          ' 
else CONVERT(VARCHAR, DateResolved, 120) end AS 'td',''
,ErrorMessage AS 'td',''
FROM [rpt].[ErrorMessages]
where reportid = 1
and (dateresolved is null
or DateUpdate = @RunDate)
ORDER BY [CustomerNbr],[PepperiOrderNbr],cast(DateFirstObserved as datetime) desc
FOR XML PATH('tr'), ELEMENTS ) AS NVARCHAR(MAX))

SET @body ='<html><body><H3>Sales Orders On Hold</H3>
<table border = 1> 
<tr>
<th> Customer Nbr </th> <th> Pepperi Order Nbr </th> <th> First Observed </th> <th> Last Observed </th> <th> Date Resolved </th> <th> Error Message </th></tr>'    
SET @body = @body + @xml +'</table></body></html>'

EXEC msdb.dbo.sp_send_dbmail  
    @profile_name = 'Account',  
    @recipients = 'bret.aylor@winzerusa.com',  
    @body = @body ,  
    @subject = 'Sales Orders On Hold',  
    @attach_query_result_as_file = 0,
	@body_format = 'HTML'
end

END
GO

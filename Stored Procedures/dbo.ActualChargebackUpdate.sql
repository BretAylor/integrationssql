SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create Procedure [dbo].[ActualChargebackUpdate]
 as
 Begin

 update AC set 
AC.[PadDescription] = ACU.[PadDescription],
AC.[ProcessDate] = ACU.[ProcessDate],
AC.[SequenceNum] = ACU.[SequenceNum],
AC.[ReportLine]  = ACU.[ReportLine]
,AC.ModifiedDate = getdate()
,AC.ModifiedBy = suser_name()
,AC.ErpSyncDate = getdate()
,AC.ErpSyncBy = suser_name()

from WINSQLBI01.Pepperi.dbo.ActualChargeback ACU with (nolock) 
INNER join dbo.ActualChargeback  AC with (nolock)
on AC.[PadDescription] = ACU.[PadDescription] AND AC.[ProcessDate] = ACU.[ProcessDate] 
AND AC.[SequenceNum] = ACU.[SequenceNum]
WHERE IsModified = 1 

 end

GO

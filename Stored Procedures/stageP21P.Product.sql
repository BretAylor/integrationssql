SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		baylor
-- Create date: 4/2020
-- Description:	Update to Pepperi_Staging 
-- =============================================
create PROCEDURE [stageP21P].[Product] 
AS

BEGIN

SET NOCOUNT ON;


select distinct '' AS CorpItemId, 
                pr.ProductNumber AS BrandedItemId, 
				pr.ProductDescription AS ItemDesc , 
				pr.ItemGenericDESCRIPTION AS ItemGenDesc, 
				pr.ReadId, 
				pr.FranchiseCost AS FrnchCst, 
				pr.Proposedfranchisecost AS PrpsdFrnchCst, 
				pr.LandedCost AS LandedCst, 
				pr.Landedcostpriceclass AS LandedCstCls, 
				pr.RecommendedPrice AS RcmddPrc, 
				pr.ITEMTAXABLEFLAG AS ItemSalesTaxCls, 
				pr.PACKAGEQUANTITY AS PkgQty, 
				pr.ENFORCEPKGQUANTITY AS EnfrcPkgQty, 
				pr.UnitofMeasure AS BaseUnit, 
                pr.ProductPriceClass AS ItemPrcCls, 
				pr.ItemClass AS ItemCls, 
				pr.KitIndicator AS BomItem, 
				pr.BigAssortment AS BigAssortmnt, 
				pr.ItemIsBomComponent AS BomCompo, 
				pr.ItemsoldonlyasCompone as ItemSldOnlyBom, 
				pr.PA1BOK AS Pa1bOk, 
				pr.ExceptionFreightTYPE AS ItemFrtTyp, 
				pr.ProductRestrictionCode AS ItemRestrCde, 
				pr.HAZMATCode AS ItemHazmatCde,
				pr.MSDSRequiredItem AS ItemMsdsReqd, 
				pr.HazardousMaterial AS HazmatRestr, 
				pr.PrivateLabel AS PrvtLbl, 
				pr.ManufacturerVendorNumbe AS SuplrId, 
                pr.ManufacturerName AS SuplrNme, 
				pr.ManufacturerItemNumber AS SuplrItemId, 
				pr.VendorLeadTIME AS DateCreated, 
				pr.ItemSequence# AS ItemSeqNbr, 
				pr.CountryofOrigin AS ItemCoo, 
				pr.CountryofOrigin AS CooName, 
				pr.GSACountryMESSAGE AS CooGsaMsg, 
				pr.BigAssortment AS CooGsaRestricted, 
				pr.UnitWeight AS Weight, 
				pr.ObsoleteReasonCode AS ObsolCde, 
				pr.ObsoleteFlag  AS ObsolFlg, 
				pr.DropShipItem AS DirectShip, 
				pr.ItemShipSource AS ItemShpSrc , 
				NULL AS ShipSuplr ,
			    CANREPCHANGEITEMDESC  AS RepChgItemDesc, 'N' AS DeleteFlag, c.Id as CategoryId,
left(ItemGenericDescription,30) as ItemShortDescription

from Pepperi.dbo.ProductAS400 pr  WITH (nolock)
	left join pepperi.dbo.InsiteProductCategory pc WITH (nolock) ON pr.ProductNumber = pc.ErpNbr
	left join PEPPERI.dbo.InsiteCategory c WITH (nolock) ON pc.id = c.CategoryLevel3Id

END
GO

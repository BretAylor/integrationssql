SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BOMDelete]
 as
 Begin
Update BOS set BOS.[Delete] = 1 
from Pepperi_Staging.dbo.BOM BOS 
INNER join WINSQLBI01.Pepperi.dbo.BOM BOW 
ON BOS.ParentItemNum  = BOW.ParentItemNum and BOS.ComponentItemNum = BOW.ComponentItemNum
where BOW.IsDeleted = 1 
end

GO

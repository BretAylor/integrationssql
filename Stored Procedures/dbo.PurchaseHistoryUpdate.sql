SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[PurchaseHistoryUpdate]
AS
Begin

Set Transaction Isolation Level Read Uncommitted 

DROP TABLE IF EXISTS #Hist
SELECT * INTO #Hist FROM WINSQLBI01.Pepperi.dbo.PurchaseHistory WHERE IsModified = 1

UPDATE PH1 
SET 
PH1.InvoiceNumber =           PH2.InvoiceNumber,
PH1.OrderNumber =             PH2.OrderNumber,
PH1.ProductDescription =      PH2.ProductDescription,
PH1.OrderQuantity =           PH2.OrderQuantity,
PH1.InvoiceQuantity =         PH2.InvoiceQuantity,
PH1.BackorderQuantity =       PH2.BackorderQuantity,
PH1.NoChargeReasonCode =      PH2.NoChargeReasonCode,
PH1.InvoiceDate =             PH2.InvoiceDate,
PH1.OrderDate =               PH2.OrderDate,
PH1.LastGrossMargin =         PH2.LastGrossMargin,
PH1.LastUnitSalesPrice =      PH2.LastUnitSalesPrice,
PH1.YtdOrderedQuantity =      PH2.YtdOrderedQuantity,
PH1.YtdSaleDollars =          PH2.YtdSaleDollars,
PH1.CustomerPartNumber =      PH2.CustomerPartNumber,
PH1.Comment =                 PH2.Comment,
PH1.ModifiedDate = getdate(), PH1.ModifiedBy = suser_name()
FROM Pepperi_Staging.dbo.PurchaseHistory PH1 
INNER JOIN #Hist PH2
on PH1.CustomerNumber = PH2.CustomerNumber 
And PH1.SalesRepNumber = PH2.SalesRepNumber 
And PH1.ProductNumber = PH2.ProductNumber 
where PH2.IsModified = 1

End



GO

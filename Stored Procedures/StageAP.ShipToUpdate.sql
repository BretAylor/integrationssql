SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		baylor
-- alter date: 05/20
-- Description:	Update ShipTo
-- =============================================
CREATE PROCEDURE [StageAP].[ShipToUpdate] 

AS
BEGIN

SET NOCOUNT ON;

-- Update CustomerNumber
UPDATE t1 
SET CustomerNumber = t3.CustomerNumber
FROM StageAP.ShipTo t1
JOIN dbo.CustomerCrossReference t2
ON t1.CustomerNumber = t2.PepperiCustomerId
JOIN stageAP.CustomerTempNumber t3
ON t2.AS400TempCustomerId = t3.TempCustomerNumber
WHERE t1.Customernumber <> t3.CustomerNumber

UPDATE t1 
SET CustomerNumber = t3.CustomerNumber
FROM dbo.ShipTo t1
JOIN dbo.CustomerCrossReference t2
ON t1.CustomerNumber = t2.PepperiCustomerId
JOIN stageAP.CustomerTempNumber t3
ON t2.AS400TempCustomerId = t3.TempCustomerNumber
WHERE t1.Customernumber <> t3.CustomerNumber

UPDATE s
SET
 s.Name = u.Name,
 s.Contact = u.Contact,
 s.Address1 = u.Address1,
 s.Address2 = u.Address2,
 s.City = u.City,
 s.State = u.State,
 s.ZipCode = u.ZipCode,
 s.Country = u.Country,
 s.PhoneNumber = u.PhoneNumber,
 s.PhoneExtension = u.PhoneExtension,
-- s.IsDefaultShipTo = u.IsDefaultShipTo,
 s.DefaultWarehouse = u.DefaultWarehouse,
 s.DefaultShipVia = u.DefaultShipVia,
 s.DefaultFreightChargeCode = u.DefaultFreightChargeCode,
 s.TaxCode = u.TaxCode,
-- s.DefaultPONumber = u.DefaultPONumber ,  -- Not updated in AS400
 s.Deleted = 0,  -- Reinstate,
S.ModifiedDate = getdate(), 
S.ModifiedBy = suser_name(),
S.ErpModifiedDate = getdate(), 
S.ErpModifiedBy = suser_name()
FROM dbo.ShipTo  AS S WITH (nolock)
    INNER JOIN StageAP.ShipTo AS U WITH (nolock)
   ON s.CustomerNumber = U.CustomerNumber
   AND s.ShipToNumber = u.ShipToNumber
WHERE s.CustomerNumber < 1000000
AND (ISNULL(s.Name,'') <> ISNULL(u.Name,'')
  or ISNULL(s.Contact,'') <> ISNULL(u.Contact,'')
  or ISNULL(s.Address1,'') <> ISNULL(u.Address1,'')
  or ISNULL(s.Address2,'') <> ISNULL(u.Address2,'')
  or ISNULL(s.City ,'')<> ISNULL(u.City,'')
  or ISNULL(s.State,'') <> ISNULL(u.State,'')
  or ISNULL(s.ZipCode,'') <> ISNULL(u.ZipCode,'')
  or ISNULL(s.PhoneNumber,0) <> ISNULL(u.PhoneNumber,0)
	 or ISNULL(s.PhoneExtension,0) <> ISNULL(u.PhoneExtension,0)
 -- or ISNULL(s.IsDefaultShipTo,0) <> ISNULL(u.IsDefaultShipTo,0)
  or ISNULL(s.DefaultWarehouse,'') <> ISNULL(u.DefaultWarehouse,'')
  or ISNULL(s.DefaultShipVia,'') <> ISNULL(u.DefaultShipVia,'')
  or ISNULL(s.DefaultFreightChargeCode,'') <> ISNULL(u.DefaultFreightChargeCode,'')
  or ISNULL(s.TaxCode,'') <> ISNULL(u.TaxCode,'')
  or ISNULL(s.DefaultPONumber,'') <> ISNULL(u.DefaultPONumber,'')
  or s.Deleted = 1)

-- Insert New
INSERT INTO [dbo].[ShipTo]
           ([CustomerNumber]
           ,[ShipToNumber]
           ,[PepperiShipToId]
           ,[Name]
           ,[Contact]
           ,[Address1]
           ,[Address2]
           ,[City]
           ,[State]
           ,[ZipCode]
           ,[Country]
           ,[PhoneNumber]
           ,[PhoneExtension]
--	       ,[IsDefaultShipTo]
           ,[DefaultWarehouse]
           ,[DefaultShipVia]
           ,[DefaultFreightChargeCode]
--           ,[DefaultPONumber]
           ,[TaxCode]
		   ,ErpModifiedDate
			,ErpModifiedBy 
 )
Select s.[CustomerNumber]
           ,s.[ShipToNumber]
           ,s.[PepperiShipToId]
           ,s.[Name]
           ,s.[Contact]
           ,s.[Address1]
           ,s.[Address2]
           ,s.[City]
           ,s.[State]
           ,s.[ZipCode]
           ,s.[Country]
           ,s.[PhoneNumber]
           ,s.[PhoneExtension]
--           ,s.[IsDefaultShipTo]
           ,s.[DefaultWarehouse]
           ,s.[DefaultShipVia]
           ,s.[DefaultFreightChargeCode]
 --          ,s.[DefaultPONumber]
           ,s.[TaxCode]
		   ,getdate()
		   ,suser_name()
FROM StageAP.ShipTo  AS S WITH (nolock)
Left JOIN dbo.ShipTo AS U WITH (nolock)
ON s.CustomerNumber = U.CustomerNumber
AND s.ShipToNumber = u.ShipToNumber
where s.CustomerNumber < 1000000
AND u.ShipToNumber is null

-- Mark as Deleted 
UPDATE s
SET
s.Deleted = 1,  
S.ModifiedDate = getdate(), 
S.ModifiedBy = suser_name(),
S.ErpModifiedDate = getdate(), 
S.ErpModifiedBy = suser_name()
FROM dbo.ShipTo  AS S WITH (nolock)
Left JOIN StageAP.ShipTo AS U WITH (nolock)
ON s.CustomerNumber = U.CustomerNumber
AND s.ShipToNumber = u.ShipToNumber
WHERE u.ShipToNumber is NULL
and s.deleted = 0
AND s.CustomerNumber < 1000000

END

GO

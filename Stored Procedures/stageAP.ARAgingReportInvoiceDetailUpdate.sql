SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		baylor
-- Create date: 4/2020
-- Description:	Update to Pepperi_Staging 
-- =============================================
CREATE PROCEDURE [stageAP].[ARAgingReportInvoiceDetailUpdate] 
AS

BEGIN

SET NOCOUNT ON;

UPDATE t2 
   SET [CustomerNumber] = t1.CustomerNumber
      ,[PepperiCustomerNumber] = t1.PepperiCustomerNumber
      ,[CustomerName] = t1.CustomerName
      ,[SalesRepNumber] = t1.SalesRepNumber
      ,[PadDescription] = t1.PadDescription
      ,[SequenceNum] = t1.SequenceNum
      ,[ARAmountDue] = t1.ARAmountDue
      ,[ARAmountCurrent] = t1.ARAmountCurrent
      ,[AgeAmount030] = t1.AgeAmount030
      ,[AgeAmount060] = t1.AgeAmount060
      ,[AgeAmount090] = t1.AgeAmount090
      ,[AgeAmount120] = t1.AgeAmount120
      ,[InvoiceNumber] = t1.InvoiceNumber
      ,[InvoiceDate] = t1.InvoiceDate
      ,[InvoiceAmountDue] = t1.InvoiceAmountDue
      ,[ErpSyncDate] = getdate()
      ,[ModifiedDate] = getdate()
      ,[ModifiedBy] = SUSER_SNAME()
from [dbo].[ARAgingReportInvoiceDetail] t1
join [WUSA-SQL01-DFW].pepperi_staging.dbo.ARAgingReportInvoiceDetail t2
on ltrim(rtrim(t1.invoiceNumber)) = ltrim(rtrim(t2.invoiceNumber))
and ltrim(rtrim(t1.paddescription)) = ltrim(rtrim(t2.paddescription))
where t2.[CustomerNumber] <> t1.CustomerNumber
      or t2.[PepperiCustomerNumber] <> t1.PepperiCustomerNumber
      or t2.[CustomerName] <> t1.CustomerName
      or t2.[SalesRepNumber] <> t1.SalesRepNumber
      or t2.[SequenceNum] <> t1.SequenceNum
      or t2.[ARAmountDue] <> t1.ARAmountDue
      or t2.[ARAmountCurrent] <> t1.ARAmountCurrent
      or t2.[AgeAmount030] <> t1.AgeAmount030
      or t2.[AgeAmount060] <> t1.AgeAmount060
      or t2.[AgeAmount090] <> t1.AgeAmount090
      or t2.[AgeAmount120] <> t1.AgeAmount120
      or t2.[InvoiceDate] <> t1.InvoiceDate
      or t2.[InvoiceAmountDue] <> t1.InvoiceAmountDue

insert [WUSA-SQL01-DFW].pepperi_staging.dbo.ARAgingReportInvoiceDetail
([CustomerNumber]
           ,[PepperiCustomerNumber]
           ,[CustomerName]
           ,[SalesRepNumber]
           ,[PadDescription]
           ,[SequenceNum]
           ,[ARAmountDue]
           ,[ARAmountCurrent]
           ,[AgeAmount030]
           ,[AgeAmount060]
           ,[AgeAmount090]
           ,[AgeAmount120]
           ,[InvoiceNumber]
           ,[InvoiceDate]
           ,[InvoiceAmountDue])
  Select t1.[CustomerNumber]
           ,t1.[PepperiCustomerNumber]
           ,t1.[CustomerName]
           ,t1.[SalesRepNumber]
           ,t1.[PadDescription]
           ,t1.[SequenceNum]
           ,t1.[ARAmountDue]
           ,t1.[ARAmountCurrent]
           ,t1.[AgeAmount030]
           ,t1.[AgeAmount060]
           ,t1.[AgeAmount090]
           ,t1.[AgeAmount120]
           ,t1.[InvoiceNumber]
           ,t1.[InvoiceDate]
           ,t1.[InvoiceAmountDue]
from ARAgingReportInvoiceDetail t1 with (nolock)
left join [WUSA-SQL01-DFW].pepperi_staging.dbo.ARAgingReportInvoiceDetail t2
on ltrim(rtrim(t1.invoiceNumber)) = ltrim(rtrim(t2.invoiceNumber))
and ltrim(rtrim(t1.paddescription)) = ltrim(rtrim(t2.paddescription))
WHERE   t2.paddescription is null

delete [WUSA-SQL01-DFW].pepperi_staging.dbo.ARAgingReportInvoiceDetail
from [WUSA-SQL01-DFW].pepperi_staging.dbo.ARAgingReportInvoiceDetail t1
left Join ARAgingReportInvoiceDetail t2 with (nolock)
on ltrim(rtrim(t1.invoiceNumber)) = ltrim(rtrim(t2.invoiceNumber))
and ltrim(rtrim(t1.paddescription)) = ltrim(rtrim(t2.paddescription))
WHERE   t2.paddescription is null


END
GO

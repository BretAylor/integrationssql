SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[CreditCardLoad]
 as
 Begin
 
 -- New Credit card decrypt
                drop table if exists  #NewCreditCardDecrypt
			    create table 	#NewCreditCardDecrypt (
				[Id] [int] NOT NULL,
				[Guid] [uniqueidentifier] NOT NULL,
				[CCDescription] [varchar](200) NOT NULL,
				[CCType] [varchar](50) NOT NULL,
				[CCNumber] [varchar](128) NOT NULL,
				[CVVNumber] [varchar](4) NOT NULL,
				[LastFourDigits] [varchar](4) NOT NULL,
				[ExpirationDate] [varchar](10) NOT NULL,
				[CustomerNumber] [varchar](255) NOT NULL,
				[FirstName] [varchar](255) NOT NULL,
				[MiddleName] [varchar](50) NULL,
				[LastName] [varchar](255) NOT NULL,
				[ZipCode] [varchar](10) NOT NULL,
				[Note] [varchar](255) NULL,
				[KeepOnFile] [bit] NULL,
				[CreatedDate] [datetime] NOT NULL,
				[CreatedBy] [varchar](50) NOT NULL,
				[ModifiedDate] [datetime] NULL,
				[ModifiedBy] [varchar](50) NULL )
				insert into #NewCreditCardDecrypt ([Id]
					  ,[Guid]
					  ,[CCDescription]
					  ,[CCType]
					  ,[CCNumber]
					  ,[CVVNumber]
					  ,[LastFourDigits]
					  ,[ExpirationDate]
					  ,[CustomerNumber]
					  ,[FirstName]
					  ,[MiddleName]
					  ,[LastName]
					  ,[ZipCode]
					  ,[Note]
					  ,[KeepOnFile]
					  ,[CreatedDate]
					  ,[CreatedBy]
					  ,[ModifiedDate]
					  ,[ModifiedBy])
	            exec CC_STAGING_UAT.dbo.[usp_SelectAndDecryptCCards]

-- Existing Credit card decrypt
                drop table if exists  #ExistingCreditCardDecrypt
			    create table 	#ExistingCreditCardDecrypt (
				[Id] [int] NOT NULL,
				[Guid] [uniqueidentifier] NOT NULL,
				[CCDescription] [varchar](200)  NULL,
				[CCType] [varchar](50) NOT NULL,
				[CCNumber] [varchar](128) NOT NULL,
				[CVVNumber] [varchar](4) NULL,
				[LastFourDigits] [varchar](4) NOT NULL,
				[ExpirationDate] [varchar](10) NOT NULL,
				[CustomerNumber] [varchar](255) NOT NULL,
				[CustomerName] [varchar](255) NOT NULL,
				[ZipCode] [varchar](10) NULL,
				[Note] [varchar](255) NULL,
				[KeepOnFile] [bit] NULL,
				[CreatedDate] [datetime] NOT NULL,
				[CreatedBy] [varchar](50) NOT NULL,
				[ModifiedDate] [datetime] NULL,
				[ModifiedBy] [varchar](50) NULL )
				insert into #ExistingCreditCardDecrypt ([Id]
					  ,[Guid]
					  ,[CCDescription]
					  ,[CCType]
					  ,[CCNumber]
					  ,[CVVNumber]
					  ,[LastFourDigits]
					  ,[ExpirationDate]
					  ,[CustomerNumber]
					  ,[CustomerName]
					  ,[ZipCode]
					  ,[Note]
					  ,[KeepOnFile]
					  ,[CreatedDate]
					  ,[CreatedBy]
					  ,[ModifiedDate]
					  ,[ModifiedBy])
	            exec CC_STAGING_UAT.[dbo].[usp_SelectAndDecryptExistingCCards] 


 delete from CreditCardAll
 insert into CreditCardAll ([Guid], CustomerNumber,  [CreditCardNum],  ExpirationDate, [CreditCardHolder], [Type] )
 select * from (
 select  cast(CC.Guid as varchar(50))  as [Guid] , CC.CustomerNumber, CD.CCNumber [CreditCardNum], cast(replace(CC.ExpirationDate,'/','') as int) as ExpirationDate , CC.CCDescription as [CreditCardHolder] , 'Existing' as [Type]  
 FROM PEPPERI_STAGING_UAT.dbo.ExistingCustomerCC CC left outer join #ExistingCreditCardDecrypt CD on CC.Guid = CD.Guid and CC.CustomerNumber = CD.CustomerNumber
 union all 
 select  cast(CC.Guid as varchar(50))  as [Guid], CC.CustomerNumber, CD.CCNumber [CreditCardNum], cast(replace(CC.ExpirationDate,'/','') as int) as ExpirationDate , CC.CCDescription as [CreditCardHolder] , 'New' as [Type]  
 FROM CC_STAGING_UAT.dbo.CustomerCC CC left outer join #NewCreditCardDecrypt CD on CC.Guid = CD.Guid and CC.CustomerNumber = CD.CustomerNumber
 ) as a 


 end

GO

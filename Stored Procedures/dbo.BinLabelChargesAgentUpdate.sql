SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BinLabelChargesAgentUpdate]
 as
 Begin
UPDATE BL  set 
 BL.ReportLine = UP.ReportLine
,BL.ModifiedDate = getdate()
,BL.ModifiedBy = suser_name()
,BL.ErpSyncDate = getdate()
,BL.ErpSyncBy = suser_name()
FROM WINSQLBI01.PEPPERI.dbo.BinLabelChargesAgentUpdates UP with (nolock) 
INNER JOIN dbo.BinLabelChargesAgent BL with (nolock) ON 
UP.PadDescription = BL.PadDescription AND 
UP.ProcessDate   = BL.ProcessDate AND 
UP.SequenceNum = BL.SequenceNum
 end

GO

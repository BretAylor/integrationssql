SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		baylor
-- Create date: 5/20
-- Description:	Update ErpSyncDate
-- =============================================
CREATE PROCEDURE [dbo].[ExtractCustomerSync] 

AS
BEGIN

SET NOCOUNT ON;

Update t1 
set ErpSyncDate = getdate(),
ErpSyncby = suser_name(),
ErpModifieddate = getdate()
from [dbo].[Customer] t1
join [StagePA].[CustomerToProcess] t2  
on t1.customernumber = t2.CustomerNumber

End
GO

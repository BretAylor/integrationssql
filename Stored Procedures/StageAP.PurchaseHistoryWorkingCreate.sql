SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [StageAP].[PurchaseHistoryWorkingCreate]
AS
BEGIN

SET NOCOUNT ON

BEGIN TRY DROP TABLE [dbo].[PurchaseHistoryWorking] END TRY BEGIN CATCH END CATCH

CREATE TABLE [dbo].[PurchaseHistoryWorking](
	[CustomerNumber] [DECIMAL](7, 0) NOT NULL,
	[ProductNumber] [VARCHAR](15) NOT NULL,
	[SalesRepNumber] [DECIMAL](5, 0) NOT NULL,
	[ProductDescription] [NVARCHAR](30) NULL,
	[PSDate] [DATE] NULL,
	[InvoiceNumber] [DECIMAL](8, 0) NULL,
	[OrderNumber] [DECIMAL](8, 0) NULL,
	[OrderQuantity] [DECIMAL](7, 0) NULL,
	[InvoiceQuantity] [DECIMAL](7, 0) NULL,
	[BackorderQuantity] [DECIMAL](7, 0) NULL,
	[NoChargeReasonCode] [CHAR](2) NULL,
	[InvoiceDate] [DATE] NULL,
	[OrderDate] [DATE] NULL,
	[LastGrossMargin] [DECIMAL](5, 4) NULL,
	[LastUnitSalesPrice] [DECIMAL](11, 4) NULL,
	[YtdOrderedQuantity] [DECIMAL](7, 0) NULL,
	[YtdSaleDollars] [DECIMAL](11, 4) NULL,
	[CustomerPartNumber] [NVARCHAR](50) NULL,
	[Comment] [NVARCHAR](62) NULL,
	[CreatedDate] [DATETIME] NOT NULL,
	[CreatedBy] [VARCHAR](50) NOT NULL,
	[ModifiedDate] [DATETIME] NULL,
	[ModifiedBy] [VARCHAR](50) NULL,
	PRIMARY KEY (CustomerNumber, ProductNumber)
) ON [PRIMARY]

end
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [StageAP].[PurchaseHistoryUpdate]
AS
BEGIN

SET NOCOUNT ON

Set Transaction Isolation Level Read Uncommitted 

DECLARE @RunDate DATETIME = GETDATE()

TRUNCATE TABLE [StageAP].[PurchaseHistoryWorking]
TRUNCATE TABLE [StageAP].[PurchaseHistoryWorkingDedup]

-- From staging
INSERT INTO [StageAP].[PurchaseHistoryWorking]
           ([CustomerNumber]
           ,[SalesRepNumber]
           ,[ProductNumber]
		   ,CompareDate
           ,[ProductDescription]
           ,[PSDate]
           ,[InvoiceNumber]
           ,[OrderNumber]
           ,[OrderQuantity]
           ,[InvoiceQuantity]
           ,[BackorderQuantity]
           ,[NoChargeReasonCode]
           ,[InvoiceDate]
           ,[OrderDate]
           ,[LastGrossMargin]
           ,[LastUnitSalesPrice]
           ,[YtdOrderedQuantity]
           ,[YtdSaleDollars]
		   ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[CustomerPartNumber]
           ,[Comment]
		   )
SELECT [CustomerNumber]
           ,[SalesRepNumber]
           ,TRIM([ProductNumber])
		   ,CASE when
		   CASE WHEN LEN(CAST(psdate AS VARCHAR(20))) = 8
			THEN 
			CAST(SUBSTRING(CAST(psdate AS VARCHAR(20)),5,2) + '/' +
			SUBSTRING(CAST(psdate AS VARCHAR(20)),7,2) + '/' +
			SUBSTRING(CAST(psdate AS VARCHAR(20)),1,4) AS DATE) 
			ELSE CAST('01/01/1900' AS DATE) 
			END > ISNULL(OrderDate,'01/01/1900')
			THEN 
			CAST(SUBSTRING(CAST(psdate AS VARCHAR(20)),5,2) + '/' +
			SUBSTRING(CAST(psdate AS VARCHAR(20)),7,2) + '/' +
			SUBSTRING(CAST(psdate AS VARCHAR(20)),1,4) AS DATE)
			ELSE
            ISNULL(OrderDate,'01/01/1900') END CompareDate
           ,[ProductDescription]
           ,CASE WHEN LEN(CAST(psdate AS VARCHAR(20))) = 8
			THEN 
			CAST(SUBSTRING(CAST(psdate AS VARCHAR(20)),5,2) + '/' +
			SUBSTRING(CAST(psdate AS VARCHAR(20)),7,2) + '/' +
			SUBSTRING(CAST(psdate AS VARCHAR(20)),1,4) AS DATE) 
			ELSE CAST('01/01/1900' AS DATE) END  [PSDate]
           ,[InvoiceNumber]
           ,[OrderNumber]
           ,[OrderQuantity]
           ,[InvoiceQuantity]
           ,[BackorderQuantity]
           ,[NoChargeReasonCode]
           ,[InvoiceDate]
           ,[OrderDate]
           ,[LastGrossMargin]
           ,[LastUnitSalesPrice]
           ,[YtdOrderedQuantity]
           ,[YtdSaleDollars]
	       ,@RunDate [CreatedDate]
           ,SUSER_NAME() [CreatedBy]
           ,@RunDate [ModifiedDate]
           ,SUSER_NAME() [ModifiedBy]
           ,[CustomerPartNumber]
           ,[Comment]
FROM stageap.purchasehistory

UNION ALL 
-- From dbo
SELECT  t1.[CustomerNumber]
           ,t1.[SalesRepNumber]
           ,TRIM(t1.[ProductNumber])
		   ,CASE WHEN ISNULL(t1.PSDate,'01/01/1900') > ISNULL(t1.OrderDate,'01/01/1900')
		   THEN t1.PSDate
		   ELSE t1.OrderDate END CompareDate
           ,t1.[ProductDescription]
           ,t1.[PSDate]
           ,t1.[InvoiceNumber]
           ,t1.[OrderNumber]
           ,t1.[OrderQuantity]
           ,t1.[InvoiceQuantity]
           ,t1.[BackorderQuantity]
           ,t1.[NoChargeReasonCode]
           ,t1.[InvoiceDate]
           ,t1.[OrderDate]
           ,t1.[LastGrossMargin]
           ,t1.[LastUnitSalesPrice]
           ,t1.[YtdOrderedQuantity]
           ,t1.[YtdSaleDollars]
		   ,t1.[CreatedDate]
           ,t1.[CreatedBy]
           ,t1.[ModifiedDate]
           ,t1.[ModifiedBy]
           ,t1.[CustomerPartNumber]
           ,t1.[Comment]
-- If record does not match to staging, then consider it deleted
FROM [dbo].[PurchaseHistory] t1
JOIN stageap.purchasehistory t2
ON t1.CustomerNumber = t2.CustomerNumber
AND t1.ProductNumber = TRIM(t2.ProductNumber)


-- Dedup
INSERT INTO [StageAP].[PurchaseHistoryWorkingDedup]
           (LineNbr
		   ,[CustomerNumber]
           ,[SalesRepNumber]
           ,[ProductNumber]
		   ,CompareDate
           ,[ProductDescription]
           ,[PSDate]
           ,[InvoiceNumber]
           ,[OrderNumber]
           ,[OrderQuantity]
           ,[InvoiceQuantity]
           ,[BackorderQuantity]
           ,[NoChargeReasonCode]
           ,[InvoiceDate]
           ,[OrderDate]
           ,[LastGrossMargin]
           ,[LastUnitSalesPrice]
           ,[YtdOrderedQuantity]
           ,[YtdSaleDollars]
           ,[CustomerPartNumber]
           ,[Comment]
		   ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
)
SELECT 
ROW_NUMBER() OVER (
    PARTITION BY CustomerNumber, ProductNumber
    ORDER BY CompareDate desc, createddate, modifiedDate) Linenbr
	,[CustomerNumber]
           ,[SalesRepNumber]
           ,[ProductNumber]
		   ,CompareDate
           ,[ProductDescription]
           ,[PSDate]
           ,[InvoiceNumber]
           ,[OrderNumber]
           ,[OrderQuantity]
           ,[InvoiceQuantity]
           ,[BackorderQuantity]
           ,[NoChargeReasonCode]
           ,[InvoiceDate]
           ,[OrderDate]
           ,[LastGrossMargin]
           ,[LastUnitSalesPrice]
           ,[YtdOrderedQuantity]
           ,[YtdSaleDollars]
           ,[CustomerPartNumber]
           ,[Comment]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
FROM [StageAP].[PurchaseHistoryWorking]
WHERE orderdate > DATEADD(YEAR,-2,GETDATE()) 
OR psdate > DATEADD(MONTH, -6, GETDATE())

INSERT [dbo].[PurchaseHistoryWorking]
           ([CustomerNumber]
           ,[SalesRepNumber]
           ,[ProductNumber]
           ,[ProductDescription]
           ,[PSDate]
           ,[InvoiceNumber]
           ,[OrderNumber]
           ,[OrderQuantity]
           ,[InvoiceQuantity]
           ,[BackorderQuantity]
           ,[NoChargeReasonCode]
           ,[InvoiceDate]
           ,[OrderDate]
           ,[LastGrossMargin]
           ,[LastUnitSalesPrice]
           ,[YtdOrderedQuantity]
           ,[YtdSaleDollars]
           ,[CustomerPartNumber]
           ,[Comment]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy])
SELECT 		   [CustomerNumber]
           ,[SalesRepNumber]
           ,[ProductNumber]
           ,[ProductDescription]
           ,[PSDate]
           ,[InvoiceNumber]
           ,[OrderNumber]
           ,[OrderQuantity]
           ,[InvoiceQuantity]
           ,[BackorderQuantity]
           ,[NoChargeReasonCode]
           ,[InvoiceDate]
           ,[OrderDate]
           ,[LastGrossMargin]
           ,[LastUnitSalesPrice]
           ,[YtdOrderedQuantity]
           ,[YtdSaleDollars]
           ,[CustomerPartNumber]
           ,[Comment]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
FROM [StageAP].[PurchaseHistoryWorkingDedup]
WHERE linenbr = 1

BEGIN TRANSACTION
EXEC sp_Rename PurchaseHistory, PurchaseHistory_Old
EXEC sp_Rename PurchaseHistoryWorking, PurchaseHistory
DROP TABLE PurchaseHistory_Old
COMMIT

TRUNCATE TABLE [StageAP].[PurchaseHistory]
TRUNCATE TABLE [StageAP].[PurchaseHistoryWorking]
TRUNCATE TABLE [StageAP].[PurchaseHistoryWorkingDedup]

end
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ExtractShipTo] 

AS
BEGIN

SET NOCOUNT ON;

select  ISNULL(RIGHT('00000' + Cast(CU.SalesRepNumber as varchar(20)),5),'') as PadDescription  , 
case when left(ST.CustomerNumber,8) > 1000000 then AS400TempCustomerId  else isnull(left(ST.CustomerNumber,7),0) end as CustomerNumber,
isnull(SR.TerritoryNum,0)  as TerritoryNumber,
ISNULL(CU.SalesRepNumber,0) as SalesRepNum,
isnull(ST.ShipToNumber,0) as ShipToNumber,
isnull(left(ST.Name,30),'')  as ShipToName,
isnull(left(ST.Contact,25),'') as ShipToContact,
isnull(ST.Address1,'' )as ShipToAddress1,
isnull(ST.Address2,'') as ShipToAddress2,
isnull(ST.City,'') as ShipToCity,
isnull(ST.State,'' )as ShipToState,
isnull(ST.ZipCode,'') as ShipToZipCode,
isnull(ST.PhoneNumber,0 ) PhoneNumber,
isnull(left(ST.PhoneExtension,4) ,0   ) PhoneExtension ,
isnull(ST.TaxCode, ''  )  TaxCode ,
isnull(ST.DefaultShipVia,''  )  DefaultShipVia,
isnull(ST.DefaultFreightChargeCode,  '') DefaultFreightChargeCode ,
0 as Fax ,-- does not exist check
'' as ShipToContact2 ,
0 as ShipToPhone2        ,
0 as ShipToExtension2	  ,
0 as ShipToFax2		  ,
'' as ShipToRoute		  ,
'' as ATTNEmail			  ,
'' as AlternateEmail	  ,
'' as LocationDirections  ,
'' as ReceivingHours	  ,
'' as DefaultPONumber 	  ,
'' as DefaultWarehouse ,
'' as Use3rdPartyAccount  ,
'' as ThirdCarrierCompany   ,
'' as ThirdPartyAccount	   ,
'' as ThirdValidationField  ,
'' as UseExtendedAddress  ,
'' as ExtendedAddress1    ,
'' as ExtendedAddress2    ,
'' as ExtendedAddress3    ,
'' as ExtendedAddress4    ,
ISNULL(PepperiShipToId,'') PepperiShipToId ,
1 as IsNew,
ST.ErpModifiedDate,
ST.ModifiedDate,
ST.ModifiedBy,
ST.ErpSyncDate
from [dbo].[ShipTo] ST WITH (nolock)
LEFT outer join dbo.Customer CU  WITH (nolock)
on left(CU.CustomerNumber,8) = left(ST.CustomerNumber,8) 
left outer join dbo.SalesRep SR WITH (nolock) 
ON SR.SalesRepresentativeNum = CU.SalesRepNumber
left outer join [dbo].[CustomerCrossReference] CR WITH (nolock) 
ON left(CR.PepperiCustomerId,8) = left(ST.CustomerNumber,8)
where cast(isnull(st.modifiedDate,'12/31/2050') as datetime) > cast(isnull(st.erpsyncdate,'01/01/1900') as datetime)
  and st.deleted = 0   -- Deletes Not Processed in AS400
-- Based on Time, not working 5/14/2020
--where ST.CustomerNumber not in (select CustomerNumber from ShipToValidationView)

END
GO

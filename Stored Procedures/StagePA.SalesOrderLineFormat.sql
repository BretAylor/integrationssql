SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		baylor
-- Create date: 05/20
-- Description:	Format SalesOrderLine for Submission to AS400
-- =============================================
CREATE PROCEDURE [StagePA].[SalesOrderLineFormat] 

AS
BEGIN

SET NOCOUNT ON;

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

DECLARE @RunDate DATETIME = (SELECT MAX(createdDate) FROM StagePA.SalesOrderToProcess)

DELETE StagePA.SalesOrderLineFormatted WHERE DATEADD(mm,-3,@RunDate) > CreatedDate  

INSERT INTO [StagePA].[SalesOrderLineFormatted]
           ([RepNbrPad]
           ,[CustomerNbr]
           ,[PepperiOrderNbr]
           ,[LineNbr]
           ,[BrandedProductNbr]
           ,[PepperiId]
           ,[WinzerProductNbr]
           ,[OrderQuantity]
           ,[OrderPrice]
           ,[PriceOverrideFlag]
           ,[OverrideMinimumQuantity]
           ,[OrderCost]
           ,[DisplayComment]
           ,[DetailCommentCode]
           ,[UnitOfMeasure]
           ,[NoChargeReasonCode]
           ,[LabelQty]
           ,[Pa1bFlag]
           ,[NbrOfBags]
           ,[FdpReference]
           ,[FranchiseCostReasonCd]
           ,[CVPCode]
           ,[SpecialFrCostId]
           ,[CVPCodeMinQty]
           ,[CVPCodeMaxSellPrice]
           ,[CustomerContract]
           ,[BasePrice]
           ,[CommissionCode]
           ,[CommissionPercent]
           ,[MarkupPercent]
           ,[MarkupAmount]
           ,[FreightUpChargeCode]
           ,[FreightUpChargePercent]
           ,[FreightUpChargeAmount]
           ,[OriginalFranchisePrice]
           ,[LandedCost]
           ,[Comment]
           ,[ItemDescription]
           ,[LineItemType]
           ,[USERFA]
           ,[CreatedDate])
SELECT [RepNbrPad]
           ,[CustomerNbr]
           ,[PepperiOrderNbr]
           ,[LineNbr]
           ,[BrandedProductNbr]
           ,[PepperiId]
           ,[WinzerProductNbr]
           ,[OrderQuantity]
           ,[OrderPrice]
           ,[PriceOverrideFlag]
           ,[OverrideMinimumQuantity]
           ,[OrderCost]
           ,[DisplayComment]
           ,[DetailCommentCode]
           ,[UnitOfMeasure]
           ,[NoChargeReasonCode]
           ,[LabelQty]
           ,[Pa1bFlag]
           ,[NbrOfBags]
           ,[FdpReference]
           ,[FranchiseCostReasonCd]
           ,[CVPCode]
           ,[SpecialFrCostId]
           ,[CVPCodeMinQty]
           ,[CVPCodeMaxSellPrice]
           ,[CustomerContract]
           ,[BasePrice]
           ,[CommissionCode]
           ,[CommissionPercent]
           ,[MarkupPercent]
           ,[MarkupAmount]
           ,[FreightUpChargeCode]
           ,[FreightUpChargePercent]
           ,[FreightUpChargeAmount]
           ,[OriginalFranchisePrice]
           ,[LandedCost]
           ,[Comment]
           ,[ItemDescription]
           ,[LineItemType]
           ,[USERFA]
           ,[CreatedDate]
FROM 
( -- Non comment
SELECT 
       ISNULL(RIGHT('00000' + Cast(SO.[RepNbr] as varchar(20)),5),'')  as RepNbrPad,
/*	    case when ISNULL(CustomerNbr,0) > 1000000 
		THEN CR.AS400TempCustomerId else ISNULL(CustomerNbr,0) 
		END as CustomerNbr, */
		CustomerNbr,
	   OL.AS400OrderNbr PepperiOrderNbr,
	   T1.LineNbr as LineNbr,
	   ISNULL(left(ltrim(RTRIM(ol.[BrandedProductNbr])),15),'') as BrandedProductNbr ,
       OL.[PepperiId]
      ,OL.[WinzerProductNbr]
	  ,isnull(OL.OrderQuantity ,0) as OrderQuantity
      ,isnull(OL.OrderPrice,0) as  OrderPrice
	  ,'' as PriceOverrideFlag 
	  , case when isnull(OL.DisplayComment ,'') = '' then 'Y' else 'N' end as OverrideMinimumQuantity 
	  ,OL.OrderCost 
	 -- ,OL.ItemDescription -- not mapped in Pepperi
	  ,isnull(ol.ItemDescription ,'') as DisplayComment
	 -- ,OL.LineItemType -- not mapped in Pepperi
	 , case when OL.DisplayComment = 'Y' then 'B' 
       when OL.DisplayComment = 'N' then 'X'
       else OL.DisplayComment end as DetailCommentCode ------------------------------------------
	  ,OL.UnitOfMeasure 
	  ,ISNULL(OL.NoChargeReasonCode ,'') as NoChargeReasonCode -- good
	--  ,isnull(OL.LabelQty,0) as  LabelQty
                   ,  isnull('1' + case when left(LabelCode,1) = 'O' then '2' 
             when left(LabelCode,1) = 'C' then '1' 
			 end +
        case when  replace(SUBSTRING(LabelCode, 3,CHARINDEX('-', LabelCode)),'-','') = 'B' then '1' 
		      when  replace(SUBSTRING(LabelCode, 3,CHARINDEX('-', LabelCode)),'-','') = 'NB' then '0' end +
		case when  replace(left(right(LabelCode,6),2),'-','') = 'M' then '1' 
		     when  replace(left(right(LabelCode,6),2),'-','') = 'NM' then '0' end +
		right(LabelCode,2),0)  as LabelQty
	  ,ISNULL(OL.Pa1bFlag ,'') as Pa1bFlag
	  ,isnull(OL.NbrOfBags ,0) as NbrOfBags
	  ,isnull(OL.FdpReference ,'') as FdpReference
	  ,'' as FranchiseCostReasonCd 
	  ,'' as CVPCode  
	  ,ISNULL(OL.SpecialFrCostId ,'') as SpecialFrCostId 
	  ,0 as CVPCodeMinQty 
	  ,0 as CVPCodeMaxSellPrice 
	  ,isnull(OL.CustomerContract ,'') as CustomerContract 
	    ,0 as BasePrice 
		,''  as CommissionCode  
		,0 as CommissionPercent 
		,0 as MarkupPercent 
		,0 as MarkupAmount 
		,'' as FreightUpChargeCode 
		,0 as FreightUpChargePercent 
		,0 as FreightUpChargeAmount
	  ,OL.OriginalFranchisePrice 
	  ,OL.LandedCost 
                  , ISNULL(t1.Comment,'') as Comment
                  , ISNULL(Ol.ItemDescription,'') as ItemDescription
                  , T1.LineItemType  as LineItemType,
                 '' as USERFA
				 ,@RunDate CreatedDate
FROM StagePA.SalesOrderLineNbr t1
JOIN [dbo].[SalesOrderLine] OL
ON t1.PepperiID = ol.PepperiId
AND t1.OrigLineNbr = ol.LineNbr
inner join [dbo].[SalesOrder] SO 
on t1.PepperiId = SO.PepperiOrderNbr 
--left join dbo.CustomerCrossReference CR 
--on left(CR.PepperiCustomerId,8) = left(SO.PepperiCustomerNbr,8)
where T1.CreatedDate = @RunDate 
and T1.LineItemType = 'I'
AND T1.ParentItemNumber is NULL
AND t1.comment IS NULL 

UNION ALL

  --  Line Comment
   SELECT 
       ISNULL(RIGHT('00000' + Cast(SO.[RepNbr] as varchar(20)),5),'')  as RepNbrPad,
/*	   case when ISNULL(CustomerNbr,0) > 1000000 
	   THEN CR.AS400TempCustomerId 
	   ELSE ISNULL(CustomerNbr,0) end as CustomerNbr, */
	   CustomerNbr,
	   OL.AS400OrderNbr PepperiOrderNbr,
	   T1.LineNbr  as LineNbr,
	  ISNULL(left(ltrim(RTRIM(OL.[BrandedProductNbr])),15),'') as BrandedProductNbr ,
       NULL as [PepperiId]
      ,OL.[WinzerProductNbr]
	  ,0 as OrderQuantity 
      ,0 as OrderPrice 
	  ,'' as PriceOverrideFlag 
	  ,'' as OverrideMinimumQuantity 
	  ,0 as OrderCost 
	 -- ,OL.ItemDescription -- not mapped in Pepperi
	  ,T1.Comment as DisplayComment
	   , case when OL.DisplayComment = 'Y' then 'B' 
       when OL.DisplayComment = 'N' then 'X'
       else OL.DisplayComment end as DetailCommentCode ------------------------------------------
	 -- ,OL.LineItemType -- not mapped in Pepperi
	  ,'' as UnitOfMeasure 
	  ,'' as NoChargeReasonCode
	  ,0 as LabelQty 
	  ,'' as Pa1bFlag
	  ,0 as NbrOfBags 
	  ,'' as FdpReference
	  ,	'' as FranchiseCostReasonCd 
	  ,'' as CVPCode  
	  ,ISNULL(OL.SpecialFrCostId ,'') as SpecialFrCostId 
	  ,0 as CVPCodeMinQty 
	  ,0 as CVPCodeMaxSellPrice 
	  ,'' as CustomerContract
	    ,0 as BasePrice 
		,''  as CommissionCode  
		,0 as CommissionPercent 
		,0 as MarkupPercent 
		,0 as MarkupAmount 
		,'' as FreightUpChargeCode 
		,0 as FreightUpChargePercent 
		,0 as FreightUpChargeAmount
	  ,0 as OriginalFranchisePrice 
	  ,0 as LandedCost 
                  , '' as  Comment
                  , '' as  ItemDescription
                  ,t1.LineItemType as LineItemType,
                 '' as USERFA
				 ,@RunDate CreatedDate
FROM StagePA.SalesOrderLineNbr t1
JOIN [dbo].[SalesOrderLine] OL
ON t1.PepperiID = ol.PepperiId
AND t1.OrigLineNbr = ol.LineNbr
join [dbo].[SalesOrder] SO    
on OL.PepperiId = SO.PepperiOrderNbr
--left outer join dbo.CustomerCrossReference CR  
--on left(CR.PepperiCustomerId,8) = left(SO.PepperiCustomerNbr,8)
where t1.CreatedDate = @RunDate
and T1.LineItemType = 'M'
and t1.ParentItemNumber is null  --  Not Kit
AND t1.comment IS NOT NULL

UNION ALL
-- OrderComment
   SELECT 
       ISNULL(RIGHT('00000' + Cast(SO.[RepNbr] as varchar(20)),5),'')  as RepNbrPad,
/*	   case when ISNULL(so.CustomerNbr,0) > 1000000 
	   THEN CR.AS400TempCustomerId 
	   ELSE ISNULL(CustomerNbr,0) end as CustomerNbr, */
	   Customernbr,
	   OL.AS400OrderNbr PepperiOrderNbr,
	   T1.LineNbr  as LineNbr,
	  '' as BrandedProductNbr ,
       NULL as [PepperiId]
      ,'' 
	  ,0 as OrderQuantity 
      ,0 as OrderPrice 
	  ,'' as PriceOverrideFlag 
	  ,'' as OverrideMinimumQuantity 
	  ,0 as OrderCost 
	  ,T1.CommentChunk as DisplayComment
	   , case when T1.DisplayComment = 'Y' then 'B' 
       when t1.DisplayComment = 'N' then 'X'
       else t1.DisplayComment end as DetailCommentCode ------------------------------------------
	  ,'' as UnitOfMeasure 
	  ,'' as NoChargeReasonCode
	  ,0 as LabelQty 
	  ,'' as Pa1bFlag
	  ,0 as NbrOfBags 
	  ,'' as FdpReference
	  ,	'' as FranchiseCostReasonCd 
	  ,'' as CVPCode  
	  ,'' as SpecialFrCostId 
	  ,0 as CVPCodeMinQty 
	  ,0 as CVPCodeMaxSellPrice 
	  ,'' as CustomerContract
	    ,0 as BasePrice 
		,''  as CommissionCode  
		,0 as CommissionPercent 
		,0 as MarkupPercent 
		,0 as MarkupAmount 
		,'' as FreightUpChargeCode 
		,0 as FreightUpChargePercent 
		,0 as FreightUpChargeAmount
	  ,0 as OriginalFranchisePrice 
	  ,0 as LandedCost 
                  , '' as  Comment
                  , '' as  ItemDescription
                  ,CASE WHEN t1.LineNbr < 9000 THEN 'T' ELSE 'B' END AS LineItemType,
                 '' as USERFA
				 ,@RunDate CreatedDate
FROM StagePA.SalesOrderCommentnbr t1
JOIN [dbo].[SalesOrderLine] OL
ON t1.PepperiOrderNbr = ol.PepperiId
AND t1.LineNbr = ol.LineNbr
join [dbo].[SalesOrder] SO    
on t1.PepperiOrderNbr = SO.PepperiOrderNbr
--left outer join dbo.CustomerCrossReference CR  
--on left(CR.PepperiCustomerId,8) = left(SO.PepperiCustomerNbr,8)
where t1.CreatedDate = @RunDate

) x

-- History
INSERT INTO [Hist].[SalesOrderLineFormatted]
           ([RepNbrPad]
           ,[CustomerNbr]
           ,[PepperiOrderNbr]
           ,[LineNbr]
           ,[BrandedProductNbr]
           ,[PepperiId]
           ,[WinzerProductNbr]
           ,[OrderQuantity]
           ,[OrderPrice]
           ,[PriceOverrideFlag]
           ,[OverrideMinimumQuantity]
           ,[OrderCost]
           ,[DisplayComment]
           ,[DetailCommentCode]
           ,[UnitOfMeasure]
           ,[NoChargeReasonCode]
           ,[LabelQty]
           ,[Pa1bFlag]
           ,[NbrOfBags]
           ,[FdpReference]
           ,[FranchiseCostReasonCd]
           ,[CVPCode]
           ,[SpecialFrCostId]
           ,[CVPCodeMinQty]
           ,[CVPCodeMaxSellPrice]
           ,[CustomerContract]
           ,[BasePrice]
           ,[CommissionCode]
           ,[CommissionPercent]
           ,[MarkupPercent]
           ,[MarkupAmount]
           ,[FreightUpChargeCode]
           ,[FreightUpChargePercent]
           ,[FreightUpChargeAmount]
           ,[OriginalFranchisePrice]
           ,[LandedCost]
           ,[Comment]
           ,[ItemDescription]
           ,[LineItemType]
           ,[USERFA]
           ,[CreatedDate])
SELECT [RepNbrPad]
           ,[CustomerNbr]
           ,[PepperiOrderNbr]
           ,[LineNbr]
           ,[BrandedProductNbr]
           ,[PepperiId]
           ,[WinzerProductNbr]
           ,[OrderQuantity]
           ,[OrderPrice]
           ,[PriceOverrideFlag]
           ,[OverrideMinimumQuantity]
           ,[OrderCost]
           ,[DisplayComment]
           ,[DetailCommentCode]
           ,[UnitOfMeasure]
           ,[NoChargeReasonCode]
           ,[LabelQty]
           ,[Pa1bFlag]
           ,[NbrOfBags]
           ,[FdpReference]
           ,[FranchiseCostReasonCd]
           ,[CVPCode]
           ,[SpecialFrCostId]
           ,[CVPCodeMinQty]
           ,[CVPCodeMaxSellPrice]
           ,[CustomerContract]
           ,[BasePrice]
           ,[CommissionCode]
           ,[CommissionPercent]
           ,[MarkupPercent]
           ,[MarkupAmount]
           ,[FreightUpChargeCode]
           ,[FreightUpChargePercent]
           ,[FreightUpChargeAmount]
           ,[OriginalFranchisePrice]
           ,[LandedCost]
           ,[Comment]
           ,[ItemDescription]
           ,[LineItemType]
           ,[USERFA]
           ,[CreatedDate]
FROM StagePA.SalesOrderLineFormatted

END

GO

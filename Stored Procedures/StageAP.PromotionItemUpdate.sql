SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		baylor
-- alter date: 8/2020
-- Description:	Update to Pepperi_Staging 
-- =============================================
CREATE PROCEDURE [StageAP].[PromotionItemUpdate] 
AS

BEGIN

SET NOCOUNT ON;

DECLARE @RunDate DATETIME = GETDATE()

UPDATE t1
SET PromotionCode = t2.PromotionCode
,ItemNumber = t2.ItemNumber
,ItemPrice = t2.ItemPrice
,PromotionStatus = t2.PromotionStatus
,CreationTimestamp = t2.CreationTimestamp
,DateLastMaintained = t2.DateLastMaintained
,Deleted = 0
,[ModifiedDate] = @RunDate
FROM  [dbo].[PromotionItem]  t1
JOIN StageAP.PromotionItem t2
ON t1.PromotionCode = t2.PromotionCode
AND t1.ItemNumber = t2.ItemNumber
WHERE t1.[DateLastMaintained] <  t2.DateLastMaintained
OR t1.Deleted = 1

INSERT INTO [dbo].[PromotionItem]
           ([PromotionCode]
           ,ItemNumber
		   ,ItemPrice
           ,[PromotionStatus]
           ,[CreationTimestamp]
		   ,[DateLastMaintained]
		   ,CreatedDate
		   ,ModifiedDate
)
SELECT		t1.[PromotionCode]
           ,t1.[ItemNumber]
		   ,t1.ItemPrice
           ,t1.[PromotionStatus]
           ,t1.[CreationTimestamp]
           ,t1.[DateLastMaintained]
		   ,@RunDate CreatedDate
		   ,@RunDate ModifiedDate
FROM StageAP.PromotionItem t1
LEFT JOIN dbo.PromotionItem	 t2
ON t1.PromotionCode = t2.PromotionCode
AND t1.ItemNumber = t2.ItemNumber
WHERE t2.ItemNumber IS NULL

-- Delete 
UPDATE t1
SET Deleted = 1,
ModifiedDate = @Rundate
FROM [dbo].[PromotionItem] t1
LEFT JOIN [StageAP].[PromotionItem] t2
ON t1.PromotionCode = t2.PromotionCode
AND t1.ItemNumber = t2.ItemNumber
WHERE t2.ItemNumber IS NULL

END
GO

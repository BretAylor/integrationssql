SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      2/6/2019
-- Description  Net sales by Month and Fiscal Year GL Summary report

-- Copyright © 2019, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History

-------------------------------------------------------------------------------
-- exec GLSummaryNetSalesReport

CREATE PROCEDURE [dbo].[GLSummaryNetSalesReport]
AS 
BEGIN

SELECT FiscalYear,
       'Other Net Sales' AS [Type],
       SUM(Aug) AS 'August',
       SUM(Sep) AS 'September',
       SUM(Oct) AS 'October',
       SUM(Nov) AS 'November',
       SUM(Dec) AS 'December',
       SUM(Jan) AS 'January',
       SUM(Feb) AS 'February',
       SUM(Mar) AS 'March',
       SUM(Apr) AS 'April',
       SUM(May) AS 'May',
       SUM(Jun) AS 'June',
       SUM(Jul) AS 'July',
	   DateAdded
FROM BI_DWH.dbo.FactGLSummaryNetSales
WHERE FiscalYear >= 2018
GROUP BY FiscalYear,DateAdded

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		baylor
-- Create date: 05/20
-- Description:	Number SalesOrder Comments for Submission to AS400
-- =============================================
CREATE PROCEDURE [StagePA].[SalesOrderCommentNumbering] 

AS
BEGIN

SET NOCOUNT ON;

TRUNCATE TABLE  StagePA.WorkSalesOrderComment
TRUNCATE TABLE  StagePA.WorkSalesOrderCommentChunks

DECLARE @RunDate DATETIME = (SELECT MAX(createdDate) FROM StagePA.SalesOrderToProcess)

--DELETE StagePA.SalesOrderComment WHERE DATEADD(mm,-3,@RunDate) > CreatedDate  

-- SystemComments
Insert StagePA.WorkSalesOrderComment
SELECT t1.PepperiOrderNbr
,1 CommentLevel
,t1.comment
,ISNULL(t1.DisplayComment,'') DisplayComment
,@RunDate
FROM [dbo].[SalesOrder] t1 
JOIN StagePA.SalesOrderToProcess t2
on t1.PepperiOrderNbr = t2.PepperiOrderNbr 
WHERE t2.createddate = @RunDate
AND t1.comment IS NOT NULL


-- Top Comment
Insert StagePA.WorkSalesOrderComment
SELECT t1.PepperiOrderNbr
,2 CommentLevel
,t1.Topcomment
,ISNULL(t1.DisplayTopComment,'') DisplayComment
,@RunDate
FROM [dbo].[SalesOrder] t1 
JOIN StagePA.SalesOrderToProcess t2
on t1.PepperiOrderNbr = t2.PepperiOrderNbr 
WHERE t2.createddate = @RunDate
AND t1.Topcomment IS NOT NULL

DECLARE @RecCount INT = (SELECT MIN(id) FROM StagePA.WorkSalesOrderComment)
DECLARE @MaxCount INT = (SELECT max(id) FROM StagePA.WorkSalesOrderComment)
DECLARE @String VARCHAR(8000)


WHILE @RecCount <= @Maxcount
BEGIN
SET @String = (SELECT comment FROM StagePA.WorkSalesOrderComment WHERE ID = @RecCount)

SELECT * 
INTO #String
FROM dbo.[Split](@String , 62)

INSERT INTO [StagePA].[WorkSalesOrderCommentChunks]
           ([ID]
           ,[PepperiOrderNbr]
           ,[CommentLevel]
           ,[comment]
           ,[DisplayComment]
           ,[CommentIndex]
           ,[CommentChunk]
           ,[CreatedDate])
SELECT [ID]
           ,[PepperiOrderNbr]
           ,[CommentLevel]
           ,[comment]
           ,[DisplayComment]
           ,NameIndex [CommentIndex]
           ,Items [CommentChunk]
           ,[CreatedDate]
FROM StagePA.WorkSalesOrderComment t1,
#String
WHERE ID = @RecCount

DROP TABLE #string 

SET @RecCount = @RecCount + 1

END

INSERT INTO [StagePA].[SalesOrderCommentnbr]
           ([LineNbr]
           ,[ID]
           ,[PepperiOrderNbr]
           ,[CommentLevel]
           ,[comment]
           ,[DisplayComment]
           ,[CommentIndex]
           ,[CommentChunk]
           ,[CreatedDate])
SELECT  ROW_NUMBER ( )  
OVER (PARTITION BY PepperiOrderNbr
ORDER BY CommentLevel, CommentIndex) LineNbr
           ,[ID]
           ,[PepperiOrderNbr]
           ,[CommentLevel]
           ,[comment]
           ,[DisplayComment]
           ,[CommentIndex]
           ,[CommentChunk]
           ,[CreatedDate]
FROM [StagePA].[WorkSalesOrderCommentChunks]

-- Bottom Comments
TRUNCATE TABLE  StagePA.WorkSalesOrderComment
TRUNCATE TABLE  [StagePA].[WorkSalesOrderCommentChunks]


Insert StagePA.WorkSalesOrderComment
SELECT t1.PepperiOrderNbr
,3 CommentLevel
,t1.BottomComment
,ISNULL(t1.DisplayBottomComment,'') DisplayComment
,@RunDate
FROM [dbo].[SalesOrder] t1 
JOIN StagePA.SalesOrderToProcess t2
on t1.PepperiOrderNbr = t2.PepperiOrderNbr 
WHERE t2.createddate = @RunDate
AND t1.BottomComment IS NOT NULL

Set @RecCount = (SELECT MIN(id) FROM StagePA.WorkSalesOrderComment)
Set @MaxCount = (SELECT max(id) FROM StagePA.WorkSalesOrderComment)

WHILE @RecCount <= @Maxcount
BEGIN
SET @String = (SELECT comment FROM StagePA.WorkSalesOrderComment WHERE ID = @RecCount)

SELECT * 
INTO #StringB
FROM dbo.[Split](@String , 62)

INSERT INTO [StagePA].[WorkSalesOrderCommentChunks]
           ([ID]
           ,[PepperiOrderNbr]
           ,[CommentLevel]
           ,[comment]
           ,[DisplayComment]
           ,[CommentIndex]
           ,[CommentChunk]
           ,[CreatedDate])
SELECT [ID]
           ,[PepperiOrderNbr]
           ,[CommentLevel]
           ,[comment]
           ,[DisplayComment]
           ,NameIndex [CommentIndex]
           ,Items [CommentChunk]
           ,[CreatedDate]
FROM StagePA.WorkSalesOrderComment t1,
#StringB
WHERE ID = @RecCount

DROP TABLE #StringB 

SET @RecCount = @RecCount + 1

end

 INSERT INTO [StagePA].[SalesOrderCommentnbr]
           ([LineNbr]
           ,[ID]
           ,[PepperiOrderNbr]
           ,[CommentLevel]
           ,[comment]
           ,[DisplayComment]
           ,[CommentIndex]
           ,[CommentChunk]
           ,[CreatedDate]) 
SELECT  ROW_NUMBER ( )  
OVER (PARTITION BY PepperiOrderNbr
ORDER BY CommentLevel, CommentIndex) + 9000 LineNbr
           ,[ID]
           ,[PepperiOrderNbr]
           ,[CommentLevel]
           ,[comment]
           ,[DisplayComment]
           ,[CommentIndex]
           ,[CommentChunk]
           ,[CreatedDate]
FROM [StagePA].[WorkSalesOrderCommentChunks]

BEGIN TRY DROP TABLE #String END TRY BEGIN CATCH END CATCH
BEGIN TRY DROP TABLE #StringB END TRY BEGIN CATCH END CATCH

end
GO

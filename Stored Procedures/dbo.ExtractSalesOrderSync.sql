SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		baylor
-- Create date: 5/20
-- Description:	Update Kit Lines
-- Logic Changes: 
-- 5/8/2020
-- 1) Update only those not in the Exclusion List
-- =============================================
CREATE PROCEDURE [dbo].[ExtractSalesOrderSync] 

AS
BEGIN

SET NOCOUNT ON;

/* Reset
Update [dbo].[SalesOrder]  
set ErpSyncDate = Null,
ErpSyncby = Null,
ErpModifieddate = Null
where PepperiOrdernbr = 95342287

Update [dbo].[SalesOrderLine]  
set ErpSyncDate = Null,
ErpSyncby = Null
where PepperiID = 95342287

*/

DECLARE @RunDate DATETIME = (SELECT MAX(createdDate) FROM StagePA.SalesOrderToProcess)

UPDATE t1 
set ErpSyncDate = getdate(),
ErpSyncby = suser_name(),
ErpModifieddate = getdate()
FROM [StagePA].[SalesOrderToProcess] t2
JOIN [dbo].[SalesOrder] t1
ON t1.PepperiOrderNbr = t2.pepperiordernbr
WHERE t2.createddate = @RunDate



End
GO

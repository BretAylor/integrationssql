SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- EXEC [dbo].[FactInventoryManualLoad]
CREATE PROCEDURE [dbo].[FactInventoryManualLoad]
AS
BEGIN
DECLARE @Month INT
SET @Month= 8
DECLARE @Year INT
SET @Year = 2019

-- In transit Purchases
DROP TABLE IF EXISTS #InTransitPurchases
SELECT 'InTransit Purchases' AS Metric,
       CAST(Year AS VARCHAR(4))+RIGHT('00' + CONVERT(VARCHAR(2),Month), 2) + '01' AS DateKey,
       NULL AS WarehouseType,
       WH.WarehouseKey,
	   IB.ItemNumber,
	   IC.Description AS ItemClass,
       --PR.ProductKey,
       --PR.ItemClassKey,
       SUM((OL.OrderQuantity - QuantityReceivedtodate) * IB.AverageCost) AS InTransitInventoryValue,
       GETDATE() AS DateAdded
INTO #InTransitPurchases
FROM BI_STAGING.dbo.OpenPurchaseorderHeader OH
    LEFT OUTER JOIN BI_STAGING.dbo.OpenPurchaseOrderLines OL
        ON OH.OrderID = OL.OrderID
           AND OH.CompanyNumber = OL.CompanyNumber
    LEFT OUTER JOIN BI_STAGING.dbo.ItemBalance IB
        ON IB.ItemNumber = OL.ItemNumber
           AND IB.WarehouseID = OL.WarehouseID
    LEFT OUTER JOIN BI_DWH.dbo.DimDate DD
        ON DD.Date = CONVERT(DATE, CONVERT(VARCHAR(2), OH.Century1) + CONVERT(VARCHAR(6), OH.OrderDate))
    LEFT OUTER JOIN BI_DWH.dbo.DimItemClass IC ON OL.ItemClass = IC.ItemClassCode
    LEFT OUTER JOIN BI_DWH.dbo.DimWarehouse WH
        ON WH.WarehouseID = IB.WarehouseID
    LEFT OUTER JOIN BI_DWH.dbo.DimProduct PR
        ON PR.ProductNum = IB.ItemNumber
	CROSS APPLY (SELECT CAST(Year AS VARCHAR(4))+RIGHT('00' + CONVERT(VARCHAR(2),Month), 2) + '01' AS DateKey) AS DK
WHERE OH.CompanyNumber = 1
      AND OH.POStatus = 'O'
      AND OH.DeleteCode = ''
      AND OH.DropShipCodeYorN = 'N'
      AND DATEDIFF(
                      MONTH,
                      CONVERT(DATE, CONVERT(VARCHAR(2), OH.Century1) + CONVERT(VARCHAR(6), OH.OrderDate)),
                      GETDATE()
                  ) <= 6
      AND OL.LineItemStatus = 'O'
      AND OL.DeleteCode = ''
      AND OL.DropShipCodeYorN = 'N'
      AND OL.OrderQuantity > 0
      AND OH.WarehouseID IN ( 1, 3, 4, 5 )
	  AND DD.Month = @Month
	  AND DD.Year =  @Year
	 
GROUP BY CAST(Year AS VARCHAR(4))+RIGHT('00' + CONVERT(VARCHAR(2),Month), 2) + '01',
         WH.WarehouseKey,
		 IB.ItemNumber,
	     IC.Description
         --PR.ProductKey,
         --PR.ItemClassKey;

-- InTransit Transfer
DROP TABLE IF EXISTS #InTransitTransfer

SELECT 'InTransit Transfer' AS Metric,
       CAST(Year AS VARCHAR(4))+RIGHT('00' + CONVERT(VARCHAR(2),Month), 2) + '01' AS DateKey,
       NULL AS WarehouseType,
       WH.WarehouseKey,
	   IB.ItemNumber,
	   IC.Description AS ItemClass,
       --PR.ProductKey,
       --PR.ItemClassKey,
       SUM((OL.OrderQuantity - QuantityReceivedtodate) * IB.AverageCost) AS InTransitInventoryValue,
       GETDATE() AS DateAdded
INTO #InTransitTransfer
FROM BI_STAGING.dbo.OpenPurchaseorderHeader OH
    LEFT OUTER JOIN BI_STAGING.dbo.OpenPurchaseOrderLines OL
        ON OH.OrderID = OL.OrderID
           AND OH.CompanyNumber = OL.CompanyNumber
    LEFT OUTER JOIN BI_STAGING.dbo.ItemBalance IB
        ON IB.ItemNumber = OL.ItemNumber
           AND IB.WarehouseID = OL.WarehouseID
    LEFT OUTER JOIN BI_DWH.dbo.DimDate DD
        ON DD.Date = CONVERT(DATE, CONVERT(VARCHAR(2), OH.Century1) + CONVERT(VARCHAR(6), OH.OrderDate))
    LEFT OUTER JOIN BI_DWH.dbo.DimItemClass IC ON OL.ItemClass = IC.ItemClassCode
    LEFT OUTER JOIN BI_DWH.dbo.DimWarehouse WH
        ON WH.WarehouseID = IB.WarehouseID
    LEFT OUTER JOIN BI_DWH.dbo.DimProduct PR
        ON PR.ProductNum = IB.ItemNumber
WHERE OH.CompanyNumber = 99
      AND OH.POStatus = 'O'
      AND OH.DeleteCode = ''
      AND OH.DropShipCodeYorN = 'N'
      AND DATEDIFF(
                      MONTH,
                      CONVERT(DATE, CONVERT(VARCHAR(2), OH.Century1) + CONVERT(VARCHAR(6), OH.OrderDate)),
                      GETDATE()
                  ) <= 6
      AND OL.LineItemStatus = 'O'
      AND OL.DeleteCode = ''
      AND OL.DropShipCodeYorN = 'N'
      AND OL.OrderQuantity > 0
	  AND DD.Month = @Month
	  AND DD.Year =  @Year

GROUP BY CAST(Year AS VARCHAR(4))+RIGHT('00' + CONVERT(VARCHAR(2),Month), 2) + '01',
         WH.WarehouseKey,
		 IB.ItemNumber,
	     IC.Description;
         --PR.ProductKey,
         --PR.ItemClassKey;


-- Purchases
DROP TABLE IF EXISTS #Purchases
-- Getting the list of buyers with their purchase order number and company number
DROP TABLE IF EXISTS #BuyerSELECT DISTINCT
       PR.CompanyNumber,
       PR.PurchaseOrderNumber,
       BU.CCDATA AS CCDATA
INTO #Buyer
FROM BI_STAGING.dbo.POReceiverDetail PR
    LEFT OUTER JOIN [BI_STAGING].dbo.OpenPurchaseorderHeader POH
        ON POH.CompanyNumber = PR.CompanyNumber
           AND POH.PurchaseOrderNumber = PR.PurchaseOrderNumber
    LEFT OUTER JOIN [BI_STAGING].[dbo].[PurchaseHistoryHeader] PHH
        ON PR.CompanyNumber = PHH.PACONO
           AND PR.PurchaseOrderNumber = PHH.PAPONO
    LEFT OUTER JOIN [BI_STAGING].[dbo].[Buyer] BU
        ON (
               BU.CCCTLK = 'BUYR' + CASE
                                        WHEN POH.BuyerInitial = '' THEN
                                            ApprovalCode
                                        ELSE
                                            POH.BuyerInitial
                                    END
               OR BU.CCCTLK = 'BUYR' + CASE
                                           WHEN PHH.PABUYR = '' THEN
                                               PHH.PAAPCD
                                           ELSE
                                               PHH.PABUYR
                                       END
           )
WHERE GLPostingDate >= 160101
      AND CONVERT(DATE, CONVERT(VARCHAR(2), PR.Century) + CONVERT(VARCHAR(6), GLPostingDate)) > '1/1/2018'
      AND PR.PurchaseOrderNumber LIKE '[a-zA-Z]%' -- exclude special orders
      AND PR.WarehouseID IN ( 1, 3, 4, 5 )
      AND BU.CCCONO = 0
      AND BU.CCAPID = 'PO';

-- Getting the purchase/buyer details
SELECT 'Purchases' AS Metric,
       CAST(Year AS VARCHAR(4))+RIGHT('00' + CONVERT(VARCHAR(2),Month), 2) + '01' AS DateKey,
       NULL AS WarehouseType,
	   WH.WarehouseKey,
	   --PRO.ProductKey,
	   --PRO.ItemClassKey,
	   IB.ItemNumber,
	   IC.Description AS ItemClass,
       ISNULL(LEFT(BU.CCDATA,25),'UNKOWN') AS Buyer,
	   PR.TotalQuantityReceived,
	   IB.AverageCost AS ItemCost,
	   PR.TotalQuantityReceived * IB.AverageCost AS ReceivedInventoryValue,
	   GETDATE() AS DateAdded

INTO #Purchases FROM BI_STAGING.dbo.POReceiverDetail PR 
LEFT OUTER JOIN BI_STAGING.dbo.ItemBalance IB ON PR.ItemNumber  = IB.ItemNumber AND PR.WarehouseID = IB.WarehouseID 
LEFT OUTER JOIN BI_DWH.dbo.DimItemClass IC ON IB.ItemClass = IC.ItemClassCode
LEFT OUTER JOIN BI_DWH.dbo.DimDate DD ON DD.Date = CONVERT(date, CONVERT(varchar(2), PR.Century) + convert(varchar(6), GLPostingDate))
LEFT OUTER JOIN #Buyer BU ON BU.CompanyNumber = PR.CompanyNumber AND BU.PurchaseOrderNumber = PR.PurchaseOrderNumber
LEFT OUTER JOIN BI_DWH.dbo.DimWarehouse WH ON WH.WarehouseID = IB.WarehouseID
LEFT OUTER JOIN BI_DWH.dbo.DimProduct PRO ON PRO.ProductNum = IB.ItemNumber
 WHERE  GLPostingDate >= 160101 AND  
        CONVERT(date, CONVERT(varchar(2), PR.Century) + convert(varchar(6), GLPostingDate)) > '1/1/2018' AND
		PR.PurchaseOrderNumber LIKE '[a-zA-Z]%' -- exclude special orders
		AND PR.WarehouseID IN (1,3,4,5)
	    AND DD.Month = @Month
	    AND DD.Year =  @Year


DROP TABLE IF EXISTS #InventoryDates
SELECT CAST(Year AS VARCHAR(4))+RIGHT('00' + CONVERT(VARCHAR(2),Month), 2) + '01' AS DateKey,
       MIN(BeginningDate) AS [MinBeginningDate],
       MAX(ProcessDate) AS [MaxProcessDate]
INTO #InventoryDates
FROM BI_DWH.dbo.DailyInventoryDetails
WHERE Month = @Month
	  AND Year =  @Year
GROUP BY CAST(Year AS VARCHAR(4))+RIGHT('00' + CONVERT(VARCHAR(2),Month), 2) + '01'

 
-- Beginning inventory
DROP TABLE IF EXISTS #BeginningInventory
SELECT 'Beginning Inventory' AS Metric,
       DID.BeginningDate,
       CAST(DID.Year AS VARCHAR(4))+RIGHT('00' + CONVERT(VARCHAR(2),DID.Month), 2) + '01' AS DateKey,
       NULL AS WarehouseType,
	   WH.WarehouseKey,
	   --PR.ProductKey,
	   --PR.ItemClassKey,
	   DID.ItemNum AS ItemNumber, 
	   DID.ItemClassName AS ItemClass,
       DID.BeginningInventory AS ReceivedInventoryValue
INTO #BeginningInventory
FROM BI_DWH.dbo.DailyInventoryDetails DID
    INNER JOIN #InventoryDates IND
        ON CAST(DID.Year AS VARCHAR(4))+RIGHT('00' + CONVERT(VARCHAR(2),DID.Month), 2) + '01' = IND.DateKey
           AND DID.BeginningDate = IND.MinBeginningDate
	LEFT OUTER JOIN BI_DWH.dbo.DimWarehouse WH
        ON WH.WarehouseID = DID.WarehouseID
    LEFT OUTER JOIN BI_DWH.dbo.DimProduct PR
        ON PR.ProductNum = DID.ItemNum
WHERE DID.Year = @Year AND DID.Month= @Month

-- Ending inventory
DROP TABLE IF EXISTS #EndingInventory;
SELECT 'Ending Inventory' AS Metric,
       DID.ProcessDate,
       CAST(DID.Year AS VARCHAR(4))+RIGHT('00' + CONVERT(VARCHAR(2),DID.Month), 2) + '01' AS DateKey,
       NULL AS WarehouseType,
	   WH.WarehouseKey,
	   --PR.ProductKey,
	   --PR.ItemClassKey,
	   DID.ItemNum AS ItemNumber, 
	   DID.ItemClassName AS ItemClass ,
       DID.EndingInventory AS ReceivedInventoryValue,
       DID.CostOfGoodsSold
INTO #EndingInventory
FROM BI_DWH.dbo.DailyInventoryDetails DID
    INNER JOIN #InventoryDates IND
        ON CAST(DID.Year AS VARCHAR(4))+RIGHT('00' + CONVERT(VARCHAR(2),DID.Month), 2) + '01' = IND.DateKey
          AND DID.ProcessDate = IND.MaxProcessDate
	LEFT OUTER JOIN BI_DWH.dbo.DimWarehouse WH
        ON WH.WarehouseID = DID.WarehouseID
    LEFT OUTER JOIN BI_DWH.dbo.DimProduct PR
        ON PR.ProductNum = DID.ItemNum

WHERE DID.Year = @Year AND DID.Month= @Month
-- CostOfGoodsSold
DROP TABLE IF EXISTS #CostOfGoodsSold;
SELECT 'Cost Of Goods Sold' AS Metric,
       DID.ProcessDate,
       CAST(DID.Year AS VARCHAR(4))+RIGHT('00' + CONVERT(VARCHAR(2),DID.Month), 2) + '01' AS DateKey,
       NULL AS WarehouseType,
	   WH.WarehouseKey,
	   --PR.ProductKey,
	   --PR.ItemClassKey,
	   DID.ItemNum AS ItemNumber, 
	   DID.ItemClassName AS ItemClass,
       DID.CostOfGoodsSold
INTO #CostOfGoodsSold
FROM BI_DWH.dbo.DailyInventoryDetails DID
    INNER JOIN #InventoryDates IND
        ON CAST(DID.Year AS VARCHAR(4))+RIGHT('00' + CONVERT(VARCHAR(2),DID.Month), 2) + '01'= IND.DateKey
           AND DID.ProcessDate = IND.MaxProcessDate
   LEFT OUTER JOIN BI_DWH.dbo.DimWarehouse WH
        ON WH.WarehouseID = DID.WarehouseID
   LEFT OUTER JOIN BI_DWH.dbo.DimProduct PR
        ON PR.ProductNum = DID.ItemNum

WHERE DID.Year = @Year AND DID.Month= @Month



DELETE FROM BI_DWH.dbo.FactInventory WHERE DateKey = CAST(@Year AS CHAR(4)) + RIGHT('00' + CONVERT(VARCHAR(2),@Month), 2) + '01' 
INSERT INTO BI_DWH.dbo.FactInventory ( 
       [MetricId]
      ,[Metric]
      ,[DateKey]
      ,[WarehouseKey]
      ,[WarehouseType]
      ,[ItemClass]
      ,[ItemNumber]
      ,[Buyer]
      ,[Inventory]
      ,[DateAdded])
SELECT 7 AS MetricId, Metric, DateKey, WarehouseKey, WarehouseType,ItemClass,ItemNumber,NULL AS Buyer, InTransitInventoryValue AS Inventory , GETDATE() AS DateAdded FROM #InTransitPurchases
UNION all
SELECT 8 AS MetricId, Metric, DateKey, WarehouseKey, WarehouseType,ItemClass,ItemNumber,NULL AS Buyer, InTransitInventoryValue AS Inventory, GETDATE() AS DateAdded  FROM #InTransitTransfer
UNION all
SELECT 3 AS MetricId, Metric, DateKey, WarehouseKey, WarehouseType,Itemclass, ItemNumber ,Buyer,ReceivedInventoryValue AS Inventory,  GETDATE() AS DateAdded  FROM #Purchases
UNION ALL 
SELECT 1 AS MetricId , Metric, DateKey, WarehouseKey, WarehouseType,ItemClass,ItemNumber,NULL AS Buyer, ReceivedInventoryValue AS Inventory , GETDATE() AS DateAdded  FROM #BeginningInventory
UNION ALL 
SELECT 6 AS MetricId, Metric, DateKey, WarehouseKey, WarehouseType,ItemClass,ItemNumber,NULL AS Buyer, ReceivedInventoryValue AS Inventory , GETDATE() AS DateAdded  FROM #EndingInventory
UNION ALL 
SELECT 2 AS MetricId, Metric, DateKey, WarehouseKey, WarehouseType,ItemClass,ItemNumber,NULL AS Buyer,CostOfGoodsSold AS Inventory , GETDATE() AS DateAdded  FROM #CostOfGoodsSold
UNION ALL
SELECT 5 AS MetricId,
       'Adjustments/Other Amount' AS Metric,
       EI.DateKey,
       EI.WarehouseKey,
       EI.WarehouseType,
	   EI.ItemClass,
	   EI.ItemNumber,
       --EI.ItemClassKey,
       --EI.ProductKey,
       NULL AS Buyer,
       EI.ReceivedInventoryValue - (BI.ReceivedInventoryValue - EI.CostOfGoodsSold + PU.ReceivedInventoryValue) AS Inventory,
       GETDATE() AS DateAdded
FROM #EndingInventory EI
    Full OUTER JOIN #BeginningInventory BI
        ON EI.ItemNumber = BI.ItemNumber
           AND EI.DateKey = BI.DateKey
           AND EI.WarehouseKey = BI.WarehouseKey
    Full OUTER JOIN #Purchases PU
        ON PU.DateKey = EI.DateKey
           AND PU.ItemNumber = EI.ItemNumber
           AND PU.WarehouseKey = EI.WarehouseKey
	




END









GO

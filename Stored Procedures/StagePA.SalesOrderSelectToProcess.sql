SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		baylor
-- Create date: 05/20
-- Description:	Number SalesOrderLine for Submission to AS400
-- =============================================
CREATE PROCEDURE [StagePA].[SalesOrderSelectToProcess] 

AS
BEGIN

SET NOCOUNT ON;

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

DECLARE @RunDate DATETIME = GETDATE()

DELETE StagePA.SalesOrderToProcess WHERE DATEADD(mm,-3,@RunDate) > CreatedDate  

insert StagePA.SalesOrderToProcess
Select
PepperiOrderNbr, @RunDate CreatedDate
FROM SalesOrder  
where erpSyncDate is null 



END

GO

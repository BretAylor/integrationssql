SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		baylor
-- Create date: 4/2020
-- Description:	Update to Pepperi_Staging 
-- =============================================
create PROCEDURE [stageAP].[CCAuthorizationUpdate] 
AS

BEGIN

SET NOCOUNT ON;

-- Transactions, so insert only

 INSERT [WUSA-SQL01-DFW].Pepperi_Staging.[dbo].[CCAutorization]
           ([CompanyNumber]
           ,[HistorySequenceNumber]
           ,[CustomerNumber]
           ,[InvoiceNumber]
           ,[TransactionCode]
           ,[TransactionAmount]
           ,[AuthorizationNumber]
           ,[ResponseCode]
           ,[TransactionDate])
 Select		t1.[CompanyNumber]
           ,t1.[HistorySequenceNumber]
           ,t1.[CustomerNumber]
           ,t1.[InvoiceNumber]
           ,t1.[TransactionCode]
           ,t1.[TransactionAmount]
           ,t1.[AuthorizationNumber]
           ,t1.[ResponseCode]
           ,t1.[TransactionDate]
from [dbo].[CCAuthorization] t1
Left join [WUSA-SQL01-DFW].Pepperi_Staging.[dbo].[CCAutorization] t2
on t1.CompanyNumber = t2.CompanyNumber
and t1.[HistorySequenceNumber] = t2.[HistorySequenceNumber]
and t1.[InvoiceNumber] = t2.[InvoiceNumber]
and t1.[TransactionCode] = t2.[TransactionCode]
and t1.[AuthorizationNumber] = t2.[AuthorizationNumber]
and t1.[TransactionDate] = t2.[TransactionDate]
where  t2.[TransactionDate] is null



END
GO

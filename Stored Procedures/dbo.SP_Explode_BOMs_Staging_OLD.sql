SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create procedure [dbo].[SP_Explode_BOMs_Staging_OLD] as 
--Cleanup
IF OBJECT_ID('tempdb..#mytemp') IS NOT NULL DROP TABLE #mytemp;
IF OBJECT_ID('Demand_Planning..BOM_Explode') IS NULL Create Table BOM_Explode(BOM varchar(255) Index ix_bom_explode, Comp_Itm Varchar(255), Tot_Qty Decimal(10,2), Par_List varchar(max));
Else Truncate table dbo.BOM_Explode;

WITH cte_GET_MFG_ITM AS 
(
	Select itemnumber from bi_staging.dbo.itemmaster where billofmaterialtype in('K','M')
)

Select * into #mytemp from cte_GET_MFG_ITM-- where imbmtp in('K','M');

DECLARE @ITMID VARCHAR(255)

WHILE EXISTS (SELECT * FROM #mytemp)
	BEGIN
		Select @ITMID = (Select top 1 itemnumber from #mytemp order by itemnumber asc)
		Insert into BOM_Explode(BOM, Comp_Itm, Tot_Qty, [Par_List])
		Select * from dbo.explode_all(@ITMID,1)
		Delete #mytemp
		where itemnumber = @ITMID;
	End

--Cleanup
Drop table #mytemp
GO

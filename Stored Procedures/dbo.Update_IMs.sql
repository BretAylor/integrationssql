SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[Update_IMs] as
EXEC [UPDATE_FCM]     --Update Item Summary
EXEC [update_Sls_Imp] --Franchise Impact
EXEC [update_cus_imp] --Customer Subclass Impact
EXEC [update_Act_Imp] --Customer Account Impact
EXEC [update_Itm_Cl_Imp]  --Update Item Class Impact
GO

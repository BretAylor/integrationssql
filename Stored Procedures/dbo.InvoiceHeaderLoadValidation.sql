SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/

CREATE PROCEDURE [dbo].[InvoiceHeaderLoadValidation] 
AS
BEGIN
DECLARE @HoldiayFlag INT
SET @HoldiayFlag = 0
DECLARE @Count INT
SET @Count= 0

SELECT @HoldiayFlag = 1  FROM BI_DWH.dbo.DimDate WHERE IsHoliday = 1 AND DATEADD(DAY,1,Date) = CAST(GETDATE()  AS DATE) 


SELECT @Count= COUNT(*)
  FROM [PEPPERI].[dbo].[InvoiceHeaders]

IF @Count = 0 AND @HoldiayFlag = 0
BEGIN

	EXEC msdb.dbo.sp_send_dbmail 
	@profile_name = 'Account'
	, @recipients = 'saad.benhalima@winzerusa.com'
    , @from_address = 'WinzerAutomatedEmail@winzerusa.com'
	--, @body = @tableHTML
       , @body_format = 'HTML'
	, @subject = 'No data found in AS400 Invoice header table - PIIHP100'
	--, @file_attachments = @fullFileName; --YOU CAN ALSO ATTACH A FILE TO THE MAIL IF NEED BE.
    end

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/

-- EXEC [dbo].[InventoryBalancesDaily]
CREATE PROCEDURE [dbo].[InventoryBalancesDaily]
AS 
BEGIN

-- add date key 
DROP TABLE IF EXISTS #Date
SELECT row_number() OVER (ORDER BY DateKey) AS [Key] ,BI_DWH.dbo.DimDate.* INTO #Date FROM BI_DWH.dbo.DimDate 

-- load inventory and assign date key and filters
DROP TABLE IF EXISTS #Inventory
SELECT 

        DA.[KEY]
      , DA.Date
      , DA.FiscalYear
	  , DA.FiscalMonth
      , DA.Year
      , DA.Month
      , DA.MonthName 
      ,[WarehouseID]
	  ,CASE WHEN INV.WarehouseID = 1 THEN 'Plano'
	        WHEN INV.WarehouseID = 3 THEN 'Reno'
			WHEN INV.WarehouseID = 4 THEN 'Santa Fe Springs'
			WHEN INV.WarehouseID = 5 THEN 'Saginaw'
			ELSE 'Other' END WarehouseName
      ,[ItemNum]
      ,[InventoryDate]
      ,[ItemClass]
      ,[InventoryTotal]
      ,[COGS]
      ,[DateAdded]
  INTO #Inventory FROM [BI_STAGING].[dbo].[Inventory] INV INNER JOIN #Date DA ON INV.InventoryDate = DA.DateKey
  WHERE INV.WarehouseID IN (1,3,4,5) AND INV.ItemClass <> '' 
        AND INV.ItemClass NOT IN ('90','89','93','91') -- Excluding Bill only, Specials, VSN, FDP
  ORDER BY DA.Date ASC

-- get the count of total records of inventory 
DECLARE @InventoryCount INT 
SET @InventoryCount = 0
SELECT @InventoryCount = COUNT(DISTINCT InventoryDate) FROM #Inventory

 -- insert the new inventory days into DailyInventoryDetails when we have at least 

IF (@InventoryCount = 2)
BEGIN

DELETE  FROM BI_DWH.dbo.DailyInventoryDetails WHERE  YEAR(ProcessDate) * 10000 + MONTH(ProcessDate) * 100 + DAY(ProcessDate) IN (SELECT MAX(InventoryDate) FROM [BI_STAGING].[dbo].[Inventory]) 
INSERT INTO BI_DWH.dbo.DailyInventoryDetails
SELECT inv2.Year,
       inv2.Month,
       inv2.MonthName,
       inv2.FiscalYear,
       inv2.FiscalMonth,
       inv2.WarehouseID,
       inv2.WarehouseName,
	   Inv2.ItemClass,
	   Inv2.ItemNum,
	   IC.Description AS [ItemClassName],
       inv1.Date AS [BeginningDate],
       inv2.Date AS [ProcessDate],
       SUM(inv1.InventoryTotal) AS [BeginningInventory],
       SUM(inv2.InventoryTotal) AS EndingInventory,
	   SUM(inv2.COGS) AS [CostOfGoodsSold],
	   GETDATE() AS DateAdded
FROM #Inventory inv1
    INNER JOIN #Inventory inv2
        ON inv1.WarehouseID = inv2.WarehouseID
           AND inv1.ItemNum = inv2.ItemNum
           AND inv1.ItemClass = inv2.ItemClass
           AND inv1.[Key] < inv2.[Key] 
	LEFT OUTER JOIN BI_DWH.dbo.DimItemClass IC ON IC.ItemClassCode = inv2.ItemClass
GROUP BY inv2.Year,
         inv2.Month,
         inv2.FiscalYear,
         inv2.FiscalMonth,
         inv1.Date,
         inv2.Date,
         inv2.MonthName,
         inv2.WarehouseID,
         inv2.WarehouseName,
		 Inv2.ItemClass,
		 IC.Description ,
		 Inv2.ItemNum
ORDER BY Inv2.YEAR, Inv2.MONTH, ProcessDate, Inv2.WarehouseID


DELETE from [BI_STAGING].[dbo].[Inventory] WHERE InventoryDate NOT IN (SELECT MAX(InventoryDate) FROM [BI_STAGING].[dbo].[Inventory]) 
delete from DailyInventoryDetails where ProcessDate < DATEADD(MONTH, -1,GETDATE()) 
end

END

GO

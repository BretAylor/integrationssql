SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      2/6/2019
-- Description  Net sales by Salesrep, day, customer, product A1Chemical report

-- Copyright © 2019, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History

-------------------------------------------------------------------------------
-- exec [dbo].[A1ChemicalNetSalesReport]

CREATE PROCEDURE [dbo].[A1ChemicalNetSalesReport]
AS
BEGIN

SELECT DD.[Year],
	   DD.FiscalYear,
       DD.FiscalMonth,
	   DD.[MonthName],
	   DD.[Date],
	   CU.CustomerName,
	   CU.CustomerNum,
	   PR.ProductDescription,
	   PR.ProductNum,
	   EM.SalesRepresentativeName,
	   EM.SalesRepresentativeNum,
	   FC.InvoiceNo ,
	   FC.OrderNo ,
	   FC.[LineNo] ,
	   FC.QtyShipped AS QuantityShipped,
	   FC.UnitPrice,
       FC.NetSales ,
	   FC.FranchiseCost,
	   FC.WinzerCost,
	   FC.Cost,
	   FC.[Type],
	   'Reno' AS [Location],
	   FC.DateAdded
FROM BI_DWH.dbo.FactA1ChemicalNetSales FC
    INNER JOIN BI_DWH.dbo.DimDate DD
        ON FC.DateKey = DD.DateKey
    LEFT OUTER JOIN BI_DWH.dbo.DimCustomer CU
        ON CU.CustomerKey = FC.CustomerKey
    LEFT OUTER JOIN BI_DWH.dbo.DimProduct PR
        ON PR.ProductKey = FC.ProductKey
    LEFT OUTER JOIN BI_DWH.dbo.DimEmployee EM
        ON EM.EmployeeKey = FC.EmployeeKey
WHERE DD.FiscalYear >= 2018 AND [Type] = 'A1Reno'

UNION ALL

SELECT DD.[Year],
	   DD.FiscalYear,
       DD.FiscalMonth,
	   DD.[MonthName],
	   DD.[Date],
	   CU.CustomerName,
	   CU.CustomerId AS CustomerNum,
	   PR.ItemDesc AS ProductDescription,
	   PR.ItemId AS ProductNum,
	   ISNULL(EM.FirstName,'') + ' '+ ISNULL(EM.Middle,'') +' ' + ISNULL(EM.LastName,'') AS SalesRepresentativeName,
	   EM.SalesRepID AS SalesRepresentativeNum,
	   FC.InvoiceNo ,
	   FC.OrderNo ,
	   FC.[LineNo] ,
	   FC.QtyShipped AS QuantityShipped,
	   FC.UnitPrice,
       FC.NetSales ,
	   FC.FranchiseCost,
	   FC.WinzerCost,
	   FC.Cost,
	   FC.[Type],
	   'Las Vegas' AS [Location],
	   FC.DateAdded
FROM BI_DWH.dbo.FactA1ChemicalNetSales FC
    INNER JOIN BI_DWH.dbo.DimDate DD
        ON FC.DateKey = DD.DateKey
    LEFT OUTER JOIN BI_DWH.dbo.DimCustomerP21 CU
        ON CU.CustomerKey = FC.CustomerKey
    LEFT OUTER JOIN BI_DWH.dbo.DimProductP21 PR
        ON PR.ProductKey = FC.ProductKey
    LEFT OUTER JOIN BI_DWH.dbo.DimEmployeeP21 EM
        ON EM.EmployeeKey = FC.EmployeeKey
WHERE DD.FiscalYear >= 2018 AND [Type] = 'A1LasVegas'




END
GO

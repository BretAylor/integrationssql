SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		baylor
-- Create date: 6/2020
-- Description:	Update to Pepperi_Staging 
-- =============================================
create PROCEDURE [StageAP].[PromoCodeByCustomerUpdate] 
AS

BEGIN

SET NOCOUNT ON;

DECLARE @RunDate DATETIME = GETDATE()

UPDATE t1
   SET [CustomerNumber] = t2.CustomerNumber
      ,[ItemNumber] = t2.ItemNumber
      ,[PromoFranchiseCost] = t2.PromoFranchiseCost
      ,[StartDate] = t2.StartDate
      ,[EndDate] = t2.EndDate
      ,[LinkToFlyer] = t2.LinkToFlyer
	  ,Deleted = 0
      ,[ModifiedDate] = @RunDate
      ,[ModifiedBy] = SUSER_NAME()
From  [dbo].[PromoCodeByCustomer]  t1
join StageAP.PromoCodeByCustomer t2
on t1.PromoID = t2.PromoID
 WHERE t1.[CustomerNumber] <>  t2.CustomerNumber
      or t1.[ItemNumber] <>  t2.ItemNumber
      or t1.[PromoFranchiseCost] <>  t2.PromoFranchiseCost
      or t1.[StartDate] <>  t2.StartDate
      or t1.[EndDate] <>  t2.EndDate
      or ISNULL(t1.[LinkToFlyer],'xxx') <>  ISNULL(t2.LinkToFlyer,'xxx')
	  OR t1.Deleted = 1

INSERT INTO [dbo].[PromoCodeByCustomer]
           ([PromoID]
           ,[CustomerNumber]
           ,[ItemNumber]
           ,[PromoFranchiseCost]
           ,[StartDate]
           ,[EndDate]
           ,[LinkToFlyer]
           ,[CreatedDate]
           ,[ModifiedDate])
SELECT t1.[PromoID]
           ,t1.[CustomerNumber]
           ,t1.[ItemNumber]
           ,t1.[PromoFranchiseCost]
           ,t1.[StartDate]
           ,t1.[EndDate]
           ,t1.[LinkToFlyer]
           ,@RunDate [CreatedDate]
           ,@RunDate [ModifiedDate]
FROM StageAP.PromoCodeByCustomer t1
LEFT JOIN dbo.PromoCodeByCustomer t2
ON t1.Promoid = t2.promoid
WHERE t2.promoid IS null

-- Delete 
UPDATE t1
SET Deleted = 1,
ModifiedDate = @Rundate
from [dbo].[PromoCodeByCustomer] t1
Left join [StageAP].[PromoCodeByCustomer] t2
on t1.PromoID = t2.Promoid
WHERE t2.PromoID IS NULL

END
GO

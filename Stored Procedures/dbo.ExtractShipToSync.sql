SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ExtractShipToSync] 

AS
BEGIN

SET NOCOUNT ON;

Update t1
set ErpSyncDate = getdate(),
ErpSyncBy = suser_name()
From ShipTo t1 
join [WINSQLBI01].[PEPPERI].[dbo].[ShipToPepperiStaging] t2 with (nolock)  
on t1.CustomerNumber = t2.[CUSTNO_CustomerNumber]  -- was staged
and t1.ShipToNumber = t2.[SHPTO#_ShipToNumber]

END
GO

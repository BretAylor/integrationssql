SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Matt Thompson
-- Create date: 12/04/2018
-- Description:	Inserts new order header for SSIS package
-- =============================================
CREATE PROCEDURE [dbo].[CustomerCrossReferenceInsert] 
	-- Add the parameters for the stored procedure here
	@p1 decimal(18,0)
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[ShipToCrossReference]
                         ([PepperiCustomerId])

						 OUTPUT CAST(INSERTED.[AS400TempCustomerId] AS decimal(7,0)) AS newas400value

VALUES (@p1)

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*************************************************************                        
** File: usp_SelectAndDecryptCCards.sql                        
** Author:Hussam Ismail
** Description: 
** Select and decrypt credit cards persisted on table
** Date: 06/12/2019                        
                        
** PARAMETERS:                        
NONE
	
** RETURN VALUE:                        
All rows in credit card table with decrypted card numbers

**************************************************************             
** Change History             
**************************************************************
** PR Date        Author            Change Description              
** -- ----------  ---------------   --------------------------
** 1  06/12/2019  Hussam Ismail		Initial development

**************************************************************
exec [usp_SelectAndDecryptCCards] 
**************************************************************/
CREATE PROCEDURE [dbo].[usp_SelectAndDecryptCCards] 
AS
	BEGIN
		--SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
		--SET NOCOUNT ON;         
		--BEGIN TRY             
		--	BEGIN TRANSACTION;

				OPEN SYMMETRIC KEY CC_Key_Symmetric  
				DECRYPTION BY CERTIFICATE CCCertificate; 

				SELECT [Id]
					  ,[Guid]
					  ,[CCDescription]
					  ,[CCType]
					  ,CONVERT(varchar, DecryptByKey([CCNumber], 1, HashBytes('SHA2_512', CONVERT(varbinary, [Guid])))) AS 'CCNumber'
					  ,[CVVNumber]
					  ,[LastFourDigits]
					  ,[ExpirationDate]
					  ,[CustomerNumber]
					  ,[FirstName]
					  ,[MiddleName]
					  ,[LastName]
					  ,[ZipCode]
					  ,[Note]
					  ,[KeepOnFile]
					  ,[CreatedDate]
					  ,[CreatedBy]
					  ,[ModifiedDate]
					  ,[ModifiedBy]
				  FROM [dbo].[CustomerCC]	
				 

		--	COMMIT TRANSACTION;
		--END TRY
		--BEGIN CATCH
		--	ROLLBACK TRANSACTION;
		--	RETURN(1);
		--END CATCH;
	END;
GO
GRANT EXECUTE ON  [dbo].[usp_SelectAndDecryptCCards] TO [WINZERNT\svc_pepperi_cc]
GO

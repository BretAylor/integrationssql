SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Declare Variables for HTML
*/

CREATE PROCEDURE [dbo].[CustomerAS400ToStagingMissingNotification]
AS
Begin
DECLARE @Style NVARCHAR(MAX)= '';
 
/*
Define CSS for html to use
*/
SET @Style += +N'<style type="text/css">' + N'.tg  {border-collapse:collapse;border-spacing:0;border-color:#aaa;}'
    + N'.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#333;background-color:#fff;}'
    + N'.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#fff;background-color:#f38630;}'
    + N'.tg .tg-9ajh{font-weight:bold;background-color:#ADD8E6}' + N'.tg .tg-hgcj{font-weight:bold;text-align:center}'
    + N'</style>';
 

 
/*
Declare Variables for DML
*/
 
--DECLARE @ReportingPeriodStart DATE;
--DECLARE @ReportingPeriodEnd DATE;
 
 -- Reporting date will from the beginning of the current week
--SET @ReportingPeriodStart = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(YEAR, -3, GETDATE())), 0);
--SET @ReportingPeriodEnd = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(YEAR, -3, GETDATE())) + 1, 0);
 
/*
Declare Variables for HTML
*/
 
DECLARE @tableHTML NVARCHAR(MAX)= '';
DROP TABLE IF EXISTS #Staging
SELECT CustomerNumber INTO #Staging FROM [172.25.6.17].PEPPERI_STAGING.dbo.Customer

SET @tableHTML += @Style + @tableHTML +
 --N'<H2>Order Summary For : ' + CAST(@ReportingPeriodStart AS CHAR(10)) + ' to ' + CAST(@ReportingPeriodEnd AS CHAR(10)) + '</H2>' 
	+ N'<table class="tg">' --DEFINE TABLE
/*
Define Column Headers and Column Span for each Header Column
*/
	--+ N'<tr>' 
 --   + N'<th class="tg-hgcj" colspan="2">Order Information</th>' 
	--+ N'<th class="tg-hgcj" colspan="2">Summary</th>'
	--+ N'</tr>' 
/*
Define Column Sub-Headers
*/
	+ N'<tr>'
	+ N'<td class="tg-9ajh">Customer Number</td>'
    + N'<td class="tg-9ajh">Created Date</td></tr>'


/*
Define data for table and cast to xml
*/
    + CAST(( SELECT 
					
                    td = BI.CustomerNumber,
					'',
                    td = BI.CreatedDate,
					''
                   
               FROM [PEPPERI].[dbo].[Customer] BI LEFT OUTER JOIN #Staging ST ON BI.CustomerNumber = ST.CustomerNumber
               WHERE ST.CustomerNumber IS NULL 
			

           FOR
             XML PATH('tr') ,
                 TYPE
           ) AS NVARCHAR(MAX)) + N'</table>'
	
 

    DECLARE @Count INT
	SET @Count = 0

	SELECT @Count = COUNT(*)
    FROM [PEPPERI].[dbo].[Customer] BI LEFT OUTER JOIN #Staging ST ON BI.CustomerNumber = ST.CustomerNumber
    WHERE ST.CustomerNumber IS NULL 



    IF(@Count <> 0 )
	begin
	EXEC msdb.dbo.sp_send_dbmail 
	@profile_name = 'Account'
	, @recipients = 'saad.benhalima@winzerusa.com'
    , @from_address = 'WinzerAutomatedEmail@winzerusa.com'
	, @body = @tableHTML
       , @body_format = 'HTML'
	, @subject = 'Missing Customer in Pepperi Staging From AS400'
	--, @file_attachments = @fullFileName; --YOU CAN ALSO ATTACH A FILE TO THE MAIL IF NEED BE.
    end
 end
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		baylor
-- Create date: 4/2020
-- Description:	Update customer with Perm customernumber
-- =============================================
CREATE PROCEDURE [StageAP].[CustomerNumberUpdate] 
AS

BEGIN

SET NOCOUNT ON;

-- Update From AS400 Table
update  cu
set CustomerNumber = ct.CustomerNumber
from stageAP.CustomerTempNumber ct
join [dbo].[Customer] cu 
on ct.TempCustomerNumber = cu.[CustomerNumber] 
and ct.SalesRepNumber  = cu.SalesRepNumber
where cu.customerNumber <> ct.CustomerNumber

-- Update Customer from Cross Reference
UPDATE t1 
SET CustomerNumber = t3.CustomerNumber
FROM dbo.Customer t1
JOIN dbo.CustomerCrossReference t2
ON t1.customernumber = t2.PepperiCustomerId
JOIN stageAP.CustomerTempNumber t3
ON t2.AS400TempCustomerId = t3.TempCustomerNumber
where t1.customerNumber <> t3.CustomerNumber

-- Update SalesOrder
UPDATE t1 
SET CustomerNbr = t3.CustomerNumber
FROM dbo.SalesOrder t1
JOIN dbo.CustomerCrossReference t2
ON t1.customernbr = t2.PepperiCustomerId
JOIN stageAP.CustomerTempNumber t3
ON t2.AS400TempCustomerId = t3.TempCustomerNumber
WHERE ISNULL(t1.ErpSyncDate,'01/01/1900') < t1.ModifiedDate
AND CustomerNbr <> t3.CustomerNumber
/*
-- Update SalesOrder from Cross Reference
UPDATE t1 
SET Customernbr = t3.CustomerNumber,
erpmodifieddate = GETDATE(),
erpmodifiedby = SUSER_SNAME(),
modifieddate = GETDATE(),
modifiedby = SUSER_SNAME(),
erpsyncdate = NULL,
erpsyncby = NULL
FROM dbo.salesorder t1
JOIN dbo.CustomerCrossReference t2
ON t1.customernbr = t2.PepperiCustomerId
JOIN stageAP.CustomerTempNumber t3
ON t2.AS400TempCustomerId = t3.TempCustomerNumber
WHERE t1.createddate > DATEADD(dd,-7, getdate())
*/


END
GO

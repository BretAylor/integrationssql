SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- Logic Changes: 
-- 5/7/2020
-- 1) Changed from Left to Join for Sales Order
-- 2) Sales Order ERPSyncDate not null, so we know sales order was picked up
-- =============================================
CREATE PROCEDURE [dbo].[ExtractSalesOrderLineKit] 

AS
BEGIN

	SET NOCOUNT ON;

	SELECT [PadDescription]
      ,[CustomerNbr]
      ,[PepperiOrderNbr]
      ,[LineNbr]
      ,[LineItemType]
      ,[ParentItemNumber]
      ,[ComponentItem]
      ,[LineItemComment]
      ,[DetailCommentCode]
      ,[OrderQuantity]
      ,[LabelQty]
      ,[OrderPrice]
      ,[FranchiseCost]
      ,[UnitOfMeasure]
      ,[CustomerContract]
      ,[BasePrice]
      ,[CommissionCode]
      ,[CommissionPercent]
      ,[MarkupPercent]
      ,[MarkupAmount]
      ,[FreightUpChargeCode]
      ,[FreightUpChargePercent]
      ,[FreightUpChargeAmount]
      ,[BaseFranchiseCost]
      ,[QtyDiscountAmount]
      ,[QtyBreakRate]
      ,[PrefixBreakAmount]
      ,[PrefixBreakRate]
      ,[SplitsLoadedFranchise]
      ,[AlphaUserField1]
      ,[AlphaUserField2]
      ,[AlphaUserField3]
      ,[AlphaUserField4]
      ,[AlphaUserField5]
      ,[AlphaUserField6]
      ,[AlphaUserField7]
      ,[AlphaUserField8]
      ,[AlphaUserField9]
      ,[AlphaUserField10]
      ,[AlphaUserField11]
      ,[AlphaUserField12]
      ,[AlphaUserField13]
      ,[AlphaUserField14]
      ,[AlphaUserField15]
      ,[AlphaUserField16]
      ,[AlphaUserField17]
      ,[AlphaUserField18]
      ,[AlphaUserField19]
      ,[AlphaUserField20]
      ,[NumericUserField1]
      ,[NumericUserField2]
      ,[NumericUserField3]
      ,[NumericUserField4]
      ,[NumericUserField5]
      ,[NumericUserField6]
      ,[NumericUserField7]
      ,[NumericUserField8]
      ,[NumericUserField9]
      ,[NumericUserField10]
      ,[NumericUserField11]
      ,[NumericUserField12]
      ,[NumericUserField13]
      ,[NumericUserField14]
      ,[NumericUserField15]
      ,[NumericUserField16]
      ,[NumericUserField17]
      ,[NumericUserField18]
      ,[NumericUserField19]
      ,[NumericUserField20]
  FROM [StagePA].[SalesOrderLineKitFormatted]

	/*
SELECT
       ISNULL(RIGHT('00000' + Cast(SO.[RepNbr] as varchar(20)),5),'')  as PadDescription,
	   case when ISNULL(CustomerNbr,0) > 1000000 then cast(CR.AS400TempCustomerId as decimal) else ISNULL(CustomerNbr,0) end as CustomerNbr,
	   isnull(PepperiOrderNbr,0) as PepperiOrderNbr,
	   10000 + row_number() OVER (PARTITION BY PepperiOrderNbr ORDER BY  BrandedProductNbr , [LineNbr] ) as LineNbr ,
	   'I' as LineItemType,
	   isnull(ParentItemNumber,'') as ParentItemNumber,
	   left(isnull(BrandedProductNbr,''),15) as ComponentItem,
	   left(isnull(OL.Comment,''),30) as LineItemComment,
'' as DetailCommentCode,
isnull(OrderQuantity,0) as OrderQuantity,
isnull(LabelQty,0) as LabelQty,
isnull(OrderPrice,0) as OrderPrice,
isnull(OrderCost,0) as FranchiseCost,
isnull(UnitOfMeasure,'') as UnitOfMeasure ,
isnull(CustomerContract,'') as CustomerContract,
0 as BasePrice,
'' as CommissionCode,
0 as CommissionPercent,
0 as MarkupPercent,
0 as MarkupAmount,
'' as FreightUpChargeCode,
0 as FreightUpChargePercent,
0 as FreightUpChargeAmount,
cast(isnull(OriginalFranchisePrice,0) as numeric(11,4))as BaseFranchiseCost ,
0 as QtyDiscountAmount,
0 as QtyBreakRate,
0 as PrefixBreakAmount,
0 as PrefixBreakRate  ,  
isnull(LandedCost,0)  as SplitsLoadedFranchise ,
''as AlphaUserField1,
''as AlphaUserField2,
''as AlphaUserField3,
''as AlphaUserField4,
''as AlphaUserField5,
''as AlphaUserField6,
''as AlphaUserField7,
''as AlphaUserField8,
''as AlphaUserField9,
''as AlphaUserField10,
''as AlphaUserField11,
''as AlphaUserField12,
''as AlphaUserField13,
''as AlphaUserField14,
''as AlphaUserField15,
''as AlphaUserField16,
''as AlphaUserField17,
''as AlphaUserField18,
''as AlphaUserField19,
''as AlphaUserField20,
0 as NumericUserField1,
0 as NumericUserField2,
0 as NumericUserField3,
0 as NumericUserField4,
0 as NumericUserField5,
0 as NumericUserField6,
0 as NumericUserField7,
0 as NumericUserField8,
0 as NumericUserField9,
0 as NumericUserField10,
0 as NumericUserField11,
0 as NumericUserField12,
0 as NumericUserField13,
0 as NumericUserField14,
0 as NumericUserField15,
0 as NumericUserField16,
0 as NumericUserField17,
0 as NumericUserField18,
0 as NumericUserField19,
0 as NumericUserField20
   
FROM [PEPPERI_STAGING].[dbo].[SalesOrderLine] OL  with (nolock) 
join dbo.[SalesOrder] SO with (nolock) 
on OL.PepperiId = SO.PepperiOrderNbr 
LEFT OUTER JOIN dbo.Customer CU with (nolock) 
ON (CU.CustomerNumber = ISNULL(SO.CustomerNbr,0) 
or CU.PepperiCustomerNumber = ISNULL(SO.CustomerNbr,0))
left outer join CustomerCrossReference CR  with (nolock) 
on left(CR.PepperiCustomerId,8) = left(CU.PepperiCustomerNumber,8)
where ParentItemNumber is not null -- Kit 
and OL.erpsyncdate is null  -- not processed
and so.erpsyncdate is not null -- checking if sales Order picked up and delivered to AS400 -- Not guaranteed
--and PepperiOrderNbr not in (select  PepperiOrderNbr from [dbo].[SalesOrderHeader_LinesValidationView])
and so.PepperiOrderNbr  not in (select [Order] from [stagePA].[ExtractSalesOrderExclude])
*/

End
GO

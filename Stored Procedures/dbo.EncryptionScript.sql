SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create Procedure [dbo].[EncryptionScript]
as
begin

CREATE MASTER KEY ENCRYPTION BY PASSWORD = 'B6DB67F1-64FF-4E79-ADFF-81801C206D5C';  


CREATE CERTIFICATE CCCertificate  
   WITH SUBJECT = 'Customer Credit Card Numbers';  
 

CREATE SYMMETRIC KEY CC_Key_Symmetric  
    WITH ALGORITHM = AES_256  
    ENCRYPTION BY CERTIFICATE CCCertificate;  


GRANT CONTROL ON CERTIFICATE::CCCertificate TO [WINZERNT\svc_pepperi_cc]; 

GRANT CONTROL ON SYMMETRIC KEY::CC_Key_Symmetric TO [WINZERNT\svc_pepperi_cc]; 


end
GO

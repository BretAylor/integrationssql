SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[CriticalChargeBackUpdate]
AS
Begin

Set Transaction Isolation Level Read Uncommitted 

UPDATE CB  set 
CB.TextCB = UP.TextCB
,CB.ModifiedDate = getdate()
,CB.ModifiedBy = suser_name()
,CB.ErpSyncDate = getdate()
,CB.ErpSyncBy = suser_name()
FROM WINSQLBI01.PEPPERI.dbo.CriticalChargeBackUpdate UP with (nolock) 
INNER JOIN dbo.Criticalchargeback CB  with (nolock) ON 
UP.PadDescription = CB.PadDescription AND 
UP.ProcessDate   = CB.ProcessDate AND 
UP.SequenceNum = CB.SequenceNum

End



GO

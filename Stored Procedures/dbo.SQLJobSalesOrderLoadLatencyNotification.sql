SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
Declare Variables for HTML
*/
-- exec [dbo].[SQLJobLoadLatencyNotification]
CREATE PROCEDURE [dbo].[SQLJobSalesOrderLoadLatencyNotification]
AS
BEGIN
DECLARE @Style NVARCHAR(MAX)= '';
 
/*
Define CSS for html to use
*/
SET @Style += +N'<style type="text/css">' + N'.tg  {border-collapse:collapse;border-spacing:0;border-color:#aaa;}'
    + N'.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#333;background-color:#fff;}'
    + N'.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#fff;background-color:#f38630;}'
    + N'.tg .tg-9ajh{font-weight:bold;background-color:#ADD8E6}' + N'.tg .tg-hgcj{font-weight:bold;text-align:center}'
    + N'</style>';
 

 
/*
Declare Variables for DML
*/
 
--DECLARE @ReportingPeriodStart DATE;
--DECLARE @ReportingPeriodEnd DATE;
 
 -- Reporting date will from the beginning of the current week
--SET @ReportingPeriodStart = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(YEAR, -3, GETDATE())), 0);
--SET @ReportingPeriodEnd = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(YEAR, -3, GETDATE())) + 1, 0);
 
/*
Declare Variables for HTML
*/
 
DECLARE @tableHTML NVARCHAR(MAX)= '';


SET @tableHTML += @Style + @tableHTML +
 --N'<H2>Order Summary For : ' + CAST(@ReportingPeriodStart AS CHAR(10)) + ' to ' + CAST(@ReportingPeriodEnd AS CHAR(10)) + '</H2>' 
	+ N'<table class="tg">' --DEFINE TABLE
/*
Define Column Headers and Column Span for each Header Column
*/
	--+ N'<tr>' 
 --   + N'<th class="tg-hgcj" colspan="2">Order Information</th>' 
	--+ N'<th class="tg-hgcj" colspan="2">Summary</th>'
	--+ N'</tr>' 
/*
Define Column Sub-Headers
*/
	+ N'<tr>'
	+ N'<td class="tg-9ajh">Job Name</td>'
    + N'<td class="tg-9ajh">Date</td>'
	+ N'<td class="tg-9ajh">Hour</td>'
	+ N'<td class="tg-9ajh">HourPart</td>'
	+ N'<td class="tg-9ajh">Duration higher then 5 Minutes</td></tr>'

/*
Define data for table and cast to xml
*/
    + CAST(( SELECT 
					
                    td = j.name,
					'',
                    td =CAST(msdb.dbo.agent_datetime(run_date, run_time) AS DATE),
					'',
                    td =DATEPART(HOUR, CAST(msdb.dbo.agent_datetime(run_date, run_time) AS DATETIME)) ,
					'',
                    td =CASE WHEN DATEPART(MINUTE, CAST(msdb.dbo.agent_datetime(run_date, run_time) AS DATETIME)) BETWEEN 0 AND 29 THEN 1 ELSE 2 END	,
					'',
					td= SUM(CAST(((run_duration/10000*3600 + (run_duration/100)%100*60 + run_duration%100 + 31 ) / 60.0)  AS DECIMAL(9,2))) ,
		            ''
					 FROM msdb.dbo.sysjobs j 
					INNER JOIN msdb.dbo.sysjobsteps s 
					 ON j.job_id = s.job_id
					INNER JOIN msdb.dbo.sysjobhistory h 
					 ON s.job_id = h.job_id 
					 AND s.step_id = h.step_id 
					 AND h.step_id <> 0
					WHERE j.enabled = 1   --Only Enabled Jobs
					AND ((run_duration/10000*3600 + (run_duration/100)%100*60 + run_duration%100 + 31 ) / 60.0)  >= 20
				    AND  CAST(msdb.dbo.agent_datetime(run_date, run_time) AS DATETIME) BETWEEN DATEADD(MINUTE, -30,GETDATE()) AND GETDATE() 
					AND [Name] = '09_Pepperi_AS400_Customer_ShipTo_SalesOrder_Load'
	   GROUP BY CAST(msdb.dbo.agent_datetime(run_date, run_time) AS DATE) , 
       DATEPART(HOUR, CAST(msdb.dbo.agent_datetime(run_date, run_time) AS DATETIME)) ,
	   CASE WHEN DATEPART(MINUTE, CAST(msdb.dbo.agent_datetime(run_date, run_time) AS DATETIME)) BETWEEN 0 AND 29 THEN 1 ELSE 2 END,
       j.Name 
					
			
					
			

           FOR
             XML PATH('tr') ,
                 TYPE
           ) AS NVARCHAR(MAX)) + N'</table>'
	
 

    DECLARE @Count INT
	SET @Count = 0

	SELECT @Count = COUNT(*)
   		            FROM msdb.dbo.sysjobs j 
					INNER JOIN msdb.dbo.sysjobsteps s 
					 ON j.job_id = s.job_id
					INNER JOIN msdb.dbo.sysjobhistory h 
					 ON s.job_id = h.job_id 
					 AND s.step_id = h.step_id 
					 AND h.step_id <> 0
					WHERE j.enabled = 1   --Only Enabled Jobs
					AND ((run_duration/10000*3600 + (run_duration/100)%100*60 + run_duration%100 + 31 ) / 60.0)  >= 20
				    AND  CAST(msdb.dbo.agent_datetime(run_date, run_time) AS DATETIME) BETWEEN DATEADD(MINUTE, -30,GETDATE()) AND GETDATE() 
					AND [Name] = '09_Pepperi_AS400_Customer_ShipTo_SalesOrder_Load'
	   GROUP BY CAST(msdb.dbo.agent_datetime(run_date, run_time) AS DATE) , 
       DATEPART(HOUR, CAST(msdb.dbo.agent_datetime(run_date, run_time) AS DATETIME)) ,
	   CASE WHEN DATEPART(MINUTE, CAST(msdb.dbo.agent_datetime(run_date, run_time) AS DATETIME)) BETWEEN 0 AND 29 THEN 1 ELSE 2 END,
       j.Name 
					

    IF(@Count <> 0 )
	BEGIN
	EXEC msdb.dbo.sp_send_dbmail 
	@profile_name = 'Account'
	, @recipients = 'saad.benhalima@winzerusa.com;Bret.Aylor@winzerusa.com'
    , @from_address = 'WinzerAutomatedEmail@winzerusa.com'
	, @body = @tableHTML
       , @body_format = 'HTML'
	, @subject = 'SQL Job Sales Order Load Latency Notification'
	--, @file_attachments = @fullFileName; --YOU CAN ALSO ATTACH A FILE TO THE MAIL IF NEED BE.
    END
 END
GO

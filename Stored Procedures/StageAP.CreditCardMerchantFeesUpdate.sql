SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		baylor
-- Create date: 5/2020
-- Description:	Update to Pepperi_Staging 
-- =============================================
CREATE PROCEDURE [StageAP].[CreditCardMerchantFeesUpdate] 
AS

BEGIN

SET NOCOUNT ON;

Set Transaction Isolation Level Read Uncommitted

UPDATE CC  set 
 CC.ReportLine = CCP.ReportLine
,CC.ModifiedDate = getdate()
,CC.ModifiedBy = suser_name()
,CC.ErpSyncDate = getdate()
,CC.ErpSyncBy = suser_name()
FROM StageAP.CreditCardMerchantFees CCP with (nolock) 
JOIN dbo.CreditCardMerchantFees CC with (nolock) ON 
CCP.FranchiseNum = CC.FranchiseNum AND 
CCP.ProcessDate   = CC.ProcessDate AND 
CCP.SequenceNum = CC.SequenceNum
where ccp.ReportLine <> cc.ReportLine

INSERT INTO [dbo].[CreditCardMerchantFees]
           ([FranchiseNum]
           ,[ProcessDate]
           ,[SequenceNum]
           ,[ReportLine])
Select t1.[FranchiseNum]
           ,t1.[ProcessDate]
           ,t1.[SequenceNum]
           ,t1.[ReportLine]
From [StageAP].[CreditCardMerchantFees] t1
left JOIN dbo.CreditCardMerchantFees t2 with (nolock) ON 
t1.FranchiseNum = t2.FranchiseNum AND 
t1.ProcessDate   = t2.ProcessDate AND 
t1.SequenceNum = t2.SequenceNum
where t2.SequenceNum is null


-- No Delete 


END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		baylor
-- Create date: 5/20
-- Description:	Update Kit Lines
-- Logic Changes: 
-- 5/8/2020
-- 1) Only Update Kit Order lines
-- 2) Make sure the Sales order was processed
-- =============================================
CREATE PROCEDURE [dbo].[ExtractSalesOrderLineKitSync] 

AS
BEGIN

SET NOCOUNT ON;

DECLARE @RunDate DATETIME = (SELECT MAX(createdDate) FROM StagePA.SalesOrderToProcess)

Update t1
set ErpSyncDate = getdate(),
erpSyncby = SUSER_SNAME()
from StagePA.SalesOrderToProcess t2
join SalesOrderLine t1
on t1.PepperiId = t2.PepperiOrderNbr
where  t2.CreatedDate = @Rundate
AND t1.ParentItemNumber is not null  -- only kits

End
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		baylor
-- Create date: 05/20
-- Description:	Number SalesOrderLine for Submission to AS400
-- =============================================
CREATE PROCEDURE [StagePA].[SalesOrderLineNumbering] 

AS
BEGIN
-- truncate table stagepa.Salesorderlinenbr
SET NOCOUNT ON;

DECLARE @RunDate DATETIME = (SELECT MAX(createdDate) FROM StagePA.SalesOrderToProcess)

DELETE StagePA.SalesOrderLineNbr WHERE DATEADD(mm,-3,@RunDate) > CreatedDate  

;WITH CTE_Line (PepperiID, BrandedProductnbr, ParentItemNumber, LineItemType, Comment, OrigLineNbr)
AS 
(
SELECT t1.PepperiID, ltrim(RTRIM(t1.[BrandedProductNbr])), t1.ParentItemNumber, 'I' LineItemType, t1.Comment
,t1.LineNbr OrigLineNbr
FROM [dbo].[SalesOrderLine] t1 
JOIN StagePA.SalesOrderToProcess t2
on t1.PepperiId = t2.PepperiOrderNbr 
WHERE t2.createddate = @RunDate
),
CTE_Comment (PepperiID, BrandedProductnbr, ParentItemNumber, LineItemType, Comment, OrigLineNbr)
AS
(
SELECT PepperiID, BrandedProductnbr, ParentItemNumber, 'M' LineItemType, Comment, OrigLineNbr
FROM CTE_Line
WHERE Comment IS NOT NULL
)
insert StagePA.SalesOrderLineNbr
SELECT ROW_NUMBER ( )   
    OVER (PARTITION BY PepperiID 
	ORDER BY origLineNbr, ParentItemNumber, BrandedProductnbr,  LineItemType, Comment) + 999 LineNbr
,PepperiID, ParentItemNumber, BrandedProductnbr,  LineItemType, Comment, OrigLineNBR, @RunDate
From
(
SELECT PepperiID, BrandedProductnbr, ParentItemNumber, LineItemType, NULL Comment, OrigLineNbr 
FROM cte_line
UNION ALL
SELECT PepperiID, BrandedProductnbr, ParentItemNumber, LineItemType, Comment,  OrigLineNbr
FROM cte_Comment
) x

END

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		baylor
-- Create date: 05/2020
-- Description:	Pepperi Orders Submitted, but not in Staging
-- =============================================
CREATE PROCEDURE [rpt].[OrderRoundTrip] 

AS
BEGIN

SET NOCOUNT ON;

set transaction isolation level read uncommitted

Declare @MaxDate datetime = (select max(CreatedDate) from [rpt].[PepperiSalesOrder])

Begin Try Drop Table #Orders end try begin catch end catch

Select t1.PepperiOrderNbr, t1.OrderDate
into #Orders
From [rpt].[PepperiSalesOrder] t1
left join [dbo].[SalesOrder] t2
on T1.PepperiOrderNbr = t2.PepperiOrderNbr
where t1.CreatedDate = @MaxDate
and t2.PepperiOrderNbr is null

-- Send Email if Issues
If (Select Count(*) from #Orders) > 0
Begin
DECLARE @xml NVARCHAR(MAX)
DECLARE @body NVARCHAR(MAX)

SET @xml = CAST(( SELECT [PepperiOrderNbr] AS 'td','',cast(orderDate as varchar) AS 'td',''
FROM #Orders
ORDER BY [PepperiOrderNbr]
FOR XML PATH('tr'), ELEMENTS ) AS NVARCHAR(MAX))

SET @body ='<html><body><H3>Pepperi Staging: Pepperi Orders Submitted but not in Staging</H3>
<table border = 1> 
<tr>
<th> PepperiOrderNbr </th> <th> Submitted </th> </tr>'    
SET @body = @body + @xml +'</table></body></html>'

EXEC [WINSQLBI01].msdb.dbo.sp_send_dbmail  
    @profile_name = 'Account',  
    @recipients = 'bret.aylor@winzerusa.com',  
    @body = @body,  
    @subject = 'Pepperi Staging: Pepperi Orders Submitted but not in Staging',  
    @attach_query_result_as_file = 0,
	@importance  = 'High',
	@body_format = 'HTML'

END

End
GO

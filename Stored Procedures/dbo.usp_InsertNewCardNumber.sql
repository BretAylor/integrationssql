SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*************************************************************                        
** File:                        
** Author:Hussam Ismail
** Description: 
** Insert encrypted credit card number together with 
** accompanying metadata
** Date: 06/11/2019                        
                        
** PARAMETERS:                        
@AccountId		- Customer External Id
@CardType
@CardNumber		- OpenText card number
@CardTitle		
@CVVNumber
@ExpDate		- format MM/YY
@FirstName
@LastName
@MiddleName
@ZipCode
	
** RETURN VALUE:                        
@CardId

**************************************************************             
** Change History             
**************************************************************
** PR Date        Author            Change Description              
** -- ----------  ---------------   --------------------------
** 1  06/11/2019  Hussam Ismail		Initial development

**************************************************************
exec [usp_InsertNewCardNumber] 
**************************************************************/
CREATE PROCEDURE [dbo].[usp_InsertNewCardNumber]
(
@AccountId varchar(50),
@CardType varchar(10),
@CardNumber	varchar(50),
@CardTitle varchar(200),
@CVVNumber varchar(10),
@ExpDate varchar(10),
@FirstName varchar(50),
@LastName varchar(50),
@MiddleName varchar(50),
@ZipCode varchar(10)
)
AS
	BEGIN
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
		SET NOCOUNT ON;         
		BEGIN TRY             
			BEGIN TRANSACTION;

				DECLARE @NewCardId UNIQUEIDENTIFIER = NEWID();
				OPEN SYMMETRIC KEY CC_Key_Symmetric  
				DECRYPTION BY CERTIFICATE CCCertificate;  

				INSERT INTO [CustomerCC] ([Guid], CCDescription, CCType, CCNumber, CVVNumber, LastFourDigits, ExpirationDate,
										  CustomerNumber, FirstName, MiddleName, LastName, ZipCode, Note, KeepOnFile,
										  CreatedDate, CreatedBy, ModifiedDate, ModifiedBy)
				VALUES (@NewCardId, @CardTitle, @CardType, 
				EncryptByKey(Key_GUID('CC_Key_Symmetric'), @CardNumber, 1, HashBytes('SHA2_512', CONVERT( varbinary, @NewCardId))),	
				@CVVNumber, RIGHT(@CardNumber, 4), left(@ExpDate,2) +'/' + right(@ExpDate,2) , 
				@AccountId, @FirstName, @MiddleName, @LastName, @ZipCode, null, null,
				GETDATE(), 'PepperiApp', null, null)
				SELECT CAST(@NewCardId AS VARCHAR(50))

			COMMIT TRANSACTION;
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION;
             
				SELECT ''

			RETURN(1);
		END CATCH;
	END;


GO
GRANT EXECUTE ON  [dbo].[usp_InsertNewCardNumber] TO [WINZERNT\svc_pepperi_cc]
GO

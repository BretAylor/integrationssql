SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		baylor
-- Create date: 05/20
-- Description:	Format SalesOrderLine Kit for Submission to AS400
-- =============================================
CREATE PROCEDURE [StagePA].[SalesOrderLineKitFormat] 

AS
BEGIN

SET NOCOUNT ON;

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

DECLARE @RunDate DATETIME = (SELECT MAX(CreatedDate) FROM StagePA.SalesOrderToProcess)

DELETE StagePA.SalesOrderLineKitFormatted WHERE DATEADD(mm,-3,@RunDate) > CreatedDate  

INSERT INTO [StagePA].[SalesOrderLineKitFormatted]
           ([PadDescription]
           ,[CustomerNbr]
           ,[PepperiOrderNbr]
           ,[LineNbr]
           ,[LineItemType]
           ,[ParentItemNumber]
           ,[ComponentItem]
           ,[LineItemComment]
           ,[DetailCommentCode]
           ,[OrderQuantity]
           ,[LabelQty]
           ,[OrderPrice]
           ,[FranchiseCost]
           ,[UnitOfMeasure]
           ,[CustomerContract]
           ,[BasePrice]
           ,[CommissionCode]
           ,[CommissionPercent]
           ,[MarkupPercent]
           ,[MarkupAmount]
           ,[FreightUpChargeCode]
           ,[FreightUpChargePercent]
           ,[FreightUpChargeAmount]
           ,[BaseFranchiseCost]
           ,[QtyDiscountAmount]
           ,[QtyBreakRate]
           ,[PrefixBreakAmount]
           ,[PrefixBreakRate]
           ,[SplitsLoadedFranchise]
           ,[AlphaUserField1]
           ,[AlphaUserField2]
           ,[AlphaUserField3]
           ,[AlphaUserField4]
           ,[AlphaUserField5]
           ,[AlphaUserField6]
           ,[AlphaUserField7]
           ,[AlphaUserField8]
           ,[AlphaUserField9]
           ,[AlphaUserField10]
           ,[AlphaUserField11]
           ,[AlphaUserField12]
           ,[AlphaUserField13]
           ,[AlphaUserField14]
           ,[AlphaUserField15]
           ,[AlphaUserField16]
           ,[AlphaUserField17]
           ,[AlphaUserField18]
           ,[AlphaUserField19]
           ,[AlphaUserField20]
           ,[NumericUserField1]
           ,[NumericUserField2]
           ,[NumericUserField3]
           ,[NumericUserField4]
           ,[NumericUserField5]
           ,[NumericUserField6]
           ,[NumericUserField7]
           ,[NumericUserField8]
           ,[NumericUserField9]
           ,[NumericUserField10]
           ,[NumericUserField11]
           ,[NumericUserField12]
           ,[NumericUserField13]
           ,[NumericUserField14]
           ,[NumericUserField15]
           ,[NumericUserField16]
           ,[NumericUserField17]
           ,[NumericUserField18]
           ,[NumericUserField19]
           ,[NumericUserField20]
           ,[CreatedDate]) 
SELECT  [PadDescription]
           ,[CustomerNbr]
           ,[PepperiOrderNbr]
           ,[LineNbr]
           ,[LineItemType]
           ,[ParentItemNumber]
           ,[ComponentItem]
           ,[LineItemComment]
           ,[DetailCommentCode]
           ,[OrderQuantity]
           ,[LabelQty]
           ,[OrderPrice]
           ,[FranchiseCost]
           ,[UnitOfMeasure]
           ,[CustomerContract]
           ,[BasePrice]
           ,[CommissionCode]
           ,[CommissionPercent]
           ,[MarkupPercent]
           ,[MarkupAmount]
           ,[FreightUpChargeCode]
           ,[FreightUpChargePercent]
           ,[FreightUpChargeAmount]
           ,[BaseFranchiseCost]
           ,[QtyDiscountAmount]
           ,[QtyBreakRate]
           ,[PrefixBreakAmount]
           ,[PrefixBreakRate]
           ,[SplitsLoadedFranchise]
           ,[AlphaUserField1]
           ,[AlphaUserField2]
           ,[AlphaUserField3]
           ,[AlphaUserField4]
           ,[AlphaUserField5]
           ,[AlphaUserField6]
           ,[AlphaUserField7]
           ,[AlphaUserField8]
           ,[AlphaUserField9]
           ,[AlphaUserField10]
           ,[AlphaUserField11]
           ,[AlphaUserField12]
           ,[AlphaUserField13]
           ,[AlphaUserField14]
           ,[AlphaUserField15]
           ,[AlphaUserField16]
           ,[AlphaUserField17]
           ,[AlphaUserField18]
           ,[AlphaUserField19]
           ,[AlphaUserField20]
           ,[NumericUserField1]
           ,[NumericUserField2]
           ,[NumericUserField3]
           ,[NumericUserField4]
           ,[NumericUserField5]
           ,[NumericUserField6]
           ,[NumericUserField7]
           ,[NumericUserField8]
           ,[NumericUserField9]
           ,[NumericUserField10]
           ,[NumericUserField11]
           ,[NumericUserField12]
           ,[NumericUserField13]
           ,[NumericUserField14]
           ,[NumericUserField15]
           ,[NumericUserField16]
           ,[NumericUserField17]
           ,[NumericUserField18]
           ,[NumericUserField19]
           ,[NumericUserField20]
           ,[CreatedDate]
FROM 
(
SELECT
       ISNULL(RIGHT('00000' + Cast(SO.[RepNbr] as varchar(20)),5),'')  as PadDescription,
--	   case when ISNULL(CustomerNbr,0) > 1000000 then cast(CR.AS400TempCustomerId as decimal) else ISNULL(CustomerNbr,0) end as CustomerNbr,
	   CustomerNbr,
	   OL.AS400OrderNbr PepperiOrderNbr, 
	   T1.LineNbr,
	   T1.LineItemType ,
	   t1.ParentItemNumber,
	   left(isnull(ol.BrandedProductNbr,''),15) as ComponentItem,
	   left(isnull(OL.Comment,''),30) as LineItemComment,
'' as DetailCommentCode,
isnull(OrderQuantity,0) as OrderQuantity,
isnull(LabelQty,0) as LabelQty,
isnull(OrderPrice,0) as OrderPrice,
isnull(OrderCost,0) as FranchiseCost,
isnull(UnitOfMeasure,'') as UnitOfMeasure ,
isnull(CustomerContract,'') as CustomerContract,
0 as BasePrice,
'' as CommissionCode,
0 as CommissionPercent,
0 as MarkupPercent,
0 as MarkupAmount,
'' as FreightUpChargeCode,
0 as FreightUpChargePercent,
0 as FreightUpChargeAmount,
cast(isnull(OriginalFranchisePrice,0) as numeric(11,4))as BaseFranchiseCost ,
0 as QtyDiscountAmount,
0 as QtyBreakRate,
0 as PrefixBreakAmount,
0 as PrefixBreakRate  ,  
isnull(LandedCost,0)  as SplitsLoadedFranchise ,
''as AlphaUserField1,
''as AlphaUserField2,
''as AlphaUserField3,
''as AlphaUserField4,
''as AlphaUserField5,
''as AlphaUserField6,
''as AlphaUserField7,
''as AlphaUserField8,
''as AlphaUserField9,
''as AlphaUserField10,
''as AlphaUserField11,
''as AlphaUserField12,
''as AlphaUserField13,
''as AlphaUserField14,
''as AlphaUserField15,
''as AlphaUserField16,
''as AlphaUserField17,
''as AlphaUserField18,
''as AlphaUserField19,
''as AlphaUserField20,
0 as NumericUserField1,
0 as NumericUserField2,
0 as NumericUserField3,
0 as NumericUserField4,
0 as NumericUserField5,
0 as NumericUserField6,
0 as NumericUserField7,
0 as NumericUserField8,
0 as NumericUserField9,
0 as NumericUserField10,
0 as NumericUserField11,
0 as NumericUserField12,
0 as NumericUserField13,
0 as NumericUserField14,
0 as NumericUserField15,
0 as NumericUserField16,
0 as NumericUserField17,
0 as NumericUserField18,
0 as NumericUserField19,
0 as NumericUserField20,
@RunDate CreatedDate
FROM StagePA.SalesOrderLineNbr t1
JOIN [dbo].[SalesOrderLine] OL
ON t1.PepperiID = ol.PepperiId
AND t1.OrigLineNbr = OL.LineNbr 
AND t1.ParentItemNumber = ISNULL(ol.ParentItemNumber,-999)
join dbo.[SalesOrder] SO 
on OL.PepperiId = SO.PepperiOrderNbr 
--LEFT JOIN dbo.Customer CU 
--ON (CU.CustomerNumber = ISNULL(SO.CustomerNbr,0) 
--or CU.PepperiCustomerNumber = ISNULL(SO.CustomerNbr,0))
--left outer join dbo.CustomerCrossReference CR   
--on left(CR.PepperiCustomerId,8) = left(CU.PepperiCustomerNumber,8)
where t1.CreatedDate = @RunDate
and T1.LineItemType = 'I'
and t1.ParentItemNumber is NOT NULL  -- Kit

UNION ALL

SELECT
       ISNULL(RIGHT('00000' + Cast(SO.[RepNbr] as varchar(20)),5),'')  as PadDescription,
--	   case when ISNULL(CustomerNbr,0) > 1000000 then cast(CR.AS400TempCustomerId as decimal) else ISNULL(CustomerNbr,0) end as CustomerNbr,
	   CustomerNbr,
	   OL.AS400OrderNbr PepperiOrderNbr,
	   T1.LineNbr,
	   T1.LineItemType ,
	   t1.ParentItemNumber,
	   left(isnull(ol.BrandedProductNbr,''),15) as ComponentItem,
	   left(isnull(t1.Comment,''),30) as LineItemComment,
'' as DetailCommentCode,
isnull(OrderQuantity,0) as OrderQuantity,
isnull(LabelQty,0) as LabelQty,
isnull(OrderPrice,0) as OrderPrice,
isnull(OrderCost,0) as FranchiseCost,
isnull(UnitOfMeasure,'') as UnitOfMeasure ,
isnull(CustomerContract,'') as CustomerContract,
0 as BasePrice,
'' as CommissionCode,
0 as CommissionPercent,
0 as MarkupPercent,
0 as MarkupAmount,
'' as FreightUpChargeCode,
0 as FreightUpChargePercent,
0 as FreightUpChargeAmount,
cast(isnull(OriginalFranchisePrice,0) as numeric(11,4))as BaseFranchiseCost ,
0 as QtyDiscountAmount,
0 as QtyBreakRate,
0 as PrefixBreakAmount,
0 as PrefixBreakRate  ,  
isnull(LandedCost,0)  as SplitsLoadedFranchise ,
''as AlphaUserField1,
''as AlphaUserField2,
''as AlphaUserField3,
''as AlphaUserField4,
''as AlphaUserField5,
''as AlphaUserField6,
''as AlphaUserField7,
''as AlphaUserField8,
''as AlphaUserField9,
''as AlphaUserField10,
''as AlphaUserField11,
''as AlphaUserField12,
''as AlphaUserField13,
''as AlphaUserField14,
''as AlphaUserField15,
''as AlphaUserField16,
''as AlphaUserField17,
''as AlphaUserField18,
''as AlphaUserField19,
''as AlphaUserField20,
0 as NumericUserField1,
0 as NumericUserField2,
0 as NumericUserField3,
0 as NumericUserField4,
0 as NumericUserField5,
0 as NumericUserField6,
0 as NumericUserField7,
0 as NumericUserField8,
0 as NumericUserField9,
0 as NumericUserField10,
0 as NumericUserField11,
0 as NumericUserField12,
0 as NumericUserField13,
0 as NumericUserField14,
0 as NumericUserField15,
0 as NumericUserField16,
0 as NumericUserField17,
0 as NumericUserField18,
0 as NumericUserField19,
0 as NumericUserField20,
@RunDate CreatedDate
FROM StagePA.SalesOrderLineNbr t1
JOIN [dbo].[SalesOrderLine] OL
ON t1.PepperiID = ol.PepperiId
AND t1.OrigLineNbr = OL.LineNbr 
AND t1.ParentItemNumber = ISNULL(ol.ParentItemNumber,-999)
join dbo.[SalesOrder] SO 
on OL.PepperiId = SO.PepperiOrderNbr 
--LEFT OUTER JOIN dbo.Customer CU 
--ON (CU.CustomerNumber = ISNULL(SO.CustomerNbr,0) 
--or CU.PepperiCustomerNumber = ISNULL(SO.CustomerNbr,0))
--left outer join dbo.CustomerCrossReference CR   
--on left(CR.PepperiCustomerId,8) = left(CU.PepperiCustomerNumber,8)
where t1.CreatedDate = @RunDate
and T1.LineItemType = 'M' 
and t1.ParentItemNumber is NOT NULL  --  Not Kit
) x

INSERT INTO [Hist].[SalesOrderLineKitFormatted]
           ([PadDescription]
           ,[CustomerNbr]
           ,[PepperiOrderNbr]
           ,[LineNbr]
           ,[LineItemType]
           ,[ParentItemNumber]
           ,[ComponentItem]
           ,[LineItemComment]
           ,[DetailCommentCode]
           ,[OrderQuantity]
           ,[LabelQty]
           ,[OrderPrice]
           ,[FranchiseCost]
           ,[UnitOfMeasure]
           ,[CustomerContract]
           ,[BasePrice]
           ,[CommissionCode]
           ,[CommissionPercent]
           ,[MarkupPercent]
           ,[MarkupAmount]
           ,[FreightUpChargeCode]
           ,[FreightUpChargePercent]
           ,[FreightUpChargeAmount]
           ,[BaseFranchiseCost]
           ,[QtyDiscountAmount]
           ,[QtyBreakRate]
           ,[PrefixBreakAmount]
           ,[PrefixBreakRate]
           ,[SplitsLoadedFranchise]
           ,[AlphaUserField1]
           ,[AlphaUserField2]
           ,[AlphaUserField3]
           ,[AlphaUserField4]
           ,[AlphaUserField5]
           ,[AlphaUserField6]
           ,[AlphaUserField7]
           ,[AlphaUserField8]
           ,[AlphaUserField9]
           ,[AlphaUserField10]
           ,[AlphaUserField11]
           ,[AlphaUserField12]
           ,[AlphaUserField13]
           ,[AlphaUserField14]
           ,[AlphaUserField15]
           ,[AlphaUserField16]
           ,[AlphaUserField17]
           ,[AlphaUserField18]
           ,[AlphaUserField19]
           ,[AlphaUserField20]
           ,[NumericUserField1]
           ,[NumericUserField2]
           ,[NumericUserField3]
           ,[NumericUserField4]
           ,[NumericUserField5]
           ,[NumericUserField6]
           ,[NumericUserField7]
           ,[NumericUserField8]
           ,[NumericUserField9]
           ,[NumericUserField10]
           ,[NumericUserField11]
           ,[NumericUserField12]
           ,[NumericUserField13]
           ,[NumericUserField14]
           ,[NumericUserField15]
           ,[NumericUserField16]
           ,[NumericUserField17]
           ,[NumericUserField18]
           ,[NumericUserField19]
           ,[NumericUserField20]
           ,[CreatedDate])
SELECT [PadDescription]
           ,[CustomerNbr]
           ,[PepperiOrderNbr]
           ,[LineNbr]
           ,[LineItemType]
           ,[ParentItemNumber]
           ,[ComponentItem]
           ,[LineItemComment]
           ,[DetailCommentCode]
           ,[OrderQuantity]
           ,[LabelQty]
           ,[OrderPrice]
           ,[FranchiseCost]
           ,[UnitOfMeasure]
           ,[CustomerContract]
           ,[BasePrice]
           ,[CommissionCode]
           ,[CommissionPercent]
           ,[MarkupPercent]
           ,[MarkupAmount]
           ,[FreightUpChargeCode]
           ,[FreightUpChargePercent]
           ,[FreightUpChargeAmount]
           ,[BaseFranchiseCost]
           ,[QtyDiscountAmount]
           ,[QtyBreakRate]
           ,[PrefixBreakAmount]
           ,[PrefixBreakRate]
           ,[SplitsLoadedFranchise]
           ,[AlphaUserField1]
           ,[AlphaUserField2]
           ,[AlphaUserField3]
           ,[AlphaUserField4]
           ,[AlphaUserField5]
           ,[AlphaUserField6]
           ,[AlphaUserField7]
           ,[AlphaUserField8]
           ,[AlphaUserField9]
           ,[AlphaUserField10]
           ,[AlphaUserField11]
           ,[AlphaUserField12]
           ,[AlphaUserField13]
           ,[AlphaUserField14]
           ,[AlphaUserField15]
           ,[AlphaUserField16]
           ,[AlphaUserField17]
           ,[AlphaUserField18]
           ,[AlphaUserField19]
           ,[AlphaUserField20]
           ,[NumericUserField1]
           ,[NumericUserField2]
           ,[NumericUserField3]
           ,[NumericUserField4]
           ,[NumericUserField5]
           ,[NumericUserField6]
           ,[NumericUserField7]
           ,[NumericUserField8]
           ,[NumericUserField9]
           ,[NumericUserField10]
           ,[NumericUserField11]
           ,[NumericUserField12]
           ,[NumericUserField13]
           ,[NumericUserField14]
           ,[NumericUserField15]
           ,[NumericUserField16]
           ,[NumericUserField17]
           ,[NumericUserField18]
           ,[NumericUserField19]
           ,[NumericUserField20]
           ,[CreatedDate]
FROM StagePA.SalesOrderLineKitFormatted

END

GO

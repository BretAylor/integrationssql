SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      1/24/2019
-- Purpose      Load FactA1ChemicalNetSales table with product, employee, customer and net sales
-- Copyright © 2019, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- exec [dbo].[FactA1ChemicalNetSalesLoad] 

CREATE PROCEDURE [dbo].[FactA1ChemicalNetSalesLoad] 
AS
BEGIN
-- A-1 Las Vegas

TRUNCATE TABLE dbo.FactA1ChemicalNetSales
INSERT INTO dbo.FactA1ChemicalNetSales
(
    DateKey,
    ProductKey,
    EmployeeKey,
    CustomerKey,
	ShipmentKey,
    InvoiceNo,
	OrderNo,
    [LineNo],
    UnitPrice,
    QtyShipped,
	WinzerCost,
	FranchiseCost,
	Cost,
    NetSales,
	[Type],
    DateAdded
)
SELECT DD.DateKey,
       PR.ProductKey,
       EM.EmployeeKey,
       CU.CustomerKey,
	   ST.ShipmentKey,
       IL.InvoiceNo,
	   IL.OrderNo,
       IL.[LineNo],
       IL.UnitPrice,
       IL.QtyShipped,
       ROUND(WI.InvoiceLineUnitWinzerCost * IL.QtyShipped, 5) AS WinzerCost,
       ROUND(WI.InvoiceLineUnitOvrdFranchiseCost * IL.QtyShipped, 5) AS FranchiseCost,
	   ROUND(WI.InvoiceLineUnitWinzerCost * IL.QtyShipped, 5) AS Cost,
       ROUND(IL.UnitPrice * IL.QtyShipped,5) AS [NetSales],
	   'A1LasVegas' AS [Type],
	   GETDATE() AS DateAdded
--INTO #A1LasVegas 
FROM  BI_STAGING.dbo.InvoiceLineP21 IL
     INNER JOIN BI_STAGING.dbo.InvoiceP21 IH
        ON IH.InvoiceNo = IL.InvoiceNo
	 INNER JOIN BI_DWH.dbo.DimDate DD
        ON DD.Date = CAST(IH.DateCreated AS DATE)
	LEFT OUTER JOIN BI_STAGING.[dbo].[WzrInvoiceLineP21] WI
        ON WI.InvoiceLineInvoiceId = IL.InvoiceNo
           AND WI.InvoiceLineId = IL.[LineNo]
    INNER JOIN BI_DWH.dbo.DimProductP21 PR
        ON PR.ItemId = IL.ItemId
    INNER JOIN BI_DWH.dbo.DimCustomerP21 CU
        ON CU.CustomerId = IH.CustomerID
    INNER JOIN BI_DWH.dbo.DimEmployeeP21 EM
        ON EM.SalesRepID = IH.SalesRepID
	LEFT OUTER JOIN dbo.DimShipToP21 ST ON ST.NbShipToNumber = IH.ShipToNum AND ST.NbShipToCustomerId = IH.CustomerID
WHERE 
       IH.CompanyNo = '1'
      AND IH.LocationId = 1006 --AND IH.invoiceType <> 'CD' -- excluding consolidated invoice, only fastserv with company number 7 and fiscal year 2018
      AND IL.OtherChargeItem = 'N'
      AND IL.InvMastUid IS NOT NULL
      AND IL.InvMastUid <> 0
      AND DD.FiscalYear >= 2018

UNION all
   --A-1 Reno

SELECT DD.DateKey,
       PR.ProductKey,
       EM.EmployeeKey,
       CU.CustomerKey,
	   ST.ShipmentKey,
       INVOICENUM,
	   ID.OrderNum,
       [OrderSequenceNum],
       ID.ActualSellPrice,
       QuantityShipped,
	   ROUND(ID.CurrentAverageCost * ID.QuantityShipped,5) AS WinzerCost, 
	   ROUND(ID.ListPrice * ID.QuantityShipped,5) AS [FanchiseCost], 
	   ROUND(ID.ListPrice * ID.QuantityShipped,5) AS Cost,
       ROUND(ID.ActualSellPrice* ID.QuantityShipped,2) AS [NetSales],
	   'A1Reno' AS [Type],
	   GETDATE() AS DateAdded
--INTO #A1Reno
FROM  [BI_STAGING].[dbo].[InvoiceLineFranchise] ID
    INNER JOIN BI_STAGING.dbo.InvoiceFranchiseSup SU
        ON ID.HistorySequenceNum = SU.HistorySequenceNum
    INNER JOIN BI_STAGING.dbo.InvoiceFranchise IFR
        ON IFR.HISTORYSEQUENCENUM = ID.HistorySequenceNum
    INNER JOIN BI_DWH.dbo.DimDate DD
        ON IFR.INVOICEDATE = DD.DateKey
    INNER JOIN BI_DWH.dbo.DimProduct PR
        ON PR.ProductNum = ID.ItemNum
    INNER JOIN BI_DWH.dbo.DimEmployee EM
        ON ID.PrimarySalesmanNum = EM.SalesRepresentativeNum
    INNER JOIN BI_DWH.dbo.DimCustomer CU
        ON CU.CustomerNum = ID.CustomerNum
	LEFT OUTER JOIN dbo.DimShipTo ST 
	   ON ST.ShipToNum = IFR.ShipToNum AND ST.CustomerNum = IFR.CustomerNum AND ST.CompanyNum = IFR.CompanyNum
WHERE IFR.CompanyNum = 1 -- InvoiceFranchise
      -- AND IFR.CustomerGLCode <>'TG' -- commented out for the new below logic
	  AND IFR.CustomerGLCode IN ('FR','NF','SA','TF') -- Added per Vicky request - 3/8/2019
      AND SU.DivisionCode = 'A1S'
	  AND ID.[Type] = 'I' AND ID.QuantityShipped  <> 0
	  AND ((IFR.OrderType= 'I') OR 
          (IFR.OrderType <> 'I' AND ID.ItemClass <> '90')) -- Added per Vicky request - 3/8/2019
 
      AND DD.FiscalYear >= 2018;
   

END

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		baylor
-- alter date: 8/2020
-- Description:	Update to Pepperi_Staging 
-- =============================================
CREATE PROCEDURE [StageAP].[PromotionCustomerUpdate] 
AS

BEGIN

SET NOCOUNT ON;

DECLARE @RunDate DATETIME = GETDATE()

UPDATE t1
SET PromotionCode = t2.PromotionCode
,CustomerNumber = t2.CustomerNumber
,PromotionStatus = t2.PromotionStatus
,CreationTimestamp = t2.CreationTimestamp
,DateLastMaintained = t2.DateLastMaintained
,Deleted = 0
,[ModifiedDate] = @RunDate
FROM  [dbo].[PromotionCustomer]  t1
JOIN StageAP.PromotionCustomer t2
ON t1.PromotionCode = t2.PromotionCode
AND t1.CustomerNumber = t2.CustomerNumber
WHERE t1.[DateLastMaintained] <  t2.DateLastMaintained
OR t1.Deleted = 1


INSERT INTO [dbo].[PromotionCustomer]
           ([PromotionCode]
           ,CustomerNumber
           ,[PromotionStatus]
           ,[CreationTimestamp]
		   ,[DateLastMaintained]
		   ,CreatedDate
		   ,ModifiedDate
)
SELECT		t1.[PromotionCode]
           ,t1.[CustomerNumber]
           ,t1.[PromotionStatus]
           ,t1.[CreationTimestamp]
           ,t1.[DateLastMaintained]
		   ,@RunDate CreatedDate
		   ,@RunDate ModifiedDate
FROM StageAP.PromotionCustomer t1
LEFT JOIN dbo.PromotionCustomer	 t2
ON t1.PromotionCode = t2.PromotionCode
AND t1.CustomerNumber = t2.CustomerNumber
WHERE t2.CustomerNumber IS NULL

-- Delete 
UPDATE t1
SET Deleted = 1,
ModifiedDate = @Rundate
FROM [dbo].[PromotionCustomer] t1
LEFT JOIN [StageAP].[PromotionCustomer] t2
ON t1.PromotionCode = t2.PromotionCode
AND t1.CustomerNumber = t2.CustomerNumber
WHERE t2.CustomerNumber IS NULL

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      12/16/2018
-- Description  Provide a snapshot about sales, costs, profits and orders by timeline, sales rep, customer and product using BI_DWH tables
-- Copyright © 2018, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History
-- 12/26/2018 Saad Benhalima added Fiscal Year, Month and Quarter
-- 12/29/2018 Saad Benhalima added Fiscal Date
-------------------------------------------------------------------------------
CREATE Procedure [dbo].[SalesReport]
As
Begin
select CU.CustomerNum, 
       CU.CustomerName, 
	   EM.SalesRepresentativeNum,
	   EM.SalesRepresentativeName,  
	   PR.ProductNum,
	   PR.ProductDescription,
	   DT.[Date],
	   DT.FiscalDate,
	   DT.FiscalYear,
	   DT.FiscalMonth,
	   DT.[MonthName],
	   DT.FiscalQuarter,
	   DT.DayName,
	   FPH.OrderNum, 
	   FPH.OrderQuantity as [FPH.OrderQuantity] , 
	   FPH.sellingPrice as Price, 
	   FPH.PFCOST as PFCOST,
	   FPH.FRCOST as FRCOST   ,
	   FPH.DateAdded as [LastRefreshDate]
	   FROM [BI_DWH].[dbo].[FactPurchaseHistory] FPH 
               left outer join BI_DWH.dbo.DimCustomer CU on FPH.CustomerKey = CU.CustomerKey
			   Left outer join BI_DWH.dbo.DimEmployee EM on FPH.EmployeeKey = EM.EmployeeKey
			   left outer join BI_DWH.dbo.DimProduct PR on PR.Productkey = FPH.ProductKey
			   left outer join BI_DWH.dbo.DimDate DT on DT.DateKey = FPH.DateKey
		

End


GO

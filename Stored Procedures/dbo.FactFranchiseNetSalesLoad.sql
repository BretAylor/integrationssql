SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      1/19/2019
-- Description  Net sales by franchise, day, customer, product

-- Copyright © 2019, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History

-------------------------------------------------------------------------------
-- exec [dbo].[FactFranchiseNetSalesLoad]
CREATE PROCEDURE [dbo].[FactFranchiseNetSalesLoad]
AS 
BEGIN
TRUNCATE TABLE [dbo].[FactInvoiceFranchise]
INSERT INTO [dbo].[FactInvoiceFranchise]
(
    DateKey,
    CustomerKey,
    EmployeeKey,
    ProductKey,
	ItemClassKey,
	ShipmentKey,
    OrderKey,
	OrderSequenceNum,
    InvoiceDate,
    CustomerNum,
    PrimarySalesmanNum,
    ItemNum,
    QuantityShipped,
    ActualSellPrice,
	CurrentAverageCost,
	ListPrice,
	WinzerCost,
	[FranchiseCost],
    [NetSales],
	DateAdded
)
-- Detailed view 
SELECT DD.DateKey,
       ISNULL(CU.CustomerKey,0),
       ISNULL(EM.EmployeeKey,0),
       ISNULL(PR.ProductKey,0) AS ProductKey,
	   ISNULL(IC.ItemClassKey,0) AS ItemClassKey,
	   ISNULL(ST.ShipmentKey,0) AS ShipmentKey,
       IL.OrderNum AS OrderKey,
	   IL.OrderSequenceNum,
       IFR.INVOICEDATE,
       IL.CustomerNum,
       IL.PrimarySalesmanNum,
       IL.ItemNum,
       IL.QuantityShipped,
       IL.ActualSellPrice,
	   IL.CurrentAverageCost,
	   IL.ListPrice,
	   ROUND(IL.CurrentAverageCost * IL.QuantityShipped,5) AS WinzerCost,
	   ROUND(IL.ListPrice * IL.QuantityShipped,5) AS [FanchiseCost],
       ROUND(IL.ActualSellPrice * IL.QuantityShipped,2) AS [NetSales],
	   GETDATE() AS DateAdded
 FROM [BI_STAGING].[dbo].[InvoiceLineFranchise] IL
    INNER JOIN BI_STAGING.dbo.InvoiceFranchiseSup SU
        ON IL.HistorySequenceNum = SU.HistorySequenceNum
    INNER JOIN [BI_STAGING].[dbo].InvoiceFranchise IFR
        ON IFR.HISTORYSEQUENCENUM = IL.HistorySequenceNum
    INNER JOIN BI_DWH.dbo.DimDate DD
        ON IFR.INVOICEDATE = DD.DateKey
    LEFT outer JOIN dbo.DimCustomer CU
        ON CU.CustomerNum = IL.CustomerNum
    LEFT outer JOIN dbo.DimEmployee EM
        ON EM.SalesRepresentativeNum = IL.PrimarySalesmanNum
    LEFT outer JOIN dbo.DimProduct PR
        ON PR.ProductNum = IL.ItemNum
	LEFT OUTER JOIN dbo.DimItemClass IC
	    ON IL.ItemClass = IC.ItemClassCode
	LEFT OUTER JOIN dbo.DimShipTo ST
	    ON IFR.ShipToNum = ST.ShipToNum AND IFR.CustomerNum = ST.CustomerNum AND IFR.CompanyNum = ST.CompanyNum

WHERE 
       IFR.INVOICEDATE >= 20170801
	   AND IFR.CompanyNum = 1 -- Table: InvoiceFranchise
       AND SU.DivisionCode <> 'A1S' -- Table: InvoiceFranchiseSup
	   --AND IFR.CustomerGLCode <> 'TG' -- commented out for the new below logic
	   AND IFR.CustomerGLCode IN ('FR','NF','SA','TF') -- Added per Vicky request - 3/8/2019
	   AND IL.QuantityShipped <> 0 
	   AND IL.[Type] = 'I' 
	   AND ((IFR.OrderType= 'I' AND IL.ItemClass = '91') OR 
	   (IFR.OrderType <> 'I' AND IL.ItemClass <> '90') OR 
	   (IFR.OrderType= 'I' AND IFR.CustomerGLCode = 'TF') OR 
	   (IFR.OrderType = 'I' AND EM.RepTypeCode = 'WR')) -- Added per Vicky request - 3/8/2019

ORDER BY DD.DateKey ASC;

END


GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      12/24/2018
-- Description  Provide databases object definitions and detailed fields overview 

-- Copyright © 2018, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History
--

-------------------------------------------------------------------------------
CREATE Procedure [dbo].[ObjectDefinitionReport] (@ObjectType nvarchar(100) = NULL, @ObjectName nvarchar(100) = NULL)
As
Begin
SELECT        OD.ID, OD.ObjectType, OD.ObjectName, OD.CreatedDate, OD.Author, OD.Description, OD.Notes, OD.RefreshPeriod, OD.RefreshFrequency, OD.RefreshTime, OD.Location, OD.SourceObject, OD.SourceName, 
                         OD.DestinationObject, OD.DestinationName, OD.JobName, OD.[SSIS Package], OFD.OrderID, OFD.Section, OFD.Field, OFD.Definition
FROM            dbo.ObjectDefinition AS OD LEFT OUTER JOIN
                         dbo.ObjectFieldDefinition AS OFD ON OD.ObjectType = OFD.ObjectType AND OD.ObjectName = OFD.ObjectName
						 where (OD.ObjectType   = (ISNULL(@ObjectType,OD.ObjectType))) and  (OD.ObjectName = (ISNULL(@ObjectName,OD.ObjectName)))
ORDER BY OD.ObjectType, OD.CreatedDate desc
End
GO

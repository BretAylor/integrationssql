SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[SalesRemunerationUpdate]
AS
BEGIN

update SR set 
SR.CalendarDay = SRU.CalendarDay
,SR.[RTDWinzerSales]       =SRU.[RTDWinzerSales]
,SR.[RTDWinzerSalesCost]   =SRU.[RTDWinzerSalesCost]
,SR.[RTDGMPercent]		   =SRU.[RTDGMPercent]
,SR.[RTDGMAmount]		   =SRU.[RTDGMAmount]
,SR.[RTDInvoiceOnlySales]  =SRU.[RTDInvoiceOnlySales]
,SR.[RTDFranDirectSales]   =SRU.[RTDFranDirectSales]
,SR.[RTDBillOnlySales]	   =SRU.[RTDBillOnlySales]
,SR.[RTDPersonalSales]	   =SRU.[RTDPersonalSales]
,SR.[RTDTotalSales]		   =SRU.[RTDTotalSales]
,SR.ModifiedDate = getdate()
,SR.ModifiedBy = suser_name()
,SR.ErpSyncDate = getdate()
,SR.ErpSyncBy = suser_name()

from WINSQLBI01.Pepperi.dbo.SalesRemunerationUpdates SRU with (nolock) 
INNER join dbo.SalesRemuneration SR with (nolock)
on SRU.RepNum = SR.RepNum  and SRU.CalendarYear = SR.CalendarYear and SRU.CalendarMonth = SR.CalendarMonth and SRU.RemunerationPeriod = SR.RemunerationPeriod

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		baylor
-- Create date: 4/2020
-- Description:	Update  Pepperi_Staging 
-- =============================================
CREATE PROCEDURE [StageAP].[VSNCatalogDetailUpdate] 
AS

BEGIN

SET NOCOUNT ON;

UPDATE t1
   SET [PartDescription] = t2.PartDescription
      ,[UnitOfMeasure] = t2.UnitOfMeasure
      ,[FranchiseCost] = t2.FranchiseCost
      ,[StandardPackageQty] = t2.StandardPackageQty
      ,[EnforceBaggingQty] = t2.EnforceBaggingQty
      ,[PackageAllInOneBag] = t2.PackageAllInOneBag
      ,[KitType] = t2.KitType
      ,[RecommendedPrice] = t2.RecommendedPrice
      ,[AssignedWinzerProductNumber] = t2.AssignedWinzerProductNumber
      ,[FreightType] = t2.FreightType
      ,[FreightAmount] = t2.FreightAmount
      ,[WeightInPounds] = t2.WeightInPounds
      ,[LandedCostSplitProfit] = t2.LandedCostSplitProfit
      ,[FastservItemNumber] = t2.FastservItemNumber
	  ,Deleted = 0
      ,[ModifiedDate] = Getdate()
      ,[ModifiedBy] = suser_name()
--Select count(*)
from [dbo].[VsnCatalogDetails] t1
Join [StageAP].[VsnCatalogDetails] t2
on t1.catalogID = t2.catalogid
and t1.[CatalogPartNumber] = t2.[CatalogPartNumber]
Where t1.[PartDescription] <> t2.PartDescription
      or t1.[UnitOfMeasure] <> t2.UnitOfMeasure
      or t1.[FranchiseCost] <> t2.FranchiseCost
      or t1.[StandardPackageQty] <> t2.StandardPackageQty
      or t1.[EnforceBaggingQty] <> t2.EnforceBaggingQty
      or t1.[PackageAllInOneBag] <> t2.PackageAllInOneBag
      or t1.[KitType] <> t2.KitType
      or t1.[RecommendedPrice] <> t2.RecommendedPrice
      or t1.[AssignedWinzerProductNumber] <> t2.AssignedWinzerProductNumber
      or t1.[FreightType] <> t2.FreightType
      or t1.[FreightAmount] <> t2.FreightAmount
      or t1.[WeightInPounds] <> t2.WeightInPounds
      or t1.[LandedCostSplitProfit] <> t2.LandedCostSplitProfit
      or t1.[FastservItemNumber] <> t2.FastservItemNumber
	  or t1.Deleted <> 0  -- Reactivate


INSERT INTO [dbo].[VsnCatalogDetails]
           ([CatalogId]
           ,[CatalogPartNumber]
           ,[PartDescription]
           ,[UnitOfMeasure]
           ,[FranchiseCost]
           ,[StandardPackageQty]
           ,[EnforceBaggingQty]
           ,[PackageAllInOneBag]
           ,[KitType]
           ,[RecommendedPrice]
           ,[AssignedWinzerProductNumber]
           ,[FreightType]
           ,[FreightAmount]
           ,[WeightInPounds]
           ,[LandedCostSplitProfit]
           ,[FastservItemNumber] )
 select t1.[CatalogId]
           ,t1.[CatalogPartNumber]
           ,t1.[PartDescription]
           ,t1.[UnitOfMeasure]
           ,t1.[FranchiseCost]
           ,t1.[StandardPackageQty]
           ,t1.[EnforceBaggingQty]
           ,t1.[PackageAllInOneBag]
           ,t1.[KitType]
           ,t1.[RecommendedPrice]
           ,t1.[AssignedWinzerProductNumber]
           ,t1.[FreightType]
           ,t1.[FreightAmount]
           ,t1.[WeightInPounds]
           ,t1.[LandedCostSplitProfit]
           ,t1.[FastservItemNumber]
from [StageAP].[VsnCatalogDetails] t1
Left Join [dbo].[VsnCatalogDetails] t2
on t1.catalogID = t2.catalogid
and t1.[CatalogPartNumber] = t2.[CatalogPartNumber]
where t2.[CatalogPartNumber] is null


-- Delete 
Update t1
Set Deleted = 1,
ModifiedDate = getdate(),
ModifiedBy = SUSER_SNAME()
from [dbo].[VsnCatalogDetails] t1
Left Join [StageAP].[VsnCatalogDetails] t2
on t1.catalogID = t2.catalogid
and t1.[CatalogPartNumber] = t2.[CatalogPartNumber]
where t1.deleted = 0
and t2.[CatalogPartNumber] is null

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- Author       Saad Benhalima
-- Created      12/31/2018
-- Description  Provide daily SSRS reports usage overview 

-- Copyright © 2018, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History
--

-------------------------------------------------------------------------------
CREATE Procedure [dbo].[SSRSUsageReport] 
As
Begin
SELECT EL.UserName,C.NAME as [ReportName],cast(EL.TIMESTART as Date) as [Date], count(EL.TIMESTART) as [Views]
FROM REPORTSERVER.DBO.EXECUTIONLOG (NOLOCK) EL INNER JOIN
REPORTSERVER.DBO.CATALOG (NOLOCK) C ON EL.REPORTID=C.ITEMID
group by EL.USERNAME,C.NAME,cast(EL.TIMESTART as Date)
order by Date desc
End
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		baylor
-- Create date: 4/2020
-- Description:	Update to Pepperi_Staging 
-- =============================================
CREATE PROCEDURE [stageP21P].[ProductImageUpdate] 
AS

BEGIN

SET NOCOUNT ON;

DECLARE @InsertCount INT =
(
SELECT COUNT(*)
from [ProductImage] t1
Left join [WUSA-SQL01-DFW].Pepperi_Staging.[dbo].[ProductImage] t2
on t1.id = t2.id
where t2.id is null
)
DECLARE @DeleteCount INT = 
(
SELECT COUNT(*)
From [WUSA-SQL01-DFW].Pepperi_Staging.dbo.ProductImage n 
left join ProductImage u
on n.Id = u.Id 
Where u.id is null 
)
DECLARE @UpdateCount INT = 
(
SELECT COUNT(*)
From [WUSA-SQL01-DFW].Pepperi_Staging.dbo.ProductImage n 
join ProductImage u
on n.Id = u.Id 
Where 
 ISNULL(n.ErpNbr,'') <> ISNULL(u.ErpNbr,'') or
 ISNULL(n.ProductId,'') <> ISNULL(u.ProductId,'') or
 ISNULL(n.SortOrder,0) <> ISNULL(u.SortOrder,0) or
 ISNULL(n.Name,'') <> ISNULL(u.Name,'') or
 ISNULL(n.SmallImagePath,'') <> ISNULL(u.SmallImagePath,'') or
 ISNULL(n.MediumImagePath,'') <> ISNULL(u.MediumImagePath,'') or
 ISNULL(n.LargeImagePath,'') <> ISNULL(u.LargeImagePath,'') or
 ISNULL(n.AltText,'') <> ISNULL(u.AltText,'') or
 deleted = 1
 )

 IF @UpdateCount > 1000
 or @InsertCount > 1000
 or @deleteCount > 1000
 BEGIN
 DECLARE @bod VARCHAR(255) = 'Update Count: ' + cast(@UpdateCount as varchar(20)) +
	', Insert Count: ' + cast(@insertCount as varchar(20)) +
	', Delete Count: ' + cast(@DeleteCount as varchar(20))

 EXEC msdb.dbo.sp_send_dbmail  
    @profile_name = 'Account',  
    @recipients = 'bret.aylor@winzerusa.com',  
    @body = @bod,
    @subject = 'Product Updates Not Processed',  
    @attach_query_result_as_file = 0,
	@body_format = 'HTML'

RETURN

end


Update n
Set n.ProductId = u.ProductId,
 n.SortOrder = u.SortOrder,
 n.Name = u.Name,
 n.SmallImagePath = u.SmallImagePath,
 n.MediumImagePath = u.MediumImagePath,
 n.LargeImagePath = u.LargeImagePath,
 n.AltText = u.AltText,
 n.Deleted = 0,
 n.modifiedby = current_user,
 n.ModifiedOn = Getdate()
From [WUSA-SQL01-DFW].Pepperi_Staging.dbo.ProductImage n 
join ProductImage u
on n.Id = u.Id 
Where 
 ISNULL(n.ErpNbr,'') <> ISNULL(u.ErpNbr,'') or
 ISNULL(n.ProductId,'') <> ISNULL(u.ProductId,'') or
 ISNULL(n.SortOrder,0) <> ISNULL(u.SortOrder,0) or
 ISNULL(n.Name,'') <> ISNULL(u.Name,'') or
 ISNULL(n.SmallImagePath,'') <> ISNULL(u.SmallImagePath,'') or
 ISNULL(n.MediumImagePath,'') <> ISNULL(u.MediumImagePath,'') or
 ISNULL(n.LargeImagePath,'') <> ISNULL(u.LargeImagePath,'') or
 ISNULL(n.AltText,'') <> ISNULL(u.AltText,'') or
 deleted = 1

 INSERT [WUSA-SQL01-DFW].Pepperi_Staging.[dbo].[ProductImage]
           ([Id]
           ,[ErpNbr]
           ,[ProductId]
           ,[SortOrder]
           ,[Name]
           ,[SmallImagePath]
           ,[MediumImagePath]
           ,[LargeImagePath]
           ,[AltText]
		   ,deleted)
 Select		t1.[Id]
           ,t1.[ErpNbr]
           ,t1.[ProductId]
           ,t1.[SortOrder]
           ,t1.[Name]
           ,t1.[SmallImagePath]
           ,t1.[MediumImagePath]
           ,t1.[LargeImagePath]
           ,t1.[AltText]
		   ,0 Deleted
from [ProductImage] t1
Left join [WUSA-SQL01-DFW].Pepperi_Staging.[dbo].[ProductImage] t2
on t1.id = t2.id
where t2.id is null

-- Delete 
Update n
Set Deleted = 1,
Modifiedon = getdate(),
ModifiedBy = SUSER_SNAME()
From [WUSA-SQL01-DFW].Pepperi_Staging.dbo.ProductImage n 
left join ProductImage u
on n.Id = u.Id 
Where u.id is null 

END
GO

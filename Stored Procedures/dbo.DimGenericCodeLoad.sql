SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      2/10/2019
-- Description  Generic Codes used from AS400

-- Copyright © 2019, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History

-------------------------------------------------------------------------------
-- exec DimGenericCodeLoad
CREATE PROCEDURE [dbo].[DimGenericCodeLoad]
AS
BEGIN


TRUNCATE TABLE dbo.DimGenericCode
INSERT INTO dbo.DimGenericCode (
    [CCCONO],
	[CCAPID],
	[CCCTLK],
	[CCRCCD],
	[CCDATA],
	[CCCONOSup],
	[CCAPIDSup],
	[CCCTLKSup],
	[CCRCCDSup],
	[CCDATASup],
	DateAdded
)

SELECT gc.CCCONO,
       gc.CCAPID,
       gc.CCCTLK,
       gc.CCRCCD,
       gc.CCDATA,
       gcs.CCCONO AS CCCONOSup,
       gcs.CCAPID AS CCAPIDSup,
       gcs.CCCTLK AS CCCTLKSup,
       gcs.CCRCCD AS CCRCCDSup,
       gcs.CCDATA AS CCDATASup,
	   GETDATE() AS DateAdded
FROM BI_STAGING.dbo.genericcode gc
    FULL JOIN  BI_STAGING.dbo.GenericCodeSup gcs
        ON gc.cccono = gcs.cccono
           AND gc.ccapid = gcs.ccapid
           AND gc.ccctlk = gcs.ccctlk;
END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		baylor
-- Create date: 5/2020
-- Description:	Update to Pepperi_Staging 
-- =============================================
CREATE PROCEDURE [StageAP].[CustomerUpdate] 
AS

BEGIN

SET NOCOUNT ON;

Set Transaction Isolation Level Read Uncommitted

declare @RunDate datetime = getdate()

UPDATE t1
   SET [PadDescription] = t2.PadDescription
      ,[CustomerNumber] = t2.CustomerNumber
      ,[TerritoryNumber] = t2.TerritoryNumber
      ,[SalesRepNumber] = t2.SalesRepNumber
      ,[CustomerName] = t2.CustomerName
      ,[PurchasingContact] = t2.PurchasingContact
      ,[AddressLine1] = t2.AddressLine1
      ,[AddressLine2] = t2.AddressLine2
      ,[City] = t2.City
      ,[State] = t2.STATE
      ,[ZipCode] = t2.ZipCode
      ,[PurchasingPhoneNumber] = t2.PurchasingPhoneNumber
      ,[PurchasingPhoneExtension] = t2.PurchasingPhoneExtension
      ,[PurchasingFax] = t2.PurchasingFax
      ,[CustomerClass] = t2.CustomerClass
      ,[CusomterSubclass] = t2.CusomterSubclass
      ,[PrintStatement] = t2.PrintStatement
      ,[ARAmountDue] = t2.ARAmountDue
      ,[ARAmountCurrent] = t2.ARAmountCurrent
      ,[AgeAmount030] = t2.AgeAmount030
      ,[AgeAmount060] = t2.AgeAmount060
      ,[AgeAmount090] = t2.AgeAmount090
      ,[AgeAmount120] = t2.AgeAmount120
      ,[TotalPayDays] = t2.TotalPayDays
      ,[LastPaymentAmount] = t2.LastPaymentAmount
      ,[LastPaymentDate] = t2.LastPaymentDate
      ,[LastPurchaseDate] = t2.LastPurchaseDate
      ,[FirstSaleDate] = t2.FirstSaleDate
      ,[MtdSalesDollars] = t2.MtdSalesDollars
      ,[YtdSalesDollars] = t2.YtdSalesDollars
      ,[LastYearFiscalSales] = t2.LastYearFiscalSales
      ,[CreditLimit] = t2.CreditLimit
      ,[MtdNumberOrders] = t2.MtdNumberOrders
      ,[YtdNumberOrders] = t2.YtdNumberOrders
      ,[NumberOrdersLastFiscal] = t2.NumberOrdersLastFiscal
      ,[TaxCode] = t2.TaxCode
      ,[AcceptBackorders] = t2.AcceptBackorders
      ,[DefaultShipVia] = t2.DefaultShipVia
      ,[TermsCode] = t2.TermsCode
	  ,[DefaultPriceLevel] = t2.DefaultPriceLevel
      ,[PriceOnPicklist] = t2.PriceOnPicklist
      ,[DiscountCode] = t2.DiscountCode
      ,[ChargeFreightCode] = t2.ChargeFreightCode
      ,[OrderType] = t2.OrderType
      ,[DefaultWarehouse] = t2.DefaultWarehouse
      ,[DefaultOrderComment] = t2.DefaultOrderComment
      ,[PORequiredFlag] = t2.PORequiredFlag
      ,[OpenOrderValue] = t2.OpenOrderValue
      ,[DefaultShipTo] = t2.DefaultShipTo
	  /* Warning: Never Update the PepperiCustomerNumber from the AS400
      ,[PepperiCustomerNumber] = t2.PepperiCustomerNumber
	  */
      ,[ObsolescenceStartDate] = t2.ObsolescenceStartDate
      ,[ContractCode] = t2.ContractCode
      ,[DefaultOpenBookContract] = t2.DefaultOpenBookContract
      ,[DefaultMarkupPercent] = t2.DefaultMarkupPercent
      ,[DefaultFreightUpchargeCode] = t2.DefaultFreightUpchargeCode
      ,[RouteInfo] = t2.RouteInfo
      ,[DefaultBinLabelBarCode] = t2.DefaultBinLabelBarCode
      ,[DefaultBinLabelImageCode] = t2.DefaultBinLabelImageCode
      ,[DefaultBinLabelType] = t2.DefaultBinLabelType
      ,[DefaultBinLabelStock] = t2.DefaultBinLabelStock
      ,[ObsolescenceEnabled] = t2.ObsolescenceEnabled
      ,[ObsolescenceTargetDollars] = t2.ObsolescenceTargetDollars
      ,[ObsolescenceMethod] = t2.ObsolescenceMethod
      ,[ObsolescenceDiscountPercent] = t2.ObsolescenceDiscountPercent
      ,[ObsolescenceMaxDollars] = t2.ObsolescenceMaxDollars
      ,[PrefixPriceDefault] = t2.PrefixPriceDefault
      ,[APContact] = t2.APContact
      ,[APPhoneNumber] = t2.APPhoneNumber
      ,[APPhoneExtension] = t2.APPhoneExtension
      ,[APFaxNbr] = t2.APFaxNbr
      ,[ShipTo000Contact] = t2.ShipTo000Contact
      ,[ShiptTo000Phone] = t2.ShiptTo000Phone
      ,[Shipto000Extension] = t2.Shipto000Extension
      ,[ShipTo000FaxNbr] = t2.ShipTo000FaxNbr
      ,[SuggestedContract] = t2.SuggestedContract
      ,[APContactEmail] = t2.APContactEmail
      ,[PurchasingContactEmail] = t2.PurchasingContactEmail
      ,[ShipTo000Email] = t2.ShipTo000Email
   --   ,[DefaultPONumber] = t2.DefaultPONumber
      ,[PrintDuplicatePickSlip] = t2.PrintDuplicatePickSlip
      ,[CVPEligibleFlag] = t2.CVPEligibleFlag
      ,[SplitProfit] = t2.SplitProfit
      ,[Deleted] = 0
      ,[ErpModifiedDate] = @RunDate
      ,[ErpModifiedBy] = SUSER_NAME()
      ,[ErpSyncDate] = @RunDate
      ,[ErpSyncBy] = SUSER_NAME()
      ,[ModifiedDate] = @RunDate
      ,[ModifiedBy] = SUSER_NAME()
FROM [dbo].[Customer] t1
Join StageAP.Customer t2
ON t1.CustomerNumber = t2.CustomerNumber
WHERE   t1.[PadDescription] <> t2.PadDescription
      or t1.[CustomerNumber] <> t2.CustomerNumber
      or t1.[TerritoryNumber] <> t2.TerritoryNumber
      or t1.[SalesRepNumber] <> t2.SalesRepNumber
      or t1.[CustomerName] <> t2.CustomerName
      or t1.[PurchasingContact] <> t2.PurchasingContact
      or t1.[AddressLine1] <> t2.AddressLine1
      or t1.[AddressLine2] <> t2.AddressLine2
      or t1.[City] <> t2.City
      or t1.[State] <> t2.STATE
      or t1.[ZipCode] <> t2.ZipCode
      or t1.[PurchasingPhoneNumber] <> t2.PurchasingPhoneNumber
      or t1.[PurchasingPhoneExtension] <> t2.PurchasingPhoneExtension
      or t1.[PurchasingFax] <> t2.PurchasingFax
      or t1.[CustomerClass] <> t2.CustomerClass
      or t1.[CusomterSubclass] <> t2.CusomterSubclass
      or t1.[PrintStatement] <> t2.PrintStatement
      or t1.[ARAmountDue] <> t2.ARAmountDue
      or t1.[ARAmountCurrent] <> t2.ARAmountCurrent
      or t1.[AgeAmount030] <> t2.AgeAmount030
      or t1.[AgeAmount060] <> t2.AgeAmount060
      or t1.[AgeAmount090] <> t2.AgeAmount090
      or t1.[AgeAmount120] <> t2.AgeAmount120
      or t1.[TotalPayDays] <> t2.TotalPayDays
      or t1.[LastPaymentAmount] <> t2.LastPaymentAmount
      or t1.[LastPaymentDate] <> t2.LastPaymentDate
      or t1.[LastPurchaseDate] <> t2.LastPurchaseDate
      or t1.[FirstSaleDate] <> t2.FirstSaleDate
      or t1.[MtdSalesDollars] <> t2.MtdSalesDollars
      or t1.[YtdSalesDollars] <> t2.YtdSalesDollars
      or t1.[LastYearFiscalSales] <> t2.LastYearFiscalSales
      or t1.[CreditLimit] <> t2.CreditLimit
      or t1.[MtdNumberOrders] <> t2.MtdNumberOrders
      or t1.[YtdNumberOrders] <> t2.YtdNumberOrders
      or t1.[NumberOrdersLastFiscal] <> t2.NumberOrdersLastFiscal
      or t1.[TaxCode] <> t2.TaxCode
      or t1.[AcceptBackorders] <> t2.AcceptBackorders
      or t1.[DefaultShipVia] <> t2.DefaultShipVia
      or t1.[TermsCode] <> t2.TermsCode
	  OR isnull(t1.[DefaultPriceLevel],-99) <> ISNULL(t2.DefaultPriceLevel, -99)
      or t1.[PriceOnPicklist] <> t2.PriceOnPicklist
      or t1.[DiscountCode] <> t2.DiscountCode
      or t1.[ChargeFreightCode] <> t2.ChargeFreightCode
      or t1.[OrderType] <> t2.OrderType
      or t1.[DefaultWarehouse] <> t2.DefaultWarehouse
      or t1.[DefaultOrderComment] <> t2.DefaultOrderComment
      or t1.[PORequiredFlag] <> t2.PORequiredFlag
      or t1.[OpenOrderValue] <> t2.OpenOrderValue
      or t1.[DefaultShipTo] <> t2.DefaultShipTo
 -- don't update     or t1.[PepperiCustomerNumber] <> t2.PepperiCustomerNumber
      or t1.[ObsolescenceStartDate] <> t2.ObsolescenceStartDate
      or t1.[ContractCode] <> t2.ContractCode
      or t1.[DefaultOpenBookContract] <> t2.DefaultOpenBookContract
      or t1.[DefaultMarkupPercent] <> t2.DefaultMarkupPercent
      or t1.[DefaultFreightUpchargeCode] <> t2.DefaultFreightUpchargeCode
      or t1.[RouteInfo] <> t2.RouteInfo
      or t1.[DefaultBinLabelBarCode] <> t2.DefaultBinLabelBarCode
      or t1.[DefaultBinLabelImageCode] <> t2.DefaultBinLabelImageCode
      or t1.[DefaultBinLabelType] <> t2.DefaultBinLabelType
      or t1.[DefaultBinLabelStock] <> t2.DefaultBinLabelStock
      or t1.[ObsolescenceEnabled] <> t2.ObsolescenceEnabled
      or t1.[ObsolescenceTargetDollars] <> t2.ObsolescenceTargetDollars
      or t1.[ObsolescenceMethod] <> t2.ObsolescenceMethod
      or t1.[ObsolescenceDiscountPercent] <> t2.ObsolescenceDiscountPercent
      or t1.[ObsolescenceMaxDollars] <> t2.ObsolescenceMaxDollars
      or t1.[PrefixPriceDefault] <> t2.PrefixPriceDefault
      or t1.[APContact] <> t2.APContact
      or t1.[APPhoneNumber] <> t2.APPhoneNumber
      or t1.[APPhoneExtension] <> t2.APPhoneExtension
      or t1.[APFaxNbr] <> t2.APFaxNbr
      or t1.[ShipTo000Contact] <> t2.ShipTo000Contact
      or t1.[ShiptTo000Phone] <> t2.ShiptTo000Phone
      or t1.[Shipto000Extension] <> t2.Shipto000Extension
      or t1.[ShipTo000FaxNbr] <> t2.ShipTo000FaxNbr
      or t1.[SuggestedContract] <> t2.SuggestedContract
      or t1.[APContactEmail] <> t2.APContactEmail
      or t1.[PurchasingContactEmail] <> t2.PurchasingContactEmail
      or t1.[ShipTo000Email] <> t2.ShipTo000Email
 --     or t1.[DefaultPONumber] <> t2.DefaultPONumber
      or t1.[PrintDuplicatePickSlip] <> t2.PrintDuplicatePickSlip
      or t1.[CVPEligibleFlag] <> t2.CVPEligibleFlag
      or t1.[SplitProfit] <> t2.SplitProfit
      or t1.[Deleted] <> 0

INSERT INTO [dbo].[Customer]
           ([PadDescription]
           ,[CustomerNumber]
           ,[TerritoryNumber]
           ,[SalesRepNumber]
           ,[CustomerName]
           ,[PurchasingContact]
           ,[AddressLine1]
           ,[AddressLine2]
           ,[City]
           ,[State]
           ,[ZipCode]
           ,[PurchasingPhoneNumber]
           ,[PurchasingPhoneExtension]
           ,[PurchasingFax]
           ,[CustomerClass]
           ,[CusomterSubclass]
           ,[PrintStatement]
           ,[ARAmountDue]
           ,[ARAmountCurrent]
           ,[AgeAmount030]
           ,[AgeAmount060]
           ,[AgeAmount090]
           ,[AgeAmount120]
           ,[TotalPayDays]
           ,[LastPaymentAmount]
           ,[LastPaymentDate]
           ,[LastPurchaseDate]
           ,[FirstSaleDate]
           ,[MtdSalesDollars]
           ,[YtdSalesDollars]
           ,[LastYearFiscalSales]
           ,[CreditLimit]
           ,[MtdNumberOrders]
           ,[YtdNumberOrders]
           ,[NumberOrdersLastFiscal]
           ,[TaxCode]
           ,[AcceptBackorders]
           ,[DefaultShipVia]
           ,[TermsCode]
		   ,[DefaultPriceLevel]
           ,[PriceOnPicklist]
           ,[DiscountCode]
           ,[ChargeFreightCode]
           ,[OrderType]
           ,[DefaultWarehouse]
           ,[DefaultOrderComment]
           ,[PORequiredFlag]
           ,[OpenOrderValue]
           ,[DefaultShipTo]
/* Warning: Never Update the PepperiCustomerNumber from the AS400
           ,[PepperiCustomerNumber]
*/
           ,[ObsolescenceStartDate]
           ,[ContractCode]
           ,[DefaultOpenBookContract]
           ,[DefaultMarkupPercent]
           ,[DefaultFreightUpchargeCode]
           ,[RouteInfo]
           ,[DefaultBinLabelBarCode]
           ,[DefaultBinLabelImageCode]
           ,[DefaultBinLabelType]
           ,[DefaultBinLabelStock]
           ,[ObsolescenceEnabled]
           ,[ObsolescenceTargetDollars]
           ,[ObsolescenceMethod]
           ,[ObsolescenceDiscountPercent]
           ,[ObsolescenceMaxDollars]
           ,[PrefixPriceDefault]
           ,[APContact]
           ,[APPhoneNumber]
           ,[APPhoneExtension]
           ,[APFaxNbr]
           ,[ShipTo000Contact]
           ,[ShiptTo000Phone]
           ,[Shipto000Extension]
           ,[ShipTo000FaxNbr]
           ,[SuggestedContract]
           ,[APContactEmail]
           ,[PurchasingContactEmail]
           ,[ShipTo000Email]
    --       ,[DefaultPONumber]
           ,[PrintDuplicatePickSlip]
           ,[CVPEligibleFlag]
           ,[SplitProfit]
		   ,[ErpModifiedDate]
			,[ErpSyncDate]
      ,[ModifiedDate]
      ,[createddate])
SELECT t1.[PadDescription]
      ,t1.[CustomerNumber]
      ,t1.[TerritoryNumber]
      ,t1.[SalesRepNumber]
      ,t1.[CustomerName]
      ,t1.[PurchasingContact]
      ,t1.[AddressLine1]
      ,t1.[AddressLine2]
      ,t1.[City]
      ,t1.[State]
      ,t1.[ZipCode]
      ,t1.[PurchasingPhoneNumber]
      ,t1.[PurchasingPhoneExtension]
      ,t1.[PurchasingFax]
      ,t1.[CustomerClass]
      ,t1.[CusomterSubclass]
      ,t1.[PrintStatement]
      ,t1.[ARAmountDue]
      ,t1.[ARAmountCurrent]
      ,t1.[AgeAmount030]
      ,t1.[AgeAmount060]
      ,t1.[AgeAmount090]
      ,t1.[AgeAmount120]
      ,t1.[TotalPayDays]
      ,t1.[LastPaymentAmount]
      ,t1.[LastPaymentDate]
      ,t1.[LastPurchaseDate]
      ,t1.[FirstSaleDate]
      ,t1.[MtdSalesDollars]
      ,t1.[YtdSalesDollars]
      ,t1.[LastYearFiscalSales]
      ,t1.[CreditLimit]
      ,t1.[MtdNumberOrders]
      ,t1.[YtdNumberOrders]
      ,t1.[NumberOrdersLastFiscal]
      ,t1.[TaxCode]
      ,t1.[AcceptBackorders]
      ,t1.[DefaultShipVia]
      ,t1.[TermsCode]
	  ,t1.[DefaultPriceLevel]
      ,t1.[PriceOnPicklist]
      ,t1.[DiscountCode]
      ,t1.[ChargeFreightCode]
      ,t1.[OrderType]
      ,t1.[DefaultWarehouse]
      ,t1.[DefaultOrderComment]
      ,t1.[PORequiredFlag]
      ,t1.[OpenOrderValue]
      ,t1.[DefaultShipTo]
	  /* Warning: Never Update the PepperiCustomerNumber from the AS400
      ,t1.[PepperiCustomerNumber]
	  */
      ,t1.[ObsolescenceStartDate]
      ,t1.[ContractCode]
      ,t1.[DefaultOpenBookContract]
      ,t1.[DefaultMarkupPercent]
      ,t1.[DefaultFreightUpchargeCode]
      ,t1.[RouteInfo]
      ,t1.[DefaultBinLabelBarCode]
      ,t1.[DefaultBinLabelImageCode]
      ,t1.[DefaultBinLabelType]
      ,t1.[DefaultBinLabelStock]
      ,t1.[ObsolescenceEnabled]
      ,t1.[ObsolescenceTargetDollars]
      ,t1.[ObsolescenceMethod]
      ,t1.[ObsolescenceDiscountPercent]
      ,t1.[ObsolescenceMaxDollars]
      ,t1.[PrefixPriceDefault]
      ,t1.[APContact]
      ,t1.[APPhoneNumber]
      ,t1.[APPhoneExtension]
      ,t1.[APFaxNbr]
      ,t1.[ShipTo000Contact]
      ,t1.[ShiptTo000Phone]
      ,t1.[Shipto000Extension]
      ,t1.[ShipTo000FaxNbr]
      ,t1.[SuggestedContract]
      ,t1.[APContactEmail]
      ,t1.[PurchasingContactEmail]
      ,t1.[ShipTo000Email]
 --     ,t1.[DefaultPONumber]
      ,t1.[PrintDuplicatePickSlip]
      ,t1.[CVPEligibleFlag]
      ,t1.[SplitProfit]
	  ,@RunDate [ErpModifiedDate]
      ,@RunDate [ErpSyncDate]
      ,@RunDate [ModifiedDate]
      ,@RunDate [Createddate] 
 FROM StageAP.Customer t1
Left JOIN dbo.Customer t2
ON t1.CustomerNumber = t2.CustomerNumber
where T2.CustomerNumber is null

UPDATE t1
SET
Deleted = 1 , 
ModifiedDate = @RunDate, 
ModifiedBy = suser_name(),
ErpModifiedDate = @RunDate,
ErpModifiedBy = suser_name(),
ERPSyncdate = @RunDate,
ErpSyncBy = @RunDate 
FROM dbo.Customer t1 
Left JOIN StageAP.Customer t2
ON t1.CustomerNumber = t2.CustomerNumber
where t1.CustomerNumber < 1000000  -- Make sure they have received there Perm Customer Number
AND t2.CustomerNumber is NULL
AND t1.deleted = 0

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/

CREATE PROCEDURE [dbo].[CustomerNumberValidation] 
AS
BEGIN


DECLARE @Count INT
SET @Count= 0


 SELECT 
       @Count= COUNT(*)
     
  FROM [WUSA-SQL01-DFW].[PEPPERI_STAGING].[dbo].[Customer] 
  WHERE PepperiCustomerNumber = CustomerNumber and createddate >= '1/1/2020' and  ErpModifiedDate  is not null
IF @Count <> 0 
BEGIN

	EXEC msdb.dbo.sp_send_dbmail 
	@profile_name = 'Account'
	, @recipients = 'bret.aylor@winzerusa.com'
    , @from_address = 'WinzerAutomatedEmail@winzerusa.com'
	--, @body = @tableHTML
       , @body_format = 'HTML'
	, @subject = 'CustomerNumberNotConverted'
	--, @file_attachments = @fullFileName; --YOU CAN ALSO ATTACH A FILE TO THE MAIL IF NEED BE.
    end

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      3/5/2019
-- Description  Net sales targets by branch, year and month. including targets

-- Copyright © 2019, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History

-------------------------------------------------------------------------------
-- exec FastServNetSalesBranchRepTargetReport

CREATE PROCEDURE [dbo].[FastServNetSalesBranchRepTargetReport]
AS
BEGIN


-- Join YTDSales, MTDSales with target data
SELECT 
	   FS.FiscalYear,
	   FS.[CurrentMonthName],
	   FS.CurrentFiscalMonth,
	   FS.SalesRepID,
	   FS.SalesRepresentativeName,
	   FS.BranchID,
	   FS.BranchDescription,
	   FS.MtdSales,
	   CASE WHEN SC.MtdGoal IS NULL THEN 0 ELSE SC.MtdGoal END  AS MtdGoal,
	   CASE WHEN SC.MthGoal IS NULL THEN 0 ELSE SC.MthGoal END AS MthGoal,
	   FS.YtdSales ,
	   CASE WHEN SC.YtdGoal IS NULL THEN 0 ELSE SC.YtdGoal END AS YtdGoal,
	   CASE WHEN SC.YthGoal IS NULL THEN 0 ELSE SC.YthGoal END AS YthGoal,
       FS.DateAdded
	   
from FastServSales_View FS
	 LEFT outer JOIN FactScoreCardTargetsP21 SC
		ON SC.EmployeeKey = FS.EmployeeKey AND SC.BranchKey = FS.BranchKey  AND SC.ScorecardGroup = 'FASTSERV'

UNION ALL 

SELECT HS.FiscalYear, HS.CurrentMonthName, DA.FiscalMonth,HS.SalesRepID,HS.SalesRepresentativeName,HS.BranchID,HS.BranchDesciption,HS.MtdSales,HS.MtdGoal,MthGoal,YtdSales,HS.YtdGoal,YthGoal,HS.DateAdded
FROM FastServNetSalesBranchRepHistorical HS LEFT OUTER JOIN  dbo.DimMonthDate DA ON HS.CurrentMonthName = DA.MonthName 


END



GO

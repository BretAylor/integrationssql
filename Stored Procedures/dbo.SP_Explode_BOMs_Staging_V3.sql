SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--EXEC [dbo].[SP_Explode_BOMs_Staging_V3] 


CREATE procedure [dbo].[SP_Explode_BOMs_Staging_V3] as 
--Cleanup
IF OBJECT_ID('tempdb..#mytemp') IS NOT NULL DROP TABLE #mytemp;
IF OBJECT_ID('Demand_Planning..BOM_ExplodeV3') IS NULL Create Table BOM_ExplodeV3(BOM varchar(255) Index ix_BOM_ExplodeV3, Comp_Itm Varchar(255), Tot_Qty Decimal(10,2), Par_List varchar(max), DateAdded DateTime);
Else Truncate table dbo.BOM_ExplodeV3;

WITH cte_GET_MFG_ITM AS 
(
	--Select itemnumber from bi_staging.dbo.itemmaster where billofmaterialtype in('K','M')
	Select distinct parentitemnumber from bi_staging.dbo.materialcomponent
)

Select * into #mytemp from cte_GET_MFG_ITM --where imbmtp in('K','M');
CREATE clustered index ix_temp_itno on #mytemp (parentitemnumber asc)

DECLARE @ITMID VARCHAR(255)
--DECLARE @Date Date
--Set @Date = GetDate()

WHILE EXISTS (SELECT * FROM #mytemp)
	BEGIN
		Select @ITMID = (Select top 1 parentitemnumber from #mytemp) --order by parentitemnumber asc)
		
			;WITH EXPL as 
			(
				select par.parentitemnumber, par.componentitemnumber, cast(par.quantityperparent as decimal(38,2)) as Qty, par.UnitofMeasure as UOM
				  from [bi_staging].[dbo].[materialcomponent] par
				 where par.ParentItemNumber = @ITMID AND par.companynumber = 0
			
				 union all

			    select child.parentitemnumber, child.componentitemnumber, Cast(PARENT.Qty * Cast(child.quantityperparent as decimal(38,2)) as decimal(38,2)) as Qty, child.UnitofMeasure as UOM
			      from EXPL PARENT, [bi_staging].[dbo].[materialcomponent] child
			     where PARENT.componentitemnumber = child.parentitemnumber
			)
			Insert into dbo.BOM_ExplodeV3 (BOM , Comp_Itm , Tot_Qty , Par_List )
			     Select @ITMID as BOM, b.componentitemnumber as Comp_Itm, sum(b.Qty) as Tot_Qty ,'' as Par_List--(Select STUFF((Select ', ' + a.parentitemnumber from EXPL a where a.componentitemnumber = b.componentitemnumber FOR XML PATH('')), 1, 2, '')) as Par_List 
			       From EXPL b 
			       left join expl c
			         on b.componentitemnumber = c.parentitemnumber
			      where nullif(c.parentitemnumber,'')is null
			   Group By b.componentitemnumber

		Delete #mytemp
		where parentitemnumber = @ITMID;
	End

	--Update dbo.BOM_ExplodeV3
	--Set DateAdded = @date

--Cleanup
Drop table #mytemp
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- exec [dbo].[AllTypesMonthlyNetSalesReport] 
CREATE PROCEDURE [dbo].[FactAllSales] 
AS
BEGIN
SELECT * FROM (
SELECT 
      'Franchise' AS [Type],
	  1 AS TypeNum,
	  OrderKey,
	  DateKey,
	  ProductKey,
	  EmployeeKey,
	  CustomerKey,
	  QuantityShipped ,
	  ListPrice , 
	  WinzerCost,
	  FranchiseCost,
	  NetSales ,
	  GETDATE() AS DateAdded

FROM BI_DWH.[dbo].[FactInvoiceFranchise] 
WHERE DateKey >= 20190101


UNION ALL

-- Add P21 costs
SELECT 
	   'FastServ' AS [Type],
	   2 AS TypeNum,
	   OrderNo AS OrderKey,
	   DateKey,
	   ProductKey,
	   EmployeeKey,
	   CustomerKey,
	   QtyShipped AS QuantityShipped,
	   UnitPrice ,
	   WinzerCost,
	   FranchiseCost,
       NetSales ,
	   GETDATE() AS DateAdded

	   
FROM BI_DWH.[dbo].[FactFastServNetSales] 
Where DateKey >= 20190101


UNION ALL 

-- add costs for AS400 A1chemical - done

SELECT 
       'A1 Chemical' AS [Type],
	   3 AS TypeNum,
	   OrderNo AS OrderKey,
	   DateKey,
	   ProductKey,
	   EmployeeKey,
	   CustomerKey,
	   QtyShipped AS QuantityShipped,
	   UnitPrice ,
	   WinzerCost, 
	   FranchiseCost, 
       NetSales,
	   GETDATE() AS DateAdded

FROM BI_DWH.dbo.FactA1ChemicalNetSales 
WHERE DateKey >= 20190101 AND [Type] = 'A1Reno'

UNION ALL 

-- Add costs for P21 A1 chemical - done
SELECT 
       'A1 Chemical' AS [Type],
	   3 AS TypeNum,
	   OrderNo AS OrderKey,
	   DateKey,
	   ProductKey,
	   EmployeeKey,
	   CustomerKey,
	   QtyShipped AS QuantityShipped,
	   UnitPrice ,
	   WinzerCost,
       FranchiseCost,
	   NetSales,
	   GETDATE() AS DateAdded

FROM BI_DWH.dbo.FactA1ChemicalNetSales FC
   
WHERE DateKey >= 20190101 AND [Type] = 'A1LasVegas'


UNION ALL 

-- Add Costs for P21 Speedy Clean 

SELECT 
       'Speedy Clean' AS [Type],
	   4 AS TypeNum,
	   InvoiceNo AS OrderId,
	   DateKey,
	   ProductKey,
	   EmployeeKey,
	   CustomerKey,
	   QtyShipped AS QuantityShipped,
	   UnitPrice ,
	   WinzerCost,
	   FranchiseCost,
	   NetSales AS NetSales ,
	   GETDATE() AS DateAdded

FROM BI_DWH.[dbo].[FactSpeedyCleanNetSales]
WHERE DateKey >= 20190101

	   ) AS a 
ORDER BY [Type], a.DateKey


END


GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      1/3/2019
-- Description  Provide Monthly working daily counts. This year, prior year, Forecast and Budget excluding weekends and holidays     
-- Copyright © 2019, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History
--
-------------------------------------------------------------------------------

CREATE Procedure [dbo].[MonthlyDayCountReport] 
as
Begin
drop table if exists #Monthdays
Declare @ThisYear int
set @ThisYear = year(getdate())
Declare @PriorYear int
set @PriorYear = year(getdate()) -1
Declare @Forecast int
set @Forecast = year(getdate())
Declare @Budget int
set @Budget = year(getdate())


select case when FiscalYear =  @ThisYear then Count(*) end as [This Year],
       case when FiscalYear =  @PriorYear then Count(*) end as [Prior Year],
	   case when FiscalYear =  @Forecast then Count(*) end as [Forecast],
       case when FiscalYear =  @Budget then Count(*) end as [Budget],    
	   FiscalMonth,
	   MonthNameShort,
	   getdate() as DateAdded
into #Monthdays From DimDate 
where FiscalYear in(@ThisYear,@PriorYear) and DayName not in ('Saturday','Sunday') and Holiday is NULL
group by  FiscalYear, FiscalMonth ,MonthNameShort
order by FiscalMonth asc

select DateAdded, FiscalMonth, MonthNameShort, sum(isnull([This Year],0)) as [ThisYear],sum(isnull([Prior Year],0)) as [PriorYear],sum(isnull([Forecast],0)) as [Forecast] ,
sum(isnull([Budget],0))  as [Budget]
from #Monthdays
group by  DateAdded,FiscalMonth, MonthNameShort
order by FiscalMonth asc

End



GO

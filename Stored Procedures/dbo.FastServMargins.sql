SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FastServMargins] 
AS
begin

SELECT YEAR(inh.datecreated) AS CalYear,                                                         
       MONTH(inh.datecreated) AS CalMonth,
       inh.datecreated AS CreatedDate,
       inh.invoiceno AS InvoiceNbr,
       inh.customerid AS CustomerId,
       cu.CustomerName ,
       inh.salesrepid AS SalesrepId,
       co.firstname + ISNULL(co.middle, ' ') + co.lastname AS SalesrepName,
       cd.customer_contract_code AS ContractCode,
       cov.price ContractPrice, 
       CASE
           WHEN cov.CorpItemId IS NULL THEN
               '4-Other'
           WHEN cd.customer_contract_code = 'LEV1' THEN
               '1-Level 1'
           WHEN cd.customer_contract_code = 'LEV2' THEN
               '2-Level 2'
           WHEN RIGHT(CAST(cu.customerid AS VARCHAR), 4) = cd.customer_contract_code THEN
               '3-Contract'
           ELSE
               '4-Other'
       END AS ContractCategory,
       inh.LocationId AS LocationId,
	    CASE
           WHEN inh.LocationId = 1480 THEN
               'BA Industrial'
           WHEN inh.LocationId = 1461 THEN
               'San Antonio'
           WHEN inh.LocationId = 1462 THEN
               'Corpus Christi'
           WHEN inh.LocationId = 1464 THEN
               'Pharr'
           WHEN inh.LocationId = 1465 THEN
               'Laredo'
           WHEN inh.LocationId = 1466 THEN
               'BrownsVille'
           WHEN inh.LocationId = 1467 THEN
               'Bryan'
           WHEN inh.LocationId = 1468 THEN
               'Las Cruces'
           WHEN inh.LocationId = 1470 THEN
               'Austin'
           ELSE
               'Other'
       END AS [BranchName],
       inl.itemid AS ItemId,
       inm.item_desc AS ItemDesc,
       inl.qtyshipped AS QtyShipped,
       inl.unitprice AS UnitPrice,
       inl.extendedprice AS ExtendedPrice,
       inl.cogsamount AS ExtendedCogsAmt,
	   GETDATE() AS RefreshDate
FROM [BI_STAGING].[dbo].[InvoiceP21] inh
    INNER JOIN [BI_STAGING].[dbo].CustomerP21 AS cu
        ON inh.customerid = cu.customerid
           AND inh.companyno = cu.companyid
    INNER JOIN [BI_STAGING].[dbo].customer_ud_p21 AS cd
        ON inh.customerid = cd.customer_id
           AND inh.companyno = cd.company_id
    INNER JOIN [BI_STAGING].[dbo].contactsP21 AS co
        ON inh.salesrepid = co.id
    INNER JOIN [BI_STAGING].[dbo].invoicelinep21 AS inl
        ON inh.invoiceno = inl.invoiceno
    LEFT JOIN  [BI_STAGING].[dbo].invmastP21 inm
        ON inl.invmastuid = inm.inv_mast_uid
    LEFT JOIN BI_STAGING.[dbo].wzrContractP21 AS cov
        ON cd.customer_contract_code = cov.contract_no
           AND inl.invmastuid = cov.inv_mast_uid
WHERE inl.otherchargeitem = 'N'
      AND inl.invmastuid <> 0
      AND inh.datecreated >= '2019-01-01'
	   

END 


SELECT * FROM [BI_STAGING].[dbo].[InvoiceP21] 
GO

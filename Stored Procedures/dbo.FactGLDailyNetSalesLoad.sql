SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      8/1/2019
-- Description  GL Daily Net Sales by fiscal year, month, day and account

-- Copyright © 2019, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History

-------------------------------------------------------------------------------
-- exec [dbo].[FactGLDailyNetSalesLoad]
CREATE PROCEDURE [dbo].[FactGLDailyNetSalesLoad]
AS
BEGIN

TRUNCATE TABLE dbo.FactGLDailyNetSales
INSERT INTO dbo.FactGLDailyNetSales
(      ChartOfAccountKey,
       TransactionDateKey,
       GLPostingDate,
       PostingYear,
	   PostingPeriod,
	   PostingMonth,
       CompanyNum,
       GlAccountNum,
	   [TransactionAmount],
       DateAdded
   
)

SELECT coa.ChartOfAccountKey,
       CONCAT([Century2],TransactionDate)AS TransactionDateKey,
       CONCAT([Century1],GLPostingDate) AS GLPostingDate,
       PostingYear,
	   PostingPeriod,
	   dm.MonthName AS PostingMonth,
       CompanyNum,
       replace(RTRIM(GlAccountNum),' ','') AS GlAccountNum,
	   -1 * CASE WHEN DebitorCreditCode = 'D' then TransactionAmount Else 0 END + CASE when DebitorCreditCode = 'C' then TransactionAmount Else 0 END AS Amount,
       GETDATE() AS DateAdded
FROM BI_DWH.[dbo].[DimChartOfAccount] coa
    INNER JOIN BI_STAGING.[dbo].[GLDailyTotal] gl
        ON coa.InternalGlAcctNo = gl.InternalGlAcctNo
    LEFT OUTER JOIN [BI_DWH].[dbo].[DimMonthDate] dm 
	    ON dm.FiscalMonth = gl.PostingPeriod
WHERE 
      GLAccountNum IN ( '00160004450000', '00160004800000', '00160004806000', '00160004807000', '00160004815000',
                            '00160004820000', '00160004825000', '00195004950000', '00195004999000', '00160004800000',
                            '00160004820000', '00160004890000'
                          )

ORDER BY CompanyNum,
         coa.ChartOfAccountKey ASC;

END



GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		baylor
-- Create date: 5/2020
-- Description:	Update to Pepperi_Staging 
-- =============================================
CREATE PROCEDURE [StageAP].[ProductInventoryUpdate] 
AS

BEGIN

SET NOCOUNT ON;

SET TRAN ISOLATION LEVEL READ UNCOMMITTED	

-- Clean
UPDATE Stageap.productinventory
SET ItemNumber = [dbo].[REPLACE_UNPRINT_CHARS](dbo.fnRemoveNonLowAsciiCharacters(TRIM(ItemNumber)))
,Warehouseid = [dbo].[REPLACE_UNPRINT_CHARS](dbo.fnRemoveNonLowAsciiCharacters(TRIM(WarehouseID)))

-- From Original Script
update PI1 SET 
PI1.ItemDescription =           PI2.ItemDescription ,
PI1.BillOfMaterialType =     	PI2.BillOfMaterialType ,
PI1.ReaderId =       			PI2.ReaderId ,       
PI1.City =    					PI2.City ,   
PI1.QuantityAvailable =  		PI2.QuantityAvailable ,
PI1.QuantitySold =  			PI2.QuantitySold ,
PI1.NumberOfOrders =  			PI2.NumberOfOrders ,  
PI1.RefreshTimeStamp =  		PI2.RefreshTimeStamp , 
PI1.modifieddate = getdate(), 
PI1.modifiedby = suser_name() 
FROM dbo.[ProductInventory] PI1 
JOIN StageAP.ProductInventory PI2 
ON PI1.ItemNumber = PI2.ItemNumber  
AND PI1.WarehouseId = PI2.WarehouseId  
where PI1.ItemDescription <>           PI2.ItemDescription or
PI1.BillOfMaterialType <>     	PI2.BillOfMaterialType or
PI1.ReaderId <>       			PI2.ReaderId or       
PI1.City <>    					PI2.City or   
PI1.QuantityAvailable <>  		PI2.QuantityAvailable or
PI1.QuantitySold <>  			PI2.QuantitySold or
PI1.NumberOfOrders <>  			PI2.NumberOfOrders or  
PI1.RefreshTimeStamp <>  		PI2.RefreshTimeStamp

INSERT INTO [dbo].[ProductInventory]
           ([ItemNumber]
           ,[ItemDescription]
           ,[BillOfMaterialType]
           ,[ReaderId]
           ,[WarehouseID]
           ,[City]
           ,[QuantityAvailable]
           ,[QuantitySold]
           ,[NumberOfOrders]
           ,[RefreshTimeStamp])
Select TRIM(t1.[ItemNumber])
           ,t1.[ItemDescription]
           ,t1.[BillOfMaterialType]
           ,t1.[ReaderId]
           ,t1.[WarehouseID]
           ,t1.[City]
           ,t1.[QuantityAvailable]
           ,t1.[QuantitySold]
           ,t1.[NumberOfOrders]
           ,t1.[RefreshTimeStamp]
FROM StageAP.ProductInventory t1 
LEFT JOIN dbo.[ProductInventory] t2 
ON t1.ItemNumber = t2.ItemNumber 
AND t1.WarehouseId = t2.WarehouseId
where t2.warehouseid IS NULL

-- Delete 
Delete T1
From dbo.ProductInventory t1
left join StageAP.ProductInventory t2 
ON TRIM(t1.ItemNumber) = TRIM(t2.ItemNumber) 
AND TRIM(t1.WarehouseId) = TRIM(t2.WarehouseId)
where t2.warehouseid is null

END
GO

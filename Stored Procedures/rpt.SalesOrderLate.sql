SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		baylor
-- Create date: 0520
-- Description:	Check Sales Order/Line before Processing
-- Why is it here? Because BIO01 cannot connect to AS400
-- =============================================
CREATE PROCEDURE [rpt].[SalesOrderLate]
AS
BEGIN
SET NOCOUNT ON;
set transaction isolation level Read Uncommitted

Begin Try Drop Table #Old End Try Begin Catch end Catch

Select cast(PepperiOrderNbr as varchar(100)) PepperiOrderNbr, convert(varchar,createddate,121) CreatedDate
into  #old
from [WUSA-SQL01-DFW].[Pepperi_Staging].[dbo].[SalesOrder] 
where [ErpSyncDate] is null
and [CreatedDate]  < dateadd(minute,-40,getdate())

-- Send Email if Issues
If @@RowCount > 0
Begin
DECLARE @xml NVARCHAR(MAX)
DECLARE @body NVARCHAR(MAX)

SET @xml = CAST(( SELECT PepperiOrderNbr AS 'td','',CreatedDate AS 'td',''
FROM #Old
ORDER BY PepperiOrderNbr
FOR XML PATH('tr'), ELEMENTS ) AS NVARCHAR(MAX))

SET @body ='<html><body><H3>Sales Orders: Processing Overdue</H3>
<table border = 1> 
<tr>
<th> Pepperi Order Number </th> <th> Created Date </th> </tr>'    
SET @body = @body + @xml +'</table></body></html>'

EXEC [WINSQLBI01].msdb.dbo.sp_send_dbmail  
    @profile_name = 'Account',  
    @recipients = 'bret.aylor@winzerusa.com',  
    @body = @body ,  
    @subject = 'Pepperi Staging: Sales Orders, Processing Overdue',  
    @attach_query_result_as_file = 0,
	@importance  = 'High',
	@body_format = 'HTML'
end





END
GO

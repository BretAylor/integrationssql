SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
Declare Variables for HTML
*/

CREATE PROCEDURE [dbo].[SalesOrderCustomerStillOnHoldNotification_20200420]
AS
BEGIN
DECLARE @Style NVARCHAR(MAX)= '';
 
/*
Define CSS for html to use
*/
SET @Style += +N'<style type="text/css">' + N'.tg  {border-collapse:collapse;border-spacing:0;border-color:#aaa;}'
    + N'.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#333;background-color:#fff;}'
    + N'.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#fff;background-color:#f38630;}'
    + N'.tg .tg-9ajh{font-weight:bold;background-color:#ADD8E6}' + N'.tg .tg-hgcj{font-weight:bold;text-align:center}'
    + N'</style>';
 

 
/*
Declare Variables for DML
*/
 
--DECLARE @ReportingPeriodStart DATE;
--DECLARE @ReportingPeriodEnd DATE;
 
 -- Reporting date will from the beginning of the current week
--SET @ReportingPeriodStart = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(YEAR, -3, GETDATE())), 0);
--SET @ReportingPeriodEnd = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(YEAR, -3, GETDATE())) + 1, 0);
 
/*
Declare Variables for HTML
*/
 
DECLARE @tableHTML NVARCHAR(MAX)= '';
 

SET @tableHTML += @Style + @tableHTML +
 --N'<H2>Order Summary For : ' + CAST(@ReportingPeriodStart AS CHAR(10)) + ' to ' + CAST(@ReportingPeriodEnd AS CHAR(10)) + '</H2>' 
	+ N'<table class="tg">' --DEFINE TABLE
/*
Define Column Headers and Column Span for each Header Column
*/
	--+ N'<tr>' 
 --   + N'<th class="tg-hgcj" colspan="2">Order Information</th>' 
	--+ N'<th class="tg-hgcj" colspan="2">Summary</th>'
	--+ N'</tr>' 
/*
Define Column Sub-Headers
*/
	+ N'<tr>'
	+ N'<td class="tg-9ajh">On Hold</td>'
	+ N'<td class="tg-9ajh">Order Nbr</td>'
    + N'<td class="tg-9ajh">Customer Nbr</td></tr>'


/*
Define data for table and cast to xml
*/
    + CAST(( SELECT td =  [On Hold],
					'',
	                td =  PepperiOrderNbr,
					'',
                    td = CustomerNbr,
					''
                   
                FROM [172.25.6.17].[Pepperi_Staging].[dbo].[SalesOrderHeader_LinesValidationView]
				WHERE ISNULL(SalesOrderCreatedDate,SalesOrderLineCreatedDate) >'9/20/2019'
			    AND  [On Hold] IN('Pepperi Customer Not in Cross Reference','Sales Order without Customer')

                 ORDER BY OrderDate DESC    
           FOR
             XML PATH('tr') ,
                 TYPE
           ) AS NVARCHAR(MAX)) + N'</table>'
		   +
 N'<a href="http://winsqlbi01/Reports/report/Reports/Sales%20Order%20Header%20Line%20Validation">Historical Report can be accessed from this link</a>' 
 

    DECLARE @Count INT
	SET @Count = 0

	SELECT @Count = COUNT(*)
    FROM [172.25.6.17].[Pepperi_Staging].[dbo].[SalesOrderHeader_LinesValidationView]
	WHERE  ISNULL(SalesOrderCreatedDate,SalesOrderLineCreatedDate) >'9/20/2019'
    AND  [On Hold] IN ('Pepperi Customer Not in Cross Reference','Sales Order without Customer')


    IF(@Count <> 0 )
	BEGIN
	EXEC msdb.dbo.sp_send_dbmail 
	@profile_name = 'Account'
	, @recipients = 'DL-ITNew@winzerusa.com;Manny.Rodriguezj@winzerusa.com;Bret.Aylor@winzerusa.com'
    , @from_address = 'WinzerAutomatedEmail@winzerusa.com'
	, @body = @tableHTML
       , @body_format = 'HTML'
	, @subject = 'Sales Order Still On Hold'
	--, @file_attachments = @fullFileName; --YOU CAN ALSO ATTACH A FILE TO THE MAIL IF NEED BE.
    END
 END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      2/10/2019
-- Description  Load DimItemClass table
-- Copyright © 2019, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History
--
-------------------------------------------------------------------------------

-- exec DimItemClassLoad
CREATE PROCEDURE [dbo].[DimItemClassLoad]
AS
BEGIN

--CREATE TABLE  DimItemClass (
--ItemClassKey INT,
--ItemClassCode NVARCHAR(2) NULL,
--Description NVARCHAR(30) NULL,
--DateAdded DATETIME NULL
--)
TRUNCATE TABLE DimItemClass
INSERT INTO DimItemClass (ItemClassCode, [Description],DateAdded)

SELECT REPLACE(CCCTLK, '@PC', '') AS [ItemClassCode],
       LEFT(CCDATA, 30) AS [Description],
	   GETDATE() AS DateAdded
FROM DimGenericCode
WHERE CCCONO = 0
      AND CCAPID = 'IA'
      AND CCCTLK LIKE '@PC%'
      AND CCCTLK NOT LIKE '@PC%A%'
      AND CCCTLK NOT LIKE '@PC%B%'
      AND CCCTLK NOT LIKE '@PC%C%'
END

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      3/5/2019
-- Description  Net sales targets by branch, year and month. including targets

-- Copyright © 2019, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History

-------------------------------------------------------------------------------
-- exec [dbo].[FastServNetSalesBranchRepTargetHistoricalLoad]

CREATE PROCEDURE [dbo].[FastServNetSalesBranchRepTargetHistoricalLoad]
AS
BEGIN

DECLARE @CurrentFYear INT 
SELECT @CurrentFYear = FiscalYear FROM BI_DWH.dbo.DimDate WHERE CAST(GETDATE() AS DATE) = Date

DECLARE @CurrentFMonth INT 
SELECT @CurrentFMonth = FiscalMonth FROM BI_DWH.dbo.DimDate WHERE CAST(GETDATE() AS DATE) = Date

DECLARE @CurrentMonthName VARCHAR(20) 
SELECT @CurrentMonthName = [MonthName] FROM BI_DWH.dbo.DimDate WHERE CAST(GETDATE() AS DATE) = Date

-------------------

-- Manual load
--DECLARE @CurrentFYear INT 
--SELECT @CurrentFYear = 2020

--DECLARE @CurrentFMonth INT 
--SELECT @CurrentFMonth = 5

--DECLARE @CurrentMonthName VARCHAR(20) 
--SELECT @CurrentMonthName = 'December'


-- Get YTDSales and MTDSales totals
DROP TABLE IF EXISTS #FastServSales
SELECT 
	   DD.FiscalYear,
	   @CurrentMonthName AS CurrentMonthName,
	   EM.SalesRepID,
	   FS.EmployeeKey,
	   ISNULL(EM.FirstName,'') + ' ' +  ISNULL(EM.Middle,'') + ' ' + ISNULL(EM.LastName,'') as SalesRepresentativeName,
	   FS.BranchKey,
	   BR.BranchID,
	   BR.BranchDescription,
	   ROUND(SUM(CASE WHEN DD.FiscalMonth = @CurrentFMonth THEN FS.NetSales ELSE 0 end),0) AS MtdSales,
	   ROUND(SUM(FS.NetSales),0) AS YtdSales ,
	   FS.DateAdded
	   
INTO #FastServSales FROM BI_DWH.[dbo].[FactFastServNetSales] FS
    INNER JOIN BI_DWH.dbo.DimDate DD
       ON FS.DateKey = DD.DateKey
	LEFT outer JOIN BI_DWH.dbo.DimBranchP21 BR 
	    ON FS.BranchKey = BR.BranchKey
    LEFT outer JOIN dbo.DimEmployeeP21 EM
		ON EM.EmployeeKey = FS.EmployeeKey
	
WHERE DD.FiscalYear = @CurrentFYear 
GROUP  BY
       DD.FiscalYear,
	   EM.SalesRepID,
	   ISNULL(EM.FirstName,'') + ' ' +  ISNULL(EM.Middle,'') + ' ' + ISNULL(EM.LastName,'') ,
	   BR.BranchID,
	   BR.BranchDescription ,
	   FS.DateAdded,
	   FS.EmployeeKey,
	   FS.BranchKey

ORDER BY  EM.SalesRepID ASC;


-- Join YTDSales, MTDSales with target data
DELETE FROM FastServNetSalesBranchRepHistorical WHERE CAST(dateAdded AS DATE) = CAST(GETDATE() AS DATE) -- CurrentMonthName = 'December' AND FiscalYear = 2020 --
INSERT INTO FastServNetSalesBranchRepHistorical 
SELECT 
	   FS.FiscalYear,
	   FS.[CurrentMonthName],
	   FS.SalesRepID,
	   FS.SalesRepresentativeName,
	   FS.BranchID,
	   FS.BranchDescription,
	   FS.MtdSales,
	   CASE WHEN SC.MtdGoal IS NULL THEN 0 ELSE SC.MtdGoal END  AS MtdGoal,
	   CASE WHEN SC.MthGoal IS NULL THEN 0 ELSE SC.MthGoal END AS MthGoal,
	   FS.YtdSales ,
	   CASE WHEN SC.YtdGoal IS NULL THEN 0 ELSE SC.YtdGoal END AS YtdGoal,
	   CASE WHEN SC.YthGoal IS NULL THEN 0 ELSE SC.YthGoal END AS YthGoal,

       FS.DateAdded
	   
from #FastServSales FS
	 LEFT outer JOIN FactScoreCardTargetsP21 SC
		ON SC.EmployeeKey = FS.EmployeeKey AND SC.BranchKey = FS.BranchKey  AND SC.ScorecardGroup = 'FASTSERV'

ORDER BY  FS.SalesRepID ASC;




END




GO

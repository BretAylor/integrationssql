SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[UPDATE_FCM]
AS 
     --Cleanup Old Table Versions
     IF OBJECT_ID('tempdb..#mytemp') IS NOT NULL
         DROP TABLE #mytemp;
     IF OBJECT_ID('Demand_Planning..F_Cost_2020_Tot_Units_CY2019_ANSP') IS NOT NULL
         DROP TABLE F_Cost_2020_Tot_Units_CY2019_ANSP;

/****************************
Update Total Units Table Data
****************************/

     WITH cte_get_items
          AS (SELECT DISTINCT       
                     a.itemnumber, 
                     a.itemdescription1, 
                     a.itemclass, 
                     a.ItemSubclass,
                     CASE
                       WHEN e.UMConversionFactor > 1 THEN(d.cost / e.UMConversionFactor)
                         ELSE d.Cost
                     END AS Vnd_Price, 
                     SUM(b.QuantityShipped) AS Tot_Units_Sold, 
                     COUNT(DISTINCT b.CustomerNum) AS Num_Customers, 
                     COUNT(DISTINCT b.PrimarySalesmanNum) AS Num_Reps, 
                     COUNT(DISTINCT b.HistorySequenceNum) AS Num_Orders
                     FROM  bi_staging.dbo.itemmaster a
                           LEFT JOIN dbo.FCOST_Rev_Sls_Hst b
                           ON a.itemnumber = b.ItemNum
                           LEFT JOIN dbo.FCOST_HST c
                           ON a.ItemNumber = c.Item_Number
                           LEFT JOIN [BI_STAGING].[dbo].[POVendorItemPrice] d
                           ON a.ItemNumber = d.ItemNumber
                              AND a.VendorNumber = d.VendorNumber
                           LEFT JOIN bi_staging.dbo.vendoritem e
                           ON d.ItemNumber = e.itemnumber
                              AND d.VendorNumber = e.vendornumber
                     WHERE NULLIF(a.billofmaterialtype, '') IS NULL
                           AND a.SuspendCode <> 'S'
                           AND NULLIF(d.WarehouseID, '') IS NULL
                     GROUP BY 
                              a.ItemNumber, 
                              a.itemdescription1, 
                              a.itemclass, 
                              a.ItemSubclass, 
                              d.Cost, 
                              e.UMConversionFactor),
          cte_ANSP
          AS (

          --SELECT DISTINCT       
          --              a.itemnumber, 
          --              SUM(b.QuantityShipped) AS Tot_Units_Sold,
          --              CASE
          --                                        WHEN SUM(b.QuantityShipped) = 0 THEN 0
          --                  ELSE SUM((b.QuantityShipped * b.ActualSellPrice)) / SUM(b.QuantityShipped)
          --              END AS ANSP
          --              FROM  bi_staging.dbo.itemmaster a
          --                    LEFT JOIN dbo.FCOST_Rev_Sls_Hst b
          --                    ON a.itemnumber = b.ItemNum
          --              WHERE NULLIF(a.billofmaterialtype, '') IS NULL
          --                    AND a.SuspendCode <> 'S'
          --                    AND b.actualsellprice > 0
          --			  AND b.QuantityShipped > 0
          --              GROUP BY 
          --                       a.ItemNumber
          --       --HAVING SUM(b.QuantityShipped) > 0
          --       --ORDER BY a.ItemNumber ASC
          SELECT DISTINCT      
                 a.itemnumber, 
                 ANSP
                 FROM dbo.ansp_2019 a),

/*****************************************************************************
Franchise Cost Report has varchar values which need to be converted to decimal
*****************************************************************************/

          cte_convert_price_hst
          AS (SELECT      
                     a.Item_Number, 
                     a.["Date_Added"] AS [Date_Added], 
                     CONVERT(DATE, a.["Date_Added"]) AS [Date_Added_DT], 
                     CAST(Replace(replace(a.[2015], '"', ''), ',', '') AS DECIMAL(28, 5)) AS [2015], 
                     CAST(Replace(replace(a.[2016], '"', ''), ',', '') AS DECIMAL(28, 5)) AS [2016], 
                     CAST(Replace(replace(a.[2017], '"', ''), ',', '') AS DECIMAL(28, 5)) AS [2017], 
                     CAST(Replace(replace(a.[2018], '"', ''), ',', '') AS DECIMAL(28, 5)) AS [2018], 
                     CAST(Replace(replace(a.[2019], '"', ''), ',', '') AS DECIMAL(28, 5)) AS [2019], 
                     CAST(b.FC_2020 AS DECIMAL(28, 5)) AS [2020]
                     FROM dbo.FCOST_HST a
                          LEFT JOIN dbo.Fcost_2020 b
                          ON a.Item_Number = b.Item_Number),

/*******************************************************
Pulls in Franchise cost values and calculates YoY change
*******************************************************/

          cte_add_FCost_History
          AS (SELECT      
                     a.itemnumber, 
                     a.itemdescription1, 
                     a.itemclass, 
                     a.ItemSubclass, 
                     a.Tot_Units_Sold, 
                     a.Num_Customers, 
                     a.Num_Reps, 
                     a.Num_Orders, 
                     a.Vnd_Price, 
                     c.ANSP, 
                     b.Date_Added, 
                     b.Date_Added_DT, 
                     YEAR(b.Date_Added) AS Year_Added, 
                     b.[2015], 
                     b.[2016],
                     CASE
                       WHEN b.[2015] = 0 THEN 0
                       WHEN b.[2016] = 0 THEN 0
                         ELSE CAST(((b.[2016] - b.[2015]) / b.[2015]) AS DECIMAL(18, 3))
                     END AS [2016_YoY], 
                     b.[2017],
                     CASE
                       WHEN b.[2016] = 0 THEN 0
                       WHEN b.[2017] = 0 THEN 0
                         ELSE CAST(((b.[2017] - b.[2016]) / b.[2016]) AS DECIMAL(18, 3))
                     END AS [2017_YoY], 
                     b.[2018],
                     CASE
                       WHEN b.[2017] = 0 THEN 0
                       WHEN b.[2018] = 0 THEN 0
                         ELSE CAST(((b.[2018] - b.[2017]) / b.[2017]) AS DECIMAL(18, 3))
                     END AS [2018_YoY], 
                     b.[2019],
                     CASE
                       WHEN b.[2018] = 0 THEN 0
                       WHEN b.[2019] = 0 THEN 0
                         ELSE CAST(((b.[2019] - b.[2018]) / b.[2018]) AS DECIMAL(18, 3))
                     END AS [2019_YoY], 
                     b.[2020],
                     CASE
                       WHEN b.[2019] = 0 THEN 0
                       WHEN b.[2020] = 0 THEN 0
                         ELSE CAST(((b.[2020] - b.[2019]) / b.[2019]) AS DECIMAL(18, 3))
                     END AS [2020_YoY],
                     CASE
                            WHEN b.[2015] > 0 THEN CAST(((b.[2019] - b.[2015]) / b.[2015]) AS DECIMAL(18, 3))
                            WHEN b.[2016] > 0 THEN CAST(((b.[2019] - b.[2016]) / b.[2016]) AS DECIMAL(18, 3))
                            WHEN b.[2017] > 0 THEN CAST(((b.[2019] - b.[2017]) / b.[2017]) AS DECIMAL(18, 3))
                            WHEN b.[2018] > 0 THEN CAST(((b.[2019] - b.[2018]) / b.[2018]) AS DECIMAL(18, 3))
                         ELSE 0
                     END AS Tot_Chg_19,
                     CASE
                            WHEN b.[2015] > 0 THEN CAST(((b.[2020] - b.[2015]) / b.[2015]) AS DECIMAL(18, 3))
                            WHEN b.[2016] > 0 THEN CAST(((b.[2020] - b.[2016]) / b.[2016]) AS DECIMAL(18, 3))
                            WHEN b.[2017] > 0 THEN CAST(((b.[2020] - b.[2017]) / b.[2017]) AS DECIMAL(18, 3))
                            WHEN b.[2018] > 0 THEN CAST(((b.[2020] - b.[2018]) / b.[2018]) AS DECIMAL(18, 3))
                         ELSE 0
                     END AS Tot_Chg_20
                     FROM cte_get_items a
                          LEFT JOIN cte_convert_price_hst b
                          ON a.ItemNumber = b.Item_Number
                          LEFT JOIN cte_ANSP c
                          ON a.itemnumber = c.itemnumber),

/*******************
Remove 0 Sales items
*******************/

          CTE_cleanup_data
          AS (SELECT       
                     a.itemnumber, 
                     a.itemdescription1, 
                     a.itemclass, 
                     a.ItemSubclass, 
                     a.Tot_Units_Sold, 
                     a.Num_Customers, 
                     a.Num_Reps, 
                     a.Num_Orders, 
                     CAST(a.Vnd_Price AS DECIMAL(18, 3)) AS Vnd_Price, 
                     a.ANSP, 
                     a.Date_Added, 
                     a.Date_Added_DT, 
                     a.Year_Added, 
                     a.[2015], 
                     a.[2016], 
                     a.[2016_YoY], 
                     a.[2017], 
                     a.[2017_YoY], 
                     a.[2018], 
                     a.[2018_YoY], 
                     a.[2019], 
                     a.[2019_YoY],
                     CASE
                       WHEN NULLIF(b.[2020_INF], 0) IS NULL THEN b.[2019]
                         ELSE b.[2020_Inf]
                     END AS [2020], 
                     a.[2020_YoY], 
                     a.Tot_Chg_19, 
                     a.Tot_Chg_20
                     FROM  cte_add_FCost_History a
                           LEFT JOIN dbo.FCOST_MST b
                           ON a.itemnumber = b.Item_Number
                     WHERE NULLIF(Tot_Units_Sold, 0) IS NOT NULL),
          CTE_Calc_Margin
          AS (SELECT      
                     a.itemnumber, 
                     a.itemdescription1, 
                     a.itemclass, 
                     a.ItemSubclass, 
                     a.Tot_Units_Sold, 
                     a.Num_Customers, 
                     a.Num_Reps, 
                     a.Num_Orders, 
                     a.Vnd_Price, 
                     a.ANSP, 
                     a.Date_Added, 
                     a.Date_Added_DT, 
                     a.Year_Added, 
                     a.[2015], 
                     a.[2016], 
                     a.[2016_YoY], 
                     a.[2017], 
                     a.[2017_YoY], 
                     a.[2018], 
                     a.[2018_YoY], 
                     a.[2019], 
                     a.[2019_YoY], 
                     a.[2020], 
                     a.[2020_YoY], 
                     a.Tot_Chg_19,
                     CASE
                       WHEN NULLIF(a.Tot_Chg_20, 0) IS NULL THEN a.Tot_Chg_19
                         ELSE a.Tot_Chg_20
                     END AS Tot_Chg_20,
                     CASE
                            WHEN NULLIF(a.vnd_price, 0) IS NULL THEN NULL
                         ELSE(a.Vnd_Price * a.Tot_Units_Sold)
                     END AS W_COGS, 
                     (CAST(a.[2019] AS DECIMAL(28, 5)) * a.Tot_Units_Sold) AS F_COGS_2019, 
                     (CAST(a.[2020] AS DECIMAL(28, 5)) * a.Tot_Units_Sold) AS F_COGS_2020,
                     CASE
                                                                              WHEN NULLIF(a.ANSP, 0) IS NULL THEN NULL
                         ELSE(a.ANSP * a.Tot_Units_Sold)
                     END AS SALES_ANSP,
                     CASE
                            WHEN NULLIF(a.ANSP, 0) IS NULL THEN NULL
                            WHEN NULLIF(a.[2020], 0) IS NULL
                                 AND NULLIF(a.[2019], 0) IS NULL THEN NULL
                            WHEN NULLIF(a.[2020], 0) IS NULL
                                 AND NULLIF(a.[2019], 0) IS NOT NULL THEN((a.ansp - a.[2019]) / a.ansp)
                         ELSE((a.ansp - a.[2020]) / a.ansp)
                     END AS F_MARGIN,
                     CASE
                            WHEN NULLIF(a.vnd_price, 0) IS NULL THEN NULL
                            WHEN NULLIF(a.[2020], 0) IS NULL
                                 AND NULLIF(a.[2019], 0) IS NULL THEN NULL
                            WHEN NULLIF(a.[2020], 0) IS NULL
                                 AND NULLIF(a.[2019], 0) IS NOT NULL THEN((a.[2019] - a.Vnd_Price) / a.[2019])
                         ELSE((a.[2020] - a.Vnd_Price) / a.[2020])
                     END AS W_MARGIN
                     FROM CTE_cleanup_data a),
          CTE_margin_delta
          AS (SELECT      
                     *,
                     CASE
                     WHEN ISNUMERIC(a.F_MARGIN) = 1
                          AND ISNUMERIC(a.W_MARGIN) = 1 THEN(CAST(a.W_MARGIN AS DECIMAL(18, 6)) - CAST(a.F_MARGIN AS DECIMAL(18, 6)))
                     --WHEN ISNUMERIC(a.F_MARGIN) = 0 THEN null
                         ELSE NULL
                     END AS GM_DELTA
                     FROM CTE_Calc_Margin a),
          CTE_margin_Buckets
          AS (SELECT      
                     a.*,
                     CASE
                       WHEN NULLIF(a.Vnd_Price, 0) IS NULL THEN 'E_DISC'
                       WHEN NULLIF(a.ANSP, 0) IS NULL THEN 'E_ANSP'
                       WHEN a.F_MARGIN > 0.45
                            AND a.GM_Delta < 0 THEN '1. FGM>45%>WGM'
                       WHEN a.F_MARGIN > 0.35
                            AND a.GM_Delta < 0 THEN '2. FGM>35%>WGM'
                       WHEN a.F_MARGIN > 0.35
                            AND a.Tot_Chg_20 <= 0 THEN '3. FGM>35%, YOY<=0%'
                       WHEN a.F_MARGIN > 0.35
                            AND a.Tot_Chg_20 <= 0.05 THEN '4. FGM>35%, 0<YOY<=5%'
                       WHEN a.F_MARGIN > 0.35
                            AND a.Tot_Chg_20 <= 0.1 THEN '5. FGM>35%, 5%<YOY<=10%'
                       WHEN a.F_MARGIN > 0.5
                            AND a.Tot_Chg_20 > .1 THEN '6. FGM>50%, YOY>10%'
                         ELSE '7. OTHER'
                     END AS [CATEGORY],
                     CASE
                            WHEN a.Tot_Chg_19 <= 0 THEN '0'
                            WHEN a.Tot_Chg_19 <= 0.01 THEN '0-1'
                            WHEN a.Tot_Chg_19 <= 0.02 THEN '1-2'
                            WHEN a.Tot_Chg_19 <= 0.03 THEN '2-3'
                            WHEN a.Tot_Chg_19 <= 0.04 THEN '3-4'
                            WHEN a.Tot_Chg_19 <= 0.05 THEN '4-5'
                            WHEN a.Tot_Chg_19 <= 0.06 THEN '5-6'
                            WHEN a.Tot_Chg_19 <= 0.07 THEN '6-7'
                            WHEN a.Tot_Chg_19 <= 0.08 THEN '7-8'
                            WHEN a.Tot_Chg_19 <= 0.09 THEN '8-9'
                            WHEN a.Tot_Chg_19 <= 0.1 THEN '9-10'
                            WHEN a.Tot_Chg_19 > 0.1 THEN '>10'
                     END AS [INC_BKT_2019],
                     CASE
                            WHEN a.[2020_YoY] IS NULL THEN '0'
                            WHEN a.[2020_YoY] <= 0.00 THEN '0'
                            WHEN a.[2020_YoY] <= 0.01 THEN '0-1'
                            WHEN a.[2020_YoY] <= 0.02 THEN '1-2'
                            WHEN a.[2020_YoY] <= 0.03 THEN '2-3'
                            WHEN a.[2020_YoY] <= 0.04 THEN '3-4'
                            WHEN a.[2020_YoY] <= 0.05 THEN '4-5'
                            WHEN a.[2020_YoY] <= 0.06 THEN '5-6'
                            WHEN a.[2020_YoY] <= 0.07 THEN '6-7'
                            WHEN a.[2020_YoY] <= 0.08 THEN '7-8'
                            WHEN a.[2020_YoY] <= 0.09 THEN '8-9'
                            WHEN a.[2020_YoY] <= 0.1 THEN '9-10'
                            WHEN a.[2020_YoY] > 0.1 THEN '>10'
                     END AS [INC_BKT_2020_Inf],
                     CASE
                            WHEN b.[2020_Push] IS NULL THEN '0'
                            WHEN (b.[2020_Push] - b.[2020_INF]) / a.[2019] <= 0.00 THEN '0'
                            WHEN (b.[2020_Push] - b.[2020_INF]) / a.[2019] <= 0.01 THEN '0-1'
                            WHEN (b.[2020_Push] - b.[2020_INF]) / a.[2019] <= 0.02 THEN '1-2'
                            WHEN (b.[2020_Push] - b.[2020_INF]) / a.[2019] <= 0.03 THEN '2-3'
                            WHEN (b.[2020_Push] - b.[2020_INF]) / a.[2019] <= 0.04 THEN '3-4'
                            WHEN (b.[2020_Push] - b.[2020_INF]) / a.[2019] <= 0.05 THEN '4-5'
                            WHEN (b.[2020_Push] - b.[2020_INF]) / a.[2019] <= 0.06 THEN '5-6'
                            WHEN (b.[2020_Push] - b.[2020_INF]) / a.[2019] <= 0.07 THEN '6-7'
                            WHEN (b.[2020_Push] - b.[2020_INF]) / a.[2019] <= 0.08 THEN '7-8'
                            WHEN (b.[2020_Push] - b.[2020_INF]) / a.[2019] <= 0.09 THEN '8-9'
                            WHEN (b.[2020_Push] - b.[2020_INF]) / a.[2019] <= 0.1 THEN '9-10'
                            WHEN (b.[2020_Push] - b.[2020_INF]) / a.[2019] > 0.1 THEN '>10'
                     END AS [INC_BKT_2020_Push], 
                     b.[2019] AS [2019_1], 
                     b.[2020_INF], 
                     b.[2020_Push], 
                     b.FLAGS, 
                     b.Chg_Notes, 
                     b.Changed
                     FROM cte_margin_delta a
                          LEFT JOIN dbo.FCOST_MST b
                          ON a.itemnumber = b.Item_Number)
          -----
          --SELECT * FROM cte_get_items a	WHERE a.itemnumber	= '686.001' 
          --GO
          ----
          SELECT      
                 a.itemnumber, 
                 a.itemdescription1, 
                 a.itemclass, 
                 a.ItemSubclass, 
                 a.Tot_Units_Sold, 
                 a.Num_Customers, 
                 a.Num_Reps, 
                 a.Num_Orders, 
                 a.Vnd_Price, 
                 a.ANSP, 
                 a.Date_Added, 
                 a.Date_Added_DT, 
                 a.Year_Added, 
                 a.[2015], 
                 a.[2016], 
                 a.[2016_YoY], 
                 a.[2017], 
                 a.[2017_YoY], 
                 a.[2018], 
                 a.[2018_YoY], 
                 a.[2019], 
                 a.[2019_YoY], 
                 a.[2020], 
                 a.[2020_YoY], 
                 a.Tot_Chg_19, 
                 a.Tot_Chg_20, 
                 a.W_COGS, 
                 a.F_COGS_2019, 
                 a.F_COGS_2020, 
                 a.SALES_ANSP, 
                 a.F_MARGIN, 
                 a.W_MARGIN, 
                 a.GM_DELTA, 
                 LEFT(a.CATEGORY, 1) AS CTG_ID, 
                 a.CATEGORY, 
                 a.[INC_BKT_2019], 
                 [INC_BKT_2020_Inf], 
                 [INC_BKT_2020_Push], 
                 a.[2019] AS [2019_1], 
                 a.[2020_INF], 
                 a.[2020_Push], 
                 a.FLAGS, 
                 a.Chg_Notes, 
                 a.Changed
          INTO 
               #mytemp
          --F_Cost_HST_Tot_Units
          --FROM cte_calc_margin
          --FROM cte_margin_delta
                 FROM CTE_margin_Buckets a
                 ORDER BY 
                          Tot_Chg_20 ASC;
     SELECT      
            *
     INTO 
          F_Cost_2020_Tot_Units_CY2019_ANSP
            FROM #mytemp;

     --Cleanup
     DROP TABLE #mytemp;
     DECLARE @now DATETIME;
     SET @now = GETDATE();
     WITH CTE_Get_Items
          AS (SELECT DISTINCT      
                     a.itemnumber, 
                     a.CATEGORY, 
                     a.INC_BKT_2019
                     FROM dbo.F_Cost_2020_Tot_Units_CY2019_ANSP a),
          CTE_Merge_Costs
          AS (SELECT      
                     a.itemnumber, 
                     a.CATEGORY, 
                     a.INC_BKT_2019, 
                     CAST(Replace(replace(c.[2019], '"', ''), ',', '') AS DECIMAL(28, 5)) AS [2019],
                     CASE
                                                                                             WHEN NULLIF(b.Item_Number, '') IS NULL THEN CAST(Replace(replace(c.[2019], '"', ''), ',', '') AS DECIMAL(28, 5))
                                                                                             WHEN NULLIF(b.FC_2020, '') IS NULL THEN CAST(Replace(replace(c.[2019], '"', ''), ',', '') AS DECIMAL(28, 5))
                         ELSE CAST(b.FC_2020 AS DECIMAL(28, 5))
                     END AS [2020_Inf], 
                     b.Flag
                     FROM CTE_Get_Items a
                          LEFT JOIN dbo.Fcost_2020 b
                          ON a.itemnumber = b.Item_Number
                          LEFT JOIN dbo.FCOST_HST c
                          ON a.itemnumber = c.Item_Number)
          SELECT      
                 *
          INTO 
               #mytemp1
                 FROM CTE_Merge_Costs;
     INSERT INTO dbo.fcost_mst
            SELECT       
                   a.itemnumber, 
                   a.[2019], 
                   a.[2020_Inf], 
                   0, 
                   a.CATEGORY, 
                   a.INC_BKT_2019, 
                   a.Flag, 
                   '', 
                   @now
                   FROM  #mytemp1 a
                   WHERE a.itemnumber NOT IN(SELECT      
                                                    a.item_number
                                                    FROM dbo.FCOST_MST a);
     DROP TABLE #mytemp1;
GO

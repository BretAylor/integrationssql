SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*************************************************************                        
** File: usp_SelectAndDecryptExistingCCards.sql                        
** Author:Saad Benhalima
** Description: 
** Select and decrypt credit cards persisted on table
** Date: 8/9/2019                        
                        
** PARAMETERS:                        
NONE
	
** RETURN VALUE:                        
All rows in existing credit card table with decrypted card numbers


**************************************************************/
Create PROCEDURE [dbo].[usp_SelectAndDecryptExistingCCards] 
AS
	BEGIN
		--SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
		--SET NOCOUNT ON;         
		--BEGIN TRY             
		--	BEGIN TRANSACTION;

				OPEN SYMMETRIC KEY CC_Key_Symmetric  
				DECRYPTION BY CERTIFICATE CCCertificate; 

				SELECT [Id]
					  ,[Guid]
					  ,[CCDescription]
					  ,[CCType]
					  ,CONVERT(varchar, DecryptByKey([CCNumber], 1, HashBytes('SHA2_512', CONVERT(varbinary, [Guid])))) AS 'CCNumber'
					  ,[CVVNumber]
					  ,[LastFourDigits]
					  ,[ExpirationDate]
					  ,[CustomerNumber]
					  ,CustomerName
					  ,[ZipCode]
					  ,[Note]
					  ,[KeepOnFile]
					  ,[CreatedDate]
					  ,[CreatedBy]
					  ,[ModifiedDate]
					  ,[ModifiedBy]
				 FROM PEPPERI_STAGING.dbo.ExistingCustomerCC
				 

		--	COMMIT TRANSACTION;
		--END TRY
		--BEGIN CATCH
		--	ROLLBACK TRANSACTION;
		--	RETURN(1);
		--END CATCH;
	END;
GO

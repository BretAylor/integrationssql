SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[update_Cus_Imp] AS 

IF OBJECT_ID('tempdb..#pivottemp') IS NOT NULL DROP TABLE #pivottemp;
IF OBJECT_ID('tempdb..#pivottemp2') IS NOT NULL DROP TABLE #pivottemp2;
IF OBJECT_ID('demand_planning..Cus_Impact_Pivot') IS NOT NULL DROP TABLE dbo.Cus_Impact_Pivot;
IF OBJECT_ID('tempdb..#costgroups') IS NOT NULL DROP TABLE #costgroups;

DECLARE @sales DECIMAL(28, 2);

SELECT                        
       @sales = (SELECT       
                        SUM(a.quantityshipped * b.[2019]) AS [Sales1]
                        FROM   dbo.FCOST_Rev_Sls_Hst a
                               LEFT JOIN dbo.FCOST_MST b
                               ON a.ItemNum = b.Item_Number
                               LEFT JOIN dbo.F_Cost_2020_Tot_Units_CY2019_ANSP C
                               ON a.ItemNum = c.itemnumber
                        WHERE a.Exl_Category IN('Valid - Component', 'Valid - Kit/Assortment')
                               AND a.quantityshipped <> 0);

PRINT @sales;

WITH Cte_Get_RAW
     AS (SELECT       
                a.subClass_Description, 
                ((SUM(a.QuantityShipped * b.[2019]) / @sales) * 100) AS [%_Tot_Sls], 
                SUM(a.QuantityShipped * b.[2019]) AS [2019],
				(SUM(a.QuantityShipped * b.[2020_INF])-SUM(a.QuantityShipped * b.[2019])) as [2020_Inf],
				(SUM(a.QuantityShipped * b.[2020_Push])-SUM(a.QuantityShipped * b.[2020_INF])) as [2020_Push],-- ((CAST(ROUND(SUM(a.QuantityShipped * b.[2020_INF]), 2) AS DECIMAL(28, 2))-CAST(ROUND(SUM(a.QuantityShipped * b.[2019]), 2) AS DECIMAL(28, 2))))) as [2020_Push],
			
				--Case
				--	When sum(a.quantityshipped * b.[2019]) = 0 THEN 0
				--	Else CAST(ROUND((SUM(a.QuantityShipped * b.[2020_Push])-SUM(a.QuantityShipped * b.[2020_INF]))--/sum(a.quantityshipped * b.[2019]), 2) AS DECIMAL(28, 2))-- ((CAST(ROUND(SUM(a.QuantityShipped * b.[2020_INF]), 2) AS DECIMAL(28, 2))-CAST(ROUND(SUM(a.QuantityShipped * b.[2019]), 2) AS DECIMAL(28, 2))))) as [2020_Push],
				--End as [2020_Push],
				LEFT(b.Category, 1) AS Category
                FROM  dbo.FCOST_Rev_Sls_Hst a
                      LEFT JOIN dbo.FCOST_MST b
                      ON a.ItemNum = b.Item_Number
                WHERE a.Exl_Category IN('Valid - Component', 'Valid - Kit/Assortment')
                GROUP BY 
                         a.subClass_Description, 
                         LEFT(b.Category, 1)),
     Cte_add_costs
     AS (SELECT      
                *
                FROM cte_get_raw)
     SELECT 
            *
     INTO 
          #pivottemp
            FROM Cte_Get_RAW a
            ORDER BY 
                     a.[%_Tot_Sls] DESC;
/*Get Total Push*/					 
WITH Cte_Get_RAW2
     AS (SELECT       
                a.subClass_Description,
                (SUM(a.QuantityShipped * b.[2019])) AS [2019],
				(SUM(a.QuantityShipped * b.[2020_Push])-SUM(a.QuantityShipped * b.[2020_INF])) as [2020_Push_Tot]-- ((CAST(ROUND(SUM(a.QuantityShipped * b.[2020_INF]), 2) AS DECIMAL(28, 2))-CAST(ROUND(SUM(a.QuantityShipped * b.[2019]), 2) AS DECIMAL(28, 2))))) as [2020_Push],
				--Case
				--	When sum(a.quantityshipped * b.[2019]) = 0 THEN 0
				--	Else CAST(ROUND((SUM(a.QuantityShipped * b.[2020_Push])-SUM(a.QuantityShipped * b.[2020_INF]))--/sum(a.quantityshipped * b.[2019]), 2) AS DECIMAL(28, 2))-- ((CAST(ROUND(SUM(a.QuantityShipped * b.[2020_INF]), 2) AS DECIMAL(28, 2))-CAST(ROUND(SUM(a.QuantityShipped * b.[2019]), 2) AS DECIMAL(28, 2))))) as [2020_Push],
				--End as [2020_Push],
				--LEFT(b.Category, 1) AS Category
                FROM  dbo.FCOST_Rev_Sls_Hst a
                      LEFT JOIN dbo.FCOST_MST b
                      ON a.ItemNum = b.Item_Number
                WHERE a.Exl_Category IN('Valid - Component', 'Valid - Kit/Assortment')
                GROUP BY 
                         a.subClass_Description
                         )
			SELECT 
						*
				 INTO 
					  #pivottemp2
						FROM Cte_Get_RAW2 a

					 --SELECT * FROM #pivottemp2 WHERE subClass_Description = '321002'
					 --SELECT * FROM #pivottemp WHERE subClass_Description = '321002'

SELECT DISTINCT      
       a.CTG_ID AS Category
INTO 
     #costgroups
       FROM dbo.F_Cost_2020_Tot_Units_CY2019_ANSP a
       ORDER BY 
                a.CTG_ID ASC;

DECLARE @columns NVARCHAR(MAX) = '', 
        @sql     NVARCHAR(MAX) = '';

SELECT      
       @columns+=QUOTENAME(a.Category) + ','
       FROM #costgroups a
       ORDER BY a.Category ASC;

--PRINT @columns;

SET @columns = LEFT(@columns, LEN(@columns) - 1);

PRINT @columns;

DROP TABLE #costgroups;
--sum(b.[%_Tot_Sls]) as [%_Tot_Sls]
SET @sql = '

With CTE_Union_Pivots AS (
	 
	 SELECT 
		p1.subClass_Description, 
		Cast(Round(sum(b.[%_Tot_Sls]),2) as decimal(28,2)) as [%_Tot_Sls], 
		Cast(Round(Sum(b.[2019]),2) as decimal(28,2)) as [2019], 
		Cast(Round(((sum(b.[2020_INF])/Sum(b.[2019]))*100),2) as decimal(28,2)) as [2020_INF], 
		Cast(Round(((sum(b.[2020_Push])/Sum(b.[2019]))*100),2) as decimal(28,2)) as [2020_Push],
		Cast(Round(((sum(b.[2020_INF])/Sum(b.[2019]))*100),2) as decimal(28,2)) + 
		Cast(Round(((sum(b.[2020_Push])/Sum(b.[2019]))*100),2) as decimal(28,2)) as [2020_Total],
		--sum(b.[2020_Push]) as [2020_Push],
		' + @columns + ',
		Round(sum(b.[2020_Push])+sum(b.[2020_INF]),2) as [2020_Tot_$]
	 FROM (
		SELECT a.subClass_Description, a.category, 
		CASE
			WHEN sum(b.[2019]) = 0 THEN 0
			Else Cast(Round(((sum(a.[2020_Push])/Sum(b.[2019]))*100),4) as decimal(28,4)) 
			END as [2020_Push1]
		FROM #pivottemp a
		left join #pivottemp2 b on
		a.subClass_Description = b.subClass_Description
		Group by a.subClass_Description, a.category, b.[2019]--a.[2020_Push]
		HAVING sum(b.[2019]) <> 0
		) t
		PIVOT (
            Sum([2020_Push1])
			FOR category IN (' + @columns + ')) as p1
		Left join #pivottemp b
		on p1.subClass_Description = b.subClass_Description
		GROUP By p1.subClass_Description,' + @columns + '
		--HAVING sum(b.[2019]) <> 0
		)
		Select *
		into demand_planning.dbo.Cus_Impact_Pivot
		from cte_union_pivots 
		
		;';

--PRINT @sql;

EXEC sp_executesql 
     @sql;



GO

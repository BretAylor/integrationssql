SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		baylor
-- Create date: 5/2020
-- Description:	Update to Pepperi_Staging 
-- =============================================
CREATE PROCEDURE [stageAP].[xxProductUpdate] 
AS

BEGIN

SET NOCOUNT ON;


UPDATE t1
   SET [Guid] = t2.Guid
      ,[WinzerProductNumber] = t2.WinzerProductNumber
      ,[BrandedProductNumber] = t2.BrandedProductNumber
      ,[ItemClass] = t2.ItemClass
      ,[Description] = t2.Description
      ,[BaseUnit] = t2.BaseUnit
      ,[TaxFlag] = t2.TaxFlag
      ,[PackageQuantity] = t2.PackageQuantity
      ,[EnforcePackageQuantity] = t2.EnforcePackageQuantity
      ,[FranchiseCost] = t2.FranchiseCost
      ,[RecommendedPrice] = t2.RecommendedPrice
      ,[PriceClass] = t2.PriceClass
      ,[MsdsRequired] = t2.MsdsRequired
      ,[Bom] = t2.Bom
      ,[Pa1bFlag] = t2.Pa1bFlag
      ,[ReadId] = t2.ReadId
      ,[ProposedFranchiseCost] = t2.ProposedFranchiseCost
      ,[FreightType] = t2.FreightType
      ,[ItemSequenceNumber] = t2.ItemSequenceNumber
      ,[PrivateLabel] = t2.PrivateLabel
      ,[SupplierId] = t2.SupplierId
      ,[SupplierName] = t2.SupplierName
      ,[SupplierItemId] = t2.SupplierItemId
      ,[CountryOfOrigin] = t2.CountryOfOrigin
      ,[CountryOfOriginName] = t2.CountryOfOriginName
      ,[CountryOfOriginGsaMessage] = t2.CountryOfOriginGsaMessage
      ,[CountryOfOriginGsaRestricted] = t2.CountryOfOriginGsaRestricted
      ,[BigAssortment] = t2.BigAssortment
      ,[IsBomComponent] = t2.IsBomComponent
      ,[SoldOnlyAsComponent] = t2.SoldOnlyAsComponent
      ,[RestrictionCode] = t2.RestrictionCode
      ,[HazmatMaterialFlag] = t2.HazmatMaterialFlag
      ,[HazmatMaterialCode] = t2.HazmatMaterialCode
      ,[ObsoleteFlag] = t2.ObsoleteFlag
      ,[ObsoleteCode] = t2.ObsoleteCode
      ,[DirectShip] = t2.DirectShip
      ,[ShipSource] = t2.ShipSource
      ,[ShipSupplier] = t2.ShipSupplier
      ,[LandedCost] = t2.LandedCost
      ,[CanRepChangeItemDescription] = t2.CanRepChangeItemDescription
      ,[CategoryId] = t2.CategoryId
      ,[ModifiedDate] = Getdate()
      ,[ModifiedBy] = suser_name()
      ,[Deleted] = 0
      ,[ItemShortDescription] = t2.ItemShortDescription
From  [WUSA-SQL01-DFW].Pepperi_Staging.[dbo].[Product]  t1
join dbo.Product t2
on t1.BrandedProductNumber = t2.BrandedProductNumber  
 WHERE  isnull(t1.[WinzerProductNumber],'') <> isnull(t2.WinzerProductNumber,'')
      or isnull(t1.[ItemClass],'') <> isnull(t2.ItemClass,'')
      or isnull(t1.[Description],'') <> isnull(t2.[Description],'')
      or isnull(t1.[BaseUnit],'') <> isnull(t2.BaseUnit,'')
      or isnull(t1.[TaxFlag],'') <> isnull(t2.TaxFlag,'')
      or isnull(t1.[PackageQuantity],0) <> isnull(t2.PackageQuantity,0)
      or isnull(t1.[EnforcePackageQuantity],'') <> isnull(t2.EnforcePackageQuantity,'')
      or isnull(t1.[FranchiseCost],0) <> isnull(t2.FranchiseCost,0)
      or isnull(t1.[RecommendedPrice],0) <> isnull(t2.RecommendedPrice,0)
      or isnull(t1.[PriceClass] ,'') <> isnull(t2.PriceClass,'')
      or isnull(t1.[MsdsRequired],'') <> isnull(t2.MsdsRequired,'')
      or isnull(t1.[Bom],'') <> isnull(t2.Bom,'')
      or isnull(t1.[Pa1bFlag],'') <> isnull(t2.Pa1bFlag,'')
      or isnull(t1.[ReadId],'') <> isnull(t2.ReadId,'')
      or isnull(t1.[ProposedFranchiseCost],0) <> isnull(t2.ProposedFranchiseCost,0)
      or isnull(t1.[FreightType],'') <> isnull(t2.FreightType,'')
      or isnull(t1.[ItemSequenceNumber],'') <> isnull(t2.ItemSequenceNumber,'')
      or isnull(t1.[PrivateLabel],'') <> isnull(t2.PrivateLabel,'')
      or isnull(t1.[SupplierId],0) <> isnull(t2.SupplierId,0)
      or isnull(t1.[SupplierName],'') <> isnull(t2.SupplierName,'')
      or isnull(t1.[SupplierItemId],0) <> isnull(t2.SupplierItemId,0)
      or isnull(t1.[CountryOfOrigin],'') <> isnull(t2.CountryOfOrigin,'')
      or isnull(t1.[CountryOfOriginName],'') <> isnull(t2.CountryOfOriginName,'')
      or isnull(t1.[CountryOfOriginGsaMessage],'') <> isnull(t2.CountryOfOriginGsaMessage,'')
      or isnull(t1.[CountryOfOriginGsaRestricted],'') <> isnull(t2.CountryOfOriginGsaRestricted,'')
      or isnull(t1.[BigAssortment],'') <> isnull(t2.BigAssortment,'')
      or isnull(t1.[IsBomComponent],'') <> isnull(t2.IsBomComponent,'')
      or isnull(t1.[SoldOnlyAsComponent],'') <> isnull(t2.SoldOnlyAsComponent,'')
      or isnull(t1.[RestrictionCode],'') <> isnull(t2.RestrictionCode,'')
      or isnull(t1.[HazmatMaterialFlag],'') <> isnull(t2.HazmatMaterialFlag,'')
      or isnull(t1.[HazmatMaterialCode],'') <> isnull(t2.HazmatMaterialCode,'')
      or isnull(t1.[ObsoleteFlag],'') <> isnull(t2.ObsoleteFlag,'')
      or isnull(t1.[ObsoleteCode],'') <> isnull(t2.ObsoleteCode,'')
      or isnull(t1.[DirectShip],'') <> isnull(t2.DirectShip,'')
      or isnull(t1.[ShipSource],'') <> isnull(t2.ShipSource,'')
      or isnull(t1.[ShipSupplier],'') <> isnull(t2.ShipSupplier,0)
      or isnull(t1.[LandedCost],0) <> isnull(t2.LandedCost,0)
      or isnull(t1.[CanRepChangeItemDescription],'') <> isnull(t2.CanRepChangeItemDescription,'')
      or isnull(t1.[CategoryId],0) <> isnull(t2.CategoryId,0)
      or isnull(t1.[Deleted],0)= 1  --Reactivate?
      or isnull(t1.[ItemShortDescription],'') <> isnull(t2.ItemShortDescription,'')


 INSERT [WUSA-SQL01-DFW].Pepperi_Staging.[dbo].[Product]
           ([Guid]
           ,[WinzerProductNumber]
           ,[BrandedProductNumber]
           ,[ItemClass]
           ,[Description]
           ,[BaseUnit]
           ,[TaxFlag]
           ,[PackageQuantity]
           ,[EnforcePackageQuantity]
           ,[FranchiseCost]
           ,[RecommendedPrice]
           ,[PriceClass]
           ,[MsdsRequired]
           ,[Bom]
           ,[Pa1bFlag]
           ,[ReadId]
           ,[ProposedFranchiseCost]
           ,[FreightType]
           ,[ItemSequenceNumber]
           ,[PrivateLabel]
           ,[SupplierId]
           ,[SupplierName]
           ,[SupplierItemId]
           ,[CountryOfOrigin]
           ,[CountryOfOriginName]
           ,[CountryOfOriginGsaMessage]
           ,[CountryOfOriginGsaRestricted]
           ,[BigAssortment]
           ,[IsBomComponent]
           ,[SoldOnlyAsComponent]
           ,[RestrictionCode]
           ,[HazmatMaterialFlag]
           ,[HazmatMaterialCode]
           ,[ObsoleteFlag]
           ,[ObsoleteCode]
           ,[DirectShip]
           ,[ShipSource]
           ,[ShipSupplier]
           ,[LandedCost]
           ,[CanRepChangeItemDescription]
           ,[CategoryId]
           ,[Deleted]
           ,[ItemShortDescription])
select    t1.[Guid]
           ,t1.[WinzerProductNumber]
           ,t1.[BrandedProductNumber]
           ,t1.[ItemClass]
           ,t1.[Description]
           ,t1.[BaseUnit]
           ,t1.[TaxFlag]
           ,t1.[PackageQuantity]
           ,t1.[EnforcePackageQuantity]
           ,t1.[FranchiseCost]
           ,t1.[RecommendedPrice]
           ,t1.[PriceClass]
           ,t1.[MsdsRequired]
           ,t1.[Bom]
           ,t1.[Pa1bFlag]
           ,t1.[ReadId]
           ,t1.[ProposedFranchiseCost]
           ,t1.[FreightType]
           ,t1.[ItemSequenceNumber]
           ,t1.[PrivateLabel]
           ,t1.[SupplierId]
           ,t1.[SupplierName]
           ,t1.[SupplierItemId]
           ,t1.[CountryOfOrigin]
           ,t1.[CountryOfOriginName]
           ,t1.[CountryOfOriginGsaMessage]
           ,t1.[CountryOfOriginGsaRestricted]
           ,t1.[BigAssortment]
           ,t1.[IsBomComponent]
           ,t1.[SoldOnlyAsComponent]
           ,t1.[RestrictionCode]
           ,t1.[HazmatMaterialFlag]
           ,t1.[HazmatMaterialCode]
           ,t1.[ObsoleteFlag]
           ,t1.[ObsoleteCode]
           ,t1.[DirectShip]
           ,t1.[ShipSource]
           ,t1.[ShipSupplier]
           ,t1.[LandedCost]
           ,t1.[CanRepChangeItemDescription]
           ,t1.[CategoryId]
           ,0 [Deleted]
           ,t1.[ItemShortDescription]
from dbo.[Product] t1
Left join [WUSA-SQL01-DFW].Pepperi_Staging.[dbo].[Product] t2
on t1.BrandedProductNumber = t2.BrandedProductNumber 
where t2.BrandedProductNumber is null

-- Delete 
Update t1
Set Deleted = 1,
ModifiedDate = getdate(),
ModifiedBy = SUSER_SNAME()
From [WUSA-SQL01-DFW].Pepperi_Staging.dbo.Product t1 
left join dbo.Product t2
on t1.BrandedProductNumber = t2.BrandedProductNumber 
where t1.deleted = 0
and t2.BrandedProductNumber is null

END
GO

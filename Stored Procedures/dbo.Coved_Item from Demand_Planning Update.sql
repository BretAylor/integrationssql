SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[Coved_Item from Demand_Planning Update]
As
Begin

update i
set ETA = case when len(a.ETA) = 0 or a.ETA is null then 'NO ETA' else a.ETA end,
Itm_Prio = a.Itm_Prio,
ModifiedDate = GetDate(),
ModifiedBy = current_user
FROM WINSQLBI01.Demand_Planning.dbo.COVID_Items a
join covid_items i
on a.item = i.item
WHERE   a.publish = 1
and (isnull(a.itm_prio,'xxx') <> isnull(i.itm_prio,'yyy')
or isnull(i.eta,'xxx') <> case when len(a.ETA) = 0 or a.ETA is null then 'NO ETA' else a.ETA end)

insert COVID_Items
(Item, ETA, Itm_Prio)
SELECT
    a.Item
  , case when len(a.ETA) = 0 or a.ETA is null then 'NO ETA' else a.ETA end AS ETA
  , a.Itm_Prio
FROM    WINSQLBI01.Demand_Planning.dbo.COVID_Items a
left join covid_items i
on a.item = i.item
WHERE   a.publish = 1
and i.item is null

Delete COVID_Items
FROM   covid_items i 
left join WINSQLBI01.Demand_Planning.dbo.COVID_Items a
on i.item = a.item
WHERE a.item is null
or a.publish = 0

End
GO

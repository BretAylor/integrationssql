SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FactViewInvPeriodUsageP21Load]
AS
BEGIN

-- EXEC [dbo].[FactViewInvPeriodUsageP21]
TRUNCATE TABLE BI_DWH.[dbo].[FactViewInvPeriodUsageP21]

INSERT INTO  BI_DWH.[dbo].[FactViewInvPeriodUsageP21] ([ID]
      ,[ProductKey]
      ,[item_id]
      ,[location_id]
      ,[inv_period_usage]
      ,[date_created]
      ,[date_last_modified]
      ,[last_maintained_by]
      ,[scheduled_usage]
      ,[forecast_usage]
      ,[forecast_deviation_percentage]
      ,[mad_percentage]
      ,[filtered_usage]
      ,[number_of_orders]
      ,[edited]
      ,[number_of_hits]
      ,[demand_period_uid]
      ,[inv_mast_uid]
      ,[usage_copied]
      ,[inv_period_usage_uid]
      ,[mean_absolute_percent_error]
      ,[exceptional_sales_flag]
      ,[exceptional_deviation_flag]
      ,[reviewed_flag]
      ,[last_reviewed_date]
      ,[usage_notes]
      ,[forecast_adjustment_percent]
      ,[saved_filtered_usage]
      ,[DateAdded])

SELECT [ID]
      ,PR.[ProductKey]
      ,[item_id]
      ,[location_id]
      ,[inv_period_usage]
      ,[date_created]
      ,[date_last_modified]
      ,[last_maintained_by]
      ,[scheduled_usage]
      ,[forecast_usage]
      ,[forecast_deviation_percentage]
      ,[mad_percentage]
      ,[filtered_usage]
      ,[number_of_orders]
      ,[edited]
      ,[number_of_hits]
      ,[demand_period_uid]
      ,[inv_mast_uid]
      ,[usage_copied]
      ,[inv_period_usage_uid]
      ,[mean_absolute_percent_error]
      ,[exceptional_sales_flag]
      ,[exceptional_deviation_flag]
      ,[reviewed_flag]
      ,[last_reviewed_date]
      ,[usage_notes]
      ,[forecast_adjustment_percent]
      ,[saved_filtered_usage]
      ,GETDATE() AS [DateAdded] 

FROM BI_STAGING.dbo.ViewInvPeriodUsageP21 PU LEFT OUTER JOIN BI_DWH.dbo.DimProductP21 PR ON PU.inv_mast_uid = PR.InvMastUid


END 
GO

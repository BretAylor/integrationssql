SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      2/6/2019
-- Description  Net sales by franchise, day, customer, produ4ct report

-- Copyright © 2019, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History

-------------------------------------------------------------------------------
-- exec FranchiseNetSalesReport

CREATE PROCEDURE [dbo].[FranchiseNetSalesReport]
AS
BEGIN

SELECT 
      DD.[Year],
	  DD.FiscalYear,
      DD.FiscalMonth,
	  DD.MonthName,
	  DD.[Date],
	  CU.CustomerName,
	  CU.CustomerNum,
	  PR.ProductDescription,
	  PR.ProductNum,
	  PR.ProductNum +' - ' + PR.ProductDescription AS ProductNumDesc,
	  EM.SalesRepresentativeName,
	  EM.SalesRepresentativeNum,
	  INF.OrderKey AS OrderNum,
	  INF.OrderSequenceNum,
	  INF.QuantityShipped,
	  INF.ActualSellPrice,
	  INF.ListPrice,
      INF.NetSales ,
	  INF.FranchiseCost,
	  INF.WinzerCost,
	  INF.DateAdded
FROM BI_DWH.[dbo].[FactInvoiceFranchise] INF
    INNER JOIN BI_DWH.dbo.DimDate DD
        ON INF.DateKey = DD.DateKey
    LEFT OUTER JOIN BI_DWH.dbo.DimCustomer CU
        ON CU.CustomerKey = INF.CustomerKey
    LEFT OUTER JOIN BI_DWH.dbo.DimProduct PR
        ON PR.ProductKey = INF.ProductKey
    LEFT OUTER JOIN BI_DWH.dbo.DimEmployee EM
        ON EM.EmployeeKey = INF.EmployeeKey
WHERE DD.FiscalYear >= 2018
ORDER BY DD.[Date] ASC;

End
GO

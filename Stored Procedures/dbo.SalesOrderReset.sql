SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		baylor
-- Create date: 5/2020
-- Description:	Reset Sales Orders
-- =============================================
CREATE PROCEDURE [dbo].[SalesOrderReset]  @PepperiOrderNbr bigint
AS

BEGIN

SET NOCOUNT ON;

/****** Script for SelectTopNRows command from SSMS  ******/
UPDATE [dbo].[SalesOrder]
SET erpsyncdate = Null,
ErpSyncBy = NULL,
ErpModifiedDate = NULL,
ErpModifiedBy = null
WHERE pepperiordernbr = @PepperiOrderNbr --96524868

UPDATE dbo.SalesOrderLine
SET ErpSyncDate = NULL,
ErpSyncBy = NULL
WHERE PepperiId = @PepperiOrderNbr --96524868

END
GO

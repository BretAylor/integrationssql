SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      2/6/2019
-- Description  Net sales by Salesrep, day, customer, product Speedy Clean report

-- Copyright © 2019, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History

-------------------------------------------------------------------------------

-- EXEC [dbo].[SpeedyCleanNetSalesReport]

CREATE PROCEDURE [dbo].[SpeedyCleanNetSalesReport]
AS
BEGIN



SELECT DD.[Year],
	   DD.FiscalYear,
       DD.FiscalMonth,
	   DD.[MonthName],
	   DD.[Date],
	   CU.CustomerName,
	   CU.CustomerId AS CustomerNum,
	   PR.ItemDesc AS ProductDescription,
	   PR.ItemId AS ProductNum,
	   ISNULL(EM.FirstName,'') + ' '+ ISNULL(EM.Middle,'')+ ' ' + ISNULL(EM.LastName,'') AS SalesRepresentativeName,
	   EM.SalesRepID AS SalesRepresentativeNum,
	   SC.InvoiceNo AS OrderNum,
	   SC.[LineNo],
	   SC.QtyShipped AS QuantityShipped,
	   SC.UnitPrice,
	   SC.WinzerCost,
       SC.NetSales ,
	   SC.DateAdded
FROM BI_DWH.[dbo].[FactSpeedyCleanNetSales] SC
    INNER JOIN BI_DWH.dbo.DimDate DD
        ON SC.DateKey = DD.DateKey
    LEFT OUTER JOIN BI_DWH.dbo.DimCustomerP21 CU
        ON CU.CustomerKey = SC.CustomerKey
    LEFT OUTER JOIN BI_DWH.dbo.DimProductP21 PR
        ON PR.ProductKey = SC.ProductKey
    LEFT OUTER JOIN BI_DWH.dbo.DimEmployeeP21 EM
        ON EM.EmployeeKey = SC.EmployeeKey
WHERE DD.FiscalYear >= 2018
ORDER BY DD.[Date] ASC


END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	CREATE PROCEDURE [dbo].[ARAgingReport]
	AS
	Begin
	
	-- Assigning customer & Sales Rep


	-- Invoice Amounts
	SELECT CAST([InvoiceNum] AS NVARCHAR(30)) AS InvoiceNum ,
	   DD.FiscalYear,
	   DD.FiscalMonth,
	   DD.MonthName,
	   DD.Date AS InvoiceDate ,
	   AD.Date AS [AgeDay],
	   DATEDIFF(DAY, AD.Date  , GETDATE()) AS [Age],
       ISNULL(CASE WHEN DATEDIFF(DAY, AD.Date, GETDATE()) <= 30 THEN [OpenBalanceInvoice]END,0) AS 'Current',
	   ISNULL(CASE WHEN DATEDIFF(DAY, AD.Date, GETDATE()) >30 AND DATEDIFF(DAY, DD.Date, GETDATE()) <= 60    THEN [OpenBalanceInvoice] END,0) AS  'Over 30',
	   ISNULL(CASE WHEN DATEDIFF(DAY, AD.Date, GETDATE()) >60  AND DATEDIFF(DAY, DD.Date, GETDATE()) <= 90 THEN [OpenBalanceInvoice] end,0) AS 'Over 60',
	   ISNULL(CASE WHEN DATEDIFF(DAY, AD.Date, GETDATE()) >90 AND DATEDIFF(DAY, DD.Date, GETDATE()) <= 120   THEN [OpenBalanceInvoice] end,0) AS 'Over 90',
	   ISNULL(CASE WHEN DATEDIFF(DAY, AD.Date, GETDATE()) >120  THEN [OpenBalanceInvoice] END,0) AS 'Over 120',
	   [OpenBalanceInvoice] AS NetSales ,
	   AR.[CustomerNum],
	   CU.CustomerName,
	   NULL AS PrimarySalesmanNum,
	   NULL AS SalesRepresentativeName
  FROM [BI_STAGING].[dbo].[ARAging] AR  
   INNER JOIN BI_DWH.dbo.DimDate DD
        ON CAST(AR.InvoiceDateCentury AS CHAR(2)) + CAST(AR.InvoiceDate AS CHAR(6)) = DD.DateKey
  LEFT outer JOIN BI_DWH.dbo.DimCustomer CU
        ON CU.CustomerNum = AR.CustomerNum
  LEFT OUTER JOIN BI_DWH.dbo.DimDate AD
        ON CAST(AR.[AgingDateCentury] AS  CHAR(2)) + CAST(AR.[AgingDate] AS CHAR(6)) = AD.DateKey
WHERE DD.Date >= '8/1/2018' 	   
end
	  
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		baylor
-- Create date: 6/2020
-- Description:	Update to Pepperi_Staging 
-- =============================================
CREATE PROCEDURE [StageAP].[SaleRepSalesSummaryUpdate] 
AS

BEGIN

SET NOCOUNT ON;

DECLARE @RunDate DATETIME = GETDATE()

-- Dedup
BEGIN TRY DROP TABLE #TempRepSales END TRY BEGIN CATCH END CATCH
;WITH cte_Dup (Linenbr, PadDescription, ProcessDate, SequenceNum, TextAR) as
(
SELECT ROW_NUMBER() OVER (
    PARTITION BY [PadDescription],[ProcessDate],[SequenceNum]
    ORDER BY TextAR) Linenbr
           ,[PadDescription]
           ,[ProcessDate]
           ,[SequenceNum]
		   ,TextAR
FROM StageAP.SalesrepSalesSummary 
)
SELECT [PadDescription]
           ,[ProcessDate]
           ,[SequenceNum]
		   ,TextAR
INTO #TempRepSales
FROM cte_Dup
WHERE linenbr = 1

UPDATE t1
   SET [TextAr] = t2.TextAr 
      ,[ModifiedDate] = @RunDate
      ,[ModifiedBy] = SUSER_NAME()
From  [dbo].[SalesrepSalesSummary]  t1
join #TempRepSales t2
on t1.PadDescription = t2.padDescription
AND t1.ProcessDate = t2.processdate
AND t1.SequenceNum = t2.sequenceNum
 WHERE  isnull(t1.[TextAR],'') <> isnull(t2.TextAR,'')


 INSERT INTO [dbo].[SalesrepSalesSummary]
           ([PadDescription]
           ,[ProcessDate]
           ,[SequenceNum]
           ,[TextAr]
		   ,CreatedDate
		   ,ModifiedDate)
SELECT t1.[PadDescription]
           ,t1.[ProcessDate]
           ,t1.[SequenceNum]
           ,t1.[TextAr]
		   ,@RunDate
		   ,@RunDate
from #TempRepSales t1
Left join [dbo].[SalesRepSalesSummary] t2
on t1.PadDescription = t2.padDescription
AND t1.ProcessDate = t2.processdate
AND t1.SequenceNum = t2.sequenceNum
where t2.SequenceNum is null

-- Delete 
DELETE t1
from [dbo].[SalesRepSalesSummary] t1
Left join #TempRepSales t2
on t1.PadDescription = t2.padDescription
AND t1.ProcessDate = t2.processdate
AND t1.SequenceNum = t2.sequenceNum
where t2.SequenceNum is null

END
GO

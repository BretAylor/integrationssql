SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ExtractSalesOrderLine] 

AS
BEGIN

SET NOCOUNT ON;

SELECT [RepNbrPad]
      ,[CustomerNbr]
      ,[PepperiOrderNbr]
      ,[LineNbr]
      ,[BrandedProductNbr]
      ,ISNULL([PepperiId],[PepperiOrderNbr]) PepperiID
      ,[WinzerProductNbr]
      ,[OrderQuantity]
      ,[OrderPrice]
      ,[PriceOverrideFlag]
      ,[OverrideMinimumQuantity]
      ,[OrderCost]
      ,CAST([dbo].[REPLACE_UNPRINT_CHARS]([DisplayComment]) AS varchar(62)) DisplayComment
      ,[DetailCommentCode]
      ,[UnitOfMeasure]
      ,[NoChargeReasonCode]
      ,[LabelQty]
      ,[Pa1bFlag]
      ,[NbrOfBags]
      ,[FdpReference]
      ,[FranchiseCostReasonCd]
      ,[CVPCode]
      ,[SpecialFrCostId]
      ,[CVPCodeMinQty]
      ,[CVPCodeMaxSellPrice]
      ,[CustomerContract]
      ,[BasePrice]
      ,[CommissionCode]
      ,[CommissionPercent]
      ,[MarkupPercent]
      ,[MarkupAmount]
      ,[FreightUpChargeCode]
      ,[FreightUpChargePercent]
      ,[FreightUpChargeAmount]
      ,[OriginalFranchisePrice]
      ,[LandedCost]
      ,[Comment]
      ,[ItemDescription]
      ,[LineItemType]
      ,[USERFA]
  FROM [StagePA].[SalesOrderLineFormatted]

END
GO

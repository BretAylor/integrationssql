SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--EXEC [dbo].[SP_Explode_BOMs_Staging_V3] 


CREATE PROCEDURE [dbo].[SP_Explode_BOMs_All] AS 
--Cleanup
IF OBJECT_ID('tempdb..#mytemp') IS NOT NULL DROP TABLE #mytemp;
IF Object_Id('Demand_Planning..BOM_Explode_All') IS NULL Create Table dbo.BOM_Explode_All(BOM varchar(255) Index ix_BOM_Explode_All, Comp_Itm Varchar(255), Tot_Qty Decimal(10,2), Par_List varchar(max), DateAdded DateTime);
ELSE TRUNCATE TABLE dbo.BOM_Explode_All;

WITH cte_GET_MFG_ITM AS 
(
	--Select itemnumber from bi_staging.dbo.itemmaster where billofmaterialtype in('K','M')
	SELECT DISTINCT parentitemnumber FROM bi_staging.dbo.materialcomponent
)

SELECT * INTO #mytemp FROM cte_GET_MFG_ITM --where imbmtp in('K','M');
CREATE CLUSTERED INDEX ix_temp_itno ON #mytemp (parentitemnumber ASC)

DECLARE @ITMID varchar(255)
--DECLARE @Date Date
--Set @Date = GetDate()

WHILE EXISTS (SELECT * FROM #mytemp)
	BEGIN
		SELECT @ITMID = (SELECT TOP 1 parentitemnumber FROM #mytemp) --order by parentitemnumber asc)
		
			;WITH EXPL AS 
			(
				SELECT par.parentitemnumber, par.componentitemnumber, Cast(par.quantityperparent AS decimal(38,2)) AS Qty, par.UnitofMeasure AS UOM
				  FROM [bi_staging].[dbo].[materialcomponent] par
				 WHERE par.ParentItemNumber = @ITMID AND par.companynumber = 0
			
				 UNION ALL

			    SELECT child.parentitemnumber, child.componentitemnumber, Cast(PARENT.Qty * Cast(child.quantityperparent AS decimal(38,2)) AS decimal(38,2)) AS Qty, child.UnitofMeasure AS UOM
			      FROM EXPL PARENT, [bi_staging].[dbo].[materialcomponent] child
			     WHERE PARENT.componentitemnumber = child.parentitemnumber
			)
			INSERT INTO dbo.bom_explode_all (BOM , Comp_Itm , Tot_Qty , Par_List )
			     SELECT @ITMID AS BOM, b.componentitemnumber AS Comp_Itm, Sum(b.Qty) AS Tot_Qty ,'' AS Par_List--(Select STUFF((Select ', ' + a.parentitemnumber from EXPL a where a.componentitemnumber = b.componentitemnumber FOR XML PATH('')), 1, 2, '')) as Par_List 
			       --INTO #mytemp3 --(BOM , Comp_Itm , Tot_Qty , Par_List )
				   FROM EXPL b 
			       LEFT JOIN expl c
			         ON b.componentitemnumber = c.parentitemnumber
			      --WHERE NullIf(c.parentitemnumber,'')IS NULL
			   GROUP BY b.componentitemnumber

		DELETE #mytemp
		WHERE parentitemnumber = @ITMID;
	END

	--Update dbo.BOM_ExplodeV3
	--Set DateAdded = @date

--Cleanup
DROP TABLE #mytemp
GO

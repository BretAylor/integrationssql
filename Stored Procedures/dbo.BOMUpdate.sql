SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BOMUpdate]
 as
 Begin
update BO SET 
  BO.ParentItemDescription = BOU.ParentItemDescription,
  BO.ComponentItemDescription = BOU.ComponentItemDescription,
  BO.QuantityPerParent = BOU.QuantityPerParent,
  BO.ReadId = BOU.ReadId,
  BO.IsModified = 1,
  BO.ModifiedDate = GETDATE(),
  BO.ModifiedBy = suser_name()
  
  FROM [PEPPERI].[dbo].[BOM] BO 
  INNER JOIN [PEPPERI].[dbo].[BOMUpdates] BOU 
  ON BO.[ParentItemNum] = BOU.[ParentItemNum] AND BO.ComponentItemNum = BOU.ComponentItemNum
  WHERE ISNULL(BO.ParentItemDescription,'') <> ISNULL(BOU.ParentItemDescription,'') OR 
        ISNULL(BO.ComponentItemDescription,'') <> ISNULL(BOU.ComponentItemDescription,'') OR
        ISNULL(BO.QuantityPerParent ,0) <> ISNULL(BOU.QuantityPerParent,0) OR 
		ISNULL(BO.ReadId ,'') <> ISNULL(BOU.ReadId ,'')
 end

GO

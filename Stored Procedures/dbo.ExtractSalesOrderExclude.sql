SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		baylor
-- Create date: 05/2020
-- Description:	Exclude incomplete Orders from current Run
-- =============================================
CREATE PROCEDURE [dbo].[ExtractSalesOrderExclude] 

AS
BEGIN

SET NOCOUNT ON;

set transaction isolation level read uncommitted

Truncate table [stagePA].[ExtractSalesOrderExclude]

DECLARE @RunDate DATETIME = (SELECT MAX(createdDate) FROM StagePA.SalesOrderToProcess)

--DELETE [stagePA].[ExtractSalesOrderExclude] WHERE DATEADD(mm,-3,@RunDate) > CreatedDate  

-- Not All SalesOrderLines are in yet 
;with cte (PepperiID, SumQTY, SumPrice) as
(select Pepperiid, 
SUM(case when OrderQuantity = 0 then 1 else OrderQuantity end) SumQTY,
Sum(OrderPrice * OrderQuantity) SumPrice --(case when OrderQuantity = 0 then 1 else OrderQuantity end)) SumPrice
from salesorderline  
where erpsyncdate is null --Not uploaded to AS400
AND (ParentItemNumber IS NULL OR ParentItemNumber = '')  -- exclude Kits
group by pepperiID)
insert [stagePA].[ExtractSalesOrderExclude]
SELECT Pepperiordernbr [Order], 'Order Totals do not match: Order:' 
+ cast(Pepperiordernbr as varchar) + ', Line Price: ' + cast(SumPrice as varchar) 
+ ', Order Total: ' + cast(OrderTotal as varchar) + ', Line QTY: ' +  cast(SumQTY as varchar) + ', Order QTY: '
+ cast([ItemCount] as varchar) + ', Informational: Notify Tina and Lior If This is not resolved next cycle ' OrderMessage
from cte t1
join SalesOrder t2
on t1.pepperiid = t2.PepperiOrderNbr
where t1.Pepperiid NOT IN (SELECT PepperiOrderNbr FROM [dbo].[SalesOrderInclusionList])  -- Short Term manual fix for label only orders 
and t2.erpsyncdate is null
and ABS(ROUND(SumPrice, 2, 0) - cast(OrderTotal as decimal(18,2))) >= 1 -- if one is true (Total or QTY), let it go through
and sumQTY < [ItemCount]  -- Not getting correct QTY nor OrderTotal from the Cloud

-- No temporary
insert [stagePA].[ExtractSalesOrderExclude]
SELECT t1.Pepperiordernbr [Order], 'Perm Customer# not available yet: ' 
+ ' ,Customer Nbr: ' +  Isnull(cast(t1.CustomerNbr as varchar),'Null') +
+ ', Informational: Notify Vicky If This is not resolved within 90 minutes (next 3 cycles) ' OrderMessage
from  SalesOrder t1
JOIN StagePA.SalesOrderToProcess t2
ON t1.PepperiOrderNbr = t2.PepperiOrderNbr
where t1.CustomerNbr > 1000000  -- Temp Number

-- CC No Match
insert [stagePA].[ExtractSalesOrderExclude]
SELECT t2.Pepperiordernbr [Order]
, 'Cannot Match CC GUID: '  + CAST(t2.PepperiCreditCardId AS VARCHAR(255)) + ', CustNbr: ' + CAST(t2.CustomerNbr AS VARCHAR(255)) + 
', Informational: Notify Lior and Tina'
FROM  StagePA.SalesOrderToProcess t1
JOIN SalesOrder t2
ON t1.PepperiOrderNbr = t2.PepperiOrderNbr
LEFT JOIN cc_Staging.[dbo].[CustomerCC] t3   
ON t2.PepperiCreditCardId  = CAST(t3.GUID AS VARCHAR(255))
LEFT JOIN ExistingCustomerCC t4
ON t2.PepperiCreditCardId  = CAST(t4.GUID AS VARCHAR(255))
where t2.PepperiCreditCardId IS NOT NULL
AND LEN(t2.PepperiCreditCardId) > 0
AND t3.GUID IS NULL
AND t4.GUID IS NULL

UPDATE t1
SET PepperiCreditCardId = ''
FROM Salesorder t1
JOIN [stagePA].[ExtractSalesOrderExclude] t2
ON t1.PepperiOrderNbr = t2.[order]
WHERE t2.OrderMessage LIKE 'Cannot Match CC GUID:%'

-- customer or Sales Rep Not Found
insert [stagePA].[ExtractSalesOrderExclude]
SELECT Pepperiordernbr [Order], 'Order Customer or Rep not available yet:  Rep Nbr: ' 
+ isnull(cast(t1.RepNbr as varchar),'Null') 
+ ' ,Customer Nbr: ' +  Isnull(cast(t1.CustomerNbr as varchar),'Null') +
cast(OrderTotal as varchar) +
+ ', Informational: Notify Tina and Lior If This is not resolved within 90 minutes (next 3 cycles) ' OrderMessage
from  SalesOrder t1
LEFT JOIN [dbo].SalesRep t2
ON t2.SalesRepresentativeNum = t1.RepNbr
LEFT JOIN Customer t3
ON t3.CustomerNumber = t1.CustomerNbr
LEFT JOIN Customer t4
ON t4.PepperiCustomerNumber = t1.CustomerNbr
where t1.erpsyncdate is null
and (t2.SalesRepresentativeNum is null
or (t3.CustomerNumber is null
and t4.PepperiCustomerNumber is null))

-- No children
Insert [stagePA].[ExtractSalesOrderExclude]
SELECT Pepperiordernbr [Order], 'Order has no Children: Informational: Notify Tina and Lior If This is not resolved next cycle ' OrderMessage
from SalesOrder t1
join SalesOrderLine t2
on t1.PepperiOrderNbr = t2.pepperiid
where t1.erpsyncdate is null
and t2.PepperiId is null

-- No Parent
Insert [stagePA].[ExtractSalesOrderExclude]
SELECT Pepperiordernbr [Order], 'Order Line(s) has no Parent:  Informational: Notify Tina and Lior If This is not resolved next cycle' OrderMessage
from SalesOrder t1
join SalesOrderLine t2
on t1.PepperiOrderNbr = t2.pepperiid
where t1.erpsyncdate is null
and t2.PepperiId is NULL

-- Send Email if Issues
If (Select Count(*) from [stagePA].[ExtractSalesOrderExclude]) > 0
BEGIN

-- Remove Order FROM processing
DELETE StagePA.SalesOrderToProcess
WHERE createddate = @rundate
AND Pepperiordernbr IN (SELECT [Order] FROM [stagePA].[ExtractSalesOrderExclude])

-- send email
DECLARE @xml NVARCHAR(MAX)
DECLARE @body NVARCHAR(MAX)

SET @xml = CAST(( SELECT [Order] AS 'td','',[OrderMessage] AS 'td',''
FROM [stagePA].[ExtractSalesOrderExclude]
ORDER BY [Order]
FOR XML PATH('tr'), ELEMENTS ) AS NVARCHAR(MAX))

SET @body ='<html><body><H3>Exclusion List: Sales Order processing</H3>
<table border = 1> 
<tr>
<th> Order </th> <th> Order Message </th> </tr>'    
SET @body = @body + @xml +'</table></body></html>'

EXEC [WINSQLBI01].msdb.dbo.sp_send_dbmail  
    @profile_name = 'Account',  
    @recipients = 'bret.aylor@winzerusa.com;Manny.Rodriguezj@winzerusa.com;jason.soffer@winzerusa.com;Ramandeep.Campbell@winzerusa.com',  
    @body = @body ,  
    @subject = 'Pepperi Staging: Orders Excluded from Current Processing',  
    @attach_query_result_as_file = 0,
	@importance  = 'High',
	@body_format = 'HTML'

END

End
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[xxCreditCardUpdates]
as
begin

drop table if exists #SalesOrders 

SELECT PepperiOrderNbr,
       CustomerNbr,
       CustomerName,
       RepNbr,
       [SalesRepresentativeName],
	   SO.PepperiCreditCardId,
	   So.CreatedDate,
	   So.ErpSyncDate
       
  into #SalesOrders FROM  [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrder] SO
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].SalesRep SR
        ON SR.SalesRepresentativeNum = SO.RepNbr
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].Customer CU
        ON CU.CustomerNumber = SO.CustomerNbr
      LEFT OUTER JOIN Pepperi_Staging.dbo.CreditCard_View CC ON CAST(SO.CustomerNbr AS VARCHAR(50)) = CAST(CC.CustomerNumber  AS VARCHAR(50))  AND SO.PepperiCreditCardId = CAST(CC.GUID AS varchar(50)) 
 
WHERE PepperiCreditCardId IS NOT NULL 
      AND CC.Guid IS NULL  and so.ErpSyncDate is null


Update SO 
SET So.PepperiCreditCardId = NULL
,SO.HoldReason = 'CC'
, So.ModifiedDate = GETDATE()
, So.ModifiedBy = SUSER_NAME()
, SO.CCFlag = 1 
from SalesOrder SO 
INNER join #SalesOrders SOT 
ON SO.PepperiOrderNbr = SOT.PepperiOrderNbr


end


GO

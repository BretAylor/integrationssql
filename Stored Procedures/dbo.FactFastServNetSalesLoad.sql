SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      1/24/2019
-- Purpose      Load FactFastServNetsales table with product, employee, customer and net sales
-- Copyright © 2019, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- exec [dbo].[FactFastServNetSalesLoad]
CREATE PROCEDURE [dbo].[FactFastServNetSalesLoad]
AS
BEGIN
-- Monthly Summary - FastServ
TRUNCATE TABLE [dbo].[FactFastServNetsales]
INSERT INTO [dbo].[FactFastServNetsales] (DateKey, ProductKey,  EmployeeKey,	CustomerKey, ShipmentKey, BranchKey, InvoiceNo,OrderNo, [LINENO], UnitPrice,	QtyShipped, WinzerCost,FranchiseCost,  NetSales, DateAdded)
SELECT ISNULL(DD.DateKey,'') AS DateKey,
       ISNULL(PR.ProductKey,'') AS ProductKey,
       ISNULL(EM.EmployeeKey,'') AS EmployeeKey,
       ISNULL(CU.CustomerKey,'') AS CustomerKey,
	   ISNULL(ST.ShipmentKey,'') AS ShipmentKey,
       ISNULL(BR.BranchKey,'') AS BranchKey,
       ISNULL(IL.InvoiceNo,'') AS InvoiceNo,
       ISNULL(IL.OrderNo,'') AS OrderNo,
       IL.[LineNo],
       IL.UnitPrice,
       IL.QtyShipped,
       ROUND(WI.InvoiceLineUnitWinzerCost * IL.QtyShipped, 5) AS WinzerCost,
       ROUND(WI.InvoiceLineUnitOvrdFranchiseCost * IL.QtyShipped, 5) AS FranchiseCost,
       ROUND(IL.UnitPrice * IL.QtyShipped, 2) AS [NetSales],
       GETDATE() AS DateAdded
FROM BI_STAGING.dbo.InvoiceLineP21 IL
    INNER JOIN BI_STAGING.dbo.InvoiceP21 IH
        ON IH.InvoiceNo = IL.InvoiceNo
    INNER JOIN BI_DWH.dbo.DimDate DD
        ON DD.Date = CAST(IH.DateCreated AS DATE)
    LEFT OUTER JOIN BI_STAGING.[dbo].[WzrInvoiceLineP21] WI
        ON WI.InvoiceLineInvoiceId = IL.InvoiceNo
           AND WI.InvoiceLineId = IL.[LineNo]
    LEFT OUTER JOIN BI_DWH.dbo.DimProductP21 PR
        ON PR.ItemId = IL.ItemId
    LEFT OUTER JOIN BI_DWH.dbo.DimEmployeeP21 EM
        ON IH.SalesRepID = EM.SalesRepID
    LEFT OUTER JOIN BI_DWH.dbo.DimCustomerP21 CU
        ON CU.CustomerId = IH.CustomerID
    LEFT OUTER JOIN BI_DWH.dbo.DimBranchP21 BR
        ON BR.BranchId = IH.BranchId
           AND BR.CompanyId = IH.CompanyNo
	LEFT OUTER JOIN dbo.DimShipToP21 ST 
	    ON ST.NbShipToNumber = IH.ShipToNum AND ST.NbShipToCustomerId = IH.CustomerID

WHERE IH.CompanyNo = 7 --AND IH.invoiceType <> 'CD' -- excluding consolidated invoice, only fastserv with company number 7 and fiscal year 2018
      AND IL.OtherChargeItem = 'N'
      AND IL.InvMastUid IS NOT NULL
      AND IL.InvMastUid <> 0
      AND DD.FiscalYear >= 2018
	 
	  


END

GO

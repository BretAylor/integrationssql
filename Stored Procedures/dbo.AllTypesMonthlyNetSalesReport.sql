SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- exec [dbo].[AllTypesMonthlyNetSalesReport] 
CREATE PROCEDURE [dbo].[AllTypesMonthlyNetSalesReport] 
AS
BEGIN
SELECT * FROM (
SELECT 
      'Franchise' AS [Type],
	  1 AS TypeNum,
	  INF.OrderKey AS OrderId,
	  DD.FiscalYear,
      DD.FiscalMonth,
	  DD.MonthName,
	  DD.Date,
	  LEFT(DD.[DayName],3) AS [DayName] ,
	  DD.FullDate  +' - '+ LEFT(DD.[DayName],3) AS DateDay,
	  CU.CustomerName,
	  PR.ProductNum,
	  PR.ProductDescription,
	  PR.ProductNum +' - ' + PR.ProductDescription AS ProductNumDesc,
	  EM.SalesRepresentativeName,
	  INF.QuantityShipped ,
	  INF.ListPrice , 
	  INF.WinzerCost,
	  INF.FranchiseCost,
	  INF.NetSales ,
	  GETDATE() AS DateAdded

FROM BI_DWH.[dbo].[FactInvoiceFranchise] INF
    INNER JOIN BI_DWH.dbo.DimDate DD
        ON INF.DateKey = DD.DateKey
	LEFT OUTER JOIN BI_DWH.dbo.DimCustomer CU
        ON CU.CustomerKey = INF.CustomerKey
    LEFT OUTER JOIN BI_DWH.dbo.DimProduct PR
        ON PR.ProductKey = INF.ProductKey
    LEFT OUTER JOIN BI_DWH.dbo.DimEmployee EM
        ON EM.EmployeeKey = INF.EmployeeKey
 WHERE DD.FiscalYear >= 2018


UNION ALL

-- Add P21 costs
SELECT 
	   'FastServ' AS [Type],
	   2 AS TypeNum,
	   FS.OrderNo AS OrderId,
	   DD.FiscalYear,
       DD.FiscalMonth,
	   DD.[MonthName],
       DD.Date,
	   LEFT(DD.[DayName],3) AS [DayName] ,
	   DD.FullDate  +'-'+ LEFT(DD.[DayName],3) AS DateDay,
	   CU.CustomerName,
	   PR.ItemID AS ProductNum,
	   PR.ItemDesc AS ProductDescription,
	   PR.ItemID +' - ' + PR.ItemDesc AS ProductNumDesc,
       ISNULL(EM.FirstName,'') + ' ' +  ISNULL(EM.Middle,'') + ' ' + ISNULL(EM.LastName,'') AS SalesRepresentativeName,
	   FS.QtyShipped AS QuantityShipped,
	   FS.UnitPrice ,
	   FS.WinzerCost,
	   FS.FranchiseCost,
       FS.NetSales ,
	   GETDATE() AS DateAdded

	   
FROM BI_DWH.[dbo].[FactFastServNetSales] FS
    INNER JOIN BI_DWH.dbo.DimDate DD
        ON FS.DateKey = DD.DateKey
	 LEFT OUTER JOIN  BI_DWH.dbo.DimCustomerP21 CU
        ON CU.CustomerKey = FS.CustomerKey
    LEFT OUTER JOIN BI_DWH.dbo.DimProductP21 PR
        ON PR.ProductKey = FS.ProductKey
    LEFT OUTER JOIN BI_DWH.dbo.DimEmployeeP21 EM
        ON EM.EmployeeKey = FS.EmployeeKey
  
WHERE DD.FiscalYear >= 2018


UNION ALL 

-- add costs for AS400 A1chemical - done

SELECT 
       'A1 Chemical' AS [Type],
	   3 AS TypeNum,
	   FC.OrderNo AS OrderId,
	   DD.FiscalYear,
       DD.FiscalMonth,
	   DD.[MonthName],
	   DD.Date, 
	   LEFT(DD.[DayName],3) AS [DayName] ,
	   DD.FullDate  +'-'+ LEFT(DD.[DayName],3) AS DateDay,
	   CU.CustomerName,
	   PR.ProductNum,
	   PR.ProductDescription,
	   PR.ProductNum +' - ' + PR.ProductDescription AS ProductNumDesc,
	   EM.SalesRepresentativeName,
	   FC.QtyShipped AS QuantityShipped,
	   FC.UnitPrice ,
	   FC.WinzerCost, 
	   FC.FranchiseCost, 
       FC.NetSales,
	   GETDATE() AS DateAdded

FROM BI_DWH.dbo.FactA1ChemicalNetSales FC
    INNER JOIN BI_DWH.dbo.DimDate DD
        ON FC.DateKey = DD.DateKey
     LEFT OUTER JOIN BI_DWH.dbo.DimCustomer CU
        ON CU.CustomerKey = FC.CustomerKey
    LEFT OUTER JOIN BI_DWH.dbo.DimProduct PR
        ON PR.ProductKey = FC.ProductKey
    LEFT OUTER JOIN BI_DWH.dbo.DimEmployee EM
        ON EM.EmployeeKey = FC.EmployeeKey
WHERE DD.FiscalYear >= 2018 AND [Type] = 'A1Reno'

UNION ALL 

-- Add costs for P21 A1 chemical - done
SELECT 
       'A1 Chemical' AS [Type],
	   3 AS TypeNum,
	   FC.OrderNo AS OrderId,
	   DD.FiscalYear,
       DD.FiscalMonth,
	   DD.[MonthName],
	   DD.Date,
	   LEFT(DD.[DayName],3) AS [DayName] ,
	   DD.FullDate  +'-'+ LEFT(DD.[DayName],3) AS DateDay,
	   CU.CustomerName,
	   PR.ItemID AS ProductNum,
	   PR.ItemDesc AS ProductDescription,
	   PR.ItemID +' - ' + PR.ItemDesc AS ProductNumDesc,
       ISNULL(EM.FirstName,'') + ' '+ ISNULL(EM.Middle,'') +' ' + ISNULL(EM.LastName,'') AS SalesRepresentativeName,
	   FC.QtyShipped AS QuantityShipped,
	   FC.UnitPrice ,
	   FC.WinzerCost,
       FC.FranchiseCost,
	   FC.NetSales,
	   GETDATE() AS DateAdded

FROM BI_DWH.dbo.FactA1ChemicalNetSales FC
    INNER JOIN BI_DWH.dbo.DimDate DD
        ON FC.DateKey = DD.DateKey
	LEFT OUTER JOIN BI_DWH.dbo.DimCustomerP21 CU
        ON CU.CustomerKey = FC.CustomerKey
    LEFT OUTER JOIN BI_DWH.dbo.DimProductP21 PR
        ON PR.ProductKey = FC.ProductKey
    LEFT OUTER JOIN BI_DWH.dbo.DimEmployeeP21 EM
        ON EM.EmployeeKey = FC.EmployeeKey
WHERE DD.FiscalYear >= 2018 AND [Type] = 'A1LasVegas'


UNION ALL 

-- Add Costs for P21 Speedy Clean 

SELECT 
       'Speedy Clean' AS [Type],
	   4 AS TypeNum,
	   SC.InvoiceNo AS OrderId,
	   DD.FiscalYear,
       DD.FiscalMonth,
	   DD.[MonthName],
	   DD.Date,
	   LEFT(DD.[DayName],3) AS [DayName] ,
	   DD.FullDate  +'-'+ LEFT(DD.[DayName],3) AS DateDay,
	   CU.CustomerName,
	   PR.ItemID AS ProductNum,
	   PR.ItemDesc AS ProductDescription,
	   PR.ItemID +' - ' + PR.ItemDesc AS ProductNumDesc,
       ISNULL(EM.FirstName,'') + ' '+ ISNULL(EM.Middle,'')+ ' ' + ISNULL(EM.LastName,'') AS SalesRepresentativeName,
	   SC.QtyShipped AS QuantityShipped,
	   SC.UnitPrice ,
	   SC.WinzerCost,
	   SC.FranchiseCost,
	   SC.NetSales AS NetSales ,
	   GETDATE() AS DateAdded

FROM BI_DWH.[dbo].[FactSpeedyCleanNetSales] SC
    INNER JOIN BI_DWH.dbo.DimDate DD
        ON SC.DateKey = DD.DateKey
   LEFT OUTER JOIN BI_DWH.dbo.DimCustomerP21 CU
        ON CU.CustomerKey = SC.CustomerKey
    LEFT OUTER JOIN BI_DWH.dbo.DimProductP21 PR
        ON PR.ProductKey = SC.ProductKey
    LEFT OUTER JOIN BI_DWH.dbo.DimEmployeeP21 EM
        ON EM.EmployeeKey = SC.EmployeeKey
WHERE DD.FiscalYear >= 2018


	   ) AS a 
ORDER BY [Type], a.FiscalYear, a.FiscalMonth


END


GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



Create PROCEDURE [dbo].[SalesOrderHeader_LinesValidation]
AS
BEGIN

SELECT 
       'Order submitted while package load is running. The order will be loaded shortly' AS [On Hold],
       ISNULL(PepperiOrderNbr,OL.PepperiId) AS PepperiOrderNbr,
       CustomerNbr,
       CustomerName,
       RepNbr,
       [SalesRepresentativeName],
       ISNULL(ItemCount,0) AS [SalesOrderHeaderQuantity],
       SUM(OrderQuantity) AS SalesOrderLinesQuantity,
       ISNULL(ItemCount,0) - SUM(OrderQuantity) AS MissingSalesOrderLinesQuantity,
	   CAST(ISNULL(SO.OrderTotal,0)AS DECIMAL(12,2)) AS SalesOrderAmount,
	   CAST(SUM(OL.OrderPrice * OL.OrderQuantity) AS DECIMAL(12,2)) AS SalesOrderLineAmount,
       CAST(ISNULL(SO.OrderTotal,0) - SUM(OL.OrderPrice * OL.OrderQuantity) AS DECIMAL(12,2)) AS MissingSalesOrderLinesAmount,
       OrderDate,
       MAX(SO.CreatedDate) AS SalesOrderCreatedDate,
       MAX(SO.ErpSyncDate) AS SalesOrderErpSyncDate,
       MAX(OL.CreatedDate) AS SalesOrderLineCreatedDate,
       MAX(OL.ErpSyncDate) AS SalesOrderLineErpSyncDate
FROM [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrderLine] OL 
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrder] SO
        ON SO.PepperiOrderNbr = OL.PepperiID
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].SalesRep SR
        ON SR.SalesRepresentativeNum = SO.RepNbr
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].Customer CU
        ON CU.CustomerNumber = SO.CustomerNbr
WHERE OL.PepperiId NOT IN(76290487)  AND DATEPART(MINUTE, GETDATE()) BETWEEN 0 AND 29 AND (
SO.CreatedDate BETWEEN CAST(CAST(CAST(GETDATE() AS DATE) AS VARCHAR(10)) +' '+CAST(DATEPART(HOUR,GETDATE())  AS CHAR(2)) +':'+'00:00' AS DATETIME)  AND CAST(CAST(CAST(GETDATE() AS DATE) AS VARCHAR(10)) +' '+CAST(DATEPART(HOUR,GETDATE())  AS CHAR(2)) +':'+'29:00' AS DATETIME) OR 
OL.CreatedDate BETWEEN CAST(CAST(CAST(GETDATE() AS DATE) AS VARCHAR(10)) +' '+CAST(DATEPART(HOUR,GETDATE())  AS CHAR(2)) +':'+'00:00' AS DATETIME)  AND CAST(CAST(CAST(GETDATE() AS DATE) AS VARCHAR(10)) +' '+CAST(DATEPART(HOUR,GETDATE())  AS CHAR(2)) +':'+'29:00' AS DATETIME))

GROUP BY ISNULL(PepperiOrderNbr,OL.PepperiId),
         CustomerNbr,
         RepNbr,
         [SalesRepresentativeName],
         ItemCount,
         SO.OrderTotal,
         CustomerName,
         OrderDate 


UNION ALL 

SELECT 
       'Order submitted while package load is running. The order will be loaded shortly' AS [On Hold],
       ISNULL(PepperiOrderNbr,OL.PepperiId) AS PepperiOrderNbr,
       CustomerNbr,
       CustomerName,
       RepNbr,
       [SalesRepresentativeName],
       ISNULL(ItemCount,0) AS [SalesOrderHeaderQuantity],
       SUM(OrderQuantity) AS SalesOrderLinesQuantity,
       ISNULL(ItemCount,0) - SUM(OrderQuantity) AS MissingSalesOrderLinesQuantity,
	   CAST(ISNULL(SO.OrderTotal,0)AS DECIMAL(12,2)) AS SalesOrderAmount,
	   CAST(SUM(OL.OrderPrice * OL.OrderQuantity) AS DECIMAL(12,2)) AS SalesOrderLineAmount,
       CAST(ISNULL(SO.OrderTotal,0) - SUM(OL.OrderPrice * OL.OrderQuantity) AS DECIMAL(12,2)) AS MissingSalesOrderLinesAmount,
       OrderDate,
       MAX(SO.CreatedDate) AS SalesOrderCreatedDate,
       MAX(SO.ErpSyncDate) AS SalesOrderErpSyncDate,
       MAX(OL.CreatedDate) AS SalesOrderLineCreatedDate,
       MAX(OL.ErpSyncDate) AS SalesOrderLineErpSyncDate
FROM [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrderLine] OL 
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrder] SO
        ON SO.PepperiOrderNbr = OL.PepperiID
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].SalesRep SR
        ON SR.SalesRepresentativeNum = SO.RepNbr
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].Customer CU
        ON CU.CustomerNumber = SO.CustomerNbr
WHERE OL.PepperiId NOT IN(76290487)  AND DATEPART(MINUTE, GETDATE()) BETWEEN 30 AND 59 AND (
SO.CreatedDate BETWEEN CAST(CAST(CAST(GETDATE() AS DATE) AS VARCHAR(10)) +' '+ CAST(DATEPART(HOUR,GETDATE())  AS CHAR(2)) +':'+'30:00'AS DATETIME)  AND CAST(CAST(CAST(GETDATE() AS DATE) AS VARCHAR(10)) +' '+ CAST(DATEPART(HOUR,GETDATE())  AS CHAR(2)) +':'+'59:00'AS DATETIME) OR 
OL.CreatedDate BETWEEN CAST(CAST(CAST(GETDATE() AS DATE) AS VARCHAR(10)) +' '+ CAST(DATEPART(HOUR,GETDATE())  AS CHAR(2)) +':'+'30:00'AS DATETIME)  AND CAST(CAST(CAST(GETDATE() AS DATE) AS VARCHAR(10)) +' '+ CAST(DATEPART(HOUR,GETDATE())  AS CHAR(2)) +':'+'59:00'AS DATETIME))

GROUP BY ISNULL(PepperiOrderNbr,OL.PepperiId),
         CustomerNbr,
         RepNbr,
         [SalesRepresentativeName],
         ItemCount,
         SO.OrderTotal,
         CustomerName,
         OrderDate 


UNION ALL
 
 
SELECT 
       'Order Quantity Mismatch' AS [On Hold],
       PepperiOrderNbr,
       CustomerNbr,
       CustomerName,
       RepNbr,
       [SalesRepresentativeName],
       ItemCount AS [SalesOrderHeaderQuantity],
       SUM(OrderQuantity) AS SalesOrderLinesQuantity,
       ItemCount - SUM(OrderQuantity) AS MissingSalesOrderLinesQuantity,
	   CAST(SO.OrderTotal AS DECIMAL(12,2)) AS SalesOrderAmount,
	   CAST(SUM(OL.OrderPrice  *  OL.OrderQuantity) AS DECIMAL(12,2)) AS SalesOrderLineAmount,
       CAST(SO.OrderTotal - SUM(OL.OrderPrice  *  OL.OrderQuantity)AS DECIMAL(12,2)) AS MissingSalesOrderLinesAmount,
       OrderDate,
       MAX(SO.CreatedDate) AS SalesOrderCreatedDate,
       MAX(SO.ErpSyncDate) AS SalesOrderErpSyncDate,
       MAX(OL.CreatedDate) AS SalesOrderLineCreatedDate,
       MAX(OL.ErpSyncDate) AS SalesOrderLineErpSyncDate
FROM [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrder] SO
    INNER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrderLine] OL
        ON SO.PepperiOrderNbr = OL.PepperiID
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].SalesRep SR
        ON SR.SalesRepresentativeNum = SO.RepNbr
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].Customer CU
        ON CU.CustomerNumber = SO.CustomerNbr
WHERE PepperiOrderNbr  NOT IN(76290487)  AND  OL.PepperiId NOT IN (SELECT DISTINCT PEpperiId FROM SalesOrderline WHERE  LabelCode IS NOT NULL OR LabelCode <> '' OR (LabelCode IS NULL AND LabelQty >= 1) )--  exclude labels only order header/lines
GROUP BY PepperiOrderNbr,
         CustomerNbr,
         RepNbr,
         [SalesRepresentativeName],
         ItemCount,
         SO.OrderTotal,
         CustomerName,
         OrderDate
HAVING SUM(OrderQuantity) <> ItemCount 


UNION ALL 

SELECT 
       'Order Amount Mismatch' AS [On Hold],
       PepperiOrderNbr,
       CustomerNbr,
       CustomerName,
       RepNbr,
       [SalesRepresentativeName],
       ItemCount AS [SalesOrderHeaderQuantity],
       SUM(OrderQuantity) AS SalesOrderLinesQuantity,
       ItemCount - SUM(OrderQuantity) AS MissingSalesOrderLinesQuantity,
	   CAST(SO.OrderTotal AS DECIMAL(12,2)) AS SalesOrderAmount,
	   CAST(SUM(OL.OrderPrice * OL.OrderQuantity) AS DECIMAL(12,2)) AS SalesOrderLineAmount,
       CAST(SO.OrderTotal - SUM(OL.OrderPrice * OL.OrderQuantity) AS DECIMAL(12,2)) AS MissingSalesOrderLinesAmount,
       OrderDate ,
       MAX(SO.CreatedDate) AS SalesOrderCreatedDate,
       MAX(SO.ErpSyncDate) AS SalesOrderErpSyncDate,
       MAX(OL.CreatedDate) AS SalesOrderLineCreatedDate,
       MAX(OL.ErpSyncDate) AS SalesOrderLineErpSyncDate
FROM [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrder] SO
    INNER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrderLine] OL
        ON SO.PepperiOrderNbr = OL.PepperiID
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].SalesRep SR
        ON SR.SalesRepresentativeNum = SO.RepNbr
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].Customer CU
        ON CU.CustomerNumber = SO.CustomerNbr
WHERE PepperiOrderNbr  NOT IN(76290487) AND (OL.ParentItemNumber IS NULL OR OL.ParentItemNumber = '') 
AND (LabelCode IS NULL OR LabelCode ='') --  exclude labels only order header/lines
GROUP BY PepperiOrderNbr,
         CustomerNbr,
         RepNbr, 
         [SalesRepresentativeName],
         ItemCount,
         SO.OrderTotal,
         CustomerName,
         OrderDate
HAVING ABS(SO.OrderTotal - SUM(OL.OrderPrice * OL.OrderQuantity)) >= 1  AND ItemCount = SUM(OrderQuantity)

UNION ALL 

SELECT 
       'Order Header Missing Lines' AS [On Hold],
       PepperiOrderNbr,
       CustomerNbr,
       CustomerName,
       RepNbr,
       [SalesRepresentativeName],
       ItemCount AS [SalesOrderHeaderQuantity],
       SUM(ISNULL(OrderQuantity,0)) AS SalesOrderLinesQuantity,
       ItemCount - SUM(ISNULL(OrderQuantity,0)) AS MissingSalesOrderLinesQuantity,
	   CAST(SO.OrderTotal AS DECIMAL(12,2)) AS SalesOrderAmount,
	   CAST(SUM(ISNULL(OL.OrderPrice * OL.OrderQuantity,0)) AS DECIMAL(12,2)) AS SalesOrderLineAmount,
       CAST(SO.OrderTotal - SUM(ISNULL(OL.OrderPrice * OL.OrderQuantity,0)) AS DECIMAL(12,2)) AS MissingSalesOrderLinesAmount,
       OrderDate,
       MAX(SO.CreatedDate) AS SalesOrderCreatedDate,
       MAX(SO.ErpSyncDate) AS SalesOrderErpSyncDate,
       MAX(OL.CreatedDate) AS SalesOrderLineCreatedDate,
       MAX(OL.ErpSyncDate) AS SalesOrderLineErpSyncDate
FROM [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrder] SO
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrderLine] OL
        ON SO.PepperiOrderNbr = OL.PepperiID
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].SalesRep SR
        ON SR.SalesRepresentativeNum = SO.RepNbr
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].Customer CU
        ON CU.CustomerNumber = SO.CustomerNbr
WHERE PepperiOrderNbr  NOT IN(76290487)  --  exclude labels only order header/lines
GROUP BY PepperiOrderNbr,
         CustomerNbr,
         RepNbr,
         [SalesRepresentativeName],
         ItemCount,
         SO.OrderTotal,
         CustomerName,
         OrderDate 
 HAVING SUM(ISNULL(OrderQuantity,-1)) = -1

 UNION ALL 

 SELECT 
       'Order Lines Missing Header' AS [On Hold],
       ISNULL(PepperiOrderNbr,OL.PepperiId) AS PepperiOrderNbr,
       CustomerNbr,
       CustomerName,
       RepNbr,
       [SalesRepresentativeName],
       ISNULL(ItemCount,0) AS [SalesOrderHeaderQuantity],
       SUM(OrderQuantity) AS SalesOrderLinesQuantity,
       ISNULL(ItemCount,0) - SUM(OrderQuantity) AS MissingSalesOrderLinesQuantity,
	   CAST(ISNULL(SO.OrderTotal,0)AS DECIMAL(12,2)) AS SalesOrderAmount,
	   CAST(SUM(OL.OrderPrice * OL.OrderQuantity) AS DECIMAL(12,2)) AS SalesOrderLineAmount,
       CAST(ISNULL(SO.OrderTotal,0) - SUM(OL.OrderPrice * OL.OrderQuantity) AS DECIMAL(12,2)) AS MissingSalesOrderLinesAmount,
       OrderDate,
       MAX(SO.CreatedDate) AS SalesOrderCreatedDate,
       MAX(SO.ErpSyncDate) AS SalesOrderErpSyncDate,
       MAX(OL.CreatedDate) AS SalesOrderLineCreatedDate,
       MAX(OL.ErpSyncDate) AS SalesOrderLineErpSyncDate
FROM [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrderLine] OL 
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrder] SO
        ON SO.PepperiOrderNbr = OL.PepperiID
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].SalesRep SR
        ON SR.SalesRepresentativeNum = SO.RepNbr
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].Customer CU
        ON CU.CustomerNumber = SO.CustomerNbr
WHERE OL.PepperiId NOT IN(76290487)   --  exclude labels only order header/lines
GROUP BY ISNULL(PepperiOrderNbr,OL.PepperiId),
         CustomerNbr,
         RepNbr,
         [SalesRepresentativeName],
         ItemCount,
         SO.OrderTotal,
         CustomerName,
         OrderDate 
 HAVING ISNULL(ItemCount,-1) = -1

UNION ALL 

  --Update SalesOrder set ErpModifiedDate = getdate()
  --WHERE ErpModifiedDate is null and 
  --PepperiOrderNbr not in (select PepperiOrderNbr from [dbo].[SalesOrderHeader_LinesValidationView])
  --and PepperiOrderNbr in (select [ORDNBR_PadOrderNumber] from WINSQLBI01.[PEPPERI].[dbo].[SalesOrderCustomKit])


SELECT 
       'Custom Kit Component Missing Parent Item' AS [On Hold],
       PepperiOrderNbr,
       CustomerNbr,
       CustomerName,
       RepNbr,
       [SalesRepresentativeName],
       ItemCount AS [SalesOrderHeaderQuantity],
       SUM(OrderQuantity) AS SalesOrderLinesQuantity,
       ItemCount - SUM(OrderQuantity) AS MissingSalesOrderLinesQuantity,
	   CAST(SO.OrderTotal AS DECIMAL(12,2)) AS SalesOrderAmount,
	   CAST(SUM(OL.OrderPrice * OL.OrderQuantity) AS DECIMAL(12,2)) AS SalesOrderLineAmount,
       CAST(SO.OrderTotal - SUM(OL.OrderPrice * OL.OrderQuantity)AS DECIMAL(12,2)) AS MissingSalesOrderLinesAmount,
       OrderDate,
       MAX(SO.CreatedDate) AS SalesOrderCreatedDate,
       MAX(SO.ErpSyncDate) AS SalesOrderErpSyncDate,
       MAX(OL.CreatedDate) AS SalesOrderLineCreatedDate,
       MAX(OL.ErpSyncDate) AS SalesOrderLineErpSyncDate
FROM [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrderLine] OL 
    INNER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrder] SO
        ON SO.PepperiOrderNbr = OL.PepperiID
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].SalesRep SR
        ON SR.SalesRepresentativeNum = SO.RepNbr
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].Customer CU
        ON CU.CustomerNumber = SO.CustomerNbr
WHERE PepperiOrderNbr  NOT IN(76290487)   AND OL.PepperiId IN (SELECT DISTINCT PepperiID FROM [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrderLine]  WHERE BrandedProductNbr LIKE '975%') -- Only Custom Kits
--  exclude labels only order header/lines
GROUP BY PepperiOrderNbr,
         CustomerNbr,
         RepNbr,
         [SalesRepresentativeName],
         ItemCount,
         SO.OrderTotal,
         CustomerName,
         OrderDate
HAVING SUM(CASE WHEN ParentItemNumber IS NULL THEN 0 ELSE 1 END ) = 0


UNION ALL 

SELECT 
       'Custom Kit Missing Components' AS [On Hold],
       PepperiOrderNbr,
       CustomerNbr,
       CustomerName,
       RepNbr,
       [SalesRepresentativeName],
       ItemCount AS [SalesOrderHeaderQuantity],
       SUM(OrderQuantity) AS SalesOrderLinesQuantity,
       ItemCount - SUM(OrderQuantity) AS MissingSalesOrderLinesQuantity,
	   CAST(SO.OrderTotal AS DECIMAL(12,2)) AS SalesOrderAmount,
	   CAST (SUM(OL.OrderPrice * OL.OrderQuantity) AS DECIMAL(12,2)) AS SalesOrderLineAmount,
       CAST(SO.OrderTotal - SUM(OL.OrderPrice * OL.OrderQuantity) AS DECIMAL(12,2)) AS MissingSalesOrderLinesAmount,
       OrderDate,
       MAX(SO.CreatedDate) AS SalesOrderCreatedDate,
       MAX(SO.ErpSyncDate) AS SalesOrderErpSyncDate,
       MAX(OL.CreatedDate) AS SalesOrderLineCreatedDate,
       MAX(OL.ErpSyncDate) AS SalesOrderLineErpSyncDate
FROM [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrderLine] OL 
    INNER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrder] SO
        ON SO.PepperiOrderNbr = OL.PepperiID
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].SalesRep SR
        ON SR.SalesRepresentativeNum = SO.RepNbr
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].Customer CU
        ON CU.CustomerNumber = SO.CustomerNbr
WHERE PepperiOrderNbr  NOT IN(76290487)  AND OL.PepperiId IN (SELECT DISTINCT PepperiID FROM [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrderLine]  WHERE BrandedProductNbr LIKE '975%') -- Only Custom Kits
 --  exclude labels only order header/lines
GROUP BY PepperiOrderNbr,
         CustomerNbr,
         RepNbr,
         [SalesRepresentativeName],
         ItemCount,
         SO.OrderTotal,
         CustomerName,
         OrderDate
HAVING SUM(CASE WHEN BrandedProductNbr NOT LIKE '975%' THEN 1 ELSE 0 END ) = 0 -- Custom kit without components

UNION 
SELECT   'Pepperi Customer Not in Cross Reference' AS [On Hold],
       PepperiOrderNbr,
       CustomerNbr,
       NULL AS CustomerName,
       NULL AS RepNbr,
       NULL AS [SalesRepresentativeName],
       NULL AS [SalesOrderHeaderQuantity],
       NULL AS SalesOrderLinesQuantity,
       NULL AS MissingSalesOrderLinesQuantity,
	   NULL AS SalesOrderAmount,
	   NULL AS SalesOrderLineAmount,
       NULL AS MissingSalesOrderLinesAmount,
       NULL AS OrderDate,
       SO.CreatedDate AS SalesOrderCreatedDate,
       NULL AS SalesOrderErpSyncDate,
       OL.CreatedDate AS SalesOrderLineCreatedDate,
       NULL AS SalesOrderLineErpSyncDate
FROM [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrderLine] OL 
    INNER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrder] SO
        ON SO.PepperiOrderNbr = OL.PepperiID
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].SalesRep SR
        ON SR.SalesRepresentativeNum = SO.RepNbr
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].Customer CU
        ON (CU.CustomerNumber = ISNULL(SO.CustomerNbr,0) OR CU.PepperiCustomerNumber = ISNULL(SO.CustomerNbr,0))
 LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].dbo.CustomerCrossReference CR ON LEFT(CR.PepperiCustomerId,8) = LEFT(CU.PepperiCustomerNumber,8)
 WHERE  CR.AS400TempCustomerId  IS NULL AND SO.CustomerNbr > 1000000



UNION ALL 

 SELECT  'Sales Order without Customer' AS [On Hold],
       PepperiOrderNbr,
       CustomerNbr,
       CustomerName,
       NULL AS RepNbr,
       NULL AS [SalesRepresentativeName],
       NULL AS [SalesOrderHeaderQuantity],
       NULL AS SalesOrderLinesQuantity,
       NULL AS MissingSalesOrderLinesQuantity,
	   NULL AS SalesOrderAmount,
	   NULL AS SalesOrderLineAmount,
       NULL AS MissingSalesOrderLinesAmount,
       NULL AS OrderDate,
       SO.CreatedDate AS SalesOrderCreatedDate,
       NULL AS SalesOrderErpSyncDate,
       NULL AS SalesOrderLineCreatedDate,
       NULL AS SalesOrderLineErpSyncDate
 FROM SalesOrder SO LEFT OUTER JOIN SalesRep SR ON SO.RepNbr = SR.[SalesRepresentativeNum] 
 LEFT OUTER JOIN dbo.Customer CU ON (CU.CustomerNumber = ISNULL(SO.CustomerNbr,0) OR CU.PepperiCustomerNumber = ISNULL(SO.CustomerNbr,0))
 LEFT OUTER JOIN dbo.shipto ST ON ST.ShipToNumber = CU.DefaultShipTo AND ST.CustomerNumber = CU.CustomerNumber
 LEFT OUTER JOIN CreditCardAll EC ON SO.PepperiCreditCardId =  EC.Guid AND CAST(SO.CustomerNbr AS [VARCHAR](255)) = CAST(EC.CustomerNumber AS [VARCHAR](255))
 LEFT OUTER JOIN CustomerCrossReference CR ON LEFT(CR.PepperiCustomerId,8) = LEFT(CU.PepperiCustomerNumber,8)
 WHERE SO.erpSyncDate IS NULL AND CASE WHEN ISNULL(CustomerNbr,0) > 1000000 THEN CR.AS400TempCustomerId ELSE ISNULL(CustomerNbr,0) END IS NULL
 
 UNION ALL 


SELECT 'Order with credit card not assigned in AS400' AS OnHold,
       PepperiOrderNbr,
       CustomerNbr,
       CustomerName,
       RepNbr,
       [SalesRepresentativeName],
       ItemCount AS [SalesOrderHeaderQuantity],
       SUM(OrderQuantity) AS SalesOrderLinesQuantity,
       ItemCount - SUM(OrderQuantity) AS MissingSalesOrderLinesQuantity,
	   CAST(SO.OrderTotal AS DECIMAL(12,2)) AS SalesOrderAmount,
	   CAST (SUM(OL.OrderPrice * OL.OrderQuantity) AS DECIMAL(12,2)) AS SalesOrderLineAmount,
       CAST(SO.OrderTotal - SUM(OL.OrderPrice * OL.OrderQuantity) AS DECIMAL(12,2)) AS MissingSalesOrderLinesAmount,
       OrderDate,
       MAX(SO.CreatedDate) AS SalesOrderCreatedDate,
       MAX(SO.ErpSyncDate) AS SalesOrderErpSyncDate,
       MAX(OL.CreatedDate) AS SalesOrderLineCreatedDate,
       MAX(OL.ErpSyncDate) AS SalesOrderLineErpSyncDate
  FROM [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrderLine] OL 
    INNER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].[SalesOrder] SO
        ON SO.PepperiOrderNbr = OL.PepperiID
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].SalesRep SR
        ON SR.SalesRepresentativeNum = SO.RepNbr
    LEFT OUTER JOIN [wusa-sql01-dfw].[PEPPERI_STAGING].[dbo].Customer CU
        ON CU.CustomerNumber = SO.CustomerNbr
      LEFT OUTER JOIN Pepperi_Staging.dbo.CreditCard_View CC ON CAST(SO.CustomerNbr AS VARCHAR(50)) = CAST(CC.CustomerNumber  AS VARCHAR(50))  AND SO.PepperiCreditCardId = CAST(CC.GUID AS VARCHAR(50)) 
 
WHERE PepperiCreditCardId IS NOT NULL 
      AND CC.Guid IS NULL  AND so.ErpSyncDate IS NULL
GROUP BY PepperiOrderNbr,
         CustomerNbr,
         RepNbr,
         [SalesRepresentativeName],
         ItemCount,
         SO.OrderTotal,
         CustomerName,
         OrderDate

 UNION ALL 

 SELECT  'Sales Orders On Hold - Other Reason' AS [On Hold],
       OrderId AS PepperiOrderNbr,
       NULL AS CustomerNbr,
       NULL AS CustomerName,
       NULL AS RepNbr,
       NULL AS [SalesRepresentativeName],
       NULL  AS [SalesOrderHeaderQuantity],
       NULL AS SalesOrderLinesQuantity,
       NULL AS MissingSalesOrderLinesQuantity,
	   NULL AS SalesOrderAmount,
	   NULL AS SalesOrderLineAmount,
       NULL AS MissingSalesOrderLinesAmount,
       NULL AS OrderDate,
       CreatedDate AS SalesOrderCreatedDate,
       NULL AS SalesOrderErpSyncDate,
       NULL AS SalesOrderLineCreatedDate,
       NULL AS SalesOrderLineErpSyncDate FROM 
	   
	   Pepperi_Staging.dbo.OrdersOnHold WHERE Active = 1 


end

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create Procedure [dbo].[FranchiseAgentSalesSummaryUpdate]
 as
 Begin
 update FS set FS.TextAr = FW.TextAr, ModifiedDate = getdate(), ModifiedBy = SUSER_NAME() 
 FROM Pepperi_Staging.dbo.FranchiseAgentSalesSummary FS 
 INNER join 
WINSQLBI01.Pepperi.dbo.FranchiseAgentSalesSummary FW on FS.PadDescription = FW.PadDescription
and FS.ProcessDate = FW.ProcessDate and FS.SequenceNum = FW.SequenceNum
where FW.IsModified = 1 

 end

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--exec [dbo].[DateLoad]
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      12/3/2018
-- Description  Create DimDate table based on calculated dates of a date range
-- Copyright © 2018, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History
-- 12/25/2018 - Saad Benhlima added the Fiscal year, Fiscal Month and Fiscal Quarter. Fiscal year starts August of each year and ends on July. 
-- 12/29/2018 - Saad Benhalima added FiscalDate field
-- 1/03/2019  - Saad Benhalima added MonthNameShort field
-------------------------------------------------------------------------------
CREATE Procedure [dbo].[DateLoad]
AS
Begin
/********************************************************************************************/
--Specify Start Date and End date here
--Value of Start Date Must be Less than Your End Date 

DECLARE @StartDate DATETIME = '01/01/2017' --Starting value of Date Range
DECLARE @EndDate DATETIME = '01/01/2025' --End Value of Date Range

--Temporary Variables To Hold the Values During Processing of Each Date of Year
DECLARE
	@DayOfWeekInMonth INT,
	@DayOfWeekInYear INT,
	@DayOfQuarter INT,
	@WeekOfMonth INT,
	@CurrentYear INT,
	@CurrentMonth INT,
	@CurrentQuarter INT

/*Table Data type to store the day of week count for the month and year*/
DECLARE @DayOfWeek TABLE (DOW INT, MonthCount INT, QuarterCount INT, YearCount INT)

INSERT INTO @DayOfWeek VALUES (1, 0, 0, 0)
INSERT INTO @DayOfWeek VALUES (2, 0, 0, 0)
INSERT INTO @DayOfWeek VALUES (3, 0, 0, 0)
INSERT INTO @DayOfWeek VALUES (4, 0, 0, 0)
INSERT INTO @DayOfWeek VALUES (5, 0, 0, 0)
INSERT INTO @DayOfWeek VALUES (6, 0, 0, 0)
INSERT INTO @DayOfWeek VALUES (7, 0, 0, 0)

--Extract and assign various parts of Values from Current Date to Variable

DECLARE @CurrentDate AS DATETIME = @StartDate
SET @CurrentMonth = DATEPART(MM, @CurrentDate)
SET @CurrentYear = DATEPART(YY, @CurrentDate)
SET @CurrentQuarter = DATEPART(QQ, @CurrentDate)

/********************************************************************************************/
--Proceed only if Start Date(Current date ) is less than End date you specified above

WHILE @CurrentDate < @EndDate
BEGIN
 
/*Begin day of week logic*/

         /*Check for Change in Month of the Current date if Month changed then 
          Change variable value*/
	IF @CurrentMonth != DATEPART(MM, @CurrentDate) 
	BEGIN
		UPDATE @DayOfWeek
		SET MonthCount = 0
		SET @CurrentMonth = DATEPART(MM, @CurrentDate)
	END

        /* Check for Change in Quarter of the Current date if Quarter changed then change 
         Variable value*/

	IF @CurrentQuarter != DATEPART(QQ, @CurrentDate)
	BEGIN
		UPDATE @DayOfWeek
		SET QuarterCount = 0
		SET @CurrentQuarter = DATEPART(QQ, @CurrentDate)
	END
       
        /* Check for Change in Year of the Current date if Year changed then change 
         Variable value*/
	

	IF @CurrentYear != DATEPART(YY, @CurrentDate)
	BEGIN
		UPDATE @DayOfWeek
		SET YearCount = 0
		SET @CurrentYear = DATEPART(YY, @CurrentDate)
	END
	
        -- Set values in table data type created above from variables 

	UPDATE @DayOfWeek
	SET 
		MonthCount = MonthCount + 1,
		QuarterCount = QuarterCount + 1,
		YearCount = YearCount + 1
	WHERE DOW = DATEPART(DW, @CurrentDate)

	SELECT
		@DayOfWeekInMonth = MonthCount,
		@DayOfQuarter = QuarterCount,
		@DayOfWeekInYear = YearCount
	FROM @DayOfWeek
	WHERE DOW = DATEPART(DW, @CurrentDate)
	
/*End day of week logic*/


/* Populate Your Dimension Table with values*/
	
	INSERT INTO [dbo].[DimDate] ( [DateKey]      ,[Date]      ,[FullDate]      ,[DayOfMonth]      ,[DaySuffix]      ,[DayName]      ,[DayOfWeek]      ,[DayOfWeekInMonth]      ,[DayOfWeekInYear]      ,[DayOfQuarter]      ,[DayOfYear]      ,[WeekOfMonth]      ,[WeekOfQuarter]      ,[WeekOfYear]      ,[Month]      ,[MonthName]      ,[MonthOfQuarter]      ,[Quarter]      ,[QuarterName]      ,[Year]      ,[YearName]      ,[MonthYear]      ,[MMYYYY]      ,[FirstDayOfMonth]      ,[LastDayOfMonth]      ,[FirstDayOfQuarter]      ,[LastDayOfQuarter]      ,[FirstDayOfYear]      ,[LastDayOfYear]      ,[IsHoliday]      ,[IsWeekday]      ,[Holiday])
	SELECT
		
		CONVERT (char(8),@CurrentDate,112) as DateKey,
		@CurrentDate AS Date,
		CONVERT (char(10),@CurrentDate,101) as FullDate,
		DATEPART(DD, @CurrentDate) AS DayOfMonth,
		--Apply Suffix values like 1st, 2nd 3rd etc..
		CASE 
			WHEN DATEPART(DD,@CurrentDate) IN (11,12,13) 
			THEN CAST(DATEPART(DD,@CurrentDate) AS VARCHAR) + 'th'
			WHEN RIGHT(DATEPART(DD,@CurrentDate),1) = 1 
			THEN CAST(DATEPART(DD,@CurrentDate) AS VARCHAR) + 'st'
			WHEN RIGHT(DATEPART(DD,@CurrentDate),1) = 2 
			THEN CAST(DATEPART(DD,@CurrentDate) AS VARCHAR) + 'nd'
			WHEN RIGHT(DATEPART(DD,@CurrentDate),1) = 3 
			THEN CAST(DATEPART(DD,@CurrentDate) AS VARCHAR) + 'rd'
			ELSE CAST(DATEPART(DD,@CurrentDate) AS VARCHAR) + 'th' 
			END AS DaySuffix,
		
		DATENAME(DW, @CurrentDate) AS DayName,
		DATEPART(DW, @CurrentDate) AS DayOfWeek,

		-- check for day of week as Per US and change it as per UK format 
			
		@DayOfWeekInMonth AS DayOfWeekInMonth,
		@DayOfWeekInYear AS DayOfWeekInYear,
		@DayOfQuarter AS DayOfQuarter,
		DATEPART(DY, @CurrentDate) AS DayOfYear,
		DATEPART(WW, @CurrentDate) + 1 - DATEPART(WW, CONVERT(VARCHAR, 
		DATEPART(MM, @CurrentDate)) + '/1/' + CONVERT(VARCHAR, 
		DATEPART(YY, @CurrentDate))) AS WeekOfMonth,
		(DATEDIFF(DD, DATEADD(QQ, DATEDIFF(QQ, 0, @CurrentDate), 0), 
		@CurrentDate) / 7) + 1 AS WeekOfQuarter,
		DATEPART(WW, @CurrentDate) AS WeekOfYear,
		DATEPART(MM, @CurrentDate) AS Month,
		DATENAME(MM, @CurrentDate) AS MonthName,
		CASE
			WHEN DATEPART(MM, @CurrentDate) IN (1, 4, 7, 10) THEN 1
			WHEN DATEPART(MM, @CurrentDate) IN (2, 5, 8, 11) THEN 2
			WHEN DATEPART(MM, @CurrentDate) IN (3, 6, 9, 12) THEN 3
			END AS MonthOfQuarter,
		DATEPART(QQ, @CurrentDate) AS Quarter,
		CASE DATEPART(QQ, @CurrentDate)
			WHEN 1 THEN 'First'
			WHEN 2 THEN 'Second'
			WHEN 3 THEN 'Third'
			WHEN 4 THEN 'Fourth'
			END AS QuarterName,
		DATEPART(YEAR, @CurrentDate) AS Year,
		'CY ' + CONVERT(VARCHAR, DATEPART(YEAR, @CurrentDate)) AS YearName,
		LEFT(DATENAME(MM, @CurrentDate), 3) + '-' + CONVERT(VARCHAR, 
		DATEPART(YY, @CurrentDate)) AS MonthYear,
		RIGHT('0' + CONVERT(VARCHAR, DATEPART(MM, @CurrentDate)),2) + 
		CONVERT(VARCHAR, DATEPART(YY, @CurrentDate)) AS MMYYYY,
		CONVERT(DATETIME, CONVERT(DATE, DATEADD(DD, - (DATEPART(DD, 
		@CurrentDate) - 1), @CurrentDate))) AS FirstDayOfMonth,
		CONVERT(DATETIME, CONVERT(DATE, DATEADD(DD, - (DATEPART(DD, 
		(DATEADD(MM, 1, @CurrentDate)))), DATEADD(MM, 1, 
		@CurrentDate)))) AS LastDayOfMonth,
		DATEADD(QQ, DATEDIFF(QQ, 0, @CurrentDate), 0) AS FirstDayOfQuarter,
		DATEADD(QQ, DATEDIFF(QQ, -1, @CurrentDate), -1) AS LastDayOfQuarter,
		CONVERT(DATETIME, '01/01/' + CONVERT(VARCHAR, DATEPART(YY, 
		@CurrentDate))) AS FirstDayOfYear,
		CONVERT(DATETIME, '12/31/' + CONVERT(VARCHAR, DATEPART(YY, 
		@CurrentDate))) AS LastDayOfYear,
		NULL AS IsHoliday,
		CASE DATEPART(DW, @CurrentDate)
			WHEN 1 THEN 0
			WHEN 2 THEN 1
			WHEN 3 THEN 1
			WHEN 4 THEN 1
			WHEN 5 THEN 1
			WHEN 6 THEN 1
			WHEN 7 THEN 0
			END AS IsWeekday,
		NULL AS Holiday

	SET @CurrentDate = DATEADD(DD, 1, @CurrentDate)
END

/********************************************************************************************/
 
			
 
--Step 4.
--Update Values of Holiday as per USA Govt. Declaration for National Holiday.

/*Update HOLIDAY Field of USA In dimension*/
	
 	/*THANKSGIVING - Fourth THURSDAY in November*/
	UPDATE [dbo].[DimDate]
		SET Holiday = 'Thanksgiving Day'
	WHERE
		[Month] = 11 
		AND [DayOfWeek] = 'Thursday' 
		AND DayOfWeekInMonth = 4

	/*CHRISTMAS*/
	UPDATE [dbo].[DimDate]
		SET Holiday = 'Christmas Day'
		
	WHERE [Month] = 12 AND [DayOfMonth]  = 25

	/*4th of July*/
	UPDATE [dbo].[DimDate]
		SET Holiday = 'Independance Day'
	WHERE [Month] = 7 AND [DayOfMonth] = 4

	/*New Years Day*/
	UPDATE [dbo].[DimDate]
		SET Holiday = 'New Year''s Day'
	WHERE [Month] = 1 AND [DayOfMonth] = 1

	/*Memorial Day - Last Monday in May*/
	UPDATE [dbo].[DimDate]
		SET Holiday = 'Memorial Day'
	FROM [dbo].[DimDate]
	WHERE DateKey IN 
		(
		SELECT
			MAX(DateKey)
		FROM [dbo].[DimDate]
		WHERE
			[MonthName] = 'May'
			AND [DayOfWeek]  = 'Monday'
		GROUP BY
			[Year],
			[Month]
		)

	/*Labor Day - First Monday in September*/
	UPDATE [dbo].[DimDate]
		SET Holiday= 'Labor Day'
	FROM [dbo].[DimDate]
	WHERE DateKey IN 
		(
		SELECT
			MIN(DateKey)
		FROM [dbo].[DimDate]
		WHERE
			[MonthName] = 'September'
			AND [DayOfWeek] = 'Monday'
		GROUP BY
			[Year],
			[Month]
		)

	/*Valentine's Day*/
	UPDATE [dbo].[DimDate]
		SET Holiday = 'Valentine''s Day'
	WHERE
		[Month] = 2 
		AND [DayOfMonth] = 14

	/*Saint Patrick's Day*/
	UPDATE [dbo].[DimDate]
		SET Holiday = 'Saint Patrick''s Day'
	WHERE
		[Month] = 3
		AND [DayOfMonth] = 17

	/*Martin Luthor King Day - Third Monday in January starting in 1983*/
	UPDATE [dbo].[DimDate]
		SET Holiday = 'Martin Luthor King Jr Day'
	WHERE
		[Month] = 1
		AND [DayOfWeek]  = 'Monday'
		AND [Year] >= 1983
		AND DayOfWeekInMonth = 3

	/*President's Day - Third Monday in February*/
	UPDATE [dbo].[DimDate]
		SET Holiday = 'President''s Day'
	WHERE
		[Month] = 2
		AND [DayOfWeek] = 'Monday'
		AND DayOfWeekInMonth = 3

	/*Mother's Day - Second Sunday of May*/
	UPDATE [dbo].[DimDate]
		SET Holiday = 'Mother''s Day'
	WHERE
		[Month] = 5
		AND [DayOfWeek] = 'Sunday'
		AND DayOfWeekInMonth = 2

	/*Father's Day - Third Sunday of June*/
	UPDATE [dbo].[DimDate]
		SET Holiday = 'Father''s Day'
	WHERE
		[Month] = 6
		AND [DayOfWeek] = 'Sunday'
		AND DayOfWeekInMonth = 3

	/*Halloween 10/31*/
	UPDATE [dbo].[DimDate]
		SET Holiday = 'Halloween'
	WHERE
		[Month] = 10
		AND [DayOfMonth] = 31

	/*Election Day - The first Tuesday after the first Monday in November*/
	BEGIN
	DECLARE @Holidays TABLE (ID INT IDENTITY(1,1), 
	DateID int, Week TINYINT, YEAR CHAR(4), DAY CHAR(2))

		INSERT INTO @Holidays(DateID, [Year],[Day])
		SELECT
			DateKey,
			[Year],
			[DayOfMonth] 
		FROM [dbo].[DimDate]
		WHERE
			[Month] = 11
			AND [DayOfWeek] = 'Monday'
		ORDER BY
			YEAR,
			DayOfMonth 

		DECLARE @CNTR INT, @POS INT, @STARTYEAR INT, @ENDYEAR INT, @MINDAY INT

		SELECT
			@CURRENTYEAR = MIN([Year])
			, @STARTYEAR = MIN([Year])
			, @ENDYEAR = MAX([Year])
		FROM @Holidays

		WHILE @CURRENTYEAR <= @ENDYEAR
		BEGIN
			SELECT @CNTR = COUNT([Year])
			FROM @Holidays
			WHERE [Year] = @CURRENTYEAR

			SET @POS = 1

			WHILE @POS <= @CNTR
			BEGIN
				SELECT @MINDAY = MIN(DAY)
				FROM @Holidays
				WHERE
					[Year] = @CURRENTYEAR
					AND [Week] IS NULL

				UPDATE @Holidays
					SET [Week] = @POS
				WHERE
					[Year] = @CURRENTYEAR
					AND [Day] = @MINDAY

				SELECT @POS = @POS + 1
			END

			SELECT @CURRENTYEAR = @CURRENTYEAR + 1
		END

		UPDATE [dbo].[DimDate]
			SET Holiday = 'Election Day'				
		FROM [dbo].[DimDate] DT
			JOIN @Holidays HL ON (HL.DateID + 1) = DT.DateKey
		WHERE
			[Week] = 1
	END
	--set flag for USA holidays in Dimension
	UPDATE [dbo].[DimDate]
SET IsHoliday = CASE WHEN Holiday IS NULL THEN 0 WHEN Holiday IS NOT NULL THEN 1 END
/*****************************************************************************************/
-- Update FiscalYear, FiscalMonth, FiscalQuarter, MonthNameShort

  drop table if exists #FiscalData
  select      FullDate, --Year,Month,
              cast(Month as nvarchar(2)) + '/' + cast( DayOfMonth as nvarchar(2)) +'/' + cast(FY.FiscalYear as nvarchar(4)) as FiscalDate,
              FY.FiscalYear,
			  FM.FiscalMonth,
	    	  MS.MonthNameShort,
			  case when FM.FiscalMonth between 1 and 3 then 1
			       when FM.FiscalMonth between 4 and 6 then 2
				   when FM.FiscalMonth between 7 and 9 then 3
				   when FM.FiscalMonth between 10 and 12 then 4 end as FiscalQuarter

			  into #FiscalData From [BI_DWH].[dbo].[DimDate] 
			  cross apply (select case when Month between 1 and 7 then Year 
                                       when Month between 8 and 12 then Year + 1 end as FiscalYear) FY
			  cross apply (select case when Month = 8 then 1
			       when Month = 9 then 2
				   when Month = 10 then 3
				   when Month = 11 then 4
				   when Month = 12 then 5
				   when Month = 1 then 6
				   when Month = 2 then 7
				   when Month = 3 then 8
				   when Month = 4 then 9
				   when Month = 5 then 10
				   when Month = 6 then 11
				   when Month = 7 then 12 end as FiscalMonth) as FM
				cross apply (select case when Month = 8 then 'Aug'
			       when Month = 9 then 'Sep'
				   when Month = 10 then 'Oct'
				   when Month = 11 then 'Nov'
				   when Month = 12 then 'Dec'
				   when Month = 1 then 'Jan'
				   when Month = 2 then 'Feb'
				   when Month = 3 then 'Mar'
				   when Month = 4 then 'Apr'
				   when Month = 5 then 'May'
				   when Month = 6 then 'Jun'
				   when Month = 7 then 'Jul' end as MonthNameShort) as MS

			  
			 
update DD set DD.FiscalDate = FD.FiscalDate, DD.FiscalYear= FD.FiscalYear , DD.FiscalMonth = FD.FiscalMonth, DD.MonthNameShort = FD.MonthNameShort,DD.FiscalQuarter = FD.FiscalQuarter
 from dbo.DimDate DD inner join #FiscalData FD on DD.FullDate = FD.FullDate



drop table if exists #PriorData
  select      FullDate, --Year,Month,
			  [Year] - 1 AS PriorYear,
			  CASE WHEN [Month] = 1 THEN 12 ELSE FiscalMonth - 1 end AS PriorMonth, -- Add year later
			  DATENAME(MM, (DATEADD(MONTH,-1,FullDate))) AS PriorMonthName, -- Add year later
			  MonthNameShort +'-'+ CAST([Year] - 1 AS CHAR(4)) AS PriorMonthYear,
			  FiscalYear - 1 AS PriorFiscalYear,
			  CASE WHEN FiscalMonth = 1 THEN 12 ELSE FiscalMonth - 1 end AS PriorFiscalMonth   ,
			  DATENAME(MM, (DATEADD(MONTH,-1,FiscalDate))) AS PriorFiscalMonthName  ,
			  MonthNameShort +'-'+ CAST(FiscalYear - 1 AS CHAR(4)) AS PriorFiscalMonthYear  
		        
			  into #PriorData
FROM [BI_DWH].[dbo].[DimDate] 
			  
update DD 
SET DD.PriorYear = FD.PriorYear, DD.PriorMonth= FD.PriorMonth , DD.PriorMonthName = FD.PriorMonthName, 
DD.PriorMonthYear = FD.PriorMonthYear,DD.PriorFiscalYear = FD.PriorFiscalYear, DD.PriorFiscalMonth = FD.PriorFiscalMonth,
DD.PriorFiscalMonthName  = FD.PriorFiscalMonthName , 
DD.PriorFiscalMonthYear = FD.PriorFiscalMonthYear
from dbo.DimDate DD inner join #PriorData FD on DD.FullDate = FD.FullDate







End


GO

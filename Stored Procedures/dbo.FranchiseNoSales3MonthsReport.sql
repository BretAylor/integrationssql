SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      2/26/2019
-- Description  franchises with no sales for the past 3 months 

-- Copyright © 2019, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History

-------------------------------------------------------------------------------
-- exec [dbo].[FranchiseNoSales3MonthsReport]



CREATE Procedure [dbo].[FranchiseNoSales3MonthsReport]
AS
Begin

DECLARE @Date DATE
DECLARE @StartDate int
SET @Date = DATEADD(Day,-90, GETDATE())
SET @StartDate = YEAR(@Date) * 10000 + MONTH(@Date) * 100 + DAY(@Date)



SELECT 
       CAST(@Date AS nvarchar(20))+ ' - '+ CAST(cast (GETDATE() AS DATE) AS NVARCHAR(20)) AS Duration,
	   EM.SalesRepresentativeNum,
       EM.SalesRepresentativeName,
       SUM(ISNULL(FI.NetSales, 0)) AS Sales,
	   GETDATE() AS DateAdded
FROM dbo.DimEmployee EM
    LEFT OUTER JOIN [BI_DWH].[dbo].[FactInvoiceFranchise] FI
        ON EM.EmployeeKey = FI.EmployeeKey  AND FI.DateKey >= @StartDate
    LEFT OUTER JOIN dbo.DimDate DD
        ON FI.DateKey = DD.DateKey
          
WHERE  EM.TerminationDateYear = 0 AND SalesRepresentativeName NOT LIKE '%P21%' AND EM.SalesRepresentativeName NOT LIKE '%test%' AND EM.SalesRepresentativeName <> 'DO NOT USE PER IT' AND EM.SalesRepresentativeName <> 'Not Being Used'
GROUP BY EM.SalesRepresentativeNum,
         EM.SalesRepresentativeName
HAVING SUM(ISNULL(FI.NetSales, 0)) = 0
ORDER BY EM.SalesRepresentativeNum

End


GO

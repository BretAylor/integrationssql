SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[RemoveExpiredCreditCards]
as
Begin

delete from [172.25.6.17].[PEPPERI_STAGING].[dbo].[ExistingCustomerCC] where GUID in (select GUID
from  [172.25.6.17].[PEPPERI_STAGING].[dbo].[ExistingCustomerCC] where CAST(DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, LEFT(RIGHT('0000'+ExpirationDate,4), 2)+'/01/'+'20'+Right(ExpirationDate,2)) + 1, 0)) AS DATE) < CAST(CAST(month(getdate()) as varchar(2))+'/1/'+cast(year(getdate()) as varchar(4)) AS DATE)
)
delete from [172.25.6.17].CC_STAGING.dbo.CustomerCC 
WHERE CAST(DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, LEFT(RIGHT('0000'+REPLACE(ExpirationDate,'/',''),4), 2)+'/01/'+'20'+Right(REPLACE(ExpirationDate,'/',''),2)) + 1, 0)) AS DATE) < CAST(CAST(month(getdate()) as varchar(2))+'/1/'+cast(year(getdate()) as varchar(4)) AS DATE)

END;


GO

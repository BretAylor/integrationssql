SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      2/26/2019
-- Description  Monitor Walk on, top 20 and buttom 20 Franchise sales

-- Copyright © 2019, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History

-------------------------------------------------------------------------------
-- exec FranchiseSalesGroupsReport


CREATE PROCEDURE [dbo].[FranchiseSalesGroupsReport]
AS
BEGIN

SELECT 
     
	  AG.Type,
	  DD.FiscalYear,
      DD.FiscalMonth,
	  DD.MonthName,
	  EM.SalesRepresentativeNum,
	  EM.SalesRepresentativeName,
	  SUM(INF.NetSales) AS NetSales ,
	  GETDATE() AS DateAdded

FROM BI_DWH.[dbo].[FactInvoiceFranchise] INF
    INNER JOIN BI_DWH.dbo.DimDate DD
        ON INF.DateKey = DD.DateKey
	INNER JOIN FranchiseSalesAllGroups_View AG 
	    ON AG.FiscalYear = DD.FiscalYear AND INF.EmployeeKey = AG.EmployeeKey
	INNER JOIN BI_DWH.dbo.DimEmployee EM
        ON EM.EmployeeKey = INF.EmployeeKey
 WHERE DD.FiscalYear >= 2018
 GROUP BY 
      DD.FiscalYear,
      DD.FiscalMonth,
	  DD.MonthName,
	  EM.SalesRepresentativeNum,
	  EM.SalesRepresentativeName,
	  AG.Type
ORDER BY AG.Type,DD.FiscalYear,DD.FiscalMonth,NetSales desc
END


GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FactScoreCardTargetsP21Load]
AS
BEGIN

TRUNCATE TABLE FactScoreCardTargetsP21
INSERT INTO FactScoreCardTargetsP21 (ScorecardGroup, BranchKey,EmployeeKey,LocationId, SalesRepID, MtdTotal,mtdGoal,MthGoal,YtdTotal,YtdGoal,YthGoal,DateAdded)
SELECT SC.ScorecardGroup, BP.BranchKey, DE.EmployeeKey,SC.LocationId, SC.SalesRepID, MtdTotal$,mtdGoal$,MthGoal$,YtdTotal$,YtdGoal$,YthGoal$,GETDATE()
 FROM BI_STAGING.dbo.ScoreCardTargetsP21 SC
    LEFT OUTER JOIN BI_DWH.dbo.DimEmployeeP21 DE
        ON CAST(SC.SalesRepID AS NVARCHAR(20)) = CAST(DE.SalesRepID AS NVARCHAR(20))
	LEFT OUTER JOIN [BI_DWH].[dbo].[DimBranchP21] BP 
		ON BP.BranchId = SC.locationId


END 

GO

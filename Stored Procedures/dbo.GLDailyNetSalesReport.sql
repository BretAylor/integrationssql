SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      2/6/2019
-- Description  Net sales by Month and Fiscal Year GL Summary report

-- Copyright © 2019, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History

-------------------------------------------------------------------------------
-- exec GLDailyNetSalesReport

CREATE PROCEDURE [dbo].[GLDailyNetSalesReport]
AS 
BEGIN

SELECT 
       DD.[Date] AS TransactionDate,
       GLPostingDate,
       PostingYear,
	   PostingPeriod,
	   PostingMonth,
	   SUM([TransactionAmount]) AS Amount,
       DateAdded
FROM BI_DWH.dbo.FactGLDailyNetSales GL LEFT OUTER JOIN DimDate DD ON GL.TransactionDateKey = DD.DateKey 
WHERE GL.PostingYear >= 2018
GROUP BY DD.[Date],
       GLPostingDate,
       PostingYear,
	   PostingPeriod,
	   PostingMonth,
	   DateAdded

END
GO

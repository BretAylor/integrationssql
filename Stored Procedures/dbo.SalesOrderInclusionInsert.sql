SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		baylor
-- Create date: 5/2020
-- Description:	Reset Sales Orders
-- =============================================
create PROCEDURE [dbo].[SalesOrderInclusionInsert]  @PepperiOrderNbr bigint
AS

BEGIN

SET NOCOUNT ON;

/****** Script for SelectTopNRows command from SSMS  ******/
Insert [dbo].[SalesOrderInclusionList] (PepperiOrderNbr)
Select @PepperiOrderNbr 

END
GO

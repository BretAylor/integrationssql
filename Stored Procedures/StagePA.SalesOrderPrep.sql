SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		baylor
-- Create date: 05/20
-- Description:	Sale Order Prep Mainline
-- =============================================
CREATE PROCEDURE [StagePA].[SalesOrderPrep] 

AS
BEGIN

SET NOCOUNT ON;

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

-- history does not work. if no orders processed this run, then it will process last run
TRUNCATE TABLE [StagePA].[SalesOrderToProcess]
TRUNCATE TABLE [StagePA].[SalesOrderLineNbr]
TRUNCATE TABLE [StagePA].[SalesOrderLineFormatted]
TRUNCATE TABLE [StagePA].[SalesOrderLineKitFormatted]
TRUNCATE table [StagePA].[SalesOrderCommentnbr]



EXEC [StagePA].[SalesOrderSelectToProcess] 
-- if table [StagePA].[SalesOrderToProcess] = 0 then send a return code

-- AS4000 cannot process 9 digit Order Numbers
UPDATE t2
SET t2.AS400OrderNbr = 
CASE WHEN t2.PepperiOrderNbr < 100000000
THEN t2.PepperiOrderNbr
ELSE T2.PepperiOrderNbr - 100000000 end
FROM StagePa.SalesOrderToProcess t1
JOIN SalesOrder t2
ON t1.pepperiordernbr = t2.PepperiOrderNbr

UPDATE t2
SET t2.[AS400OrderNbr] = 
CASE WHEN t2.PepperiID < 100000000
THEN t2.PepperiID
ELSE T2.PepperiID - 100000000 end
FROM StagePa.SalesOrderToProcess t1
JOIN [dbo].[SalesOrderLine] t2
ON t1.pepperiordernbr = t2.PepperiID


EXEC [dbo].[ExtractSalesOrderExclude]
EXEC [StagePA].[SalesOrderLineNumbering]
EXEC [StagePA].[SalesOrderCommentNumbering]
EXEC [StagePA].[SalesOrderFormat]
EXEC [StagePA].[SalesOrderLineFormat] 
EXEC [StagePA].[SalesOrderLineKitFormat] 

END

GO

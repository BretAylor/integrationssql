SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- Logic Changes:
-- 5/8/2020
-- 1) Don't Update Kit Sales order lines
-- 2) Check for Sales Order - processed
-- =============================================
CREATE PROCEDURE [dbo].[ExtractSalesOrderLineSync] 

AS
BEGIN

SET NOCOUNT ON;

DECLARE @RunDate DATETIME = (SELECT MAX(createdDate) FROM StagePA.SalesOrderToProcess)

Update t1
set ErpSyncDate = getdate(),
erpSyncby = SUSER_SNAME()
from StagePA.SalesOrderToProcess t2
JOIN SalesOrderLine t1
on t1.PepperiId = t2.PepperiOrderNbr
where t2.CreatedDate = @RunDate
AND t1.ParentItemNumber is null  -- Non-Kits


END
GO

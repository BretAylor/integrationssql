SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      1/24/2019
-- Purpose      Load FactSpeedyCleanNetSales table with product, employee, customer and net sales
-- Copyright © 2019, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- EXEC [dbo].[FactSpeedyCleanNetSalesLoad]
CREATE PROCEDURE [dbo].[FactSpeedyCleanNetSalesLoad]
AS
BEGIN

TRUNCATE TABLE [dbo].[FactSpeedyCleanNetSales]
INSERT INTO [dbo].[FactSpeedyCleanNetSales] (DateKey, ProductKey,  EmployeeKey,	CustomerKey, ShipmentKey, InvoiceNo, [LINENO], UnitPrice,	QtyShipped, WinzerCost, FranchiseCost,  NetSales, DateAdded)


SELECT DD.DateKey,
       PR.ProductKey,
       EM.EmployeeKey,
	   CU.CustomerKey,
	   ST.ShipmentKey,
	   IL.InvoiceNo,
	   IL.[LINENO],
	   IL.UnitPrice,
	   IL.QtyShipped,
	   ROUND(WI.InvoiceLineUnitWinzerCost * IL.QtyShipped, 5) AS WinzerCost,
       ROUND(WI.InvoiceLineUnitOvrdFranchiseCost * IL.QtyShipped, 5) AS FranchiseCost,
       ROUND(IL.UnitPrice * IL.QtyShipped,2) AS [NetSales],
	   GETDATE() AS DateAdded
FROM BI_STAGING.dbo.InvoiceLineP21 IL
    INNER JOIN BI_STAGING.dbo.InvoiceP21 IH
        ON IH.InvoiceNo = IL.InvoiceNo
	INNER JOIN BI_DWH.dbo.DimDate DD
        ON DD.Date = CAST(IH.DateCreated AS DATE)
	LEFT OUTER JOIN BI_STAGING.[dbo].[WzrInvoiceLineP21] WI
        ON WI.InvoiceLineInvoiceId = IL.InvoiceNo
           AND WI.InvoiceLineId = IL.[LineNo]
    LEFT OUTER JOIN BI_DWH.dbo.DimCustomerP21 CU
        ON CU.CustomerID = IH.CustomerID
    LEFT OUTER JOIN BI_DWH.dbo.DimEmployeeP21 EM
        ON IH.SalesRepID = EM.SalesRepID
    LEFT OUTER JOIN BI_DWH.dbo.DimProductP21 PR
        ON PR.ItemId = IL.ItemId
    LEFT OUTER JOIN dbo.DimShipToP21 ST 
	    ON ST.NbShipToNumber = IH.ShipToNum AND ST.NbShipToCustomerId = IH.CustomerID
WHERE DD.FiscalYear >= 2018
      AND IH.CompanyNo = '1'
      AND IH.LocationId = 1052
      AND IL.OtherChargeItem = 'N'
      AND IL.InvMastUid IS NOT NULL
      AND IL.InvMastUid <> 0

END


GO

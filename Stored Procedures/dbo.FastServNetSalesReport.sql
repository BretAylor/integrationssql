SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      2/6/2019
-- Description  Net sales by franchise, day, customer, product FastServ report

-- Copyright © 2019, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History

-------------------------------------------------------------------------------
-- exec FastServNetSalesReport

CREATE PROCEDURE [dbo].[FastServNetSalesReport]
AS
BEGIN

SELECT DD.[Year],
	   DD.FiscalYear,
       DD.FiscalMonth,
	   DD.[MonthName],
	   DD.[Date],
	   CU.CustomerName,
	   CU.CustomerId AS CustomerNum,
	   PR.ItemDesc AS ProductDescription,
	   PR.ItemId AS ProductNum,
	   ISNULL(EM.FirstName,'') + ' ' +  ISNULL(EM.Middle,'') + ' ' + ISNULL(EM.LastName,'') as SalesRepresentativeName,
	   EM.SalesRepId AS SalesRepresentativeNum,
	   BR.BranchID,
	   BR.BranchDescription,
	   FS.InvoiceNo,
	   FS.OrderNo,
       FS.[LineNo],
	   FS.QtyShipped AS QuantityShipped,
	   FS.UnitPrice,
       FS.NetSales ,
	   FS.WinzerCost,
	   FS.FranchiseCost,
	   FS.DateAdded
	   
FROM BI_DWH.[dbo].[FactFastServNetSales] FS
    INNER JOIN BI_DWH.dbo.DimDate DD
        ON FS.DateKey = DD.DateKey
    LEFT OUTER JOIN  BI_DWH.dbo.DimCustomerP21 CU
        ON CU.CustomerKey = FS.CustomerKey
    LEFT OUTER JOIN BI_DWH.dbo.DimProductP21 PR
        ON PR.ProductKey = FS.ProductKey
    LEFT OUTER JOIN BI_DWH.dbo.DimEmployeeP21 EM
        ON EM.EmployeeKey = FS.EmployeeKey
	LEFT OUTER JOIN BI_DWH.dbo.DimBranchP21 BR 
	    ON FS.BranchKey = BR.BranchKey
WHERE DD.FiscalYear >= 2018

ORDER BY DD.[Date] ASC;

END
GO

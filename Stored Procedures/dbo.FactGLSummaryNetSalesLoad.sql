SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------
-- Author       Saad Benhalima
-- Created      1/25/2019
-- Description  GL Summary Net sales by fiscal year, account and month

-- Copyright © 2019, WINZER, All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History

-------------------------------------------------------------------------------
-- exec [dbo].[FactGLSummaryNetSalesLoad]
CREATE PROCEDURE [dbo].[FactGLSummaryNetSalesLoad]
AS
BEGIN

TRUNCATE TABLE dbo.FactGLSummaryNetSales
INSERT INTO dbo.FactGLSummaryNetSales
(
    ChartOfAccountKey,
    fiscalyear,
    companynum,
    GlAccountNum,
    [Aug],
    [Sep],
    [Oct],
    [Nov],
    [Dec],
    [Jan],
    [Feb],
    [Mar],
    [Apr],
    [May],
    [Jun],
    [Jul],
	DateAdded
)
SELECT chartofaccountkey,
       fiscalyear,
       companynum,
       replace(RTRIM(GlAccountNum),' ','') AS GlAccountNum,
       SUM(-1 * DebitAmountPeriod01) + SUM(CreditAmountPeriod01) AS [Aug],
       SUM(-1 * DebitAmountPeriod02) + SUM(CreditAmountPeriod02) AS [Sep],
       SUM(-1 * DebitAmountPeriod03) + SUM(CreditAmountPeriod03) AS [Oct],
       SUM(-1 * DebitAmountPeriod04) + SUM(CreditAmountPeriod04) AS [Nov],
       SUM(-1 * DebitAmountPeriod05) + SUM(CreditAmountPeriod05) AS [Dec],
       SUM(-1 * DebitAmountPeriod06) + SUM(CreditAmountPeriod06) AS [Jan],
       SUM(-1 * DebitAmountPeriod07) + SUM(CreditAmountPeriod07) AS [Feb],
       SUM(-1 * DebitAmountPeriod08) + SUM(CreditAmountPeriod08) AS [Mar],
       SUM(-1 * DebitAmountPeriod09) + SUM(CreditAmountPeriod09) AS [Apr],
       SUM(-1 * DebitAmountPeriod10) + SUM(CreditAmountPeriod10) AS [May],
       SUM(-1 * DebitAmountPeriod11) + SUM(CreditAmountPeriod11) AS [Jun],
       SUM(-1 * DebitAmountPeriod12) + SUM(CreditAmountPeriod12) AS [Jul],
	   GETDATE() AS DateAdded
FROM BI_DWH.[dbo].[DimChartOfAccount] coa
    INNER JOIN BI_STAGING.[dbo].[GLSummaryTotal] gl
        ON coa.InternalGlAcctNo = gl.InternalGlAcctNo
WHERE 
      GLAccountNum IN ( '00160004450000', '00160004800000', '00160004806000', '00160004807000', '00160004815000',
                            '00160004820000', '00160004825000', '00195004950000', '00195004999000', '00160004800000',
                            '00160004820000', '00160004890000'
                          )
GROUP BY CompanyNum,
         FiscalYear,
         GlAccountNum,
         Description1,
         coa.ChartOfAccountKey
ORDER BY CompanyNum,
         coa.ChartOfAccountKey ASC;

end
GO

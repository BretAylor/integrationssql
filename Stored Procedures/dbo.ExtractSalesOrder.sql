SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ExtractSalesOrder] 

AS
BEGIN

SET NOCOUNT ON;


SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

DECLARE @RunDate DATETIME = (SELECT MAX(createddate) FROM [StagePA].[SalesOrderToProcess])

select
      CAST(ISNULL(RIGHT('00000' + Cast([RepNbr] as varchar(20)),5),'') AS Varchar(5)) as RepNbrPad,
 CustomerNbr CustomerNumber,
  case when Cu.TerritoryNumber = 0  or Cu.TerritoryNumber is null  then  CAST(SR.TerritoryNum AS VARCHAR(3)) else  CAST(Cu.TerritoryNumber AS VARCHAR(3)) end as TerritoryNum, -- Added and mapped
      case when isnull(Cast(ST.DefaultWarehouse as varchar(2)),'') = '' then isnull(Cast(CU.DefaultWarehouse as varchar(2)),'') else isnull(Cast(ST.DefaultWarehouse as varchar(2)),'')  end   as ShipWarehouse, -- Added and mapped
      ISNULL(RepNbr ,0) as RepNbr,
      so.AS400OrderNbr as PepperiOrderNbr,  
      convert(varchar(8),cast(OrderDate as date),112)  OrderDate,
      convert(varchar(8),cast(RequestedDate as date),112)  RequestedDate ,
      CAST(ISNULL(PONbr,'') AS Varchar(22)) AS   PONbr , 
   CAST(ISNULL(left(ShipVia ,5),'') AS Varchar(5)) as ShipVia , 
   ISNULL(TermCode ,'') as  TermCode,
    ISNULL(LEFT(cast(Comment as varchar(max)),62),'') as Comment  , 
   case when ISNULL(DisplayComment,'') = 'N' or ISNULL(DisplayComment,'') = ' ' then 'X' else ISNULL(DisplayComment,'') end as  DisplayComment,
   ISNULL(so.OrderType,'') as  OrderType , 
      ISNULL(Ordertotal,0) as  Ordertotal,
   ISNULL(ShipToNbr,0) as ShipToNbr,
   ISNULL(ShipToContact ,'') as ShipToContact,
   ISNULL(left(ShipToName,30),'') as ShipToName ,
   ISNULL(ShipToAddress1,'') as ShipToAddress1 ,
   ISNULL(ShipToAddress2,'') as ShipToAddress2 ,
   ISNULL(ShipToCity ,'') as ShipToCity ,
   ISNULL(ShipToState,'') as  ShipToState ,
   ISNULL(ShipToZip,'') as ShipToZip  , 
--   cast(replace(replace(ISNULL(right(ShipToPhone,10),''),'-',''),' ','') as varchar(16) )as ShipToPhone,
cast(replace(replace(replace(ISNULL(right(ShipToPhone,10),''),'-',''),' ',''),'.','') as decimal(10,0) )as ShipToPhone ,  
   ISNULL(ShipToExt,0) as ShipToExt  ,
   ISNULL(so.TaxCodeFlag,'') as  TaxCodeFlag ,
   isnull(isnull(left(AllowBackorders,1),cu.AcceptBackorders),'') AllowBackorders ,
   case when ISNULL(left(HoldReason,2),'') = '' then 'N' else 'Y' end  as HoldFlag, -- Added and not mapped
   ISNULL(left(HoldReason,2),'') as HoldReason  ,
   isnull(ChargeFreightFlag,'') as ChargeFreightFlag, -- Added and needs to be mapped and Lior need to map it
  case when isnull(left(FreightCode,3),'')  in  ('FFF','FSF') then 'CFF' else isnull(left(FreightCode,3),'') end  as FreightCode  ,
   case when isnull(SuppressPicklistPrice,0) = 0 then 'N' else 'Y' end SuppressPicklistPrice , -- to check with James on Y/N and it is mapped
   '' as  OrderStatus , -- set to blank
   CAST(ISNULL(CU.CustomerName,'') AS VARCHAR(30)) AS OrderBillToName, 
      isnull(cast(CU.PurchasingContact as varchar(25)),'') as BillToContactName, 
      CAST(ISNULL(CU.AddressLine1,'') AS VARCHAR(30)) as OrderBillToAddress1, 
      CAST(ISNULL(CU.AddressLine2,'')  AS VARCHAR(30)) AS OrderBillToAddress2, 
      CAST(ISNULL(CU.City,'')   AS VARCHAR(20)) as OrderBillToCity, 
      CAST(ISNULL(CU.State,'')   AS VARCHAR(2)) as OrderBillToState, 
      CAST(ISNULL(CU.ZipCode,'')   AS VARCHAR(10)) as BillToZipCode, 
      isnull(CU.PurchasingPhoneNumber,0) as BillToPhoneNumber, 
   isnull(CU.PurchasingPhoneExtension,0) as BillToPhoneExt, 
      isnull(CU.PurchasingFax,0) as FaxNumber, 
   ISNULL(PaymentMethod ,'') as PaymentMethod ,
   '' AS InvoiceClient  ,
   ISNULL(SplitFreightAmount ,0) as SplitFreightAmount  ,
   0 as TRACNT ,
   isnull(left(EC.CreditCardNum,18),'') as CreditCardNumber, -- to add later
    isnull(EC.ExpirationDate,0)  as CreditCardExpDate, -- to add later
   isnull(left(EC.CreditCardHolder,25),'') as CreditCardHolder, -- to add later
   0 as PadMarginPlacehold, -- default to 0 
   0 as SentDatePlacehold, -- default to 0 
      0 as ObsolDollarDiscount, -- default to 0 
      0 as ObsolPercentDiscount, -- default to 0 
   '' as ItemDescriptionPrintInstructions, -- default to blank
      '' as OrderRequiresBillOnlyScan, -- default to blank
      ''as User2alpha ,-- default to blank
   case when FreightCode = 'CSF' then 'SPLIT' else '' end as UserRequestedFreightType , 
      0 as UploadStatusNumericCode 
	  ,isnull(TopComment,'') TopComment
	  ,isnull(DisplayTopComment,'') DisplayTopComment
	  ,isnull(BottomComment,'') BottomComment
	  ,isnull(DisplayBottomComment,'') DisplayBottomComment
FROM [StagePA].[SalesOrderToProcess] t1
JOIN SalesOrder SO  
ON t1.pepperiordernbr = so.PepperiOrderNbr
left outer join SalesRep SR  
on SO.RepNbr = SR.[SalesRepresentativeNum] 
LEFT OUTER JOIN dbo.Customer CU  
ON (CU.CustomerNumber = ISNULL(SO.CustomerNbr,0) 
or CU.PepperiCustomerNumber = ISNULL(SO.CustomerNbr,0))
LEFT OUTER JOIN dbo.shipto ST  
on ST.ShipToNumber = CU.DefaultShipTo 
and ST.CustomerNumber = CU.CustomerNumber
left outer join CreditCardAll EC  
on SO.PepperiCreditCardId =  EC.Guid 
--and cast(SO.CustomerNbr as [varchar](255)) = cast(EC.CustomerNumber as [varchar](255))  07/23/2020
WHERE t1.createddate = @Rundate

END
GO

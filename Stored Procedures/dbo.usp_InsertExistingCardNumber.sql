SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*************************************************************   
**************************************************************             
** Change History             
**************************************************************
** PR Date        Author            Change Description              
** -- ----------  ---------------   --------------------------
** 1  8/8/2019  Saad Benhalima		Initial development

**************************************************************
exec [usp_InsertExistingCardNumber] 
**************************************************************/
CREATE PROCEDURE [dbo].[usp_InsertExistingCardNumber]

AS
	BEGIN


	             --load ExistingCustomerCC into temp table
				drop table if exists #TempExistingCustomerCC
		        select * into #TempExistingCustomerCC from [WINSQLBI01].Pepperi.dbo.[ExistingCustomerCC] where IsNew = 1
				-- delete data from ExistingCustomerCC
				--truncate table [PEPPERI_STAGING].dbo.[ExistingCustomerCC] 
				-- Load encypted credit card numbers
				DECLARE @NewCardId UNIQUEIDENTIFIER = NEWID();
				OPEN SYMMETRIC KEY CC_Key_Symmetric  
				DECRYPTION BY CERTIFICATE CCCertificate;  

				INSERT INTO [PEPPERI_STAGING].dbo.[ExistingCustomerCC] ([Guid], CCDescription, CCType, CCNumber,LastFourDigits, CVVNumber, ExpirationDate, 
										  CustomerNumber, CustomerName, ZipCode, Note, KeepOnFile)

					
				select NewCardId.Guid, CCDescription, CCType, EncryptByKey(Key_GUID('CC_Key_Symmetric'), CCNumber, 1, HashBytes('SHA2_512', CONVERT( varbinary, NewCardId.Guid))) as CCNumber,
				LastFourDigits, CVVNumber,  ExpirationDate, 
				CustomerNumber, CustomerName, ZipCode, Note, KeepOnFile from #TempExistingCustomerCC
				cross apply (select NEWID() as Guid) as NewCardId 
				
				

	
	END


GO

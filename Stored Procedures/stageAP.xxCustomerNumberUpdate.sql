SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		baylor
-- Create date: 4/2020
-- Description:	Update customer will Perm customernumber
-- =============================================
CREATE PROCEDURE [stageAP].[xxCustomerNumberUpdate] 
AS

BEGIN

SET NOCOUNT ON;

update  cu
set CustomerNumber = ct.CustomerNumber
,ModifiedDate = getdate()   
,Modifiedby = SUSER_SNAME()
from stageAP.CustomerTempNumber ct
join [WUSA-SQL01-DFW].Pepperi_Staging.[dbo].[Customer] cu 
on ct.TempCustomerNumber = cu.[CustomerNumber] 
and ct.SalesRepNumber  = cu.SalesRepNumber
where cu.customerNumber <> ct.CustomerNumber


IF @@RowCount > 0
	EXEC msdb.dbo.sp_send_dbmail 
	@profile_name = 'Account'
	, @recipients = 'Bret.Aylor@winzerusa.com'
    , @from_address = 'WinzerAutomatedEmail@winzerusa.com'
	, @body = 'Customer Number Updated' 
       , @body_format = 'HTML'
	, @subject = 'Pepperi_Staging.[StageAP].[CustomerNumberUpdate] Customer Number Updated' 
	--, @file_attachments = @fullFileName; --YOU CAN ALSO ATTACH A FILE TO THE MAIL IF NEED BE.


END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
-- EXEC [dbo].[FranchiseRemunerationValidation] 
CREATE PROCEDURE [dbo].[FranchiseRemunerationValidation] 
AS
BEGIN

DECLARE @Count INT
SET @Count= 0

DECLARE @Period int
DECLARE @TodayDate int
DECLARE @Average int

SELECT @Period = MIN(PeriodDate) FROM FranchiseRemunerationPeriod
SELECT @TodayDate = CAST(CAST(YEAR(GETDATE()) AS VARCHAR(4))+ RIGHT('00'+CAST(MONTH(GETDATE()) AS VARCHAR(2)),2) + RIGHT('00'+CAST(DAY(GETDATE()) AS VARCHAR(2)),2) AS NUMERIC(8,0))

IF( @Period = @TodayDate )
begin
select   @Average = AVG(Total)   FROM (
SELECT  Processdt, count(distinct Repno)  as Total
  FROM [172.25.6.17].[PEPPERI_STAGING].[dbo].[FranchiseRemunerationReport]  where Processdt NOT IN (SELECT * FROM FranchiseRemunerationPeriod)
  group by Processdt) as a

SELECT @Count = CASE WHEN COUNT(DISTINCT RepNo) < @Average - 10 THEN 1 
            WHEN COUNT(DISTINCT RepNo) > @Average + 10 THEN 1 
		ELSE 0  END 
		
 FROM [172.25.6.17].[PEPPERI_STAGING].[dbo].[FranchiseRemunerationReport]  where Processdt   IN (SELECT * FROM FranchiseRemunerationPeriod)
END 

IF @Count = 1 
BEGIN

	EXEC msdb.dbo.sp_send_dbmail 
	@profile_name = 'Account'
	, @recipients = 'saad.benhalima@winzerusa.com'
    , @from_address = 'WinzerAutomatedEmail@winzerusa.com'
	--, @body = @tableHTML
       , @body_format = 'HTML'
	, @subject = 'Franchise Remuneration report needs validation'
	--, @file_attachments = @fullFileName; --YOU CAN ALSO ATTACH A FILE TO THE MAIL IF NEED BE.
    end

END
GO

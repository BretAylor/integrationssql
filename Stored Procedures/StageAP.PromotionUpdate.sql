SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		baylor
-- alter date: 8/2020
-- Description:	Update to Pepperi_Staging 
-- =============================================
CREATE PROCEDURE [StageAP].[PromotionUpdate] 
AS

BEGIN

SET NOCOUNT ON;

DECLARE @RunDate DATETIME = GETDATE()

UPDATE t1
SET PromotionDescription = t2.PromotionDescription
,PromotionStartDateNumeric = t2.[PromotionStartDateNumeric]
,PromotionStartDate = CAST(SUBSTRING(CAST(t2.PromotionStartDateNumeric AS VARCHAR(20)),5,2) + '/' +
			SUBSTRING(CAST(t2.PromotionStartDateNumeric AS VARCHAR(20)),7,2) + '/' +
			SUBSTRING(CAST(t2.PromotionStartDateNumeric AS VARCHAR(20)),1,4) AS DATE) 
,PromotionEndDateNumeric = t2.PromotionEndDateNumeric
,PromotionEndDate = CAST(SUBSTRING(CAST(t2.PromotionEndDateNumeric AS VARCHAR(20)),5,2) + '/' +
			SUBSTRING(CAST(t2.PromotionEndDateNumeric AS VARCHAR(20)),7,2) + '/' +
			SUBSTRING(CAST(t2.PromotionEndDateNumeric AS VARCHAR(20)),1,4) AS DATE) 
,PromotionFlyerPath = t2.PromotionFlyerPath
,PromotionStatus = t2.PromotionStatus
,CreationTimestamp = t2.CreationTimestamp
,DateLastMaintained = t2.DateLastMaintained
,Deleted = 0
,[ModifiedDate] = @RunDate
FROM  [dbo].[Promotion]  t1
JOIN StageAP.Promotion t2
ON t1.PromotionCode = t2.PromotionCode
WHERE t1.[DateLastMaintained] <  t2.DateLastMaintained
OR t1.Deleted = 1


INSERT INTO [dbo].[Promotion]
           ([PromotionCode]
           ,[PromotionDescription]
           ,[PromotionStartDateNumeric]
           ,[PromotionStartDate]
           ,[PromotionEndDateNumeric]
           ,[PromotionEndDate]
           ,[PromotionFlyerPath]
           ,[PromotionStatus]
           ,[CreationTimestamp]
		   ,[DateLastMaintained]
		   ,CreatedDate
		   ,ModifiedDate
)
SELECT		t1.[PromotionCode]
           ,t1.[PromotionDescription]
           ,t1.[PromotionStartDateNumeric]
           ,CAST(SUBSTRING(CAST(t1.PromotionStartDateNumeric AS VARCHAR(20)),5,2) + '/' +
			SUBSTRING(CAST(t1.PromotionStartDateNumeric AS VARCHAR(20)),7,2) + '/' +
			SUBSTRING(CAST(t1.PromotionStartDateNumeric AS VARCHAR(20)),1,4) AS DATE) [PromotionStartDate]
           ,t1.[PromotionEndDateNumeric]
           ,CAST(SUBSTRING(CAST(t1.PromotionEndDateNumeric AS VARCHAR(20)),5,2) + '/' +
			SUBSTRING(CAST(t1.PromotionEndDateNumeric AS VARCHAR(20)),7,2) + '/' +
			SUBSTRING(CAST(t1.PromotionEndDateNumeric AS VARCHAR(20)),1,4) AS DATE) [PromotionEndDate]
           ,t1.[PromotionFlyerPath]
           ,t1.[PromotionStatus]
           ,t1.[CreationTimestamp]
           ,t1.[DateLastMaintained]
		   ,@RunDate CreatedDate
		   ,@RunDate ModifiedDate
FROM StageAP.Promotion t1
LEFT JOIN dbo.Promotion	 t2
ON t1.PromotionCode = t2.PromotionCode
WHERE t2.PromotionCode IS NULL

-- Delete 
UPDATE t1
SET Deleted = 1,
ModifiedDate = @Rundate
FROM [dbo].[Promotion] t1
LEFT JOIN [StageAP].[Promotion] t2
ON t1.PromotionCode = t2.PromotionCode
WHERE t2.PromotionCode IS NULL

END
GO

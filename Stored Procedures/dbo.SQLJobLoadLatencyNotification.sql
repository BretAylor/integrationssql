SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Declare Variables for HTML
*/
-- exec [dbo].[SQLJobLoadLatencyNotification]
CREATE PROCEDURE [dbo].[SQLJobLoadLatencyNotification]
AS
Begin
DECLARE @Style NVARCHAR(MAX)= '';
 
/*
Define CSS for html to use
*/
SET @Style += +N'<style type="text/css">' + N'.tg  {border-collapse:collapse;border-spacing:0;border-color:#aaa;}'
    + N'.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#333;background-color:#fff;}'
    + N'.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#fff;background-color:#f38630;}'
    + N'.tg .tg-9ajh{font-weight:bold;background-color:#ADD8E6}' + N'.tg .tg-hgcj{font-weight:bold;text-align:center}'
    + N'</style>';
 

 
/*
Declare Variables for DML
*/
 
--DECLARE @ReportingPeriodStart DATE;
--DECLARE @ReportingPeriodEnd DATE;
 
 -- Reporting date will from the beginning of the current week
--SET @ReportingPeriodStart = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(YEAR, -3, GETDATE())), 0);
--SET @ReportingPeriodEnd = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(YEAR, -3, GETDATE())) + 1, 0);
 
/*
Declare Variables for HTML
*/
 
DECLARE @tableHTML NVARCHAR(MAX)= '';


SET @tableHTML += @Style + @tableHTML +
 --N'<H2>Order Summary For : ' + CAST(@ReportingPeriodStart AS CHAR(10)) + ' to ' + CAST(@ReportingPeriodEnd AS CHAR(10)) + '</H2>' 
	+ N'<table class="tg">' --DEFINE TABLE
/*
Define Column Headers and Column Span for each Header Column
*/
	--+ N'<tr>' 
 --   + N'<th class="tg-hgcj" colspan="2">Order Information</th>' 
	--+ N'<th class="tg-hgcj" colspan="2">Summary</th>'
	--+ N'</tr>' 
/*
Define Column Sub-Headers
*/
	+ N'<tr>'
	+ N'<td class="tg-9ajh">Job Name</td>'
    + N'<td class="tg-9ajh">Step</td>'
	+ N'<td class="tg-9ajh">Step Name</td>'
	+ N'<td class="tg-9ajh">Run Date Time</td>'
	+ N'<td class="tg-9ajh">Run Duration (Hours)</td></tr>'

/*
Define data for table and cast to xml
*/
    + CAST(( SELECT 
					
                    td = j.name,
					'',
                    td =s.step_id,
					'',
                    td =s.step_name,
                    '',
                    td =CAST(msdb.dbo.agent_datetime(run_date, run_time) AS DATETIME),
					'',
                    td =CAST(((run_duration/10000*3600 + (run_duration/100)%100*60 + run_duration%100 + 31 ) / 60) /60.0 as DECIMAL(9,2))	,
					''
		
					From msdb.dbo.sysjobs j 
					INNER JOIN msdb.dbo.sysjobsteps s 
					 ON j.job_id = s.job_id
					INNER JOIN msdb.dbo.sysjobhistory h 
					 ON s.job_id = h.job_id 
					 AND s.step_id = h.step_id 
					 AND h.step_id <> 0
					where j.enabled = 1   --Only Enabled Jobs
					AND ((run_duration/10000*3600 + (run_duration/100)%100*60 + run_duration%100 + 31 ) / 60) /60.0 >= 3
					AND  CAST(msdb.dbo.agent_datetime(run_date, run_time) AS DATE) = CAST(GETDATE() AS DATE)
					AND (DATEPART(HOUR, msdb.dbo.agent_datetime(run_date, run_time)) BETWEEN 5 AND 9 OR  DATEPART(HOUR, msdb.dbo.agent_datetime(run_date, run_time)) BETWEEN 17 AND 21)
					
			

           FOR
             XML PATH('tr') ,
                 TYPE
           ) AS NVARCHAR(MAX)) + N'</table>'
	
 

    DECLARE @Count INT
	SET @Count = 0

	SELECT @Count = COUNT(*)
    From msdb.dbo.sysjobs j 
					INNER JOIN msdb.dbo.sysjobsteps s 
					 ON j.job_id = s.job_id
					INNER JOIN msdb.dbo.sysjobhistory h 
					 ON s.job_id = h.job_id 
					 AND s.step_id = h.step_id 
					 AND h.step_id <> 0
					where j.enabled = 1   --Only Enabled Jobs
					AND ((run_duration/10000*3600 + (run_duration/100)%100*60 + run_duration%100 + 31 ) / 60) /60.0 >= 3
					AND  CAST(msdb.dbo.agent_datetime(run_date, run_time) AS DATE) = CAST(GETDATE() AS DATE)
					AND (DATEPART(HOUR, msdb.dbo.agent_datetime(run_date, run_time)) BETWEEN 5 AND 9 OR  DATEPART(HOUR, msdb.dbo.agent_datetime(run_date, run_time)) BETWEEN 17 AND 21)
	



    IF(@Count <> 0 )
	begin
	EXEC msdb.dbo.sp_send_dbmail 
	@profile_name = 'Account'
	, @recipients = 'Jack.Rahner@winzerusa.com;mark.wise@winzerusa.com;Manny.Rodriguezj@winzerusa.com;Richard.Kusse@winzerusa.com;Bret.Aylor@winzerusa.com;Vicky.Nicholson@winzerusa.com;Tina.Serpico@winzerusa.com;james.brush@winzerusa.com;lior.g@pepperi.com'
    , @from_address = 'WinzerAutomatedEmail@winzerusa.com'
	, @body = @tableHTML
       , @body_format = 'HTML'
	, @subject = 'SQL Job Load Latency (More than 3 hours) Notification'
	--, @file_attachments = @fullFileName; --YOU CAN ALSO ATTACH A FILE TO THE MAIL IF NEED BE.
    end
 END
 

GO

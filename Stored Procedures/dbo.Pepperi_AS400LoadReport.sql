SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[Pepperi_AS400LoadReport] @Table VARCHAR(100)
AS 
BEGIN
select * from (
select 'Customer' as [Table], 
        cast(CreatedDate as date) as CreatedDate, 
		--CreatedBy ,
		--CAST(ModifiedDate as date) as ModifiedDate, 
		--Modifiedby ,
		--CAST(ErpModifiedDate as date) as ErpModifiedDate, 
		--ErpModifiedBy ,
        cast(ErpSyncDate as date) as ErpSyncDate, 
		--ErpSyncBy , 
		Count(*) as TotalRecords ,
		NULL AS TotalReps,
		NULL AS TotalCustomers
from [172.25.6.17].Pepperi_Staging.dbo.Customer 
group by cast(ErpSyncDate  as date), 
         --ErpSyncBy ,
		 cast(CreatedDate as date)-- ,
		 --createdby,
		-- ErpModifiedBy ,
		-- cast(ErpModifiedDate as date)
		-- Modifiedby ,
		-- cast(ModifiedDate as date)

union all 

select 'ShipTo' as [Table], 
        cast(CreatedDate as date) as CreatedDate, 
		--CreatedBy ,
		--CAST(ModifiedDate as date) as ModifiedDate, 
		--Modifiedby ,
		--CAST(ErpModifiedDate as date) as ErpModifiedDate, 
		--ErpModifiedBy ,
        cast(ErpSyncDate as date) as ErpSyncDate, 
		--ErpSyncBy , 
		Count(*) as TotalRecords ,
		NULL AS TotalReps,
		NULL AS TotalCustomers
		
from [172.25.6.17].Pepperi_Staging.dbo.ShipTo 
group by cast(ErpSyncDate  as date), 
       --  ErpSyncBy ,
		 cast(CreatedDate as date)-- ,
		 --createdby,
		 --ErpModifiedBy ,
		 --CAST(ErpModifiedDate as date)
		-- Modifiedby ,
		-- cast(ModifiedDate as date)
union all 

select 'SalesOrderHeader' as [Table], 
        cast(CreatedDate as date) as CreatedDate, 
		--CreatedBy ,
		--CAST(ModifiedDate as date) as ModifiedDate, 
		--Modifiedby ,
		--CAST(ErpModifiedDate as date) as ErpModifiedDate, 
		--ErpModifiedBy ,
        CAST(ErpSyncDate as date) as ErpSyncDate, 
		--ErpSyncBy , 
		Count(*) as TotalRecords ,
		COUNT(DISTINCT RepNbr) AS TotalReps,
		COUNT(DISTINCT CustomerNbr) AS TotalCustomers
from [172.25.6.17].Pepperi_Staging.dbo.SalesOrder 
group by cast(ErpSyncDate  as date), 
         --ErpSyncBy ,
		 cast(CreatedDate as date) --,
		 --createdby--,
		 --ErpModifiedBy ,
		 --CAST(ErpModifiedDate as date)

		 --Modifiedby ,
		 --CAST(ModifiedDate as date)
union all 
select 'SalesOrderLine' as [Table], 
        cast(CreatedDate as date) as CreatedDate, 
		--CreatedBy ,
		--CAST(ModifiedDate as date) as ModifiedDate, 
		--Modifiedby ,
		--NULL AS  ErpModifiedDate, 
		--NULL AS ErpModifiedBy ,
        cast(ErpSyncDate as date) as ErpSyncDate, 
		--ErpSyncBy , 
		Count(*) as TotalRecords ,
		NULL AS TotalReps,
		NULL AS TotalCustomers
from [172.25.6.17].Pepperi_Staging.dbo.SalesOrderline  WHERE ParentItemNumber IS NULL 
group by cast(ErpSyncDate  as date), 
         --ErpSyncBy ,
		 cast(CreatedDate as date) --,
		 --createdby
		-- Modifiedby ,
		-- cast(ModifiedDate as date)
union all 
select 'SalesOrderCustomKit' as [Table], 
        cast(SL.CreatedDate as date) as CreatedDate, 
		--SL.CreatedBy ,
		--CAST(SL.ModifiedDate as date) as ModifiedDate, 
		--SL.Modifiedby ,
		--CAST(SO.ErpModifiedDate AS DATE) AS ErpModifiedDate, 
		--SO.ErpModifiedBy ,
        cast(SL.ErpSyncDate as date) as ErpSyncDate, 
		--SL.ErpSyncBy , 
		Count(*) as TotalRecords ,
		NULL AS TotalReps,
		NULL AS TotalCustomers
from [172.25.6.17].Pepperi_Staging.dbo.SalesOrder SO INNER JOIN [172.25.6.17].Pepperi_Staging.dbo.SalesOrderLine SL ON SO.PepperiOrderNbr = SL.PepperiId WHERE ParentItemNumber IS NOT NULL 
group by cast(SL.ErpSyncDate  as date), 
         --SL.ErpSyncBy ,
		 cast(SL.CreatedDate as date) --,
		 --SL.createdby,
		 --SL.Modifiedby ,
		 --CAST(SL.ModifiedDate as date),
		 --CAST(SO.ErpModifiedDate AS DATE), 
		 --SO.ErpModifiedBy 

) as a  WHERE [TABLE] IN (@Table)
order by [Table],  CreatedDate   desc



end
GO
